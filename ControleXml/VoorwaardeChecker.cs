﻿using HDN.CONTROL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using HDNRESULT;

namespace HDN
{
    partial class ControleXml
    {
        public void CheckVoorwaardeWalker(CheckerInfo checkerInfo, PropertyInfo baseClass, object baseObject, PathCheckerInfo pathCheckerInfo, int index)
        {
            if (checkerInfo.veldList.Length == index)
            {
                if (!checkerInfo.checkerFunction(checkerInfo, baseObject))
                {
                    log.InfoFormat("{0} {1}", pathCheckerInfo.pathString, checkerInfo.foutMelding);
                    GlobalOutputInfo.checkErrors.Add(pathCheckerInfo.pathString + "  " + checkerInfo.foutMelding);
                };
                return;
            }

            if (baseObject is IList && baseObject.GetType().IsGenericType)
            {
                if (((System.Collections.IList)baseObject).Count == 0)
                {
                    return;
                }

                if (pathCheckerInfo.pathIndex.Count <= index)
                {
                    // dit is het geval voor bijvoorbeeld
                    // <Conditie>        < Veld naam = "//Lening/Leningdeel/BancaireDekking/CodeBancaireDekking" />
                    // <VoorwaardeCode>  < Veld naam = "//Lening/Leningdeel/BancaireDekking/InlegAfspraken/SoortInleg" />
                    foreach (object listElementObject in (System.Collections.IList)baseObject)
                    {
                        baseClass = listElementObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[index]);
                        baseObject = baseClass.GetValue(listElementObject, null);
                        CheckVoorwaardeWalker(checkerInfo, baseClass, baseObject, pathCheckerInfo, index + 1);
                    }
                }
                else
                {
                    // pak het element uit de lijst
                    object listElementObject = ((System.Collections.IList)baseObject)[pathCheckerInfo.pathIndex[index]];
                    baseClass = listElementObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[index]);
                    baseObject = baseClass.GetValue(listElementObject, null);
                    CheckVoorwaardeWalker(checkerInfo, baseClass, baseObject, pathCheckerInfo, index + 1);
                }
            }
            else
            {
                try
                {
                    MethodInfo methodName = baseObject.GetType().GetMethod("ShouldSerialize" + checkerInfo.veldList[index]);
                    var methodResult = methodName.Invoke(baseObject, null);
                    if (!(bool)methodResult)
                        return;

                    var property = baseObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[index]);
                    PropertyInfo propInfo = baseObject.GetType().GetProperty(property.Name, BindingFlags.Instance | BindingFlags.Public);
                    methodName = propInfo.GetGetMethod();
                    methodResult = methodName.Invoke(baseObject, null);
                }
                catch (IOException)
                {
                    // no ShouldSerialize function, ignore"
                    return;
                }

                baseClass = baseObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[index]);
                baseObject = baseClass.GetValue(baseObject, null);
                if (baseObject == null)
                {
                    // basobject is null, ignore
                    return;
                }
                CheckVoorwaardeWalker(checkerInfo, baseClass, baseObject, pathCheckerInfo, index + 1);
            }
        }

        public void CheckVoorwaarde(RegelType regel, CheckerInfo checkerInfo, PathCheckerInfo pathInfo)
        {
 
            if (regel.VoorwaardeCode.CodeLijst.Code.Count > 0)
            {
                checkerInfo.soort = CheckerSoort.VoorwaardeCode;
                checkerInfo.codeLijst = regel.VoorwaardeCode.CodeLijst;
                checkerInfo.veldList = ToArray(regel.VoorwaardeCode.Veld.naam);
                checkerInfo.conditiePath = regel.VoorwaardeCode.Veld.naam;
                checkerInfo.checkerFunction = CodeChecker;
            }
            else if (regel.VoorwaardeVerplicht.Veld.naam != null)
            {
                checkerInfo.soort = CheckerSoort.VoorwaardeVerplicht;
                checkerInfo.veldList = ToArray(regel.VoorwaardeVerplicht.Veld.naam);
                checkerInfo.conditiePath = regel.VoorwaardeVerplicht.Veld.naam;
                checkerInfo.checkerFunction = VerplichtChecker;
            }
            else if (regel.VoorwaardeExpressie.Veld.naam != null)
            {
                checkerInfo.soort = CheckerSoort.VoorwaardeExpressie;
                checkerInfo.codeLijst = regel.Conditie[0].CodeLijst;
                checkerInfo.veldList = ToArray(regel.VoorwaardeExpressie.Veld.naam);
                checkerInfo.conditiePath = regel.VoorwaardeExpressie.Veld.naam;
                checkerInfo.checkerFunction = ExpressieChecker;
                checkerInfo.expressie = regel.VoorwaardeExpressie.Expressie;
            }
            else if (regel.VoorwaardeCount.Veld.naam != null)
            {
                checkerInfo.soort = CheckerSoort.VoorwaardeCount;
                checkerInfo.codeLijst = regel.Conditie[0].CodeLijst;
                checkerInfo.veldList = ToArray(regel.VoorwaardeCount.Veld.naam);
                checkerInfo.conditiePath = regel.VoorwaardeCount.Veld.naam;
                checkerInfo.checkerFunction = ExpressieChecker;
                checkerInfo.expressie = regel.VoorwaardeCount.Expressie;
            }
            else
            {
                return;
            }

            PropertyInfo baseClass = _hdnXsdObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[0]);
            object baseObject = baseClass.GetValue(_hdnXsdObject, null);
            CheckVoorwaardeWalker(checkerInfo, baseClass, baseObject, pathInfo, 1);
        }
    }
}

