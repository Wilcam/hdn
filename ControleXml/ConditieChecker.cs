﻿using HDN.CONTROL;
using HDNRESULT;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HDN
{
    partial class ControleXml
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void CheckCondities()
        {
            foreach (RegelType regel in _controle.Regels)
            {
                CheckerInfo checkerInfo = new();
                checkerInfo.soort = CheckerSoort.VoorwaardeCode;
                checkerInfo.codeLijst = regel.Conditie[0].CodeLijst;
                checkerInfo.conditiePath = regel.Conditie[0].Veld.naam;
                if (regel.VoorwaardeCode.CodeLijst.Code.Count > 0)
                    checkerInfo.voorwaardePath = regel.VoorwaardeCode.Veld.naam;
                else if (regel.VoorwaardeVerplicht.Veld.naam != null)
                    checkerInfo.voorwaardePath = regel.VoorwaardeVerplicht.Veld.naam;
                else if (regel.VoorwaardeExpressie.Veld.naam != null)
                    checkerInfo.voorwaardePath = regel.VoorwaardeExpressie.Veld.naam;
                else if (regel.VoorwaardeCount.Veld.naam != null)
                    checkerInfo.voorwaardePath = regel.VoorwaardeCount.Veld.naam;
                else if (regel.VoorwaardeVerplicht.Entiteit.naam != null)
                    checkerInfo.voorwaardePath = regel.VoorwaardeVerplicht.Entiteit.naam;
                else if (regel.VoorwaardeCount.Entiteit.naam != null)
                    checkerInfo.voorwaardePath = regel.VoorwaardeCount.Entiteit.naam;
                else
                {
                    GlobalOutputInfo.buildErrors.Add("CheckCondities: ongeldige voorwaarde");
                    continue;
                }


                checkerInfo.veldList = ToArray(regel.Conditie[0].Veld.naam);
                checkerInfo.checkerFunction = CodeChecker;
                checkerInfo.foutMelding = regel.Fout.FoutMelding;
                checkerInfo.pathCheckerList.Clear();

                PropertyInfo baseClass = _hdnXsdObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[0]);
                object baseObject = baseClass.GetValue(_hdnXsdObject, null);
                PathCheckerInfo pathCheckerInfo = new();
                pathCheckerInfo.pathString = checkerInfo.veldList[0];
                pathCheckerInfo.pathIndex.Add(0);

                CheckConditieWalker(checkerInfo, baseClass, baseObject, pathCheckerInfo, 1);
                List<PathCheckerInfo> firstPathCheckerInfoList = checkerInfo.pathCheckerList;
                if (regel.Conditie.Count == 1)
                {
                    for (int i = 0; i < firstPathCheckerInfoList.Count; i++)
                        if (firstPathCheckerInfoList[i].shouldCheck)
                            CheckVoorwaarde(regel, checkerInfo, firstPathCheckerInfoList[i]);
                }
                else if (regel.Conditie.Count == 2)
                {
                    checkerInfo.codeLijst = regel.Conditie[1].CodeLijst;
                    checkerInfo.conditiePath = regel.Conditie[1].Veld.naam;
                    checkerInfo.veldList = ToArray(regel.Conditie[1].Veld.naam);
                    baseClass = _hdnXsdObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[0]);
                    baseObject = baseClass.GetValue(_hdnXsdObject, null);
                    checkerInfo.pathCheckerList = new List<PathCheckerInfo>();
                    pathCheckerInfo = new PathCheckerInfo
                    {
                        pathString = checkerInfo.veldList[0]
                    };
                    pathCheckerInfo.pathIndex.Add(0);
                    CheckConditieWalker(checkerInfo, baseClass, baseObject, pathCheckerInfo, 1);
                    List<PathCheckerInfo> secondPathCheckerInfoList = checkerInfo.pathCheckerList;
                    if (firstPathCheckerInfoList.Count != secondPathCheckerInfoList.Count)
                    {
                        // als er in allebei paden zijn opgegeven dan is er niks te checken (welke hoort bij welke is niet te bepalen)
                        var myRegex = new System.Text.RegularExpressions.Regex(@"\[");
                        IEnumerable<PathCheckerInfo> conditie1ListMetPaden = firstPathCheckerInfoList.Where(x => myRegex.IsMatch(x.pathString)).ToList();
                        IEnumerable<PathCheckerInfo> conditie2ListMetPaden = secondPathCheckerInfoList.Where(x => myRegex.IsMatch(x.pathString)).ToList();
                        if ((conditie1ListMetPaden.Count() > 1) && (conditie2ListMetPaden.Any()))
                        {
                            var msg1 = "Skipped: 2 condities met verschillende paden";
                            var msg2 = conditie1ListMetPaden.First().pathString + "||" + conditie2ListMetPaden.First().pathString;
                            //Helpers.LogWriter.LogWrite(msg1, msg2);
                            GlobalOutputInfo.checkErrors.Add(msg1 + "   " + msg2);
                            log.InfoFormat("{0} {1}", msg1, msg2);

                            continue;
                        }
                        if ((secondPathCheckerInfoList.Count > 1) && (firstPathCheckerInfoList.Count > 1))
                        {
                            var msg1 = "Skipped: 2 condities met meerdere paden";
                            var msg2 = conditie1ListMetPaden.First().pathString + "||" + conditie2ListMetPaden.First().pathString;
                            //Helpers.LogWriter.LogWrite(msg1, msg2);
                            GlobalOutputInfo.checkErrors.Add(msg1 + "   " + msg2);
                            log.InfoFormat("{0} {1}", msg1, msg2);
                            continue;
                        }
                    }

                    if (regel.Conditie[1].optie == ConditieTypeOptie.and)
                    {
                        if ((firstPathCheckerInfoList.Count == 0) || (secondPathCheckerInfoList.Count == 0))
                            continue;
                        else if (firstPathCheckerInfoList.Count == secondPathCheckerInfoList.Count)
                        {
                            for (int i = 0; i < firstPathCheckerInfoList.Count; i++)
                                if (firstPathCheckerInfoList[i].shouldCheck && secondPathCheckerInfoList[i].shouldCheck)
                                    CheckVoorwaarde(regel, checkerInfo, firstPathCheckerInfoList[i]);
                        }
                        else if (firstPathCheckerInfoList.Count == 1)
                        {
                            for (int i = 0; i < secondPathCheckerInfoList.Count; i++)
                                if (firstPathCheckerInfoList[0].shouldCheck && secondPathCheckerInfoList[i].shouldCheck)
                                    CheckVoorwaarde(regel, checkerInfo, secondPathCheckerInfoList[i]);
                        }
                        else if (secondPathCheckerInfoList.Count == 1)
                        {
                            for (int i = 0; i < firstPathCheckerInfoList.Count; i++)
                                if (secondPathCheckerInfoList[0].shouldCheck && firstPathCheckerInfoList[i].shouldCheck)
                                    CheckVoorwaarde(regel, checkerInfo, firstPathCheckerInfoList[i]);
                        }
                        else
                        {
                            throw new System.ArgumentException("wat nou weer", "CheckCondities");
                        }
                    }
                    else
                    {
                        if ((firstPathCheckerInfoList.Count == 0) && (secondPathCheckerInfoList.Count == 0))
                            continue;
                        else if (firstPathCheckerInfoList.Count == secondPathCheckerInfoList.Count)
                        {
                            for (int i = 0; i < firstPathCheckerInfoList.Count; i++)
                                if (firstPathCheckerInfoList[i].shouldCheck || secondPathCheckerInfoList[i].shouldCheck)
                                    CheckVoorwaarde(regel, checkerInfo, firstPathCheckerInfoList[i]);
                        }
                        else if (firstPathCheckerInfoList.Count == 1)
                        {
                            for (int i = 0; i < secondPathCheckerInfoList.Count; i++)
                                if (firstPathCheckerInfoList[0].shouldCheck || secondPathCheckerInfoList[i].shouldCheck)
                                    CheckVoorwaarde(regel, checkerInfo, secondPathCheckerInfoList[i]);
                        }
                        else if (secondPathCheckerInfoList.Count == 1)
                        {
                            for (int i = 0; i < firstPathCheckerInfoList.Count; i++)
                                if (secondPathCheckerInfoList[0].shouldCheck || firstPathCheckerInfoList[i].shouldCheck)
                                    CheckVoorwaarde(regel, checkerInfo, firstPathCheckerInfoList[i]);
                        }
                        else
                        {
                            throw new System.ArgumentException("wat nou weer", "CheckCondities");
                        }
                    }
                }
            }
        }
        public void CheckConditieWalker(CheckerInfo checkerInfo, PropertyInfo baseClass, object baseObject, PathCheckerInfo pathCheckerInfo, int index)
        {
            if (checkerInfo.veldList.Length == index)
            {

                if (checkerInfo.checkerFunction(checkerInfo, baseObject))
                {
                    pathCheckerInfo.shouldCheck = true;
                }
                checkerInfo.pathCheckerList.Add(pathCheckerInfo);
                return;
            }

            if (baseObject is IList && baseObject.GetType().IsGenericType)
            {
                if (((System.Collections.IList)baseObject).Count == 0)
                {
                    return;
                }

                string pathInfo = pathCheckerInfo.pathString;

                int i = 0;
                foreach (object listElementObject in (System.Collections.IList)baseObject)
                {
                    pathCheckerInfo.pathString = pathInfo + "[" + i.ToString() + "]" + "." + checkerInfo.veldList[index];

                    // check eerst of ie serialized moet worden, anders wordt er een lege aangemaakt
                    var methodName = listElementObject.GetType().GetMethod("ShouldSerialize" + checkerInfo.veldList[index]);
                    var methodResult = methodName.Invoke(listElementObject, null);
                    if (!(bool)methodResult)
                    {
                        i++;
                        continue;
                    }

                    PropertyInfo childClass = listElementObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[index]);
                    object childObject = childClass.GetValue(listElementObject, null);
                    PathCheckerInfo newPathCheckerInfo = new();
                    newPathCheckerInfo.pathString = pathCheckerInfo.pathString;
                    newPathCheckerInfo.pathIndex = new List<int>(pathCheckerInfo.pathIndex)
                    {
                        i
                    };
                    CheckConditieWalker(checkerInfo, childClass, childObject, newPathCheckerInfo, index + 1);
                    i++;
                }
            }
            else
            {
                try
                {
                    var methodName = baseObject.GetType().GetMethod("ShouldSerialize" + checkerInfo.veldList[index]);
                    var methodResult = methodName.Invoke(baseObject, null);
                    if (!(bool)methodResult)
                        return;

                    pathCheckerInfo.pathString = pathCheckerInfo.pathString + "." + checkerInfo.veldList[index];
                    var property = baseObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[index]);
                    PropertyInfo propInfo = baseObject.GetType().GetProperty(property.Name, BindingFlags.Instance | BindingFlags.Public);
                    methodName = propInfo.GetGetMethod();
                    methodResult = methodName.Invoke(baseObject, null);
                    pathCheckerInfo.pathIndex.Add(0);
                }
                catch (IOException)
                {
                    // no ShouldSerialize function, ignore;
                    return;
                }

                var ff = baseObject.GetType().GetProperties();
                baseClass = baseObject.GetType().GetProperties().Single(pi => pi.Name == checkerInfo.veldList[index]);
                baseObject = baseClass.GetValue(baseObject, null);
                if (baseObject == null)
                {
                    PathCheckerInfo newPathCheckerInfo = new();
                    newPathCheckerInfo.pathIndex.Add(0);
                    checkerInfo.pathCheckerList.Add(newPathCheckerInfo);
                    return;
                }
                CheckConditieWalker(checkerInfo, baseClass, baseObject, pathCheckerInfo, index + 1);
            }
        }
    }
}

