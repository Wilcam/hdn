﻿using HDN.CONTROL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Reflection;
using System.Linq;
using System.Collections;
using HDNRESULT;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif




namespace HDN
{
    public enum CheckerSoort
    {
        ConditieType = 1, VoorwaardeVerplicht, VoorwaardeCount, VoorwaardeBoolean, VoorwaardeExpressie, VoorwaardeCode
    }

    delegate bool Checker(CheckerInfo checkerInfo, object baseObject);

    class PathCheckerInfo
    {
        public List<int> pathIndex;
        public bool shouldCheck;
        public string pathString;

        public PathCheckerInfo()
        {
            pathIndex = new List<int>();
        }
    }

    class CheckerInfo
    {
        public CheckerSoort soort;
        public CodeLijstType codeLijst;
        public string conditiePath;
        public string voorwaardePath;
        public string[] veldList;

        public string foutMelding;
        public ExpressieType expressie;
        public Checker checkerFunction;
        public List<PathCheckerInfo> pathCheckerList;

        public CheckerInfo()
        {
            pathCheckerList = new List<PathCheckerInfo>();
        }
    }

    partial class ControleXml
    {
        
        private string _hdnControleXml;
        private HDNControles _controle;
#if LX
        private LevenAanvraagType _hdnXsdObject;
        public ControleXml(LevenAanvraagType hdnXsdObject)
#else
        private OfferteAanvraagType _hdnXsdObject;
        public ControleXml(OfferteAanvraagType hdnXsdObject)
#endif
        {
            _hdnXsdObject = hdnXsdObject;
            _hdnControleXml = ControleXmlConfig.GetHdnControleXml();
            _controle = new HDNControles();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(_controle.GetType());
            System.IO.FileStream fileStreamIn = new FileStream(_hdnControleXml, FileMode.Open);
            _controle = (HDNControles)serializer.Deserialize(fileStreamIn);
            fileStreamIn.Close();
        }

        public void Check()
        {
            CheckCondities();
        }

        private string[] ToArray(string theString)
        {
            string veld = theString[2..];
            string[] veldList = veld.Split('/');
            return veldList;
        }

        private bool CodeChecker(CheckerInfo checkerInfo, object baseObject)
        {
            bool isOK;

            if (baseObject == null)
                return false;

            if (!(baseObject is Enum))
                return true;

            Enum methodResult = (Enum)baseObject;

            if (checkerInfo.codeLijst.Code.Count == 0)
                return true;

            if (checkerInfo.codeLijst.optie == "not")
                isOK = true;
            else
                isOK = false;

            for (int i = 0; i < checkerInfo.codeLijst.Code.Count; i++)
            {
                if (checkerInfo.codeLijst.optie == "not")
                {
                    if (methodResult.ToName() != checkerInfo.codeLijst.Code[i].waarde)
                    {
                        isOK &= true;
                    }
                }
                else
                {
                    if (methodResult.ToName() == checkerInfo.codeLijst.Code[i].waarde)
                        isOK |= true;
                }
            }
            return isOK;
        }

        private bool ExpressieChecker(CheckerInfo checkerInfo, object baseObject)
        {

            if (baseObject == null)
                return true;

            // soms wordt een getal als string meegegeven  (bijv. bij Ing)
            if (baseObject is string @string)
                baseObject = Convert.ToDecimal(@string);

            if (baseObject is decimal)
            {
                switch (checkerInfo.expressie.soort)
                {
                    case ExpressieTypeSoort.Item:
                        return (decimal)baseObject == checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item1:
                        return (decimal)baseObject > checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item2:
                        return (decimal)baseObject < checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item3:
                        return (decimal)baseObject <= checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item4:
                        return (decimal)baseObject >= checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item5:
                        return (decimal)baseObject != checkerInfo.expressie.waarde;
                    default:
                        GlobalOutputInfo.buildErrors.Add("ExpressieChecker: ongeldige expressie");
                        return false;
                }
            }
            else
            {
                GlobalOutputInfo.buildErrors.Add("ExpressieChecker: ongeldig type");
                return false;
            }            
        }

        private bool CountChecker(CheckerInfo checkerInfo, object baseObject)
        {
              if (baseObject == null)
                return true;

            if (baseObject is decimal)
            {
                switch (checkerInfo.expressie.soort)
                {
                    case ExpressieTypeSoort.Item:
                        return (decimal)baseObject == checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item1:
                        return (decimal)baseObject > checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item2:
                        return (decimal)baseObject < checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item3:
                        return (decimal)baseObject <= checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item4:
                        return (decimal)baseObject >= checkerInfo.expressie.waarde;
                    case ExpressieTypeSoort.Item5:
                        return (decimal)baseObject != checkerInfo.expressie.waarde;
                    default:
                        GlobalOutputInfo.buildErrors.Add("CountChecker: ongeldige expressie");
                        return false;
                }
            }
            else
            {
                GlobalOutputInfo.buildErrors.Add("CountChecker: ongeldig type");
                return false;
            }           
        }

        private bool VerplichtChecker(CheckerInfo checkerInfo, object baseObject)
        {
            bool isOK;

            if (baseObject == null)
                return false;

            if (baseObject.GetType().IsEnum)
            {
                // er is iets ingevuld (geen null) dan ok
                isOK = true;
            }
            else if (baseObject is DatumType)
            {
                isOK = (baseObject as DatumType).Dag != null;
            }
            else if (baseObject is string)
            {
                isOK = !string.IsNullOrEmpty((baseObject as string));
            }
            else if (baseObject is decimal @decimal)
            {
                isOK = @decimal >= 0;
            }
            else
            {
                isOK = baseObject != null;
            }
            return isOK;
        }
    }
}
