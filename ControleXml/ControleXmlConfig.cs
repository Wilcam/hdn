﻿using HDN.Factories;
using System;
using System.Collections.Generic;
using System.Text;

namespace HDN
{
    static class ControleXmlConfig
    {
        public static string GetHdnControleXml()
        {
#if RABO
            return Constants.ConfigBase + "Rabo/AX_Rabobank_20.0.9.5_ControlXml.xml";
#elif COLIBRI
            return Constants.ConfigBase + "Colibri/AX_Colibri Hypotheken_20.0.3.2_ControlXml.xml";
#elif MUNT
            return Constants.ConfigBase + "Munt/AX_Munt Hypotheken_20.0.4.3_ControlXml.xml";
#elif ASR
            return Constants.ConfigBase + "Asr/AX_ASR_20.0.11.8_ControlXml.xml";
#elif ABN
            return Constants.ConfigBase + "Abn/AX_ABN AMRO Bank_20.0.6.3_ControlXml.xml";
#elif ABNGRP
            return Constants.ConfigBase + "AbnGroep/AX_ABN AMRO Hypotheken Groep_20.0.6.3_ControlXml.xml";
#elif AEGON
            return Constants.ConfigBase + "Aegon/AX_Aegon Verzekeringen_20.0.9.4_ControlXml.xml";
#elif ATTENS
            return Constants.ConfigBase + "Attens/AX_Attens Hypotheken_20.0.6.4_ControlXml.xml";
#elif VENN
            return Constants.ConfigBase + "Venn/AX_Venn Hypotheken_20.0.9.4_ControlXml.xml";
#elif ING
            return Constants.ConfigBase + "Ing/AX_ING_20.0.6.2_ControlXml.xml";
#elif HYPOTRUST
            return Constants.ConfigBase + "Hypotrust/AX_Hypotrust_20.0.3.2_ControlXml.xml";
#elif CENTRAAL
            return Constants.ConfigBase + "Centraal/AX_Centraal Beheer_20.0.4.3_ControlXml.xml";
#elif NN
            return Constants.ConfigBase + "Nn/AX_Nationale Nederlanden Hypotheken_20.0.6.4_ControlXml.xml";
#elif WOONNU
            return Constants.ConfigBase + "Woonnu/AX_Woonnu_20.0.3.1_ControlXml.xml";
#elif NIBC
            return Constants.ConfigBase + "Nibc/AX_NIBC Bank N.V._20.0.9.3_ControlXml.xml";
#elif BLG
            return Constants.ConfigBase + "Blg/AX_BLG Hypotheken_20.1.0.1_ControlXml.xml";
#elif DELTA
            return Constants.ConfigBase + "Delta/AX_Delta Lloyd_20.0.2.1_ControlXml.xml";
#elif ARGENTA
            return Constants.ConfigBase + "Argenta/AX_Argenta_20.0.3.2_ControlXml.xml";
#elif ALLIANZ
            return Constants.ConfigBase + "Allianz/AX_Allianz_20.0.3.2_ControlXml.xml";
#elif ASN
            return Constants.ConfigBase + "Asn/AX_ASN Bank_20.0.2.4_ControlXml.xml";
#elif BNP
            return Constants.ConfigBase + "Bnp/AX_BNP Paribas Personal Finance_20.1.0.1_ControlXml.xml";
#elif DYNAMIC
            return Constants.ConfigBase + "Dynamic/AX_Dynamic_20.0.5.1_ControlXml.xml";
#elif HWOONT
            return Constants.ConfigBase + "Hwoont/AX_HollandWoont_20.0.3.4_ControlXml.xml";
#elif IQWOON
            return Constants.ConfigBase + "Iqwoon/AX_IQWOON_20.0.3.3_ControlXml.xml";
#elif LLOYDS
            return Constants.ConfigBase + "Lloyds/AX_Lloyds Bank_20.0.3.1_ControlXml.xml";
#elif LOT
            return Constants.ConfigBase + "Lot/AX_Lot Hypotheken_20.0.2.1_ControlXml.xml";
#elif MERIUS
            return Constants.ConfigBase + "Merius/AX_Merius_20.0.8.3_ControlXml.xml";
#elif OBVION
            return Constants.ConfigBase + "Obvion/AX_Obvion_20.0.6.2_ControlXml.xml";
#elif PHILIPS
            return Constants.ConfigBase + "Philips/AX_Philips Pensioenfonds_20.0.2.3_ControlXml.xml";
#elif REAAL
            return Constants.ConfigBase + "Reaal/AX_REAAL Leven_20.0.2.2_ControlXml.xml";
#elif REGIO
            return Constants.ConfigBase + "Regiobank/AX_RegioBank_20.0.2.4_ControlXml.xml";
#elif ROBUUST
            return Constants.ConfigBase + "Robuust/AX_Robuust Hypotheken_20.0.3.3_ControlXml.xml";
#elif SNS
            return Constants.ConfigBase + "Sns/AX_SNS Bank_20.1.0.1_ControlXml.xml";
#elif SYNTRUS
            return Constants.ConfigBase + "Syntrus/AX_Syntrus Achmea Hypotheken_20.0.6.4_ControlXml.xml";
#elif TELLIUS
            return Constants.ConfigBase + "Tellius/AX_Tellius Hypotheken_20.0.6.3_ControlXml.xml";
#elif TULP
            return Constants.ConfigBase + "Tulp/AX_Tulp Hypotheken_20.0.2.3_ControlXml.xml";
#elif VISTA
            return Constants.ConfigBase + "Vista/AX_Vista Hypotheken_20.0.3.1_ControlXml.xml";
#elif WFONDS
            return Constants.ConfigBase + "Woonfonds/AX_Woonfonds Hypotheken_20.0.4.2_ControlXml.xml";
#else
            throw new System.ArgumentException("Onbekende maatschappij", "getHdnControleXml");           
#endif
        }
    }
}
