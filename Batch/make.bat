@ECHO OFF
set sourceDir=C:\Users\wilco\projects\HDN\bin\Debug\netcoreapp3.1\
set destDir=C:\Users\wilco\projects\HDNMAIN\references\
echo start building hdn factory for all companies
setlocal EnableDelayedExpansion
set count=1
for %%x in (
        ABN,
        ABNGRP,
        AEGON,
        ARGENTA,
        ASR,
        ATTENS,
        BLG,
        CENTRAAL,
        COLIBRI,
        DELTA,
        HYPOTRUST,
        ING,
        MONEYOU,
        MUNT,
        NIBC,
        NN,
        RABO,
        VENN,
        WOONNU,
        ALLIANZ,
        ASN,
        BIJBOUWE,
        BNP,
        DYNAMIC,
        HWOONT,
        IQWOON,
        LLOYDS,
        LOT,
        MERIUS,
        OBVION,
        PHILIPS,
        REAAL,
        REGIO,
        ROBUUST,
        SNS,
        SYNTRUS,
        TELLIUS,
        TULP,
        VISTA,
        WFONDS
       ) do (
 
       echo  !count! start building hdn factory for %%x                
         "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe" /t:HDN /p:DefineConstants=^"%%x^"  /p:AssemblyName=^"%%x^"  /nologo /verbosity:quiet  
         copy %sourceDir%%%x.dll %destDir% 
         set /a count+=1
       ) 
       echo end building hdn factory for all companies
