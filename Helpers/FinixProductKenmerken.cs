﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HDN
{
    public static class ProductKenmerken
    {
        public static int pkSNSBoeteVrij = 1;
        public static int pkSNSSpaarrekening = 2;
        public static int pkSNSWoonlastenVerzekering = 3;

        public static int pkABNAMROBetaalrek = 4;
        public static int pkABNAMROTypeOnderpand = 5;

        public static int pkPostbankVoordeelBoeteVrij = 6;
        public static int pkPostbankVoordeelOfferteRente = 7;

        public static int pkINGBoeteVrij = 8;
        public static int pkINGOfferteRente = 9;
        public static int pkINGActieRente = 10;

        public static int pkUCBmetCardifHOP = 11;
        public static int pkZwitserlevenmetCardifHOP = 12;

        public static int pkTransparantHypOfferteLengte = 13;

        public static int pkSNSRegioBudgetGeenBoete = 14;
        public static int pkSNSRegioBudgetPriverekening = 15;
        public static int pkSNSRegioBudgetWoonlastenVerz = 16;

        public static int pkDirektbankBudgetKorting = 17;

        public static int pkNNBasisKorting = 18;
        public static int pkNNBoetevrij = 19;
        public static int pkNNDalrente = 20;

        public static int pkFortisBasisKorting = 21;
        public static int pkFortisBoetevrij = 22;
        public static int pkFortisDalrente = 23;

        public static int pkWUOffertekorting = 24;
        public static int pkWULoyaliteitskorting = 25;
        public static int pkWUDepotkorting = 26;

        public static int pkWoonFlexxTransparantHypOfferteLengte = 27;
        public static int pkWoonMatchTransparantHypOfferteLengte = 28;

        public static int pkREAALVictorieMetWoonlast = 29;

        public static int pkMoneYouEasy100percOrv = 30;
        public static int pkMoneYouEasy100percMLB = 31;
        public static int pkMoneYouEasy50percMix = 32;
        public static int pkMoneYouEasy100percMix = 33;
        public static int pkMoneYouEasyGeenProvisie = 35;

        public static int pkZwitserlevenBestedingsvrijheidkeuze = 36;

        // Nieuwe ING per 01-01-2009  (IN)
        public static int pkINBoeteVrij = 37;
        public static int pkINOfferteRente = 38;
        public static int pkINWBoeteVrij = 39;
        public static int pkINWOfferteRente = 40;
        public static int pkINStartPakketRente = 42;
        public static int pkINNieuwPakketRente = 43;
        public static int pkINExtraPakketRente = 44;
        public static int pkINVSalarisrekRente = 45;
        public static int pkINVStartPakketRente = 46;
        public static int pkINVNieuwPakketRente = 47;
        public static int pkINVExtraPakketRente = 48;
        public static int pkINVPlusPakketRente = 49;
        //SNS personeel
        public static int pkSNSPprive = 51;
        public static int pkSNSPwoonaov = 52;
        public static int pkSNSPofferte = 53;

        public static int pkWoonMatchTransparantZero = 54;
        public static int pkWoonMatchBasicZero = 55;
        public static int pkmoTvateZero = 56;

        public static int pkAegonDigitaalAanleveren = 57; //oud

        // Holland Woningfinanciering
        public static int pkHWOfferteoptiesKeuze = 59;

        //BLG
        public static int pkBLGOfferteoptiesKeuze = 60;

        //Aegon Vervolg
        public static int pkAEGOfferteoptiesKeuze = 61;

        //BOS
        public static int pkBOSZero = 62;

        //BOSBudget
        public static int pkBOSBudgetZero = 63;

        //SNS Budget
        public static int pkSNSBudgetProvisieKeuze = 64;
        public static int pkSNSBudgetPercAfsluitProvisie = 65;

        //WUH (part2)
        public static int pkWUDepotkorting100000 = 66;

        //Woonlife Slim
        public static int pkWLSSnelPasseer = 67;

        //Argenta SpecialLine Zero
        public static int pkArgSLzero = 68;

        //ASR geen bijverband
        public static int pkASRGeenbijverband = 69;

        //Florius
        public static int pkFloriusFavoRentekorting = 70;
        public static int pkFloriusFavoAfsluitkorting = 71;

        //SNS
        public static int pkSNSPasseren3Maanden = 72;

        //Argenta SpecialLine 110& Annuïteitenhypotheek'
        public static int pkArgSL100Ann = 73;

        // ING regulier
        public static int pkINRBoeteVrij = 74;
        public static int pkINROfferteRente = 75;
        public static int pkINGloyaliteit = 76;

        // SNSRegio Budget
        public static int pkSNSRegioBudget3Mnd = 77;

        // ABN AMRO
        public static int pkABNwerknemersarrangement = 78;

        // RABOBANK
        public static int pkRabobankBetaalpakket = 79;

        // ABN AMRO
        public static int pkABNDuurzaamheid = 80;

        //Rabobank
        public static int pkRabobankDuurzaamheid = 81;

        public static int pkLaatsteNummertje = 81;  //deze aanpassen bij elk nieuw nummer
    }

}