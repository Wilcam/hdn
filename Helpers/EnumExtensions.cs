﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HDN
{
    public static class EnumExtensions
    {
        public static bool IsAny<T>(this T Enum, params T[] Args) where T : struct, IComparable
        {
            return Args.Where(a => Enum.Equals(a)).Any();
        }

        public static string GetAttribute<T>(this Enum value) where T : Attribute
        {
            var type = value.GetType();
            var memberInfo = type.GetMember(value.ToString());

            var attribs = memberInfo[0].CustomAttributes.ToList();
            if (attribs.Count == 0)
            {
                // soms is count 0, dan deze gebruiken, kan dat niet altijd?
                var ff = memberInfo[0].Name;
                return ff;
            }

            var attrib = attribs[0].ConstructorArguments[0].Value.ToString();
            return attrib;
        }


        public static string ToName(this Enum value)
        {
            var value1 = value.GetAttribute<DescriptionAttribute>();
            return value1;
        }
    }
}
