﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HDN
{
    public static class Constants
    {
        public static string ClientRef => "ClientRef";
        public static string PartnerRef => "PartnerRef";
        public static string TussenPersoonRef => "TussenPersoonRef";
        public static string ServiceProviderRef => "ServiceProviderRef";
        public static string NotarisRef => "NotarisRef";
        public static decimal HdnVersieNummer => 20.0M;
        public static string ConfigBase => "Models/";
        public static int AantalLeningdelenInScenario => 6;


    }

}
