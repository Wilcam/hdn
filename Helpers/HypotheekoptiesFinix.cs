﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HDN
{
    public static class HypotheekoptiesFinix
    {
        public enum Opties
        {
            pkSNSBoeteVrij = 1,
            pkSNSSpaarrekening = 2,
            pkSNSWoonlastenVerzekering = 3,

            pkABNAMROBetaalrek = 4,
            pkABNAMROTypeOnderpand = 5,

            pkPostbankVoordeelBoeteVrij = 6,
            pkPostbankVoordeelOfferteRente = 7,

            pkINGBoeteVrij = 8,
            pkINGOfferteRente = 9,
            pkINGActieRente = 10,

            pkUCBmetCardifHOP = 11,
            pkZwitserlevenmetCardifHOP = 12,

            pkTransparantHypOfferteLengte = 13,

            pkSNSRegioBudgetGeenBoete = 14,
            pkSNSRegioBudgetPriverekening = 15,
            pkSNSRegioBudgetWoonlastenVerz = 16,

            pkDirektbankBudgetKorting = 17,

            pkNNBasisKorting = 18,
            pkNNBoetevrij = 19,
            pkNNDalrente = 20,

            pkFortisBasisKorting = 21,
            pkFortisBoetevrij = 22,
            pkFortisDalrente = 23,

            pkWUOffertekorting = 24,
            pkWULoyaliteitskorting = 25,
            pkWUDepotkorting = 26,

            pkWoonFlexxTransparantHypOfferteLengte = 27,
            pkWoonMatchTransparantHypOfferteLengte = 28,

            pkREAALVictorieMetWoonlast = 29,

            pkMoneYouEasy100percOrv = 30,
            pkMoneYouEasy100percMLB = 31,
            pkMoneYouEasy50percMix = 32,
            pkMoneYouEasy100percMix = 33,
            pkMoneYouEasyOfferteLengte = 34,
            pkMoneYouEasyGeenProvisie = 35,

            pkZwitserlevenBestedingsvrijheidkeuze = 36,

            // Nieuwe ING per 01-01-2009  (IN)
            pkINBoeteVrij = 37,
            pkINOfferteRente = 38,
            pkINWBoeteVrij = 39,
            pkINWOfferteRente = 40,
            pkINSalarisrekRente = 41,
            pkINStartPakketRente = 42,
            pkINNieuwPakketRente = 43,
            pkINExtraPakketRente = 44,
            pkINVSalarisrekRente = 45,
            pkINVStartPakketRente = 46,
            pkINVNieuwPakketRente = 47,
            pkINVExtraPakketRente = 48,
            pkINVPlusPakketRente = 49,
            //SNS personeel
            pkSNSPboete = 50,
            pkSNSPprive = 51,
            pkSNSPwoonaov = 52,
            pkSNSPofferte = 53,

            pkWoonMatchTransparantZero = 54,
            pkWoonMatchBasicZero = 55,
            pkmoTvateZero = 56,

            pkAegonDigitaalAanleveren = 57, //oud

            // Holland Woningfinanciering
            pkHWOfferteoptiesKeuze = 59,

            //BLG
            pkBLGOfferteoptiesKeuze = 60,

            //Aegon Vervolg
            pkAEGOfferteoptiesKeuze = 61,

            //BOS
            pkBOSZero = 62,

            //BOSBudget
            pkBOSBudgetZero = 63,

            //SNS Budget
            pkSNSBudgetProvisieKeuze = 64,
            pkSNSBudgetPercAfsluitProvisie = 65,

            //WUH (part2)
            pkWUDepotkorting100000 = 66,

            //Woonlife Slim
            pkWLSSnelPasseer = 67,

            //Argenta SpecialLine Zero
            pkArgSLzero = 68,

            //ASR geen bijverband
            pkASRGeenbijverband = 69,

            //Florius
            pkFloriusFavoRentekorting = 70,
            pkFloriusFavoAfsluitkorting = 71,

            //SNS
            pkSNSPasseren3Maanden = 72,

            //Argenta SpecialLine 110& Annuïteitenhypotheek'
            pkArgSL100Ann = 73,

            // ING regulier
            pkINRBoeteVrij = 74,
            pkINROfferteRente = 75,
            pkINGloyaliteit = 76,

            // SNSRegio Budget
            pkSNSRegioBudget3Mnd = 77,

            // ABN AMRO
            pkABNwerknemersarrangement = 78,

            // RABOBANK
            pkRabobankBetaalpakket = 79,

            // ABN AMRO
            pkABNDuurzaamheid = 80,

            //Rabobank
            pkRabobankDuurzaamheid = 81,

            pkLaatsteNummertje = 81  //deze aanpassen bij elk nieuw nummer
        }
    }
}
