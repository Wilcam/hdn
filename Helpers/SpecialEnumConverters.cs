﻿#if ASR || CENTRAAL || HYPOTRUST || ING || NIBC
#define OPLEIDINGSNIVEAU
#endif

#if !AEGON && !ING && !NN && !WOONNU && !DYNAMIC && !PHILIPS
#define FISCALEREGIMETYPE
#endif

#if !DYNAMIC && !LOT && !PHILIPS && !REAAL
#define ENERGIEKLASSE
#endif

#if !COLIBRI && !VENN && !CENTRAAL && !WOONNU && !BLG && !DELTA && !ASN && !BIJBOUWE && !DNGB && !DYNAMIC && !HWOONT && !IQWOON && !LOT && !OBVION && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !TULP && !VISTA
#define MAATWERKOPLCODEMIJTYPE
#endif

using System;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using Finix;
using HDNRESULT;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN
{
    public static class SpecialEnums
    {
        internal static RechtsVormType GetHdnRechtsVorm(RechtsVormEnum? rechtsVorm)
        {
            switch (rechtsVorm)
            {
                case RechtsVormEnum.Eenmanszaak:
                    return RechtsVormType.EMZEenmanszaak;
                case RechtsVormEnum.Maatschap:
                    return RechtsVormType.MAMaatschap;
                case RechtsVormEnum.VennootschaponderfirmaVOF:
                    return RechtsVormType.VOFVennootschaponderfirma;
                case RechtsVormEnum.CommanditairevennootschapCV:
                    return RechtsVormType.CVCommanditairevennootschap;
                case RechtsVormEnum.BeslotenvennootschapBV:
                    return RechtsVormType.BVBeslotenvennootschap;
                case RechtsVormEnum.NaamlozevennootschapNV:
                    return RechtsVormType.NVNaamlozevennootschap;
                case RechtsVormEnum.Vereniging:
                case RechtsVormEnum.Stichting:
                case RechtsVormEnum.CoöpmetbeperkteaansprBA:
                case RechtsVormEnum.CoöpmetuitgeslotenaansprUA:
                case RechtsVormEnum.CoöpmetwettelijkeaansprWA:
                case RechtsVormEnum.Onderlingewaarborgmaatschappij:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetHdnRechtsVorm: ongeldige parameter, default waarde gebruikt");
                    return (RechtsVormType)0;
            }
        }

        internal static SoortOndernemingType GetHdnSoortOndermening(OndernemingSoortEnum? soortOnderneming)
        {
            switch (soortOnderneming)
            {
                case OndernemingSoortEnum.ZZPZelfstandigezonderpersoneel:
                    return SoortOndernemingType.Item01ZelfstandigeZonderPersoneelZZP;
                case OndernemingSoortEnum.Zelfstandigemetpersoneel:
                    return SoortOndernemingType.Item03Zelfstandigemetpersoneel;
                case OndernemingSoortEnum.VOFVennootschaponderfirma:
                case OndernemingSoortEnum.ZelfstandigeNV:
                case OndernemingSoortEnum.Freelancer:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetHdnSoortOndermening: ongeldige parameter, default waarde gebruikt");
                    return (SoortOndernemingType)0;
            }
        }

        internal static CodeObjectType2 GetHdnCodeObject(SoortWoningEnum? soortWoning)
        {
            return soortWoning switch
            {
                SoortWoningEnum.Onbekend => CodeObjectType2.Item01eengezinswoning,
                SoortWoningEnum.Eengezinswoning => CodeObjectType2.Item01eengezinswoning,
                SoortWoningEnum.Eengezinswoningmetgarage => CodeObjectType2.Item02eengezinswoningmetgarage,
                SoortWoningEnum.FlatAppartement => CodeObjectType2.Item03flatappartement,
                SoortWoningEnum.FlatAppartementmetgarage => CodeObjectType2.Item04flatappartementmetgarage,
                SoortWoningEnum.Winkelwoonhuis => CodeObjectType2.Item05winkelwoonhuis,
                SoortWoningEnum.Woonhuismetbedrijfsruimte => CodeObjectType2.Item06woonhuismetbedrijfsruimte,
                SoortWoningEnum.Winkel => CodeObjectType2.Item07winkel,
                SoortWoningEnum.Bedrijfspand => CodeObjectType2.Item08bedrijfspand,
                SoortWoningEnum.Recreatiewoning => CodeObjectType2.Item09recreatiewoning,
                SoortWoningEnum.Boerderij => CodeObjectType2.Item10boerderij,
                SoortWoningEnum.Zakelijkonderpand => CodeObjectType2.Item11zakelijkonderpand,
                SoortWoningEnum.Agrarischegrondbouwgrond => CodeObjectType2.Item12agrarischegrondbouwgrond,
                SoortWoningEnum.Garage => CodeObjectType2.Item13garage,
                SoortWoningEnum.Woonboerderij => CodeObjectType2.Item01eengezinswoning,
                SoortWoningEnum.Woonschip => CodeObjectType2.Item15Woonschip,
                SoortWoningEnum.Woonschipmetbetonnencasco => CodeObjectType2.Item15Woonschip,
                SoortWoningEnum.Woonschipmetstalencasco => CodeObjectType2.Item15Woonschip,
                _ => CodeObjectType2.Item01eengezinswoning,
            };
        }



#if FISCALEREGIMETYPE
        public static FiscaleRegimeType GetHdnFiscaalRegime(int? finixFiscaalRegime)
        {

            return finixFiscaalRegime switch
            {
                0 => FiscaleRegimeType.Item01Voor112013,
                1 => FiscaleRegimeType.Item02Vanaf112013,
                _ => FiscaleRegimeType.Item03Fiscaalregimenietvantoepassing,
            };
        }
#endif

        public static SoortDienstBetrekkingType GetHdnDienstBetrekking(Finix.SoortDienstverbandEnum? dienstVerband)
        {
            switch (dienstVerband)
            {
                case SoortDienstverbandEnum.loondienstfulltimevast:
                    return SoortDienstBetrekkingType.Item10loondienstvast;
                case SoortDienstverbandEnum.loondienstfulltimetijdelijkplusintentieverklaring:
                    return SoortDienstBetrekkingType.Item11loondiensttijdelijkmetintentie;
                case SoortDienstverbandEnum.loondienstparttimevast:
                    return SoortDienstBetrekkingType.Item10loondienstvast;
                case SoortDienstverbandEnum.loondienstparttimetijdelijkplusintentieverklaring:
                    return SoortDienstBetrekkingType.Item11loondiensttijdelijkmetintentie;
                case SoortDienstverbandEnum.zelfstandig:
                    break;
                case SoortDienstverbandEnum.WAOonderhevigaanperiodiekeherkeuring:
                    break;
                case SoortDienstverbandEnum.student:
                    break;
                case SoortDienstverbandEnum.Pensioen:
                    break;
                case SoortDienstverbandEnum.WWwachtgeld:
                    break;
                case SoortDienstverbandEnum.huisvrouwhuisman:
                    break;
                case SoortDienstverbandEnum.geenberoep:
                    break;
                case SoortDienstverbandEnum.onbekend:
                    break;
                case SoortDienstverbandEnum.DirecteurGrootAandeelhouderDGA:
                    break;
                case SoortDienstverbandEnum.loondienstfulltimetijdelijkzonderintentieverklaring:
                    return SoortDienstBetrekkingType.Item12loondiensttijdelijkzonderintentie;
                case SoortDienstverbandEnum.loondienstparttimetijdelijkzonderintentieverklaring:
                    return SoortDienstBetrekkingType.Item12loondiensttijdelijkzonderintentie;
                case SoortDienstverbandEnum.Flexwerker:
                    break;
                case SoortDienstverbandEnum.Seizoensarbeider:
                    break;
                case SoortDienstverbandEnum.WIAWGAuitkering3580arbeidsongeschikt:
                    break;
                case SoortDienstverbandEnum.WIAIVAuitkering80arbeidsongeschikt:
                    break;
                case SoortDienstverbandEnum.WAOuitgeslotenvanherkeuring:
                    break;
                case SoortDienstverbandEnum.Vroegprepensioen:
                    break;
                case SoortDienstverbandEnum.ANWNabestaandeuitkering:
                    break;
                case SoortDienstverbandEnum.Bijstand:
                    break;
                case SoortDienstverbandEnum.Wachtgeld:
                    break;
                case SoortDienstverbandEnum.RWWRijksgroepregelingWerklozeWerknemers:
                    break;
                case SoortDienstverbandEnum.WW:
                    break;
                case SoortDienstverbandEnum.Wajong:
                    break;
                case SoortDienstverbandEnum.Nabestaandenpensioen:
                    break;
                case SoortDienstverbandEnum.flexibelearbeidsrelatiemetperspectiefverklaring:
                    return SoortDienstBetrekkingType.Item08flexibelearbeidsrelatiemetperspectiefverklaring;
                case SoortDienstverbandEnum.flexibelearbeidsrelatiezonderperspectiefverklaring:
                    return SoortDienstBetrekkingType.Item09flexibelearbeidsrelatiezonderperspectiefverklaring;
                default:
                    break;
            }
            GlobalOutputInfo.buildErrors.Add("GetHdnDienstBetrekking: ongeldige parameter, default waarde gebruikt");
            return (SoortDienstBetrekkingType)0;

        }

        public static SoortUitkeringType GetHdnUitkering(Finix.SoortDienstverbandEnum? dienstVerband)
        {
            switch (dienstVerband)
            {
                case SoortDienstverbandEnum.WIAWGAuitkering3580arbeidsongeschikt:
                    return SoortUitkeringType.Item11WGA;
                case SoortDienstverbandEnum.WIAIVAuitkering80arbeidsongeschikt:
                    return SoortUitkeringType.Item12IVA;
                case SoortDienstverbandEnum.WAOuitgeslotenvanherkeuring:
                    return SoortUitkeringType.Item14WAOzonderherkeuring;
                case SoortDienstverbandEnum.Wachtgeld:
                    return SoortUitkeringType.Item04Wachtgeld;
                case SoortDienstverbandEnum.Wajong:
                    return SoortUitkeringType.Item08Wajong;
                case SoortDienstverbandEnum.Bijstand:
                    return SoortUitkeringType.Item02Bijstand;
                case SoortDienstverbandEnum.WW:
                    return SoortUitkeringType.Item03WW;
                case SoortDienstverbandEnum.Pensioen:
                case SoortDienstverbandEnum.Vroegprepensioen:
                    return SoortUitkeringType.Item15Ouderdomspensioen;
                case SoortDienstverbandEnum.Nabestaandenpensioen:
                    return SoortUitkeringType.Item16Nabestaandenpensioen;
            }
            GlobalOutputInfo.buildErrors.Add("GetHdnDienstBetrekking: ongeldige parameter, default waarde gebruikt");
            return (SoortUitkeringType)0;

        }

        public static BeroepType? GetHdnBeroep(string beroep)
        {
            foreach (BeroepType enumValue in Enum.GetValues(typeof(BeroepType)))
            {
                if (enumValue.ToName()[5..] == beroep)
                    return enumValue;
            }
            return null;
        }

        public static VerhuurType GetHdnVerhuurcode(VerhuurTypeTypeEnum? code)
        {
            if (code == null)
                return VerhuurType.Item01geenverhuur;

            foreach (VerhuurType enumValue in Enum.GetValues(typeof(VerhuurType)))
            {
                if (enumValue.ToName()[3..] == code.ToName())
                    return enumValue;
            }

            return VerhuurType.Item01geenverhuur;
        }

        public static CodeLeningMijType GetHdnCodeLeningMij(string codeMij)
        {
            foreach (CodeLeningMijType enumValue in Enum.GetValues(typeof(CodeLeningMijType)))
            {
                if (enumValue.ToName() == codeMij)
                    return enumValue;
            }

            GlobalOutputInfo.buildErrors.Add("GetHdnCodeLeningMij: ongeldige parameter, default waarde gebruikt");
            return (CodeLeningMijType)0;
        }


        public static GeregistreerdeLeningType GetHdnGeregistreerdeLeningType(SoortVerplichtingEnum? soortverplichting)
        {
            return soortverplichting switch
            {
                SoortVerplichtingEnum.RKdoorlopendekredieten => GeregistreerdeLeningType.RKDoorlopendkrediet,
                SoortVerplichtingEnum.AKaflopendekredieten => GeregistreerdeLeningType.AKAflopendkrediet,
                //SoortVerplichtingEnum.SRschuldbemiddelingensaneringskredieten => GeregistreerdeLeningType.,
                //SoortVerplichtingEnum.TCtelecomunicatievoorzieningen => GeregistreerdeLeningType.tele,
                //SoortVerplichtingEnum.SLStudielening => GeregistreerdeLeningType.studie,
                SoortVerplichtingEnum.RestschuldfinancieringzonderNHG => GeregistreerdeLeningType.RHRestschuldzonderaanspraakopNHG,
                SoortVerplichtingEnum.RestschuldfinancieringmetNHG => GeregistreerdeLeningType.RNRestschuldmetaanspraakopNHG,
                //SoortVerplichtingEnum.Privatelease => GeregistreerdeLeningType.lease,
                //SoortVerplichtingEnum.VKverzendhuiskredieten => GeregistreerdeLeningType.verz,
                _ => GeregistreerdeLeningType.AKAflopendkrediet,
            };
        }


#if ENERGIEKLASSE
        public static void SetHdnEnergieLabel(EnergielabelEnum? finixValue, object hdnTarget, string hdnFieldName)
        {
            {
                var property = hdnTarget.GetType().GetProperty(hdnFieldName);
                if (((finixValue != null) && (finixValue != EnergielabelEnum.Geen)) || !(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    property.SetValue(hdnTarget, SpecialEnums.GetHdnEnergieLabel(finixValue));
                }
            }
        }

        public static EnergieKlasseType GetHdnEnergieLabel(EnergielabelEnum? klasse)
        {
            if (klasse != null)
            {
                string klasseString = klasse.ToName();

                foreach (EnergieKlasseType enumValue in Enum.GetValues(typeof(EnergieKlasseType)))
                {
                    if (enumValue.ToName()[3..] == klasseString)
                        return enumValue;
                }
            }
            GlobalOutputInfo.buildErrors.Add("GetHdnEnergieLabel: ongeldige parameter, default waarde gebruikt");
            return (EnergieKlasseType)0;
        }
#endif

#if MAATWERKOPLCODEMIJTYPE
        public static MaatwerkOplCodeMijType GetHdnMotivatieReden(string motivatie)
        {
            foreach (MaatwerkOplCodeMijType enumValue in Enum.GetValues(typeof(MaatwerkOplCodeMijType)))
            {
                if (enumValue.ToName()[6..] == motivatie)
                    return enumValue;
            }

            GlobalOutputInfo.buildErrors.Add("GetHdnMotivatieReden: ongeldige parameter, default waarde gebruikt");
            return (MaatwerkOplCodeMijType)0;

        }
#endif

        public static RekenexpertType GetHdnRekenExpert(RekenExpertTypeEnum? expert)
        {
            string deExpert = expert.ToName();
            foreach (RekenexpertType enumValue in Enum.GetValues(typeof(RekenexpertType)))
            {
                if (enumValue.ToName()[3..] == deExpert)
                    return enumValue;
            }
            GlobalOutputInfo.buildErrors.Add("GetHdnRekenExpert: ongeldige parameter, default waarde gebruikt");
            return (RekenexpertType)0;
        }

        public static MutatieType? GetHdnMutatie(OnderpandMutatieMeldingTypeEnum? mutatie)
        {
            if (mutatie == null)
                return null;

            string deMutatie = mutatie.ToName();
            foreach (MutatieType enumValue in Enum.GetValues(typeof(MutatieType)))
            {
                if (enumValue.ToName()[3..] == deMutatie)
                    return enumValue;
            }
            GlobalOutputInfo.buildErrors.Add("GetHdnMutatie: ongeldige parameter, default waarde gebruikt");
            return (MutatieType)0;
        }

        public static NotarisType GetHdnTypeNotaris(NotarisTypeEnum? type)
        {
            switch (type)
            {
                case NotarisTypeEnum.Passerend:
                    return NotarisType.Item1Passerend;
                case NotarisTypeEnum.Transporterend:
                    return NotarisType.Item2Transporterend;
                case NotarisTypeEnum.Opstellergarantieverklaring:
                    return NotarisType.Item3Opstellergarantieverklaring;
                case NotarisTypeEnum.Geenselectie:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetHdnTypeNotaris: ongeldige parameter, default waarde gebruikt");
                    return (NotarisType)0;
            }
        }

        public static SoortNieuwbouwType GetHdnSoortNieuwbouw(SoortNieuwbouwEnum? soort)
        {
            return soort switch
            {
                SoortNieuwbouwEnum.CPOCollectiefParticulierOpdrachtgeverschap => SoortNieuwbouwType.Item01CPO,
                SoortNieuwbouwEnum.Zelfbouw => SoortNieuwbouwType.Item02Zelfbouw,
                SoortNieuwbouwEnum.Projectbouw => SoortNieuwbouwType.Item03Projectbouw,
                _ => SoortNieuwbouwType.Item03Projectbouw,
            };
        }

#if OPLEIDINGSNIVEAU
        internal static OpleidingsNiveauType getHdnOpleidingsNiveau(OpleidingsNiveauTypeEnum? hOOpleiding)
        {
            if (hOOpleiding == null)
            {
                return OpleidingsNiveauType.Item08Onbekend;
            }

            switch (hOOpleiding)
            {
                case OpleidingsNiveauTypeEnum.Lageronderwijs:
                    return OpleidingsNiveauType.Item01Lageronderwijs;
                case OpleidingsNiveauTypeEnum.VoortgezetonderwijsMAVOHAVO:
                    return OpleidingsNiveauType.Item02VoortgezetonderwijsMAVOHAVO;
                case OpleidingsNiveauTypeEnum.VWO:
                    return OpleidingsNiveauType.Item03VWO;
                case OpleidingsNiveauTypeEnum.Lagerberoepsonderwijs:
                    return OpleidingsNiveauType.Item04Lagerberoepsonderwijs;
                case OpleidingsNiveauTypeEnum.Middelbaarberoepsonderwijs:
                    return OpleidingsNiveauType.Item05Middelbaarberoepsonderwijs;
                case OpleidingsNiveauTypeEnum.Hogerberoepsonderwijs:
                    return OpleidingsNiveauType.Item06Hogerberoepsonderwijs;
                case OpleidingsNiveauTypeEnum.postAcademisch:
                    return OpleidingsNiveauType.Item07postAcademisch;
                default:
                    return OpleidingsNiveauType.Item08Onbekend;
            }
        }
#endif


    }
}

