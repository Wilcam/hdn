﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text.RegularExpressions;
using Finix;
using HDNRESULT;



#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif MONEYOU
using HDN.MONEYOU;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN
{
    

    public static class Utils
    {
        public static DatumType GetHdnDate(DateTime dateTime)
        {
            DatumType theDateType = new()
            {
                Dag = dateTime.Day.ToString(),
                Maand = dateTime.Month.ToString(),
                Jaar = dateTime.Year.ToString()
            };
            return theDateType;
        }

#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
        public static DatumType? GetHdnDate(string dateTimeString)
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
        {
            if (dateTimeString == null)
                return null;

            DateTime dateTime = Convert.ToDateTime(dateTimeString);
            DatumType theDateType = new()
            {
                Dag = dateTime.Day.ToString(),
                Maand = dateTime.Month.ToString(),
                Jaar = dateTime.Year.ToString()
            };
            return theDateType;
        }

        public static TijdType GetHdnTime(DateTime dateTime)
        {
            TijdType theTimeType = new()
            {
                Seconden = dateTime.Second.ToString(),
                Minuten = dateTime.Minute.ToString(),
                Uur = dateTime.Hour.ToString()
            };
            return theTimeType;
        }

        static public bool IsNullable<T>(T obj)
        {
            if (obj == null) return true; // obvious
            Type type = typeof(T);
            if (!type.IsValueType) return true; // ref-type
            if (Nullable.GetUnderlyingType(type) != null) return true; // Nullable<T>
            return false; // value-type
        }

        public static int GetMonthPassed(string endDateString, DateTime startDate)
        {
            DateTime endDate = Convert.ToDateTime(endDateString);
            return (endDate.Month - startDate.Month) + 12 * (endDate.Year - startDate.Year);
        }

        public static int GetMonthPassed(string endDateString, string startDateString)
        {
            DateTime startDate = Convert.ToDateTime(startDateString);
            DateTime endDate = Convert.ToDateTime(endDateString);
            return (endDate.Month - startDate.Month) + 12 * (endDate.Year - startDate.Year);
        }

        

        public static int GetIntValue(object obj, string propName)
        {
            var property = obj.GetType().GetProperties().Single(pi => pi.Name == propName);
            PropertyInfo propInfo = obj.GetType().GetProperty(property.Name, BindingFlags.Instance | BindingFlags.Public);
            MethodInfo methodName = propInfo.GetGetMethod();

            var res = methodName.Invoke(obj, null);
            if (res == null)
                return 0;

            return (int)res;
        }

        public static string GetStringValue(object obj, string propName)
        {
            var property = obj.GetType().GetProperties().Single(pi => pi.Name == propName);
            PropertyInfo propInfo = obj.GetType().GetProperty(property.Name, BindingFlags.Instance | BindingFlags.Public);
            MethodInfo methodName = propInfo.GetGetMethod();
            return (string)methodName.Invoke(obj, null);
        }

        public static DateTime GetDateValue(object obj, string propName)
        {
            var property = obj.GetType().GetProperties().Single(pi => pi.Name == propName);
            PropertyInfo propInfo = obj.GetType().GetProperty(property.Name, BindingFlags.Instance | BindingFlags.Public);
            MethodInfo methodName = propInfo.GetGetMethod();
            var strDate = methodName.Invoke(obj, null);
            return Convert.ToDateTime(strDate);
        }

        public static decimal GetDecimalValue(object obj, string propName, bool zonderafronden = false)
        {
            var property = obj.GetType().GetProperties().Single(pi => pi.Name == propName);
            PropertyInfo propInfo = obj.GetType().GetProperty(property.Name, BindingFlags.Instance | BindingFlags.Public);
            MethodInfo methodName = propInfo.GetGetMethod();
            var ff = methodName.Invoke(obj, null);
            if (ff == null)
                return GetHdnDecimal(0);

            if (zonderafronden)
                return GetHdnDecimalZonderAfronden((decimal)methodName.Invoke(obj, null));
            else
                return GetHdnDecimal((decimal)methodName.Invoke(obj, null));
        }

        public static decimal GetHdnDecimal(decimal? finixAmount)
        {
            if (finixAmount == null)
                return 0.00M;

            decimal dec = 0.00M;
            dec += (decimal)finixAmount;
            dec = Decimal.Round(dec);
            dec += 0.00M;

            return dec;
        }

        /*public static decimal GetHdnDecimal(decimal? hdnAmount, decimal? finixAmount)
        {
            if ((finixAmount != 0) || (hdnAmount != null))
                return  GetHdnDecimal(finixAmount);
            return GetHdnDecimal(hdnAmount);
        }*/

        public static void SetHdnDecimal(decimal? finixValue, object hdnTarget, string hdnFieldName, bool zonderafronden = false)
        {
            var property = hdnTarget.GetType().GetProperty(hdnFieldName);
            if (((finixValue != null) && (finixValue != 0)) || !(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                // trigger for object to set the right value for 0 ( 0.00 iso 0), to do so, be sure the value is changed, henge set value of 1 first
                property.SetValue(hdnTarget, GetHdnDecimal(1));
                if (zonderafronden)
                    property.SetValue(hdnTarget, GetHdnDecimalZonderAfronden(finixValue));
                else
                    property.SetValue(hdnTarget, GetHdnDecimal(finixValue));
            }
        }

        public static void SetHdnString(string finixValue, object hdnTarget, string hdnFieldName, string? InvalidChar = null)
        {
            if (finixValue is null)
            {
                throw new ArgumentNullException(nameof(finixValue));
            }

            if (InvalidChar is null)
            {
                throw new ArgumentNullException(nameof(InvalidChar));
            }

            var property = hdnTarget.GetType().GetProperty(hdnFieldName);
            if (((finixValue != null) && (finixValue != "")) || !(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                if ((InvalidChar != null) && (finixValue != null))
                    finixValue = finixValue.Replace(InvalidChar, string.Empty);
                property.SetValue(hdnTarget, finixValue);
            }
        }

        public static void SetHdnYesNo(JaNeeEnum? finixValue, object hdnTarget, string hdnFieldName)
        {
            var property = hdnTarget.GetType().GetProperty(hdnFieldName);
            if ((finixValue != null) || !(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                BooleanType hdnValue = (finixValue == JaNeeEnum.Ja) ? BooleanType.JJa : BooleanType.NNee;
                property.SetValue(hdnTarget, hdnValue);
            }
        }
        public static void SetHdnYesNo(DataBooleanFieldT? finixValue, object hdnTarget, string hdnFieldName)
        {
            var property = hdnTarget.GetType().GetProperty(hdnFieldName);
            if ((finixValue != null) || !(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                BooleanType hdnValue = (finixValue == DataBooleanFieldT.@true) ? BooleanType.JJa : BooleanType.NNee;
                property.SetValue(hdnTarget, hdnValue);
            }
        }

        public static decimal GetHdnDecimalZonderAfronden(decimal? finixAmount)
        {
            if (finixAmount == null)
                return 0.00M;

            decimal dec = 0.00M;
            dec += (decimal)finixAmount;
            dec = Decimal.Round(dec, 2);

            return dec;
        }

        public static int GetTermijnenPerJaar(TermijnBetalenEnum? termijnBetalen)
        {
            if (termijnBetalen == null)
                return 0;

            return termijnBetalen switch
            {
                TermijnBetalenEnum.Item4Weeks => 13,
                TermijnBetalenEnum.Maand => 12,
                TermijnBetalenEnum.Kwartaal => 4,
                TermijnBetalenEnum.Halfjaar => 2,
                TermijnBetalenEnum.Jaar => 1,
                _ => 0,
            };
        }

    }
}
