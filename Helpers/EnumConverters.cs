﻿

using System;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using Finix;
using HDNRESULT;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN
{
    public static class Enums
    {
        public static BooleanType GetHdnBooleanType(Finix.DataBooleanFieldT finixBool)
        {
            if (finixBool == DataBooleanFieldT.@true)
                return BooleanType.JJa;
            return BooleanType.NNee;
        }
        public static AflossingsVormType GetHdnAflossingsVormType(Finix.SoortHypotheekEnum soortHypotheek)
        {
            switch (soortHypotheek)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return AflossingsVormType.Item05Aflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return AflossingsVormType.Item01Annuiteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return AflossingsVormType.Item02Lineair;
                case SoortHypotheekEnum.Hybridehypotheek:
                    return AflossingsVormType.Item03Levensverzekering;
                case SoortHypotheekEnum.Levenhypotheek:
                    return AflossingsVormType.Item03Levensverzekering;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return AflossingsVormType.Item03Levensverzekering;
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                    return AflossingsVormType.Item03Levensverzekering;
                case SoortHypotheekEnum.Effectenhypotheek:
                    return AflossingsVormType.Item10Beleggingshypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return AflossingsVormType.Item13Spaarrekening;
                case SoortHypotheekEnum.BEWhypotheek:
                    return AflossingsVormType.Item10Beleggingshypotheek;
                default:
                    GlobalOutputInfo.buildErrors.Add("GetHdnAflossingsVormType: ongeldige parameter, default waarde gebruikt");
                    return (AflossingsVormType)0;
            }
        }

        

        public static BooleanType GetHdnBooleanType(bool theBool)
        {
            if (theBool)
                return BooleanType.JJa;
            return BooleanType.NNee;
        }

        public static BooleanType GetHdnBooleanType(Finix.DataBooleanFieldT? finixBool)
        {
            if (finixBool == null)
                return BooleanType.NNee;


            if (finixBool == DataBooleanFieldT.@true)
                return BooleanType.JJa;
            return BooleanType.NNee;
        }

        public static BooleanType GetHdnBooleanType(Finix.JaNeeEnum? finixBool)
        {
            if (finixBool == null)
                return BooleanType.NNee;

            if (finixBool == JaNeeEnum.Ja)
                return BooleanType.JJa;
            return BooleanType.NNee;
        }

        internal static CodeUitkeringType GetHdnCodeUitkering(OverlijdensdekkingMethodeEnum? dekkingsmethode)
        {
            switch (dekkingsmethode)
            {
                case OverlijdensdekkingMethodeEnum.Gelijkblijvend:
                    return CodeUitkeringType.Item03gelijkblijvend;
                case OverlijdensdekkingMethodeEnum.Gelijkblijvendopeinddatum:
                    return CodeUitkeringType.Item03gelijkblijvend;
                case OverlijdensdekkingMethodeEnum.Lineairdalend:
                    return CodeUitkeringType.Item01Lineairdalend;
                case OverlijdensdekkingMethodeEnum.Lineairdalendtotnul:
                    return CodeUitkeringType.Item01Lineairdalend;
                case OverlijdensdekkingMethodeEnum.Annuitairdalend:
                    return CodeUitkeringType.Item02annuitairdalend;
                case OverlijdensdekkingMethodeEnum.Annuitairdalendtotnul:
                    return CodeUitkeringType.Item02annuitairdalend;
                case OverlijdensdekkingMethodeEnum.Geen:
                case OverlijdensdekkingMethodeEnum.Erfrente:
                case OverlijdensdekkingMethodeEnum.Premierestitutie:
                case OverlijdensdekkingMethodeEnum.kapitaalbijleven:
                case OverlijdensdekkingMethodeEnum.waardeverzekering:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetHdnCodeUitkering: ongeldige parameter, default waarde gebruikt");
                    return (CodeUitkeringType)0;
            }
        }

       

        public static BurgerlijkeStaatType GetHdnBurgerlijkeStaatType(BurgerlijkeStaatEnum? finixStaat)
        {
            return finixStaat switch
            {
                BurgerlijkeStaatEnum.Alleenstaand => BurgerlijkeStaatType.Item03Alleenstaand,
                BurgerlijkeStaatEnum.Gehuwdgemeenschapvangoederen => BurgerlijkeStaatType.Item01Gehuwdgemeenschapvangoederen,
                BurgerlijkeStaatEnum.Gehuwdinbeperktegemeenschapvangoederen => BurgerlijkeStaatType.Item09Gehuwdinbeperktegemeenschapvangoederen,
                BurgerlijkeStaatEnum.Gehuwdmethuwelijksevoorwaarden => BurgerlijkeStaatType.Item02Gehuwdmethuwelijksevoorwaarden,
                BurgerlijkeStaatEnum.Partnerregistratieinbeperktegemeenschapvangoederen => BurgerlijkeStaatType.Item10Partnerregistratieinbeperktegemeenschapvangoederen,
                BurgerlijkeStaatEnum.Partnerregistratieingemeenschapvangoederen => BurgerlijkeStaatType.Item06Partnerregistratieingemeenschapvangoederen,
                BurgerlijkeStaatEnum.Samenwonendmetsamenlevingscontract => BurgerlijkeStaatType.Item04Samenwonendmetsamenlevingscontract,
                BurgerlijkeStaatEnum.Samenwonendzondersamenlevingscontract => BurgerlijkeStaatType.Item05Samenwonendzondersamenlevingscontract,
                BurgerlijkeStaatEnum.Partnerregistratieonderregistratievoorwaarden => BurgerlijkeStaatType.Item07Partnerregistratieonderregistratievoorwaarden,
                _ => BurgerlijkeStaatType.Item03Alleenstaand,
            };
        }

        public static CodeFinancieleDekkingType GetHdnFinancieleDekkingType(AflossingsVormType? finixAflosvorm)
        {
            return finixAflosvorm switch
            {
                AflossingsVormType.Item03Levensverzekering => CodeFinancieleDekkingType.Item01levensverzekering,
                AflossingsVormType.Item13Spaarrekening => CodeFinancieleDekkingType.Item07Spaarverzekering,
                _ => CodeFinancieleDekkingType.Item01levensverzekering,
            };
        }

        public static KoppelingBestaandEnum OnveranderdVoortzettenKoppelAanBestaand(int leningdeelNR)
        {
            return leningdeelNR switch
            {
                1 => KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel1,
                2 => KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel2,
                3 => KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel3,
                4 => KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel4,
                _ => KoppelingBestaandEnum.Nietgebruikeninadvies,
            };
        }

        
        public static MaatschappijType GetHdnMaatschappij(string mij)
        {
            // naam in finix is leben dit moet leven zijn volgens HDN
            mij = mij.Replace("Leben", "Leven");

            foreach (MaatschappijType enumValue in Enum.GetValues(typeof(MaatschappijType)))
            {
                if (enumValue.ToName()[3..] == mij)
                    return enumValue;
            }

            foreach (MaatschappijType enumValue in Enum.GetValues(typeof(MaatschappijType)))
            {
                if (mij.Contains(enumValue.ToName()[3..]))
                    return enumValue;
            }
            return MaatschappijType.ZZAnders;
        }

        

        public static KoppelingBestaandEnum GetOngewijzigdvoortzettenBestaandEnumValue(int nr)
        {
            switch (nr)
            {
                case 1:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel1;
                case 2:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel2;
                case 3:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel3;
                case 4:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanbestaandleningdeel4;
                default:
                    GlobalOutputInfo.buildErrors.Add("GetOngewijzigdvoortzettenBestaandEnumValue: ongeldige parameter, default waarde gebruikt");
                    return (KoppelingBestaandEnum)0;
            }
        }

        public static KoppelingBestaandEnum GetOngewijzigdvoortzettenNieuwEnumValue(int nr)
        {
            switch (nr)
            {
                case 1:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanleningdeel1;
                case 2:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanleningdeel2;
                case 3:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanleningdeel3;
                case 4:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanleningdeel4;
                case 5:
                    return KoppelingBestaandEnum.Ongewijzigdvoortzettenkoppelenaanleningdeel5;
                default:
                    GlobalOutputInfo.buildErrors.Add("GetOngewijzigdvoortzettenNieuwEnumValue: ongeldige parameter, default waarde gebruikt");
                    return (KoppelingBestaandEnum)0;
            }
        }


    
        public static void SetHdnLand(LandWerkgeverTypeEnum? finixValue, object hdnTarget, string hdnFieldName)
        {
            {
                var property = hdnTarget.GetType().GetProperty(hdnFieldName);
                if ((finixValue != null) || !(property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    property.SetValue(hdnTarget, GetHdnLand(finixValue));
                }
            }
        }
        private static LandType GetHdnLand(LandWerkgeverTypeEnum? land)
        {
            if (land != null)
            {
                string hetLand = Enum.GetName(typeof(LandWerkgeverTypeEnum), land);
                foreach (LandType enumValue in Enum.GetValues(typeof(LandType)))
                {
                    if (enumValue.ToName()[3..] == hetLand)
                        return enumValue;
                }
            }
            GlobalOutputInfo.buildErrors.Add("GetHdnLand: ongeldige parameter, default waarde gebruikt");
            return LandType.NLNederland;
        }

    

        public static LegitimatieSoortType GetHdnLegitimatie(LegitimatieSoortEnum idType)
        {
            switch (idType)
            {
                case LegitimatieSoortEnum.Paspoort:
                    return LegitimatieSoortType.PPPaspoort;
                case LegitimatieSoortEnum.Rijbewijs:
                    return LegitimatieSoortType.RBRijbewijs;
                case LegitimatieSoortEnum.Europeseidentiteitskaart:
                    return LegitimatieSoortType.EIEuropeseidentiteitskaart;
                case LegitimatieSoortEnum.Visum:
                    return LegitimatieSoortType.VIVisum;
                default:
                    GlobalOutputInfo.buildErrors.Add("GetHdnLegitimatie: ongeldige parameter, default waarde gebruikt");
                    return (LegitimatieSoortType)0;
            }
        }

       


    }
}

