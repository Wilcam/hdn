﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text.RegularExpressions;
using Finix;
using HDNRESULT;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif MONEYOU
using HDN.MONEYOU;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN
{
    

    public static class SpecialUtils
    {

        public static List<int> GetHypotheekOpties(string ProductKenmerken)
        {
            List<string> list = ProductKenmerken.Split('#').ToList();
            var myRegex = new Regex(@"\d{1,2}=1");
            IEnumerable<string> tmpList = list.Where(x => myRegex.IsMatch(x)).ToList();

            List<int> returnList = new List<int>();
            foreach (var el in tmpList)
            {
                returnList.Add(Int16.Parse(el.Substring(0, el.IndexOf("=1"))));
            }
            return returnList;
        }

        public static RenteBedenktijdType GetRenteBedenktijd(int finixNr)
        {
            //RBTsoortT = (rbtsOngedefinieerd, rbtsGeen, rbtsVooraf, rbtsAchteraf);
            return finixNr switch
            {
                0 => RenteBedenktijdType.Item01geen,
                1 => RenteBedenktijdType.Item01geen,
                2 => RenteBedenktijdType.Item02vooraf,
                _ => RenteBedenktijdType.Item03achteraf,
            };
        }

        public static RenteAfspraakType GetRenteAfspraak(int finixNr)
        {
            //  RVPsoortT = ( rvpsOngedefinieerd,rvpsVast,rvpsVariabel, rvpsBandbreedte, rvpsCorrectierente, rvpsPlafond, rvpsOverbrug, rvpsRentePlafond);
            switch (finixNr)
            {
                case 1:
                    return RenteAfspraakType.Item01rentevast;
                case 2:
                    return RenteAfspraakType.Item03continuvariabel;
                case 3:
                case 4:
                case 5:
                    return RenteAfspraakType.Item02rentedempendsysteem;
                default:
                    GlobalOutputInfo.buildErrors.Add("RenteAfspraakType: ongeldige parameter, default waarde gebruikt");
                    return (RenteAfspraakType)0;
            }
        }


     



        public static HSKoopFieldT GetToekomstigOnderpandFinancieel(HuidigeSituatieDataT HuidigeSituatie)
        {
            if (HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.HuidigeWoonSituatieClient == WoonsituatieEnum.Koopwoning)
                return HuidigeSituatie.HuidigeWoonsituatieClient.HuidigeWoonsituatie.Koop;

            if (HuidigeSituatie.AlgemeneGegevens.PartnerAanwezig == DataBooleanFieldT.@true)
                if (HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.HuidigeWoonSituatiePartner == WoonsituatieEnum.Koopwoning)
                    if (HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.WoningPartnerWordtGezamelijk == DataBooleanFieldT.@true)
                    {
                        return HuidigeSituatie.HuidigeWoonsituatiePartner.HuidigeWoonsituatie.Koop;
                    }
            return null;
        }

        public static PersoonsGegevensFieldT GetToekomstigOnderpandAdres(HuidigeSituatieDataT HuidigeSituatie)
        {
            if (HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.HuidigeWoonSituatieClient == WoonsituatieEnum.Koopwoning)
                return HuidigeSituatie.AlgemeneGegevens.ClientGegevens;

            if (HuidigeSituatie.AlgemeneGegevens.PartnerAanwezig == DataBooleanFieldT.@true)
                if (HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.HuidigeWoonSituatiePartner == WoonsituatieEnum.Koopwoning)
                    if (HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.WoningPartnerWordtGezamelijk == DataBooleanFieldT.@true)
                    {
                        return HuidigeSituatie.AlgemeneGegevens.PartnerGegevens;
                    }
            return null;
        }

        
      
        public static bool IsNieuweWoning(SoortScenarioEnum? soort)
        {
            return soort switch
            {
                SoortScenarioEnum.Aankoopbestaandebouw => true,
                SoortScenarioEnum.Aankoopnieuwbouw => true,
                SoortScenarioEnum.Uitponden => false,
                SoortScenarioEnum.Item1stehypotheekonbelastewoning => false,
                SoortScenarioEnum.Omzettenevtverhogenbestaandleningdeel => false,
                SoortScenarioEnum.Oversluiten => false,
                SoortScenarioEnum.Tweedehypotheek => false,
                SoortScenarioEnum.Verhogen => false,
                _ => false,
            };
        }
        public static CodeUitkeringType GetHdnCodeUitkering(string dekkingsmethode)
        {
            if (dekkingsmethode.Contains("ineair"))
                return CodeUitkeringType.Item01Lineairdalend;
            else if (dekkingsmethode.Contains("nnuitair"))
                return CodeUitkeringType.Item02annuitairdalend;
            else
                return CodeUitkeringType.Item03gelijkblijvend;
        }

        static public bool IsCorrectCodeLeningMij(string codeMij)
        {
            bool found = false;
            foreach (CodeLeningMijType enumValue in Enum.GetValues(typeof(CodeLeningMijType)))
            {
                if (enumValue.ToName() == codeMij)
                    found = true;
            }
            return found;
        }

        static public bool IsNullable<T>(T obj)
        {
            if (obj == null) return true; // obvious
            Type type = typeof(T);
            if (!type.IsValueType) return true; // ref-type
            if (Nullable.GetUnderlyingType(type) != null) return true; // Nullable<T>
            return false; // value-type
        }
    }
}
