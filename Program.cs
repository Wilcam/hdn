﻿using System.Globalization;
using Finix;
using System.IO;
using HDN.CONTROL;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;
using HDNRESULT;

#if ASR
using HDN.ASR;
using Factory = HDN.Factories.AsrFactory;
#elif ABN
using HDN.ABN;
using Factory = HDN.Factories.AbnFactory;
#elif ABNGRP
using HDN.ABNGRP;
using Factory = HDN.Factories.AbnGroepFactory;
#elif ING
using HDN.ING;
using Factory = HDN.Factories.IngFactory;
#elif MUNT
using HDN.MUNT;
using Factory = HDN.Factories.MuntFactory;
#elif ATTENS
using HDN.ATTENS;
using Factory = HDN.Factories.AttensFactory;
#elif RABO
using HDN.RABO;
using Factory = HDN.Factories.RaboFactory;
#elif COLIBRI
using HDN.COLIBRI;
using Factory = HDN.Factories.ColibriFactory;
#elif VENN
using HDN.VENN;
using Factory = HDN.Factories.VennFactory;
#elif AEGON
using HDN.AEGON;
using Factory = HDN.Factories.AegonFactory;
#elif HYPOTRUST
using HDN.HYPOTRUST;
using Factory = HDN.Factories.HypotrustFactory;
#elif MONEYOU
using HDN.MONEYOU;
using Factory = HDN.Factories.MoneyouFactory;
#elif CENTRAAL
using HDN.CENTRAAL;
using Factory = HDN.Factories.CentraalFactory;
#elif NN
using HDN.NN;
using Factory = HDN.Factories.NnFactory;
#elif WOONNU
using HDN.WOONNU;
using Factory = HDN.Factories.WoonnuFactory;
#elif NIBC
using HDN.NIBC;
using Factory = HDN.Factories.NibcFactory;
#elif BLG
using HDN.BLG;
using Factory = HDN.Factories.BlgFactory;
#elif DELTA
using HDN.DELTA;
using Factory = HDN.Factories.DeltaFactory;
#elif ARGENTA
using HDN.ARGENTA;
using Factory = HDN.Factories.ArgentaFactory;
#elif ALLIANZ
using HDN.ALLIANZ;
using Factory = HDN.Factories.AllianzFactory;
#elif ASN
using HDN.ASN;
using Factory = HDN.Factories.AsnFactory;
#elif BIJBOUWE
using HDN.BIJBOUWE;
using Factory = HDN.Factories.BijbouweFactory;
#elif BNP
using HDN.BNP;
using Factory = HDN.Factories.BnpFactory;
#elif DNGB
using HDN.DNGB;
using Factory = HDN.Factories.DngbFactory;
#elif DYNAMIC
using HDN.DYNAMIC;
using Factory = HDN.Factories.DynamicFactory;
#elif HWOONT
using HDN.HWOONT;
using Factory = HDN.Factories.HwoontFactory;
#elif IQWOON
using HDN.IQWOON;
using Factory = HDN.Factories.IqwoonFactory;
#elif LLOYDS
using HDN.LLOYDS;
using Factory = HDN.Factories.LloydsFactory;
#elif LOT
using HDN.LOT;
using Factory = HDN.Factories.LotFactory;
#elif MERIUS
using HDN.MERIUS;
using Factory = HDN.Factories.MeriusFactory;
#elif OBVION
using HDN.OBVION;
using Factory = HDN.Factories.ObvionFactory;
#elif PHILIPS
using HDN.PHILIPS;
using Factory = HDN.Factories.PhilipsFactory;
#elif REAAL
using HDN.REAAL;
using Factory = HDN.Factories.ReaalFactory;
#elif REGIO
using HDN.REGIO;
using Factory = HDN.Factories.RegioFactory;
#elif ROBUUST
using HDN.ROBUUST;
using Factory = HDN.Factories.RobuustFactory;
#elif SNS
using HDN.SNS;
using Factory = HDN.Factories.SnsFactory;
#elif SYNTRUS
using HDN.SYNTRUS;
using Factory = HDN.Factories.SyntrusFactory;
#elif TELLIUS
using HDN.TELLIUS;
using Factory = HDN.Factories.TelliusFactory;
#elif TULP
using HDN.TULP;
using Factory = HDN.Factories.TulpFactory;
#elif VISTA
using HDN.VISTA;
using Factory = HDN.Factories.VistaFactory;
#elif WFONDS
using HDN.WFONDS;
using Factory = HDN.Factories.WoonfondsFactory;
#endif

namespace HDN
{
    class Program


    {

        static Program()
        {
            log4net.GlobalContext.Properties["assembly"] = Assembly.GetExecutingAssembly().GetName().Name + "_" + Assembly.GetExecutingAssembly().GetName().Version + "_";
            log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));
        }

        ///private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main()
        {

            string finixImportFile = "TestInputFiles/test2out.xml";
            string axExportFile = "TestInputFiles/test2AX.xml";                        
            Factory factory = new(File.ReadAllText(finixImportFile));           
            OutputInfo ff = factory.Run(axExportFile);
            //OutputInfo ff = factory.OnlyCheck("ERROR-QUIVOOI-AX200006_140121162335.719.xml");
            ff.ShowInfo();
            Console.WriteLine("Done");          
            Console.ReadLine();
        }
    }
}
