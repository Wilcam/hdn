﻿Procedure om HDN berichten te produceren.
Voor zowel Finix als voor HDN schema's zal  vanuit de XSD's  de datamodellen worden gegenereerd
Met deze datamodellen wordt dan een HDN bericht samengesteld

1 Verkrijgen van XSD van datamodel van Finix
	in de finix code is een procedure aanwezig die een XSD maakt van het datamodel
	deze procedure heet: generateXSDFromDataModel
	die functie levert een finix.xsd bestand op
2 Plaats de XSD in HDN/models/Finix
3 Voer vervolgens  het volgende commando uit  "C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.8 Tools\xsd.exe" finix.xsd /classes /namespace:Finix 
4 dit levert in HDN/models/Finix finix.cs op met daarin het datamodel

5 Verkrijgen van XSD van datamodel van diverse HDN maatschappijen
	deze XSD's worden door de maatschappijen aangeleverd, dus hier hoeft verder niks voor te gebeuren
6  wijzig het XSD zodat het juiste model wordt gegenereerd
    vervang '<xsd:element minOccurs="0" ' door '<xsd:element minOccurs="0" nillable="true" 
7 Ga in Visual Studio met de rechtermuis op het XSD staan en selecteer 'run xsd2code++' (deze tool doet veel beter zijn werk dan de gratis xsd.ex )
8 Selecteer in de opgekomen dialog in het menu properties 'load default properties'  (deze zijn voor het genereren al goed ingesteld)
9 Verander de namespace in de juiste namespace (= maatschappij afkorting, dus bijvoorbeeld voor ABN Bank is dat ABN)
10 Druk op Genereer
11 dit levert in HDN/models/<namespace> een directory op met als naam <namespace> moet daarin alle files van het datamodel van de maatschappij

Nu is het voor deze maatschappij mogelijk om een HDN bericht te genereren, voorlopig alleen nog AX berichten

De code dit dit regelt staat in HDN/factories,  per maatschappij is er een factory die allemaal als base class BaseFactory hebben, hierin gebeurt het meeste
	