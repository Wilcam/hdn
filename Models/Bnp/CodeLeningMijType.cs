// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.BNP
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum CodeLeningMijType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("BN019 BNP Paribas Hypotheek Trois")]
    BN019BNPParibasHypotheekTrois,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN011 PLUS+UNIEK Voordeel Hypotheek")]
    BN011PLUSUNIEKVoordeelHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN004 UCB Excellence Voordeel Hypotheek")]
    BN004UCBExcellenceVoordeelHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN015 WoonGerust+ hypotheek")]
    BN015WoonGerusthypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN002 UCB Excellence Hypotheek")]
    BN002UCBExcellenceHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN006 Liberté Hypotheek")]
    BN006LibertéHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN012 Excellence Hypotheek")]
    BN012ExcellenceHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN007 Liberté Voordeel Hypotheek")]
    BN007LibertéVoordeelHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN003 Beta Plus Hypotheek")]
    BN003BetaPlusHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN009 Confidence Voordeel Hypotheek")]
    BN009ConfidenceVoordeelHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN020 BNP Paribas Hypotheek Quatre")]
    BN020BNPParibasHypotheekQuatre,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN017 BNP Paribas Hypotheek")]
    BN017BNPParibasHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN008 Confidence Hypotheek")]
    BN008ConfidenceHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN014 WoonGerust hypotheek")]
    BN014WoonGerusthypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN016 Hypotheekshop Netto Plus (BNPParibasPF)")]
    BN016HypotheekshopNettoPlusBNPParibasPF,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN001 UCB Hypotheek")]
    BN001UCBHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN005 UCB PLUS+UNIEK Hypotheek")]
    BN005UCBPLUSUNIEKHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("BN018 BNP Paribas Hypotheek Deux")]
    BN018BNPParibasHypotheekDeux,
}
}
#pragma warning restore
