// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.COLIBRI
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum RechtsVormType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("BV Besloten vennootschap")]
    BVBeslotenvennootschap,
    
    [System.Xml.Serialization.XmlEnumAttribute("CV Commanditaire vennootschap")]
    CVCommanditairevennootschap,
    
    [System.Xml.Serialization.XmlEnumAttribute("EMZ Eenmanszaak")]
    EMZEenmanszaak,
    
    [System.Xml.Serialization.XmlEnumAttribute("MA Maatschap")]
    MAMaatschap,
    
    [System.Xml.Serialization.XmlEnumAttribute("NV Naamloze vennootschap")]
    NVNaamlozevennootschap,
    
    [System.Xml.Serialization.XmlEnumAttribute("ST Stichting")]
    STStichting,
    
    [System.Xml.Serialization.XmlEnumAttribute("VOF Vennootschap onder firma")]
    VOFVennootschaponderfirma,
}
}
#pragma warning restore
