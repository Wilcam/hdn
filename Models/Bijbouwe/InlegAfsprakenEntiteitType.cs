// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.BIJBOUWE
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="InlegAfsprakenEntiteitType")]
public class InlegAfsprakenEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private bool _shouldSerializeBetalingsTermijn;
    
    private bool _shouldSerializeSoortInleg;
    
    private bool _shouldSerializeInlegBedrag;
    
    private InlegType _soortInleg;
    
    private string _aanvangNaIngangsDtInMnd;
    
    private string _duurInMnd;
    
    private decimal _inlegBedrag;
    
    private BetalingsTermijnType _betalingsTermijn;
    
    private System.Nullable<decimal> _inlegUitDepot;
    
    private REFPartijNAWDataType _refDebiteurNAWData;
    
    private string _volgnummer;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// Geeft de soort inleg weer.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public InlegType SoortInleg
    {
        get
        {
            return this._soortInleg;
        }
        set
        {
            if ((_soortInleg.Equals(value) != true))
            {
                this._soortInleg = value;
                this.OnPropertyChanged("SoortInleg");
            }
            _shouldSerializeSoortInleg = true;
        }
    }
    
    /// <summary>
    /// Geschatte aanvangstermijn (in maanden) na ingangsdatum.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger")]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("\\d{1,3}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string AanvangNaIngangsDtInMnd
    {
        get
        {
            return this._aanvangNaIngangsDtInMnd;
        }
        set
        {
            if ((this._aanvangNaIngangsDtInMnd == value))
            {
                return;
            }
            if (((this._aanvangNaIngangsDtInMnd == null) 
                        || (_aanvangNaIngangsDtInMnd.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "AanvangNaIngangsDtInMnd";
                Validator.ValidateProperty(value, validatorPropContext);
                this._aanvangNaIngangsDtInMnd = value;
                this.OnPropertyChanged("AanvangNaIngangsDtInMnd");
            }
        }
    }
    
    /// <summary>
    /// Looptijd in maanden, 999 = zonder einddatum.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger")]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("\\d{1,3}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string DuurInMnd
    {
        get
        {
            return this._duurInMnd;
        }
        set
        {
            if ((this._duurInMnd == value))
            {
                return;
            }
            if (((this._duurInMnd == null) 
                        || (_duurInMnd.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "DuurInMnd";
                Validator.ValidateProperty(value, validatorPropContext);
                this._duurInMnd = value;
                this.OnPropertyChanged("DuurInMnd");
            }
        }
    }
    
    /// <summary>
    /// Het bedrag wat ingelegd wordt. Behorende bij de keuze in betalingstermijn.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal InlegBedrag
    {
        get
        {
            return this._inlegBedrag;
        }
        set
        {
            if ((_inlegBedrag.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "InlegBedrag";
                Validator.ValidateProperty(value, validatorPropContext);
                this._inlegBedrag = value;
                this.OnPropertyChanged("InlegBedrag");
            }
            _shouldSerializeInlegBedrag = true;
        }
    }
    
    /// <summary>
    /// Geeft aan met welke frequentie de rente en eventuele aflossing zal worden betaald.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public BetalingsTermijnType BetalingsTermijn
    {
        get
        {
            return this._betalingsTermijn;
        }
        set
        {
            if ((_betalingsTermijn.Equals(value) != true))
            {
                this._betalingsTermijn = value;
                this.OnPropertyChanged("BetalingsTermijn");
            }
            _shouldSerializeBetalingsTermijn = true;
        }
    }
    
    /// <summary>
    /// Geeft het bedrag van de inleg dat uit het depot moet worden betaald. Bedrag per betalingstermijn.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<decimal> InlegUitDepot
    {
        get
        {
            if (this._inlegUitDepot.HasValue)
            {
                return this._inlegUitDepot.Value;
            }
            else
            {
                return default(System.Nullable<decimal>);
            }
        }
        set
        {
            if ((_inlegUitDepot.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "InlegUitDepot";
                Validator.ValidateProperty(value, validatorPropContext);
                this._inlegUitDepot = value;
                this.OnPropertyChanged("InlegUitDepot");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool InlegUitDepotSpecified
    {
        get
        {
            return this._inlegUitDepot.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._inlegUitDepot = null;
            }
        }
    }
    
    /// <summary>
    /// Geeft de naam van de entiteit waarin de NAW gegevens zijn opgenomen van degene die verantwoordelijk is voor de betalingen.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public REFPartijNAWDataType RefDebiteurNAWData
    {
        get
        {
            if ((this._refDebiteurNAWData == null))
            {
                this._refDebiteurNAWData = new REFPartijNAWDataType();
            }
            return this._refDebiteurNAWData;
        }
        set
        {
            if ((this._refDebiteurNAWData == value))
            {
                return;
            }
            if (((this._refDebiteurNAWData == null) 
                        || (_refDebiteurNAWData.Equals(value) != true)))
            {
                this._refDebiteurNAWData = value;
                this.OnPropertyChanged("RefDebiteurNAWData");
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(InlegAfsprakenEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether InlegUitDepot should be serialized
    /// </summary>
    public virtual bool ShouldSerializeInlegUitDepot()
    {
        return InlegUitDepot.HasValue;
    }
    
    /// <summary>
    /// Test whether InlegBedrag should be serialized
    /// </summary>
    public virtual bool ShouldSerializeInlegBedrag()
    {
        if (_shouldSerializeInlegBedrag)
        {
            return true;
        }
        return (_inlegBedrag != default(decimal));
    }
    
    /// <summary>
    /// Test whether SoortInleg should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSoortInleg()
    {
        if (_shouldSerializeSoortInleg)
        {
            return true;
        }
        return (_soortInleg != default(InlegType));
    }
    
    /// <summary>
    /// Test whether BetalingsTermijn should be serialized
    /// </summary>
    public virtual bool ShouldSerializeBetalingsTermijn()
    {
        if (_shouldSerializeBetalingsTermijn)
        {
            return true;
        }
        return (_betalingsTermijn != default(BetalingsTermijnType));
    }
    
    /// <summary>
    /// Test whether RefDebiteurNAWData should be serialized
    /// </summary>
    public virtual bool ShouldSerializeRefDebiteurNAWData()
    {
        return (_refDebiteurNAWData != null);
    }
    
    /// <summary>
    /// Test whether AanvangNaIngangsDtInMnd should be serialized
    /// </summary>
    public virtual bool ShouldSerializeAanvangNaIngangsDtInMnd()
    {
        return !string.IsNullOrEmpty(AanvangNaIngangsDtInMnd);
    }
    
    /// <summary>
    /// Test whether DuurInMnd should be serialized
    /// </summary>
    public virtual bool ShouldSerializeDuurInMnd()
    {
        return !string.IsNullOrEmpty(DuurInMnd);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current InlegAfsprakenEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an InlegAfsprakenEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output InlegAfsprakenEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out InlegAfsprakenEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(InlegAfsprakenEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out InlegAfsprakenEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static InlegAfsprakenEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((InlegAfsprakenEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static InlegAfsprakenEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((InlegAfsprakenEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current InlegAfsprakenEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an InlegAfsprakenEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output InlegAfsprakenEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out InlegAfsprakenEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(InlegAfsprakenEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out InlegAfsprakenEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static InlegAfsprakenEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
