// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.CENTRAAL
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="KadastraleAanduidingEntiteitType")]
public class KadastraleAanduidingEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private string _sectie;
    
    private string _nr;
    
    private string _appartementsVolgNr;
    
    private string _grootte;
    
    private string _kadastraleGemeente;
    
    private string _volgnummer;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// Aanduiding van kadastrale sectie van onderpand.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(2)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Sectie
    {
        get
        {
            return this._sectie;
        }
        set
        {
            if ((this._sectie == value))
            {
                return;
            }
            if (((this._sectie == null) 
                        || (_sectie.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "Sectie";
                Validator.ValidateProperty(value, validatorPropContext);
                this._sectie = value;
                this.OnPropertyChanged("Sectie");
            }
        }
    }
    
    /// <summary>
    /// Nummer waaronder onderpand geregistreerd is in het kadaster.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger")]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("\\d{1,15}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Nr
    {
        get
        {
            return this._nr;
        }
        set
        {
            if ((this._nr == value))
            {
                return;
            }
            if (((this._nr == null) 
                        || (_nr.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "Nr";
                Validator.ValidateProperty(value, validatorPropContext);
                this._nr = value;
                this.OnPropertyChanged("Nr");
            }
        }
    }
    
    /// <summary>
    /// Geeft het appartementsvolgnummer.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(10)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string AppartementsVolgNr
    {
        get
        {
            return this._appartementsVolgNr;
        }
        set
        {
            if ((this._appartementsVolgNr == value))
            {
                return;
            }
            if (((this._appartementsVolgNr == null) 
                        || (_appartementsVolgNr.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "AppartementsVolgNr";
                Validator.ValidateProperty(value, validatorPropContext);
                this._appartementsVolgNr = value;
                this.OnPropertyChanged("AppartementsVolgNr");
            }
        }
    }
    
    /// <summary>
    /// Oppervlakte van de grond waarop het onderpand staat en die behoort tot het onderpand (in vierkante meters).
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger", IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("\\d{1,8}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Grootte
    {
        get
        {
            return this._grootte;
        }
        set
        {
            if ((this._grootte == value))
            {
                return;
            }
            if (((this._grootte == null) 
                        || (_grootte.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "Grootte";
                Validator.ValidateProperty(value, validatorPropContext);
                this._grootte = value;
                this.OnPropertyChanged("Grootte");
            }
        }
    }
    
    /// <summary>
    /// Aanduiding van gemeente volgens het kadaster.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(30)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string KadastraleGemeente
    {
        get
        {
            return this._kadastraleGemeente;
        }
        set
        {
            if ((this._kadastraleGemeente == value))
            {
                return;
            }
            if (((this._kadastraleGemeente == null) 
                        || (_kadastraleGemeente.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "KadastraleGemeente";
                Validator.ValidateProperty(value, validatorPropContext);
                this._kadastraleGemeente = value;
                this.OnPropertyChanged("KadastraleGemeente");
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(KadastraleAanduidingEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether Sectie should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSectie()
    {
        return !string.IsNullOrEmpty(Sectie);
    }
    
    /// <summary>
    /// Test whether Nr should be serialized
    /// </summary>
    public virtual bool ShouldSerializeNr()
    {
        return !string.IsNullOrEmpty(Nr);
    }
    
    /// <summary>
    /// Test whether AppartementsVolgNr should be serialized
    /// </summary>
    public virtual bool ShouldSerializeAppartementsVolgNr()
    {
        return !string.IsNullOrEmpty(AppartementsVolgNr);
    }
    
    /// <summary>
    /// Test whether Grootte should be serialized
    /// </summary>
    public virtual bool ShouldSerializeGrootte()
    {
        return !string.IsNullOrEmpty(Grootte);
    }
    
    /// <summary>
    /// Test whether KadastraleGemeente should be serialized
    /// </summary>
    public virtual bool ShouldSerializeKadastraleGemeente()
    {
        return !string.IsNullOrEmpty(KadastraleGemeente);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current KadastraleAanduidingEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an KadastraleAanduidingEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output KadastraleAanduidingEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out KadastraleAanduidingEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(KadastraleAanduidingEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out KadastraleAanduidingEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static KadastraleAanduidingEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((KadastraleAanduidingEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static KadastraleAanduidingEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((KadastraleAanduidingEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current KadastraleAanduidingEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an KadastraleAanduidingEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output KadastraleAanduidingEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out KadastraleAanduidingEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(KadastraleAanduidingEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out KadastraleAanduidingEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static KadastraleAanduidingEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
