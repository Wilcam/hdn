// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.ARGENTA
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum MaatwerkOplCodeMijType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("AR006 Uit eigen vermogen inbrengen premiedepot, lopende premievrije polis of eers" +
        "te inleg")]
    AR006Uiteigenvermogeninbrengenpremiedepotlopendepremievrijepolisofeersteinleg,
    
    [System.Xml.Serialization.XmlEnumAttribute("AR002 Vrij beschikbaar vermogen")]
    AR002Vrijbeschikbaarvermogen,
    
    [System.Xml.Serialization.XmlEnumAttribute("AR003 Huidige woonlasten hoger dan nieuwe woonlasten")]
    AR003Huidigewoonlastenhogerdannieuwewoonlasten,
    
    [System.Xml.Serialization.XmlEnumAttribute("AR005 Inkomen uit verpand vermogen")]
    AR005Inkomenuitverpandvermogen,
    
    [System.Xml.Serialization.XmlEnumAttribute("AR004 Netto besteedbaar inkomen")]
    AR004Nettobesteedbaarinkomen,
    
    [System.Xml.Serialization.XmlEnumAttribute("AR008 Overig")]
    AR008Overig,
    
    [System.Xml.Serialization.XmlEnumAttribute("AR001 Toekomstperspectief/Inkomensgroei")]
    AR001ToekomstperspectiefInkomensgroei,
    
    [System.Xml.Serialization.XmlEnumAttribute("AR007 Persoonlijk Budgetadvies NIBUD")]
    AR007PersoonlijkBudgetadviesNIBUD,
}
}
#pragma warning restore
