// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.NN
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="PartijNAWDataEntiteitType")]
public class PartijNAWDataEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private bool _shouldSerializeSoortPartij;
    
    private SoortPartijType _soortPartij;
    
    private string _voorNaam;
    
    private string _gebAchterNaam;
    
    private string _gebTussenVoegsels;
    
    private string _correspondentieNaam;
    
    private string _correspondentieTussenvoegsels;
    
    private System.Nullable<LandType> _nationaliteit;
    
    private string _geboortePlaats;
    
    private System.Nullable<LandType> _geboorteLand;
    
    private string _voorLetters;
    
    private System.Nullable<GeslachtType> _geslacht;
    
    private DatumType _geboorteDt;
    
    private string _straatNaam;
    
    private string _huisNr;
    
    private string _huisNrToevoeging;
    
    private string _postcode;
    
    private string _plaatsNaam;
    
    private System.Nullable<LandType> _land;
    
    private string _rekeningNr;
    
    private string _volgnummer;
    
    private string _id;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// Geeft aan of partij een natuurlijk persoon is of een rechtspersoon
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public SoortPartijType SoortPartij
    {
        get
        {
            return this._soortPartij;
        }
        set
        {
            if ((_soortPartij.Equals(value) != true))
            {
                this._soortPartij = value;
                this.OnPropertyChanged("SoortPartij");
            }
            _shouldSerializeSoortPartij = true;
        }
    }
    
    /// <summary>
    /// Dit betreft de volledige eerste voornaam van de natuurlijke persoon.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(35)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string VoorNaam
    {
        get
        {
            return this._voorNaam;
        }
        set
        {
            if ((this._voorNaam == value))
            {
                return;
            }
            if (((this._voorNaam == null) 
                        || (_voorNaam.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "VoorNaam";
                Validator.ValidateProperty(value, validatorPropContext);
                this._voorNaam = value;
                this.OnPropertyChanged("VoorNaam");
            }
        }
    }
    
    /// <summary>
    /// De geboortenaam van de partij.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(100)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string GebAchterNaam
    {
        get
        {
            return this._gebAchterNaam;
        }
        set
        {
            if ((this._gebAchterNaam == value))
            {
                return;
            }
            if (((this._gebAchterNaam == null) 
                        || (_gebAchterNaam.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "GebAchterNaam";
                Validator.ValidateProperty(value, validatorPropContext);
                this._gebAchterNaam = value;
                this.OnPropertyChanged("GebAchterNaam");
            }
        }
    }
    
    /// <summary>
    /// De tussenvoegsels behorende bij de geboortenaam.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(10)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string GebTussenVoegsels
    {
        get
        {
            return this._gebTussenVoegsels;
        }
        set
        {
            if ((this._gebTussenVoegsels == value))
            {
                return;
            }
            if (((this._gebTussenVoegsels == null) 
                        || (_gebTussenVoegsels.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "GebTussenVoegsels";
                Validator.ValidateProperty(value, validatorPropContext);
                this._gebTussenVoegsels = value;
                this.OnPropertyChanged("GebTussenVoegsels");
            }
        }
    }
    
    /// <summary>
    /// Achternaam in het dagelijks gebruik, indien deze afwijkt van de achternaam bij geboorte. Correspondentienaam is alleen toegestaan als deze afwijkt van de achternaam bij geboorte.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(100)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CorrespondentieNaam
    {
        get
        {
            return this._correspondentieNaam;
        }
        set
        {
            if ((this._correspondentieNaam == value))
            {
                return;
            }
            if (((this._correspondentieNaam == null) 
                        || (_correspondentieNaam.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "CorrespondentieNaam";
                Validator.ValidateProperty(value, validatorPropContext);
                this._correspondentieNaam = value;
                this.OnPropertyChanged("CorrespondentieNaam");
            }
        }
    }
    
    /// <summary>
    /// Tussenvoegsels behorend bij de CorrespondentieAchternaam
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(10)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string CorrespondentieTussenvoegsels
    {
        get
        {
            return this._correspondentieTussenvoegsels;
        }
        set
        {
            if ((this._correspondentieTussenvoegsels == value))
            {
                return;
            }
            if (((this._correspondentieTussenvoegsels == null) 
                        || (_correspondentieTussenvoegsels.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "CorrespondentieTussenvoegsels";
                Validator.ValidateProperty(value, validatorPropContext);
                this._correspondentieTussenvoegsels = value;
                this.OnPropertyChanged("CorrespondentieTussenvoegsels");
            }
        }
    }
    
    /// <summary>
    /// Nationaliteit code (ISO Landenlijst 3166, versie 1 met aanvullend versie 3 voor vervallen gebieden). Noot: versie 1 bevat tweeletterige codes, versie 3 bevat vierletterige (!) codes.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<LandType> Nationaliteit
    {
        get
        {
            if (this._nationaliteit.HasValue)
            {
                return this._nationaliteit.Value;
            }
            else
            {
                return default(System.Nullable<LandType>);
            }
        }
        set
        {
            if ((_nationaliteit.Equals(value) != true))
            {
                this._nationaliteit = value;
                this.OnPropertyChanged("Nationaliteit");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool NationaliteitSpecified
    {
        get
        {
            return this._nationaliteit.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._nationaliteit = null;
            }
        }
    }
    
    /// <summary>
    /// De geboorteplaats van de natuurlijke persoon
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(35)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string GeboortePlaats
    {
        get
        {
            return this._geboortePlaats;
        }
        set
        {
            if ((this._geboortePlaats == value))
            {
                return;
            }
            if (((this._geboortePlaats == null) 
                        || (_geboortePlaats.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "GeboortePlaats";
                Validator.ValidateProperty(value, validatorPropContext);
                this._geboortePlaats = value;
                this.OnPropertyChanged("GeboortePlaats");
            }
        }
    }
    
    /// <summary>
    /// Het geboorteland van de natuurlijke persoon. (ISO Landenlijst 3166, versie 1 met aanvullend versie 3 voor vervallen gebieden). Noot: versie 1 bevat tweeletterige codes, versie 3 bevat vierletterige (!) codes.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<LandType> GeboorteLand
    {
        get
        {
            if (this._geboorteLand.HasValue)
            {
                return this._geboorteLand.Value;
            }
            else
            {
                return default(System.Nullable<LandType>);
            }
        }
        set
        {
            if ((_geboorteLand.Equals(value) != true))
            {
                this._geboorteLand = value;
                this.OnPropertyChanged("GeboorteLand");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool GeboorteLandSpecified
    {
        get
        {
            return this._geboorteLand.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._geboorteLand = null;
            }
        }
    }
    
    /// <summary>
    /// Voorletter(s), in hoofdletters. Afwijkende voorletters als Th dienen letterlijk overgenomen te worden. Bijvoorbeeld: Theodoor Jan Carolus kan genoteerd worden als ThJC
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(15)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string VoorLetters
    {
        get
        {
            return this._voorLetters;
        }
        set
        {
            if ((this._voorLetters == value))
            {
                return;
            }
            if (((this._voorLetters == null) 
                        || (_voorLetters.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "VoorLetters";
                Validator.ValidateProperty(value, validatorPropContext);
                this._voorLetters = value;
                this.OnPropertyChanged("VoorLetters");
            }
        }
    }
    
    /// <summary>
    /// Het geslacht van de natuurlijke persoon
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<GeslachtType> Geslacht
    {
        get
        {
            if (this._geslacht.HasValue)
            {
                return this._geslacht.Value;
            }
            else
            {
                return default(System.Nullable<GeslachtType>);
            }
        }
        set
        {
            if ((_geslacht.Equals(value) != true))
            {
                this._geslacht = value;
                this.OnPropertyChanged("Geslacht");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool GeslachtSpecified
    {
        get
        {
            return this._geslacht.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._geslacht = null;
            }
        }
    }
    
    /// <summary>
    /// De geboortedatum van de betreffende persoon.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public DatumType GeboorteDt
    {
        get
        {
            if ((this._geboorteDt == null))
            {
                this._geboorteDt = new DatumType();
            }
            return this._geboorteDt;
        }
        set
        {
            if ((this._geboorteDt == value))
            {
                return;
            }
            if (((this._geboorteDt == null) 
                        || (_geboorteDt.Equals(value) != true)))
            {
                this._geboorteDt = value;
                this.OnPropertyChanged("GeboorteDt");
            }
        }
    }
    
    /// <summary>
    /// Straatnaam, indien sprake is van een correspondentie adres kan ook de aanduiding 'postbus' gebruikt worden.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(100)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string StraatNaam
    {
        get
        {
            return this._straatNaam;
        }
        set
        {
            if ((this._straatNaam == value))
            {
                return;
            }
            if (((this._straatNaam == null) 
                        || (_straatNaam.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "StraatNaam";
                Validator.ValidateProperty(value, validatorPropContext);
                this._straatNaam = value;
                this.OnPropertyChanged("StraatNaam");
            }
        }
    }
    
    /// <summary>
    /// Huisnummer, indien sprake is van een correspondentie adres kan dit ook een postbusnummer zijn.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger")]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("\\d{1,5}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string HuisNr
    {
        get
        {
            return this._huisNr;
        }
        set
        {
            if ((this._huisNr == value))
            {
                return;
            }
            if (((this._huisNr == null) 
                        || (_huisNr.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "HuisNr";
                Validator.ValidateProperty(value, validatorPropContext);
                this._huisNr = value;
                this.OnPropertyChanged("HuisNr");
            }
        }
    }
    
    /// <summary>
    /// Huisnummertoevoeging, bijvoorbeeld I, Huis, zwart etc..
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(10)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string HuisNrToevoeging
    {
        get
        {
            return this._huisNrToevoeging;
        }
        set
        {
            if ((this._huisNrToevoeging == value))
            {
                return;
            }
            if (((this._huisNrToevoeging == null) 
                        || (_huisNrToevoeging.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "HuisNrToevoeging";
                Validator.ValidateProperty(value, validatorPropContext);
                this._huisNrToevoeging = value;
                this.OnPropertyChanged("HuisNrToevoeging");
            }
        }
    }
    
    /// <summary>
    /// Postcode, behorend bij ingevulde straatnaam, huisnummer of bij ingevuld postbusnummer. Voor Nederland 6 posities lang, zonder spatie ertussen.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("[A-Z0-9][A-Z0-9\\- ]{0,10}[A-Z0-9]")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Postcode
    {
        get
        {
            return this._postcode;
        }
        set
        {
            if ((this._postcode == value))
            {
                return;
            }
            if (((this._postcode == null) 
                        || (_postcode.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "Postcode";
                Validator.ValidateProperty(value, validatorPropContext);
                this._postcode = value;
                this.OnPropertyChanged("Postcode");
            }
        }
    }
    
    /// <summary>
    /// Geeft de plaatsnaam.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(35)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string PlaatsNaam
    {
        get
        {
            return this._plaatsNaam;
        }
        set
        {
            if ((this._plaatsNaam == value))
            {
                return;
            }
            if (((this._plaatsNaam == null) 
                        || (_plaatsNaam.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "PlaatsNaam";
                Validator.ValidateProperty(value, validatorPropContext);
                this._plaatsNaam = value;
                this.OnPropertyChanged("PlaatsNaam");
            }
        }
    }
    
    /// <summary>
    /// ISO Landenlijst 3166, versie 1 met aanvullend versie 3 voor vervallen gebieden. Noot: versie 1 bevat tweeletterige codes, versie 3 bevat vierletterige (!) codes.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<LandType> Land
    {
        get
        {
            if (this._land.HasValue)
            {
                return this._land.Value;
            }
            else
            {
                return default(System.Nullable<LandType>);
            }
        }
        set
        {
            if ((_land.Equals(value) != true))
            {
                this._land = value;
                this.OnPropertyChanged("Land");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool LandSpecified
    {
        get
        {
            return this._land.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._land = null;
            }
        }
    }
    
    /// <summary>
    /// Het IBAN rekeningnummer van de partij. Hoofdletters zijn verplicht, invoeren zonder spaties.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("[A-Z]{2}[0-9]{2}[A-Z0-9]{1,30}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string RekeningNr
    {
        get
        {
            return this._rekeningNr;
        }
        set
        {
            if ((this._rekeningNr == value))
            {
                return;
            }
            if (((this._rekeningNr == null) 
                        || (_rekeningNr.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "RekeningNr";
                Validator.ValidateProperty(value, validatorPropContext);
                this._rekeningNr = value;
                this.OnPropertyChanged("RekeningNr");
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="ID")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ID
    {
        get
        {
            return this._id;
        }
        set
        {
            if ((this._id == value))
            {
                return;
            }
            if (((this._id == null) 
                        || (_id.Equals(value) != true)))
            {
                this._id = value;
                this.OnPropertyChanged("ID");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(PartijNAWDataEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether Nationaliteit should be serialized
    /// </summary>
    public virtual bool ShouldSerializeNationaliteit()
    {
        return Nationaliteit.HasValue;
    }
    
    /// <summary>
    /// Test whether GeboorteLand should be serialized
    /// </summary>
    public virtual bool ShouldSerializeGeboorteLand()
    {
        return GeboorteLand.HasValue;
    }
    
    /// <summary>
    /// Test whether Geslacht should be serialized
    /// </summary>
    public virtual bool ShouldSerializeGeslacht()
    {
        return Geslacht.HasValue;
    }
    
    /// <summary>
    /// Test whether Land should be serialized
    /// </summary>
    public virtual bool ShouldSerializeLand()
    {
        return Land.HasValue;
    }
    
    /// <summary>
    /// Test whether SoortPartij should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSoortPartij()
    {
        if (_shouldSerializeSoortPartij)
        {
            return true;
        }
        return (_soortPartij != default(SoortPartijType));
    }
    
    /// <summary>
    /// Test whether GeboorteDt should be serialized
    /// </summary>
    public virtual bool ShouldSerializeGeboorteDt()
    {
        return (_geboorteDt != null);
    }
    
    /// <summary>
    /// Test whether VoorNaam should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVoorNaam()
    {
        return !string.IsNullOrEmpty(VoorNaam);
    }
    
    /// <summary>
    /// Test whether GebAchterNaam should be serialized
    /// </summary>
    public virtual bool ShouldSerializeGebAchterNaam()
    {
        return !string.IsNullOrEmpty(GebAchterNaam);
    }
    
    /// <summary>
    /// Test whether GebTussenVoegsels should be serialized
    /// </summary>
    public virtual bool ShouldSerializeGebTussenVoegsels()
    {
        return !string.IsNullOrEmpty(GebTussenVoegsels);
    }
    
    /// <summary>
    /// Test whether CorrespondentieNaam should be serialized
    /// </summary>
    public virtual bool ShouldSerializeCorrespondentieNaam()
    {
        return !string.IsNullOrEmpty(CorrespondentieNaam);
    }
    
    /// <summary>
    /// Test whether CorrespondentieTussenvoegsels should be serialized
    /// </summary>
    public virtual bool ShouldSerializeCorrespondentieTussenvoegsels()
    {
        return !string.IsNullOrEmpty(CorrespondentieTussenvoegsels);
    }
    
    /// <summary>
    /// Test whether GeboortePlaats should be serialized
    /// </summary>
    public virtual bool ShouldSerializeGeboortePlaats()
    {
        return !string.IsNullOrEmpty(GeboortePlaats);
    }
    
    /// <summary>
    /// Test whether VoorLetters should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVoorLetters()
    {
        return !string.IsNullOrEmpty(VoorLetters);
    }
    
    /// <summary>
    /// Test whether StraatNaam should be serialized
    /// </summary>
    public virtual bool ShouldSerializeStraatNaam()
    {
        return !string.IsNullOrEmpty(StraatNaam);
    }
    
    /// <summary>
    /// Test whether HuisNr should be serialized
    /// </summary>
    public virtual bool ShouldSerializeHuisNr()
    {
        return !string.IsNullOrEmpty(HuisNr);
    }
    
    /// <summary>
    /// Test whether HuisNrToevoeging should be serialized
    /// </summary>
    public virtual bool ShouldSerializeHuisNrToevoeging()
    {
        return !string.IsNullOrEmpty(HuisNrToevoeging);
    }
    
    /// <summary>
    /// Test whether Postcode should be serialized
    /// </summary>
    public virtual bool ShouldSerializePostcode()
    {
        return !string.IsNullOrEmpty(Postcode);
    }
    
    /// <summary>
    /// Test whether PlaatsNaam should be serialized
    /// </summary>
    public virtual bool ShouldSerializePlaatsNaam()
    {
        return !string.IsNullOrEmpty(PlaatsNaam);
    }
    
    /// <summary>
    /// Test whether RekeningNr should be serialized
    /// </summary>
    public virtual bool ShouldSerializeRekeningNr()
    {
        return !string.IsNullOrEmpty(RekeningNr);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    /// <summary>
    /// Test whether ID should be serialized
    /// </summary>
    public virtual bool ShouldSerializeID()
    {
        return !string.IsNullOrEmpty(ID);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current PartijNAWDataEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an PartijNAWDataEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output PartijNAWDataEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out PartijNAWDataEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(PartijNAWDataEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out PartijNAWDataEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static PartijNAWDataEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((PartijNAWDataEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static PartijNAWDataEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((PartijNAWDataEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current PartijNAWDataEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an PartijNAWDataEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output PartijNAWDataEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out PartijNAWDataEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(PartijNAWDataEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out PartijNAWDataEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static PartijNAWDataEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
