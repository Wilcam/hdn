// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.ASR
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum CodeDekkingMijType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("AU002 ASR Risicoverzekering Gelijkblijvend")]
    AU002ASRRisicoverzekeringGelijkblijvend,
    
    [System.Xml.Serialization.XmlEnumAttribute("AU003 ASR Risicoverzekering Annuitair dalend")]
    AU003ASRRisicoverzekeringAnnuitairdalend,
    
    [System.Xml.Serialization.XmlEnumAttribute("AU004 ASR Risicoverzekering Lineair Dalend")]
    AU004ASRRisicoverzekeringLineairDalend,
}
}
#pragma warning restore
