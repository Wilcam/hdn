// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.ROBUUST
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum CodeDeelMijType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("RF003 Robuust Lineair")]
    RF003RobuustLineair,
    
    [System.Xml.Serialization.XmlEnumAttribute("RF001 Robuust Aflossingsvrij")]
    RF001RobuustAflossingsvrij,
    
    [System.Xml.Serialization.XmlEnumAttribute("RF002 Robuust Annuïteit")]
    RF002RobuustAnnuïteit,
}
}
#pragma warning restore
