// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.DYNAMIC
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum CodeDeelMijType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("DY002 Dynamic Annuitair")]
    DY002DynamicAnnuitair,
    
    [System.Xml.Serialization.XmlEnumAttribute("DY003 Dynamic Lineair")]
    DY003DynamicLineair,
    
    [System.Xml.Serialization.XmlEnumAttribute("DY001 Dynamic Aflossingsvrij")]
    DY001DynamicAflossingsvrij,
}
}
#pragma warning restore
