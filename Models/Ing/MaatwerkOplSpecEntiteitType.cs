// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.ING
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="MaatwerkOplSpecEntiteitType")]
public class MaatwerkOplSpecEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private bool _shouldSerializeExplainReden;
    
    private ExplainRedenType _explainReden;
    
    private System.Nullable<MaatwerkOplCodeMijType> _maatwerkOplCodeMij;
    
    private string _maatwerkOplToelichting;
    
    private string _volgnummer;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// Geeft de generieke explainreden weer. Als er sprake is van maatschappijspecifiek maatwerk, dient dit veld gevuld te worden met de code 01 Maatschappijspecifiek en dient dit verder gespecificeerd te worden in het veld MaatwerkOplCodeMij.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public ExplainRedenType ExplainReden
    {
        get
        {
            return this._explainReden;
        }
        set
        {
            if ((_explainReden.Equals(value) != true))
            {
                this._explainReden = value;
                this.OnPropertyChanged("ExplainReden");
            }
            _shouldSerializeExplainReden = true;
        }
    }
    
    /// <summary>
    /// Geeft de maatschappij specifieke code voor de maatwerkoplossing weer.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<MaatwerkOplCodeMijType> MaatwerkOplCodeMij
    {
        get
        {
            if (this._maatwerkOplCodeMij.HasValue)
            {
                return this._maatwerkOplCodeMij.Value;
            }
            else
            {
                return default(System.Nullable<MaatwerkOplCodeMijType>);
            }
        }
        set
        {
            if ((_maatwerkOplCodeMij.Equals(value) != true))
            {
                this._maatwerkOplCodeMij = value;
                this.OnPropertyChanged("MaatwerkOplCodeMij");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool MaatwerkOplCodeMijSpecified
    {
        get
        {
            return this._maatwerkOplCodeMij.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._maatwerkOplCodeMij = null;
            }
        }
    }
    
    /// <summary>
    /// Algemene toelichting met betrekking tot de maatwerkoplossing.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(255)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string MaatwerkOplToelichting
    {
        get
        {
            return this._maatwerkOplToelichting;
        }
        set
        {
            if ((this._maatwerkOplToelichting == value))
            {
                return;
            }
            if (((this._maatwerkOplToelichting == null) 
                        || (_maatwerkOplToelichting.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "MaatwerkOplToelichting";
                Validator.ValidateProperty(value, validatorPropContext);
                this._maatwerkOplToelichting = value;
                this.OnPropertyChanged("MaatwerkOplToelichting");
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(MaatwerkOplSpecEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether MaatwerkOplCodeMij should be serialized
    /// </summary>
    public virtual bool ShouldSerializeMaatwerkOplCodeMij()
    {
        return MaatwerkOplCodeMij.HasValue;
    }
    
    /// <summary>
    /// Test whether ExplainReden should be serialized
    /// </summary>
    public virtual bool ShouldSerializeExplainReden()
    {
        if (_shouldSerializeExplainReden)
        {
            return true;
        }
        return (_explainReden != default(ExplainRedenType));
    }
    
    /// <summary>
    /// Test whether MaatwerkOplToelichting should be serialized
    /// </summary>
    public virtual bool ShouldSerializeMaatwerkOplToelichting()
    {
        return !string.IsNullOrEmpty(MaatwerkOplToelichting);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current MaatwerkOplSpecEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an MaatwerkOplSpecEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output MaatwerkOplSpecEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out MaatwerkOplSpecEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(MaatwerkOplSpecEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out MaatwerkOplSpecEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static MaatwerkOplSpecEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((MaatwerkOplSpecEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static MaatwerkOplSpecEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((MaatwerkOplSpecEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current MaatwerkOplSpecEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an MaatwerkOplSpecEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output MaatwerkOplSpecEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out MaatwerkOplSpecEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(MaatwerkOplSpecEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out MaatwerkOplSpecEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static MaatwerkOplSpecEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
