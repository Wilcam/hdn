// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.TELLIUS
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="VermogenEntiteitType")]
public class VermogenEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private bool _shouldSerializeBrutoJaarInkBoxDrie;
    
    private System.Nullable<SoortVermogenType> _soortVermogen;
    
    private decimal _brutoJaarInkBoxDrie;
    
    private DatumType _ingangsDtBrutoJaarInkBoxDrie;
    
    private DatumType _eindDtBrutoJaarInkBoxDrie;
    
    private System.Nullable<BooleanType> _verpandVermogenJN;
    
    private string _volgnummer;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// De aard van het vermogen waaruit inkomsten worden verkregen.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<SoortVermogenType> SoortVermogen
    {
        get
        {
            if (this._soortVermogen.HasValue)
            {
                return this._soortVermogen.Value;
            }
            else
            {
                return default(System.Nullable<SoortVermogenType>);
            }
        }
        set
        {
            if ((_soortVermogen.Equals(value) != true))
            {
                this._soortVermogen = value;
                this.OnPropertyChanged("SoortVermogen");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool SoortVermogenSpecified
    {
        get
        {
            return this._soortVermogen.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._soortVermogen = null;
            }
        }
    }
    
    /// <summary>
    /// Geeft het bedrag aan bruto jaarinkomen uit vermogen mee. Inkomsten die de consument vrij kan besteden zonder dat de vermogensbron wordt aangetast. Dit inkomen kan dienen ter bepaling van de maximale leencapaciteit (conform gedragscode CHF).
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal BrutoJaarInkBoxDrie
    {
        get
        {
            return this._brutoJaarInkBoxDrie;
        }
        set
        {
            if ((_brutoJaarInkBoxDrie.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "BrutoJaarInkBoxDrie";
                Validator.ValidateProperty(value, validatorPropContext);
                this._brutoJaarInkBoxDrie = value;
                this.OnPropertyChanged("BrutoJaarInkBoxDrie");
            }
            _shouldSerializeBrutoJaarInkBoxDrie = true;
        }
    }
    
    /// <summary>
    /// Geeft de ingangsdatum van de inkomsten in Box drie aan.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public DatumType IngangsDtBrutoJaarInkBoxDrie
    {
        get
        {
            if ((this._ingangsDtBrutoJaarInkBoxDrie == null))
            {
                this._ingangsDtBrutoJaarInkBoxDrie = new DatumType();
            }
            return this._ingangsDtBrutoJaarInkBoxDrie;
        }
        set
        {
            if ((this._ingangsDtBrutoJaarInkBoxDrie == value))
            {
                return;
            }
            if (((this._ingangsDtBrutoJaarInkBoxDrie == null) 
                        || (_ingangsDtBrutoJaarInkBoxDrie.Equals(value) != true)))
            {
                this._ingangsDtBrutoJaarInkBoxDrie = value;
                this.OnPropertyChanged("IngangsDtBrutoJaarInkBoxDrie");
            }
        }
    }
    
    /// <summary>
    /// Geeft de eindatum van ontvangst van inkomsten uit box drie.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public DatumType EindDtBrutoJaarInkBoxDrie
    {
        get
        {
            if ((this._eindDtBrutoJaarInkBoxDrie == null))
            {
                this._eindDtBrutoJaarInkBoxDrie = new DatumType();
            }
            return this._eindDtBrutoJaarInkBoxDrie;
        }
        set
        {
            if ((this._eindDtBrutoJaarInkBoxDrie == value))
            {
                return;
            }
            if (((this._eindDtBrutoJaarInkBoxDrie == null) 
                        || (_eindDtBrutoJaarInkBoxDrie.Equals(value) != true)))
            {
                this._eindDtBrutoJaarInkBoxDrie = value;
                this.OnPropertyChanged("EindDtBrutoJaarInkBoxDrie");
            }
        }
    }
    
    /// <summary>
    /// Geeft aan of de opgegeven inkomsten uit vermogen verpand zijn.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<BooleanType> VerpandVermogenJN
    {
        get
        {
            if (this._verpandVermogenJN.HasValue)
            {
                return this._verpandVermogenJN.Value;
            }
            else
            {
                return default(System.Nullable<BooleanType>);
            }
        }
        set
        {
            if ((_verpandVermogenJN.Equals(value) != true))
            {
                this._verpandVermogenJN = value;
                this.OnPropertyChanged("VerpandVermogenJN");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool VerpandVermogenJNSpecified
    {
        get
        {
            return this._verpandVermogenJN.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._verpandVermogenJN = null;
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(VermogenEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether SoortVermogen should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSoortVermogen()
    {
        return SoortVermogen.HasValue;
    }
    
    /// <summary>
    /// Test whether VerpandVermogenJN should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVerpandVermogenJN()
    {
        return VerpandVermogenJN.HasValue;
    }
    
    /// <summary>
    /// Test whether BrutoJaarInkBoxDrie should be serialized
    /// </summary>
    public virtual bool ShouldSerializeBrutoJaarInkBoxDrie()
    {
        if (_shouldSerializeBrutoJaarInkBoxDrie)
        {
            return true;
        }
        return (_brutoJaarInkBoxDrie != default(decimal));
    }
    
    /// <summary>
    /// Test whether IngangsDtBrutoJaarInkBoxDrie should be serialized
    /// </summary>
    public virtual bool ShouldSerializeIngangsDtBrutoJaarInkBoxDrie()
    {
        return (_ingangsDtBrutoJaarInkBoxDrie != null);
    }
    
    /// <summary>
    /// Test whether EindDtBrutoJaarInkBoxDrie should be serialized
    /// </summary>
    public virtual bool ShouldSerializeEindDtBrutoJaarInkBoxDrie()
    {
        return (_eindDtBrutoJaarInkBoxDrie != null);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current VermogenEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an VermogenEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output VermogenEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out VermogenEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(VermogenEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out VermogenEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static VermogenEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((VermogenEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static VermogenEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((VermogenEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current VermogenEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an VermogenEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output VermogenEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out VermogenEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(VermogenEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out VermogenEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static VermogenEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
