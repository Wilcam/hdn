// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.DELTA
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum KlantSegmentType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("01 Private Banker")]
    Item01PrivateBanker,
    
    [System.Xml.Serialization.XmlEnumAttribute("02 Basis")]
    Item02Basis,
    
    [System.Xml.Serialization.XmlEnumAttribute("03 Top Kern")]
    Item03TopKern,
    
    [System.Xml.Serialization.XmlEnumAttribute("04 MKB")]
    Item04MKB,
    
    [System.Xml.Serialization.XmlEnumAttribute("05 Eigen personeel")]
    Item05Eigenpersoneel,
    
    [System.Xml.Serialization.XmlEnumAttribute("06 Personeel tussenpersoon")]
    Item06Personeeltussenpersoon,
    
    [System.Xml.Serialization.XmlEnumAttribute("07 Personeel derden")]
    Item07Personeelderden,
}
}
#pragma warning restore
