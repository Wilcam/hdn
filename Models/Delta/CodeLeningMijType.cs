// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.DELTA
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum CodeLeningMijType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("DL016 Varianova Hypotheek")]
    DL016VarianovaHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL018 Solide Koers Budget Hypotheek")]
    DL018SolideKoersBudgetHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL022 Delta Lloyd Plus Hypotheek")]
    DL022DeltaLloydPlusHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL017 Delta Lloyd Budget Hypotheek")]
    DL017DeltaLloydBudgetHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL011 Solide Koers Hypotheek")]
    DL011SolideKoersHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL015 DrieSterrenHypotheek")]
    DL015DrieSterrenHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL001 Delta Lloyd Hypotheek")]
    DL001DeltaLloydHypotheek,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL020 Hypotheekshop Netto Plus")]
    DL020HypotheekshopNettoPlus,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL021 DrieSterrenHypotheek Personeel")]
    DL021DrieSterrenHypotheekPersoneel,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL005 Delta Lloyd Hypotheek Personeel")]
    DL005DeltaLloydHypotheekPersoneel,
    
    [System.Xml.Serialization.XmlEnumAttribute("DL019 Delta Lloyd Nieuwbouw Hypotheek")]
    DL019DeltaLloydNieuwbouwHypotheek,
}
}
#pragma warning restore
