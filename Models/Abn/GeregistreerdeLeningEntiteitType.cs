// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.ABN
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="GeregistreerdeLeningEntiteitType")]
public class GeregistreerdeLeningEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private bool _shouldSerializeMaatschappij;
    
    private bool _shouldSerializeLeningLopendJN;
    
    private bool _shouldSerializeSoortGeregistreerdeLening;
    
    private GeregistreerdeLeningType _soortGeregistreerdeLening;
    
    private string _soortGeregistreerdeLeningOmschr;
    
    private BooleanType _leningLopendJN;
    
    private string _contractNr;
    
    private MaatschappijType _maatschappij;
    
    private string _maatschappijOmschr;
    
    private System.Nullable<decimal> _leningPerJaar;
    
    private System.Nullable<decimal> _actueelSaldo;
    
    private System.Nullable<decimal> _oorspronkelijkeLening;
    
    private System.Nullable<decimal> _limietDoorlopendKrediet;
    
    private DatumType _startDt;
    
    private DatumType _eindDt;
    
    private System.Nullable<BooleanType> _aflossingVoorPasserenJN;
    
    private System.Nullable<decimal> _aflosBedrag;
    
    private System.Nullable<BooleanType> _fiscaalAftrekbaarJN;
    
    private System.Nullable<decimal> _extraAfgelostBedrag;
    
    private string _volgnummer;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// Type van de geregistreerde lening.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public GeregistreerdeLeningType SoortGeregistreerdeLening
    {
        get
        {
            return this._soortGeregistreerdeLening;
        }
        set
        {
            if ((_soortGeregistreerdeLening.Equals(value) != true))
            {
                this._soortGeregistreerdeLening = value;
                this.OnPropertyChanged("SoortGeregistreerdeLening");
            }
            _shouldSerializeSoortGeregistreerdeLening = true;
        }
    }
    
    /// <summary>
    /// Vrije omschrijving van het soort geregistreerde lening niet voorkomend in de keuzelijst "SoortGeregistreerdeLening". Alleen vullen indien keuze niet voorkomt in keuzelijst.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(100)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string SoortGeregistreerdeLeningOmschr
    {
        get
        {
            return this._soortGeregistreerdeLeningOmschr;
        }
        set
        {
            if ((this._soortGeregistreerdeLeningOmschr == value))
            {
                return;
            }
            if (((this._soortGeregistreerdeLeningOmschr == null) 
                        || (_soortGeregistreerdeLeningOmschr.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "SoortGeregistreerdeLeningOmschr";
                Validator.ValidateProperty(value, validatorPropContext);
                this._soortGeregistreerdeLeningOmschr = value;
                this.OnPropertyChanged("SoortGeregistreerdeLeningOmschr");
            }
        }
    }
    
    /// <summary>
    /// Geeft aan of het hier een nieuw af te sluiten consumptieve lening betreft die derhalve nog niet bekend is bij BKR maar wel meegenomen dient te worden in de risicobepaling.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public BooleanType LeningLopendJN
    {
        get
        {
            return this._leningLopendJN;
        }
        set
        {
            if ((_leningLopendJN.Equals(value) != true))
            {
                this._leningLopendJN = value;
                this.OnPropertyChanged("LeningLopendJN");
            }
            _shouldSerializeLeningLopendJN = true;
        }
    }
    
    /// <summary>
    /// Contract, rekening of overeenkomstnummer van het bestaande / lopende contract.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(20)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string ContractNr
    {
        get
        {
            return this._contractNr;
        }
        set
        {
            if ((this._contractNr == value))
            {
                return;
            }
            if (((this._contractNr == null) 
                        || (_contractNr.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "ContractNr";
                Validator.ValidateProperty(value, validatorPropContext);
                this._contractNr = value;
                this.OnPropertyChanged("ContractNr");
            }
        }
    }
    
    /// <summary>
    /// Unieke HDN code van financiële instelling.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public MaatschappijType Maatschappij
    {
        get
        {
            return this._maatschappij;
        }
        set
        {
            if ((_maatschappij.Equals(value) != true))
            {
                this._maatschappij = value;
                this.OnPropertyChanged("Maatschappij");
            }
            _shouldSerializeMaatschappij = true;
        }
    }
    
    /// <summary>
    /// Vrije omschrijving van de maatschappij niet voorkomend in de keuzelijst "MaatschappijType". Alleen vullen indien keuze niet voorkomt in keuzelijst.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(100)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string MaatschappijOmschr
    {
        get
        {
            return this._maatschappijOmschr;
        }
        set
        {
            if ((this._maatschappijOmschr == value))
            {
                return;
            }
            if (((this._maatschappijOmschr == null) 
                        || (_maatschappijOmschr.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "MaatschappijOmschr";
                Validator.ValidateProperty(value, validatorPropContext);
                this._maatschappijOmschr = value;
                this.OnPropertyChanged("MaatschappijOmschr");
            }
        }
    }
    
    /// <summary>
    /// Geeft het bruto bedrag dat per jaar aan de verplichting moet worden voldaan (aan rente en aflossing).
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<decimal> LeningPerJaar
    {
        get
        {
            if (this._leningPerJaar.HasValue)
            {
                return this._leningPerJaar.Value;
            }
            else
            {
                return default(System.Nullable<decimal>);
            }
        }
        set
        {
            if ((_leningPerJaar.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "LeningPerJaar";
                Validator.ValidateProperty(value, validatorPropContext);
                this._leningPerJaar = value;
                this.OnPropertyChanged("LeningPerJaar");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool LeningPerJaarSpecified
    {
        get
        {
            return this._leningPerJaar.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._leningPerJaar = null;
            }
        }
    }
    
    /// <summary>
    /// Geeft de hoogte van het actuele kredietbedrag (huidig schuldrestant) aan.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<decimal> ActueelSaldo
    {
        get
        {
            if (this._actueelSaldo.HasValue)
            {
                return this._actueelSaldo.Value;
            }
            else
            {
                return default(System.Nullable<decimal>);
            }
        }
        set
        {
            if ((_actueelSaldo.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "ActueelSaldo";
                Validator.ValidateProperty(value, validatorPropContext);
                this._actueelSaldo = value;
                this.OnPropertyChanged("ActueelSaldo");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool ActueelSaldoSpecified
    {
        get
        {
            return this._actueelSaldo.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._actueelSaldo = null;
            }
        }
    }
    
    /// <summary>
    /// Het oorspronkelijke bedrag van de lopende lening.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<decimal> OorspronkelijkeLening
    {
        get
        {
            if (this._oorspronkelijkeLening.HasValue)
            {
                return this._oorspronkelijkeLening.Value;
            }
            else
            {
                return default(System.Nullable<decimal>);
            }
        }
        set
        {
            if ((_oorspronkelijkeLening.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "OorspronkelijkeLening";
                Validator.ValidateProperty(value, validatorPropContext);
                this._oorspronkelijkeLening = value;
                this.OnPropertyChanged("OorspronkelijkeLening");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool OorspronkelijkeLeningSpecified
    {
        get
        {
            return this._oorspronkelijkeLening.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._oorspronkelijkeLening = null;
            }
        }
    }
    
    /// <summary>
    /// Het bedrag van de limiet van het doorlopend krediet.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<decimal> LimietDoorlopendKrediet
    {
        get
        {
            if (this._limietDoorlopendKrediet.HasValue)
            {
                return this._limietDoorlopendKrediet.Value;
            }
            else
            {
                return default(System.Nullable<decimal>);
            }
        }
        set
        {
            if ((_limietDoorlopendKrediet.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "LimietDoorlopendKrediet";
                Validator.ValidateProperty(value, validatorPropContext);
                this._limietDoorlopendKrediet = value;
                this.OnPropertyChanged("LimietDoorlopendKrediet");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool LimietDoorlopendKredietSpecified
    {
        get
        {
            return this._limietDoorlopendKrediet.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._limietDoorlopendKrediet = null;
            }
        }
    }
    
    /// <summary>
    /// De startdatum van de lening.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public DatumType StartDt
    {
        get
        {
            if ((this._startDt == null))
            {
                this._startDt = new DatumType();
            }
            return this._startDt;
        }
        set
        {
            if ((this._startDt == value))
            {
                return;
            }
            if (((this._startDt == null) 
                        || (_startDt.Equals(value) != true)))
            {
                this._startDt = value;
                this.OnPropertyChanged("StartDt");
            }
        }
    }
    
    /// <summary>
    /// De einddatum van de lening.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public DatumType EindDt
    {
        get
        {
            if ((this._eindDt == null))
            {
                this._eindDt = new DatumType();
            }
            return this._eindDt;
        }
        set
        {
            if ((this._eindDt == value))
            {
                return;
            }
            if (((this._eindDt == null) 
                        || (_eindDt.Equals(value) != true)))
            {
                this._eindDt = value;
                this.OnPropertyChanged("EindDt");
            }
        }
    }
    
    /// <summary>
    /// Geeft aan of de lening wordt afgelost voor het passeren van de hypotheek.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<BooleanType> AflossingVoorPasserenJN
    {
        get
        {
            if (this._aflossingVoorPasserenJN.HasValue)
            {
                return this._aflossingVoorPasserenJN.Value;
            }
            else
            {
                return default(System.Nullable<BooleanType>);
            }
        }
        set
        {
            if ((_aflossingVoorPasserenJN.Equals(value) != true))
            {
                this._aflossingVoorPasserenJN = value;
                this.OnPropertyChanged("AflossingVoorPasserenJN");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool AflossingVoorPasserenJNSpecified
    {
        get
        {
            return this._aflossingVoorPasserenJN.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._aflossingVoorPasserenJN = null;
            }
        }
    }
    
    /// <summary>
    /// Geeft de hoogte van het af te lossen bedrag aan (inclusief evt. boete voor vervroegde aflossing)
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<decimal> AflosBedrag
    {
        get
        {
            if (this._aflosBedrag.HasValue)
            {
                return this._aflosBedrag.Value;
            }
            else
            {
                return default(System.Nullable<decimal>);
            }
        }
        set
        {
            if ((_aflosBedrag.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "AflosBedrag";
                Validator.ValidateProperty(value, validatorPropContext);
                this._aflosBedrag = value;
                this.OnPropertyChanged("AflosBedrag");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool AflosBedragSpecified
    {
        get
        {
            return this._aflosBedrag.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._aflosBedrag = null;
            }
        }
    }
    
    /// <summary>
    /// Geeft aan of de lening fiscaal aftrekbaar is.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<BooleanType> FiscaalAftrekbaarJN
    {
        get
        {
            if (this._fiscaalAftrekbaarJN.HasValue)
            {
                return this._fiscaalAftrekbaarJN.Value;
            }
            else
            {
                return default(System.Nullable<BooleanType>);
            }
        }
        set
        {
            if ((_fiscaalAftrekbaarJN.Equals(value) != true))
            {
                this._fiscaalAftrekbaarJN = value;
                this.OnPropertyChanged("FiscaalAftrekbaarJN");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool FiscaalAftrekbaarJNSpecified
    {
        get
        {
            return this._fiscaalAftrekbaarJN.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._fiscaalAftrekbaarJN = null;
            }
        }
    }
    
    /// <summary>
    /// Het bedrag dat bovenop het reguliere aflosbedrag is terugbetaald (bijvoorbeeld bij afbetaling van studieschulden).
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<decimal> ExtraAfgelostBedrag
    {
        get
        {
            if (this._extraAfgelostBedrag.HasValue)
            {
                return this._extraAfgelostBedrag.Value;
            }
            else
            {
                return default(System.Nullable<decimal>);
            }
        }
        set
        {
            if ((_extraAfgelostBedrag.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "ExtraAfgelostBedrag";
                Validator.ValidateProperty(value, validatorPropContext);
                this._extraAfgelostBedrag = value;
                this.OnPropertyChanged("ExtraAfgelostBedrag");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool ExtraAfgelostBedragSpecified
    {
        get
        {
            return this._extraAfgelostBedrag.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._extraAfgelostBedrag = null;
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(GeregistreerdeLeningEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether LeningPerJaar should be serialized
    /// </summary>
    public virtual bool ShouldSerializeLeningPerJaar()
    {
        return LeningPerJaar.HasValue;
    }
    
    /// <summary>
    /// Test whether ActueelSaldo should be serialized
    /// </summary>
    public virtual bool ShouldSerializeActueelSaldo()
    {
        return ActueelSaldo.HasValue;
    }
    
    /// <summary>
    /// Test whether OorspronkelijkeLening should be serialized
    /// </summary>
    public virtual bool ShouldSerializeOorspronkelijkeLening()
    {
        return OorspronkelijkeLening.HasValue;
    }
    
    /// <summary>
    /// Test whether LimietDoorlopendKrediet should be serialized
    /// </summary>
    public virtual bool ShouldSerializeLimietDoorlopendKrediet()
    {
        return LimietDoorlopendKrediet.HasValue;
    }
    
    /// <summary>
    /// Test whether AflossingVoorPasserenJN should be serialized
    /// </summary>
    public virtual bool ShouldSerializeAflossingVoorPasserenJN()
    {
        return AflossingVoorPasserenJN.HasValue;
    }
    
    /// <summary>
    /// Test whether AflosBedrag should be serialized
    /// </summary>
    public virtual bool ShouldSerializeAflosBedrag()
    {
        return AflosBedrag.HasValue;
    }
    
    /// <summary>
    /// Test whether FiscaalAftrekbaarJN should be serialized
    /// </summary>
    public virtual bool ShouldSerializeFiscaalAftrekbaarJN()
    {
        return FiscaalAftrekbaarJN.HasValue;
    }
    
    /// <summary>
    /// Test whether ExtraAfgelostBedrag should be serialized
    /// </summary>
    public virtual bool ShouldSerializeExtraAfgelostBedrag()
    {
        return ExtraAfgelostBedrag.HasValue;
    }
    
    /// <summary>
    /// Test whether SoortGeregistreerdeLening should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSoortGeregistreerdeLening()
    {
        if (_shouldSerializeSoortGeregistreerdeLening)
        {
            return true;
        }
        return (_soortGeregistreerdeLening != default(GeregistreerdeLeningType));
    }
    
    /// <summary>
    /// Test whether LeningLopendJN should be serialized
    /// </summary>
    public virtual bool ShouldSerializeLeningLopendJN()
    {
        if (_shouldSerializeLeningLopendJN)
        {
            return true;
        }
        return (_leningLopendJN != default(BooleanType));
    }
    
    /// <summary>
    /// Test whether Maatschappij should be serialized
    /// </summary>
    public virtual bool ShouldSerializeMaatschappij()
    {
        if (_shouldSerializeMaatschappij)
        {
            return true;
        }
        return (_maatschappij != default(MaatschappijType));
    }
    
    /// <summary>
    /// Test whether StartDt should be serialized
    /// </summary>
    public virtual bool ShouldSerializeStartDt()
    {
        return (_startDt != null);
    }
    
    /// <summary>
    /// Test whether EindDt should be serialized
    /// </summary>
    public virtual bool ShouldSerializeEindDt()
    {
        return (_eindDt != null);
    }
    
    /// <summary>
    /// Test whether SoortGeregistreerdeLeningOmschr should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSoortGeregistreerdeLeningOmschr()
    {
        return !string.IsNullOrEmpty(SoortGeregistreerdeLeningOmschr);
    }
    
    /// <summary>
    /// Test whether ContractNr should be serialized
    /// </summary>
    public virtual bool ShouldSerializeContractNr()
    {
        return !string.IsNullOrEmpty(ContractNr);
    }
    
    /// <summary>
    /// Test whether MaatschappijOmschr should be serialized
    /// </summary>
    public virtual bool ShouldSerializeMaatschappijOmschr()
    {
        return !string.IsNullOrEmpty(MaatschappijOmschr);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current GeregistreerdeLeningEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an GeregistreerdeLeningEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output GeregistreerdeLeningEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out GeregistreerdeLeningEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(GeregistreerdeLeningEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out GeregistreerdeLeningEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static GeregistreerdeLeningEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((GeregistreerdeLeningEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static GeregistreerdeLeningEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((GeregistreerdeLeningEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current GeregistreerdeLeningEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an GeregistreerdeLeningEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output GeregistreerdeLeningEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out GeregistreerdeLeningEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(GeregistreerdeLeningEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out GeregistreerdeLeningEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static GeregistreerdeLeningEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
