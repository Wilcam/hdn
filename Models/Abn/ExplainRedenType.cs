// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.ABN
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum ExplainRedenType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("01 Maatschappijspecifiek")]
    Item01Maatschappijspecifiek,
    
    [System.Xml.Serialization.XmlEnumAttribute("02 Senioren verhuisregeling")]
    Item02Seniorenverhuisregeling,
    
    [System.Xml.Serialization.XmlEnumAttribute("03 Senioren tijdelijk tekort")]
    Item03Seniorentijdelijktekort,
    
    [System.Xml.Serialization.XmlEnumAttribute("04 Persoonlijk Budgetadvies NIBUD")]
    Item04PersoonlijkBudgetadviesNIBUD,
    
    [System.Xml.Serialization.XmlEnumAttribute("05 Inkomen uit verpand vermogen")]
    Item05Inkomenuitverpandvermogen,
    
    [System.Xml.Serialization.XmlEnumAttribute("06 Netto besteedbaar inkomen")]
    Item06Nettobesteedbaarinkomen,
    
    [System.Xml.Serialization.XmlEnumAttribute("07 Toekomstperspectief/Inkomensgroei")]
    Item07ToekomstperspectiefInkomensgroei,
    
    [System.Xml.Serialization.XmlEnumAttribute("08 Uit eigen vermogen inbrengen premiedepot, lopende premievrije polis of eerste " +
        "inleg")]
    Item08Uiteigenvermogeninbrengenpremiedepotlopendepremievrijepolisofeersteinleg,
    
    [System.Xml.Serialization.XmlEnumAttribute("09 Vrij beschikbaar vermogen")]
    Item09Vrijbeschikbaarvermogen,
    
    [System.Xml.Serialization.XmlEnumAttribute("10 Huidige woonlasten hoger dan nieuwe woonlasten")]
    Item10Huidigewoonlastenhogerdannieuwewoonlasten,
    
    [System.Xml.Serialization.XmlEnumAttribute("11 Overig")]
    Item11Overig,
}
}
#pragma warning restore
