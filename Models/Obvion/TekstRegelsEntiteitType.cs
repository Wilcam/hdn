// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.OBVION
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="TekstRegelsEntiteitType")]
public class TekstRegelsEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private string _tekstRegel;
    
    private string _tekstRegel2;
    
    private string _tekstRegel3;
    
    private string _tekstRegel4;
    
    private string _tekstRegel5;
    
    private string _volgnummer;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// Vrije tekstregels
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(255)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string TekstRegel
    {
        get
        {
            return this._tekstRegel;
        }
        set
        {
            if ((this._tekstRegel == value))
            {
                return;
            }
            if (((this._tekstRegel == null) 
                        || (_tekstRegel.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "TekstRegel";
                Validator.ValidateProperty(value, validatorPropContext);
                this._tekstRegel = value;
                this.OnPropertyChanged("TekstRegel");
            }
        }
    }
    
    /// <summary>
    /// Vrije tekstregels
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(255)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string TekstRegel2
    {
        get
        {
            return this._tekstRegel2;
        }
        set
        {
            if ((this._tekstRegel2 == value))
            {
                return;
            }
            if (((this._tekstRegel2 == null) 
                        || (_tekstRegel2.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "TekstRegel2";
                Validator.ValidateProperty(value, validatorPropContext);
                this._tekstRegel2 = value;
                this.OnPropertyChanged("TekstRegel2");
            }
        }
    }
    
    /// <summary>
    /// Vrije tekstregels
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(255)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string TekstRegel3
    {
        get
        {
            return this._tekstRegel3;
        }
        set
        {
            if ((this._tekstRegel3 == value))
            {
                return;
            }
            if (((this._tekstRegel3 == null) 
                        || (_tekstRegel3.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "TekstRegel3";
                Validator.ValidateProperty(value, validatorPropContext);
                this._tekstRegel3 = value;
                this.OnPropertyChanged("TekstRegel3");
            }
        }
    }
    
    /// <summary>
    /// Vrije tekstregels
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(255)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string TekstRegel4
    {
        get
        {
            return this._tekstRegel4;
        }
        set
        {
            if ((this._tekstRegel4 == value))
            {
                return;
            }
            if (((this._tekstRegel4 == null) 
                        || (_tekstRegel4.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "TekstRegel4";
                Validator.ValidateProperty(value, validatorPropContext);
                this._tekstRegel4 = value;
                this.OnPropertyChanged("TekstRegel4");
            }
        }
    }
    
    /// <summary>
    /// Vrije tekstregels
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.ComponentModel.DataAnnotations.StringLengthAttribute(255)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string TekstRegel5
    {
        get
        {
            return this._tekstRegel5;
        }
        set
        {
            if ((this._tekstRegel5 == value))
            {
                return;
            }
            if (((this._tekstRegel5 == null) 
                        || (_tekstRegel5.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "TekstRegel5";
                Validator.ValidateProperty(value, validatorPropContext);
                this._tekstRegel5 = value;
                this.OnPropertyChanged("TekstRegel5");
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(TekstRegelsEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether TekstRegel should be serialized
    /// </summary>
    public virtual bool ShouldSerializeTekstRegel()
    {
        return !string.IsNullOrEmpty(TekstRegel);
    }
    
    /// <summary>
    /// Test whether TekstRegel2 should be serialized
    /// </summary>
    public virtual bool ShouldSerializeTekstRegel2()
    {
        return !string.IsNullOrEmpty(TekstRegel2);
    }
    
    /// <summary>
    /// Test whether TekstRegel3 should be serialized
    /// </summary>
    public virtual bool ShouldSerializeTekstRegel3()
    {
        return !string.IsNullOrEmpty(TekstRegel3);
    }
    
    /// <summary>
    /// Test whether TekstRegel4 should be serialized
    /// </summary>
    public virtual bool ShouldSerializeTekstRegel4()
    {
        return !string.IsNullOrEmpty(TekstRegel4);
    }
    
    /// <summary>
    /// Test whether TekstRegel5 should be serialized
    /// </summary>
    public virtual bool ShouldSerializeTekstRegel5()
    {
        return !string.IsNullOrEmpty(TekstRegel5);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current TekstRegelsEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an TekstRegelsEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output TekstRegelsEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out TekstRegelsEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(TekstRegelsEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out TekstRegelsEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static TekstRegelsEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((TekstRegelsEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static TekstRegelsEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((TekstRegelsEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current TekstRegelsEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an TekstRegelsEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output TekstRegelsEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out TekstRegelsEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(TekstRegelsEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out TekstRegelsEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static TekstRegelsEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
