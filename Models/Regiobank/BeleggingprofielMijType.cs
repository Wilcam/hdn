// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.REGIO
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum BeleggingprofielMijType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("RG001 Geel (defensief)")]
    RG001Geeldefensief,
    
    [System.Xml.Serialization.XmlEnumAttribute("RG002 Oranje (neutraal)")]
    RG002Oranjeneutraal,
    
    [System.Xml.Serialization.XmlEnumAttribute("RG003 Rood (offensief)")]
    RG003Roodoffensief,
    
    [System.Xml.Serialization.XmlEnumAttribute("RG004 Paars (zeer offensief)")]
    RG004Paarszeeroffensief,
}
}
#pragma warning restore
