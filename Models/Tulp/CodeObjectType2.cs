// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.TULP
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum CodeObjectType2
{
    
    [System.Xml.Serialization.XmlEnumAttribute("01 eengezinswoning")]
    Item01eengezinswoning,
    
    [System.Xml.Serialization.XmlEnumAttribute("02 eengezinswoning met garage")]
    Item02eengezinswoningmetgarage,
    
    [System.Xml.Serialization.XmlEnumAttribute("03 flat/appartement")]
    Item03flatappartement,
    
    [System.Xml.Serialization.XmlEnumAttribute("04 flat/appartement met garage")]
    Item04flatappartementmetgarage,
    
    [System.Xml.Serialization.XmlEnumAttribute("05 winkel-woonhuis")]
    Item05winkelwoonhuis,
    
    [System.Xml.Serialization.XmlEnumAttribute("06 woonhuis met bedrijfsruimte")]
    Item06woonhuismetbedrijfsruimte,
    
    [System.Xml.Serialization.XmlEnumAttribute("07 winkel")]
    Item07winkel,
    
    [System.Xml.Serialization.XmlEnumAttribute("08 bedrijfspand")]
    Item08bedrijfspand,
    
    [System.Xml.Serialization.XmlEnumAttribute("09 recreatiewoning")]
    Item09recreatiewoning,
    
    [System.Xml.Serialization.XmlEnumAttribute("10 boerderij")]
    Item10boerderij,
    
    [System.Xml.Serialization.XmlEnumAttribute("11 zakelijk onderpand")]
    Item11zakelijkonderpand,
    
    [System.Xml.Serialization.XmlEnumAttribute("12 agrarische grond/bouwgrond")]
    Item12agrarischegrondbouwgrond,
    
    [System.Xml.Serialization.XmlEnumAttribute("13 garage")]
    Item13garage,
    
    [System.Xml.Serialization.XmlEnumAttribute("14 bouwkavel")]
    Item14bouwkavel,
    
    [System.Xml.Serialization.XmlEnumAttribute("15 Woonschip")]
    Item15Woonschip,
    
    [System.Xml.Serialization.XmlEnumAttribute("16 Pleziervaartuig")]
    Item16Pleziervaartuig,
}
}
#pragma warning restore
