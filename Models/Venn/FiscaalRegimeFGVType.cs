// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.VENN
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
public enum FiscaalRegimeFGVType
{
    
    [System.Xml.Serialization.XmlEnumAttribute("01 Pre brede herwaardering")]
    Item01Prebredeherwaardering,
    
    [System.Xml.Serialization.XmlEnumAttribute("02 Brede herwaardering")]
    Item02Bredeherwaardering,
    
    [System.Xml.Serialization.XmlEnumAttribute("03 IB 2001")]
    Item03IB2001,
}
}
#pragma warning restore
