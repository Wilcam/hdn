// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code++. Version 5.0.0.47. www.xsd2code.com
//  </auto-generated>
// ------------------------------------------------------------------------------
#pragma warning disable
namespace HDN.RABO
{
using System;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization;

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Runtime.Serialization.DataContractAttribute(Name="UitkeringEntiteitType")]
public class UitkeringEntiteitType : System.ComponentModel.INotifyPropertyChanged
{
    
    #region Private fields
    private bool _shouldSerializeSoortUitkering;
    
    private bool _shouldSerializeBrutoJaarUitkering;
    
    private SoortUitkeringType _soortUitkering;
    
    private System.Nullable<SoortVerzekeringType> _soortVerzekering;
    
    private decimal _brutoJaarUitkering;
    
    private DatumType _ingangsDtUitkering;
    
    private DatumType _eindDtUitkering;
    
    private System.Nullable<BooleanType> _volledigeAOWOpbouwJN;
    
    private string _aantalAOWJaren;
    
    private string _volgnummer;
    
    private static XmlSerializer serializer;
    #endregion
    
    /// <summary>
    /// Geeft het soort uitkering aan.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public SoortUitkeringType SoortUitkering
    {
        get
        {
            return this._soortUitkering;
        }
        set
        {
            if ((_soortUitkering.Equals(value) != true))
            {
                this._soortUitkering = value;
                this.OnPropertyChanged("SoortUitkering");
            }
            _shouldSerializeSoortUitkering = true;
        }
    }
    
    /// <summary>
    /// Geeft het soort verzekering weer.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<SoortVerzekeringType> SoortVerzekering
    {
        get
        {
            if (this._soortVerzekering.HasValue)
            {
                return this._soortVerzekering.Value;
            }
            else
            {
                return default(System.Nullable<SoortVerzekeringType>);
            }
        }
        set
        {
            if ((_soortVerzekering.Equals(value) != true))
            {
                this._soortVerzekering = value;
                this.OnPropertyChanged("SoortVerzekering");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool SoortVerzekeringSpecified
    {
        get
        {
            return this._soortVerzekering.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._soortVerzekering = null;
            }
        }
    }
    
    /// <summary>
    /// De bruto basisuitkering incl. vakantietoeslag. Het brutojaarbedrag van gebruikelijke aantal uitkeringsweken behorende bij de uitkering.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("-?\\d{1,12}\\.\\d{2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public decimal BrutoJaarUitkering
    {
        get
        {
            return this._brutoJaarUitkering;
        }
        set
        {
            if ((_brutoJaarUitkering.Equals(value) != true))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "BrutoJaarUitkering";
                Validator.ValidateProperty(value, validatorPropContext);
                this._brutoJaarUitkering = value;
                this.OnPropertyChanged("BrutoJaarUitkering");
            }
            _shouldSerializeBrutoJaarUitkering = true;
        }
    }
    
    /// <summary>
    /// Geeft de ingangsdatum van de uitkering aan.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public DatumType IngangsDtUitkering
    {
        get
        {
            if ((this._ingangsDtUitkering == null))
            {
                this._ingangsDtUitkering = new DatumType();
            }
            return this._ingangsDtUitkering;
        }
        set
        {
            if ((this._ingangsDtUitkering == value))
            {
                return;
            }
            if (((this._ingangsDtUitkering == null) 
                        || (_ingangsDtUitkering.Equals(value) != true)))
            {
                this._ingangsDtUitkering = value;
                this.OnPropertyChanged("IngangsDtUitkering");
            }
        }
    }
    
    /// <summary>
    /// Geeft de einddatum van de uitkering aan.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public DatumType EindDtUitkering
    {
        get
        {
            if ((this._eindDtUitkering == null))
            {
                this._eindDtUitkering = new DatumType();
            }
            return this._eindDtUitkering;
        }
        set
        {
            if ((this._eindDtUitkering == value))
            {
                return;
            }
            if (((this._eindDtUitkering == null) 
                        || (_eindDtUitkering.Equals(value) != true)))
            {
                this._eindDtUitkering = value;
                this.OnPropertyChanged("EindDtUitkering");
            }
        }
    }
    
    /// <summary>
    /// Geeft aan of er sprake is van een volledige AOW opbouw. (Wel of geen korting op AOW i.v.m. woonachtig zijn buiten Nederland).
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public System.Nullable<BooleanType> VolledigeAOWOpbouwJN
    {
        get
        {
            if (this._volledigeAOWOpbouwJN.HasValue)
            {
                return this._volledigeAOWOpbouwJN.Value;
            }
            else
            {
                return default(System.Nullable<BooleanType>);
            }
        }
        set
        {
            if ((_volledigeAOWOpbouwJN.Equals(value) != true))
            {
                this._volledigeAOWOpbouwJN = value;
                this.OnPropertyChanged("VolledigeAOWOpbouwJN");
            }
        }
    }
    
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool VolledigeAOWOpbouwJNSpecified
    {
        get
        {
            return this._volledigeAOWOpbouwJN.HasValue;
        }
        set
        {
            if (value==false)
            {
                this._volledigeAOWOpbouwJN = null;
            }
        }
    }
    
    /// <summary>
    /// Geeft aan hoeveel jaar AOW er maximaal opgebouwd kan worden tot de AOW leeftijd.
    /// </summary>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="nonNegativeInteger", IsNullable=true)]
    [System.ComponentModel.DataAnnotations.RegularExpressionAttribute("\\d{1,2}")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string AantalAOWJaren
    {
        get
        {
            return this._aantalAOWJaren;
        }
        set
        {
            if ((this._aantalAOWJaren == value))
            {
                return;
            }
            if (((this._aantalAOWJaren == null) 
                        || (_aantalAOWJaren.Equals(value) != true)))
            {
                System.ComponentModel.DataAnnotations.ValidationContext validatorPropContext = new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
                validatorPropContext.MemberName = "AantalAOWJaren";
                Validator.ValidateProperty(value, validatorPropContext);
                this._aantalAOWJaren = value;
                this.OnPropertyChanged("AantalAOWJaren");
            }
        }
    }
    
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="positiveInteger")]
    [System.Runtime.Serialization.DataMemberAttribute()]
    public string Volgnummer
    {
        get
        {
            return this._volgnummer;
        }
        set
        {
            if ((this._volgnummer == value))
            {
                return;
            }
            if (((this._volgnummer == null) 
                        || (_volgnummer.Equals(value) != true)))
            {
                this._volgnummer = value;
                this.OnPropertyChanged("Volgnummer");
            }
        }
    }
    
    private static XmlSerializer Serializer
    {
        get
        {
            if ((serializer == null))
            {
                serializer = new XmlSerializerFactory().CreateSerializer(typeof(UitkeringEntiteitType));
            }
            return serializer;
        }
    }
    
    public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    
    /// <summary>
    /// Test whether SoortVerzekering should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSoortVerzekering()
    {
        return SoortVerzekering.HasValue;
    }
    
    /// <summary>
    /// Test whether VolledigeAOWOpbouwJN should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolledigeAOWOpbouwJN()
    {
        return VolledigeAOWOpbouwJN.HasValue;
    }
    
    /// <summary>
    /// Test whether BrutoJaarUitkering should be serialized
    /// </summary>
    public virtual bool ShouldSerializeBrutoJaarUitkering()
    {
        if (_shouldSerializeBrutoJaarUitkering)
        {
            return true;
        }
        return (_brutoJaarUitkering != default(decimal));
    }
    
    /// <summary>
    /// Test whether SoortUitkering should be serialized
    /// </summary>
    public virtual bool ShouldSerializeSoortUitkering()
    {
        if (_shouldSerializeSoortUitkering)
        {
            return true;
        }
        return (_soortUitkering != default(SoortUitkeringType));
    }
    
    /// <summary>
    /// Test whether IngangsDtUitkering should be serialized
    /// </summary>
    public virtual bool ShouldSerializeIngangsDtUitkering()
    {
        return (_ingangsDtUitkering != null);
    }
    
    /// <summary>
    /// Test whether EindDtUitkering should be serialized
    /// </summary>
    public virtual bool ShouldSerializeEindDtUitkering()
    {
        return (_eindDtUitkering != null);
    }
    
    /// <summary>
    /// Test whether AantalAOWJaren should be serialized
    /// </summary>
    public virtual bool ShouldSerializeAantalAOWJaren()
    {
        return !string.IsNullOrEmpty(AantalAOWJaren);
    }
    
    /// <summary>
    /// Test whether Volgnummer should be serialized
    /// </summary>
    public virtual bool ShouldSerializeVolgnummer()
    {
        return !string.IsNullOrEmpty(Volgnummer);
    }
    
    public virtual void OnPropertyChanged(string propertyName)
    {
        System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
        if ((handler != null))
        {
            handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
    
    #region Serialize/Deserialize
    /// <summary>
    /// Serializes current UitkeringEntiteitType object into an XML string
    /// </summary>
    /// <returns>string XML value</returns>
    public virtual string Serialize()
    {
        System.IO.StreamReader streamReader = null;
        System.IO.MemoryStream memoryStream = null;
        try
        {
            memoryStream = new System.IO.MemoryStream();
            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            System.Xml.XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);
            Serializer.Serialize(xmlWriter, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            streamReader = new System.IO.StreamReader(memoryStream);
            return streamReader.ReadToEnd();
        }
        finally
        {
            if ((streamReader != null))
            {
                streamReader.Dispose();
            }
            if ((memoryStream != null))
            {
                memoryStream.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes workflow markup into an UitkeringEntiteitType object
    /// </summary>
    /// <param name="input">string workflow markup to deserialize</param>
    /// <param name="obj">Output UitkeringEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool Deserialize(string input, out UitkeringEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(UitkeringEntiteitType);
        try
        {
            obj = Deserialize(input);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool Deserialize(string input, out UitkeringEntiteitType obj)
    {
        System.Exception exception = null;
        return Deserialize(input, out obj, out exception);
    }
    
    public new static UitkeringEntiteitType Deserialize(string input)
    {
        System.IO.StringReader stringReader = null;
        try
        {
            stringReader = new System.IO.StringReader(input);
            return ((UitkeringEntiteitType)(Serializer.Deserialize(XmlReader.Create(stringReader))));
        }
        finally
        {
            if ((stringReader != null))
            {
                stringReader.Dispose();
            }
        }
    }
    
    public static UitkeringEntiteitType Deserialize(System.IO.Stream s)
    {
        return ((UitkeringEntiteitType)(Serializer.Deserialize(s)));
    }
    #endregion
    
    /// <summary>
    /// Serializes current UitkeringEntiteitType object into file
    /// </summary>
    /// <param name="fileName">full path of outupt xml file</param>
    /// <param name="exception">output Exception value if failed</param>
    /// <returns>true if can serialize and save into file; otherwise, false</returns>
    public virtual bool SaveToFile(string fileName, out System.Exception exception)
    {
        exception = null;
        try
        {
            SaveToFile(fileName);
            return true;
        }
        catch (System.Exception e)
        {
            exception = e;
            return false;
        }
    }
    
    public virtual void SaveToFile(string fileName)
    {
        System.IO.StreamWriter streamWriter = null;
        try
        {
            string xmlString = Serialize();
            System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
            streamWriter = xmlFile.CreateText();
            streamWriter.WriteLine(xmlString);
            streamWriter.Close();
        }
        finally
        {
            if ((streamWriter != null))
            {
                streamWriter.Dispose();
            }
        }
    }
    
    /// <summary>
    /// Deserializes xml markup from file into an UitkeringEntiteitType object
    /// </summary>
    /// <param name="fileName">string xml file to load and deserialize</param>
    /// <param name="obj">Output UitkeringEntiteitType object</param>
    /// <param name="exception">output Exception value if deserialize failed</param>
    /// <returns>true if this Serializer can deserialize the object; otherwise, false</returns>
    public static bool LoadFromFile(string fileName, out UitkeringEntiteitType obj, out System.Exception exception)
    {
        exception = null;
        obj = default(UitkeringEntiteitType);
        try
        {
            obj = LoadFromFile(fileName);
            return true;
        }
        catch (System.Exception ex)
        {
            exception = ex;
            return false;
        }
    }
    
    public static bool LoadFromFile(string fileName, out UitkeringEntiteitType obj)
    {
        System.Exception exception = null;
        return LoadFromFile(fileName, out obj, out exception);
    }
    
    public new static UitkeringEntiteitType LoadFromFile(string fileName)
    {
        System.IO.FileStream file = null;
        System.IO.StreamReader sr = null;
        try
        {
            file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
            sr = new System.IO.StreamReader(file);
            string xmlString = sr.ReadToEnd();
            sr.Close();
            file.Close();
            return Deserialize(xmlString);
        }
        finally
        {
            if ((file != null))
            {
                file.Dispose();
            }
            if ((sr != null))
            {
                sr.Dispose();
            }
        }
    }
}
}
#pragma warning restore
