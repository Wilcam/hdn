﻿using Finix;
using HDN.COLIBRI;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if COLIBRI

namespace HDN.Factories
{
    public class ColibriFactory : BaseFactory
    {

        public ColibriFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.CLColibriHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.CLColibriHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {                                  
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.CL001Annuïteiten;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.CL002Lineair;
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                   GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
            }
        }
    };
}

#endif