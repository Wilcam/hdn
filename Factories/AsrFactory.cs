﻿using Finix;
using HDN.ASR;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if ASR

namespace HDN.Factories
{
    public class AsrFactory : BaseFactory
    {

        public AsrFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override string GetRenteVastInMnd(LeningdeelDataT leningdeelFinix)
        {
            if (leningdeelFinix.Arrangement == "ASRWTLRH")
                return 995.ToString();
            else
                return base.GetRenteVastInMnd(leningdeelFinix);
        }

        protected override string GetDuurInMnd(LeningdeelDataT leningdeelFinix)
        {
            if (leningdeelFinix.Arrangement == "ASRWTLRH")
                return 995.ToString();
            else
                return base.GetDuurInMnd(leningdeelFinix);
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.AUASR;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.AUASR;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            if (leningdeelFinix.Arrangement == "ASRWTLRH")
                return CodeDeelMijType.AU018ASRLevensrenteHypotheek;

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.AU003ASRAflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.AU010RestschuldfinancieringAnnuitair;
                    return CodeDeelMijType.AU001ASRAnnuiteitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.AU011RestschuldfinancieringLineair;
                    return CodeDeelMijType.AU002ASRLineairehypotheek;               
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.AU004ASRSpaarhypotheek;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.AU005ASRLevenhypotheek;
                case SoortHypotheekEnum.Effectenhypotheek:
                    return CodeDeelMijType.AU005ASRLevenhypotheek;
                case SoortHypotheekEnum.Hybridehypotheek:
                    return CodeDeelMijType.AU005ASRLevenhypotheek;
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                    return CodeDeelMijType.AU005ASRLevenhypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                   GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
            }          
        }      
    };
}

#endif
