﻿using Finix;
using HDN.MONEYOU;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if MONEYOU

namespace HDN.Factories
{
    public class MoneyouFactory : BaseFactory
    {

        public MoneyouFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.MYMoneYou;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.MYMoneYou;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.MY004Aflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.MY001Annuiteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    break;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.MY003Spaar;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.MY002Leven;
                case SoortHypotheekEnum.Effectenhypotheek:
                    return CodeDeelMijType.MY005Belegging;
                case SoortHypotheekEnum.Hybridehypotheek:
                    break;
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                    break;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    break;
                case SoortHypotheekEnum.Krediethypotheek:
                    break;
                case SoortHypotheekEnum.BEWhypotheek:
                    break;
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
break;
            }
                   GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 

        }
    };
}

#endif
