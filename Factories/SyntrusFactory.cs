﻿using Finix;
using HDN.SYNTRUS;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if SYNTRUS

namespace HDN.Factories
{
    public class SyntrusFactory : BaseFactory
    {

        public SyntrusFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.SYSyntrusAchmeaHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.SYSyntrusAchmeaHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.SY001SyntrusAchmeaAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.SY002SyntrusAchmeaAnnuïteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.SY004SyntrusAchmeaLineair;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.SY005SyntrusAchmeaSpaar;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.SY003SyntrusAchmeaLeven;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

       
    }
}

#endif