﻿#if ABN || ABNGRP || MONEYOU || RABO || MERIUS || WFONDS || TEMPLATE 
#define INKOMENPROGNOSELOPENDJR
#endif

#if !RABO && !AEGON && !ATTENS && !ING && !NN && !BLG && !ASN && !BIJBOUWE && !DYNAMIC && !MERIUS && !OBVION && !PHILIPS && !REGIO && !SNS && !SYNTRUS && !TELLIUS
#define SPECONDERNEMING
#endif

#if !ABN && !ABNGRP && !MONEYOU
#define SPECONDERNEMINGEXTRA
#endif

#if !ARGENTA
#define ONDERNEMINGLIST
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        void BuildZelfstandigInkomen(string idRef, PersoonsGegevensFieldT persoon, InkomenItemDataT[] loonInkomens, SpecificatieInkomenFieldT specificatieInkomen,
            BijAfVerItemDataT bijAf, AnalyseHypotheekverledenDataT hypVerleden, persoonFieldT extraPersoon, ZelfstandigInkomenGegevensFieldT onderneming,
            UPODataT[] upos, OndernemingDataT[] ondernemingen, string pensioenDatum, HypotheekgeverEntiteitType hypotheekGever)
        {
            if (specificatieInkomen.InkomenZelfstandige == DataBooleanFieldT.@true)
            {
                OndernemingEntiteitType HdnOnderneming = new OndernemingEntiteitType();
                HdnOnderneming.RechtsVorm = SpecialEnums.GetHdnRechtsVorm(onderneming.RechtsVorm);
                HdnOnderneming.SoortOnderneming = SpecialEnums.GetHdnSoortOndermening(onderneming.SoortOnderneming);
                HdnOnderneming.IngangsDtOnderneming = Utils.GetHdnDate(onderneming.StartDatumOnderneming);
                HdnOnderneming.DGAJN = BooleanType.NNee;
                HdnOnderneming.Inkomen1jr = 1.00M;  // bug in generator
                HdnOnderneming.Inkomen1jr = Utils.GetHdnDecimal(onderneming.Bedrag3);
                HdnOnderneming.Inkomen2jr = 1.00M;  // bug in generator
                HdnOnderneming.Inkomen2jr = Utils.GetHdnDecimal(onderneming.Bedrag2);
                HdnOnderneming.Inkomen3jr = 1.00M;  // bug in generator
                HdnOnderneming.Inkomen3jr = Utils.GetHdnDecimal(onderneming.Bedrag1);
                HdnOnderneming.Inkomenzelfstandige = 1.00M;  // bug in generator
                HdnOnderneming.Inkomenzelfstandige = (HdnOnderneming.Inkomen1jr + HdnOnderneming.Inkomen2jr + HdnOnderneming.Inkomen3jr) / 3;
                if (HdnOnderneming.Inkomenzelfstandige > Utils.GetHdnDecimal(onderneming.Bedrag3))
                    HdnOnderneming.Inkomenzelfstandige = Utils.GetHdnDecimal(onderneming.Bedrag3);
#if INKOMENPROGNOSELOPENDJR
                Utils.SetHdnDecimal(onderneming.PrognoseBedrag, HdnOnderneming, nameof(HdnOnderneming.InkomenPrognoseLopendJr));
#endif
                if (onderneming.DGA == JaNeeEnum.Ja)
                    HdnOnderneming.DGAJN = BooleanType.JJa;
                HdnOnderneming.Volgnummer = "1";
                hypotheekGever.Inkomsten.Onderneming.Add(HdnOnderneming);
            }

            if (specificatieInkomen.HeeftInkomensVerklaringOndernemer == DataBooleanFieldT.@true)
            {
                hypotheekGever.Inkomsten.InkomensverklaringOndernemer.Volgnummer = "1";
                hypotheekGever.Inkomsten.InkomensverklaringOndernemer.Toetsinkomen = Utils.GetHdnDecimal(specificatieInkomen.Toetsinkomen);
                hypotheekGever.Inkomsten.InkomensverklaringOndernemer.AantalOndernemingen = specificatieInkomen.AantalOndernemingen.GetValueOrDefault().ToString();
                hypotheekGever.Inkomsten.InkomensverklaringOndernemer.UitgifteDt = Utils.GetHdnDate(specificatieInkomen.UitgifteDatum);
                hypotheekGever.Inkomsten.InkomensverklaringOndernemer.Dossiernummer = specificatieInkomen.Dossiernummer;
                hypotheekGever.Inkomsten.InkomensverklaringOndernemer.Rekenexpert = SpecialEnums.GetHdnRekenExpert(specificatieInkomen.Rekenexpert);
#if SPECONDERNEMING
                int ondernemingvolgnr = 1;
                foreach (OndernemingDataT ond in ondernemingen)
                {

                    OndernemingEntiteitType1 ondernemingEnt = new OndernemingEntiteitType1();
                    ondernemingEnt.RechtsVorm = SpecialEnums.GetHdnRechtsVorm(ond.Rechtsvorm);
                    ondernemingEnt.Volgnummer = ondernemingvolgnr.ToString();
                    ondernemingvolgnr++;
                    ondernemingEnt.IngangsDtOnderneming = Utils.GetHdnDate(ond.Startdatum);
                    ondernemingEnt.SoortOnderneming = SpecialEnums.GetHdnSoortOndermening(ond.SoortOnderneming);
#if SPECONDERNEMINGEXTRA
                    ondernemingEnt.KvKnummer = ond.kvknummer.GetValueOrDefault().ToString();
                    ondernemingEnt.DGAJN = Enums.GetHdnBooleanType(ond.DGA);
#endif
#if ONDERNEMINGLIST
                    hypotheekGever.Inkomsten.InkomensverklaringOndernemer.Onderneming.Add(ondernemingEnt);
#else
                    hypotheekGever.Inkomsten.InkomensverklaringOndernemer.Onderneming =ondernemingEnt;
#endif
                }
#endif
            }
        }
    }
}

