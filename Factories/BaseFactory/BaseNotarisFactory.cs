﻿#if !NN
#define NOTARISLIST
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        protected void BuildNotaris()
        {
            if (_hypBer.NotarisID.NotarisGUID == null)
                return;

            NotarisEntiteitType notaris = new NotarisEntiteitType();
            notaris.RefContactPersoonData = BuildNawNotaris();
            notaris.BedrijfsNaam = _hypBer.NotarisID.BedrijfsNaam;
            notaris.Volgnummer = "1";
            notaris.CodeNotaris = SpecialEnums.GetHdnTypeNotaris(_hypBer.NotarisID.NotarisRol);
            notaris.TelefoonNrWerk = _hypBer.NotarisID.TelefoonWerk;
#if NOTARISLIST
            _hdnAanvraag.Notaris.Add(notaris);
#else
            _hdnAanvraag.Notaris = notaris;
#endif
        }

        protected REFPartijNAWDataType BuildNawNotaris()
        {
            PartijNAWDataEntiteitType naw = new PartijNAWDataEntiteitType
            {
                ID = Constants.NotarisRef,
                SoortPartij = SoortPartijType.Item02rechtspersoon,
                PlaatsNaam = _hypBer.NotarisID.Plaatsnaam,
                StraatNaam = _hypBer.NotarisID.Straatnaam,
                HuisNr = _hypBer.NotarisID.Huisnummer,
                HuisNrToevoeging = _hypBer.NotarisID.Huisnummertoevoeging,
                Postcode = _hypBer.NotarisID.Postcode,
                Geslacht = (GeslachtType)(int)_hypBer.NotarisID.Geslacht.GetValueOrDefault(),
                VoorLetters = _hypBer.NotarisID.Voorletters,
                VoorNamen = _hypBer.NotarisID.VoorNaam,
                GebAchterNaam = _hypBer.NotarisID.AchterNaam,
                GebTussenVoegsels = _hypBer.NotarisID.TussenVoegsels,
                Land = LandType.NLNederland,
            };

            naw.Volgnummer = (_hdnAanvraag.PartijNAWData.Count + 1).ToString();
            _hdnAanvraag.PartijNAWData.Add(naw);
            REFPartijNAWDataType refNaw = new REFPartijNAWDataType();
            refNaw.IDREF = Constants.NotarisRef;
            return refNaw;
        }
    }
}
