﻿#if CENTRAAL || HYPOTRUS || ING || NIBC || ARGENTA || ALLIANZ || BIJBOUWE || BNP || IQWOON || MERIUS || WFONDS || TEMPLATE 
#define PCTCONSUMPTIEF
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        static VerplichtingenEntiteitType BuildAliVerplichting(DuurBedragFieldT finixVerplichting, ref int volgnummerVerplichtingen, PersoonsGegevensFieldT persoon)
        {
            VerplichtingenEntiteitType verplichting = new();
            verplichting.Verplichting = VerplichtingType.Item02Alimentatieverplichtingtgvpartner;
            verplichting.VerplichtingPerJaar = Utils.GetHdnDecimal(finixVerplichting.Bedrag);
            verplichting.StartDt = Utils.GetHdnDate(persoon.HOHuwelijkOntbondenDatum);
            DateTime startDate = Convert.ToDateTime(persoon.HOHuwelijkOntbondenDatum);
            verplichting.EindDt = Utils.GetHdnDate(startDate.AddMonths(finixVerplichting.Duur.GetValueOrDefault()));            
            verplichting.Volgnummer = volgnummerVerplichtingen.ToString();
            volgnummerVerplichtingen++;
            return verplichting;
        }

        VerplichtingenEntiteitType BuildAliKindVerplichting(KindDataT kindData, ref int volgnummerVerplichtingen)
        {
            VerplichtingenEntiteitType verplichting = new();
            verplichting.Verplichting = VerplichtingType.Item01Alimentatieverplichtingtgvkinderen;
            verplichting.VerplichtingPerJaar = Utils.GetHdnDecimal(kindData.AlimentatieBedrag);
            PersoonsGegevensFieldT persoon;
            persoon = _finixDossier.HuidigeSituatie.AlgemeneGegevens.ClientGegevens;
            if (kindData.AlimentatieBetaler == EigenaarEnum.Partner)
                persoon = _finixDossier.HuidigeSituatie.AlgemeneGegevens.PartnerGegevens;
            verplichting.StartDt = Utils.GetHdnDate(persoon.HOHuwelijkOntbondenDatum);          
            verplichting.EindDt = Utils.GetHdnDate(kindData.AlimentatieEinddatum);
            verplichting.Volgnummer = volgnummerVerplichtingen.ToString();
            volgnummerVerplichtingen++;
            return verplichting;      
        }

        void BuildVerplichtingen(SpecificatieInkomenFieldT specificatieInkomen, BijAfVerItemDataT bijAf, EigenaarEnum eigenaar, HypotheekgeverEntiteitType hypotheekGever, PersoonsGegevensFieldT persoon)
        {
            int volgnummerVerplichtingen = 1;
            
            if (specificatieInkomen.AftrekpostAlimentatie == DataBooleanFieldT.@true)
            {
                if (bijAf.AlimentatieVerplichtingen.Bedrag.GetValueOrDefault() > 0)
                    hypotheekGever.Verplichtingen.Add(BuildAliVerplichting(bijAf.AlimentatieVerplichtingen, ref volgnummerVerplichtingen, persoon));
            }

            if (_finixDossier.HuidigeSituatie.AlgemeneGegevens.KinderenGegevens != null)
                for (int i = 0; i < _finixDossier.HuidigeSituatie.AlgemeneGegevens.KinderenGegevens.Length; i++)
                {
                    KindDataT kindData = _finixDossier.HuidigeSituatie.AlgemeneGegevens.KinderenGegevens[i];
                    if ((kindData.AlimentatieBedrag > 0) && (kindData.AlimentatieBetaler == eigenaar))
                    {
                        hypotheekGever.Verplichtingen.Add(BuildAliKindVerplichting(kindData, ref volgnummerVerplichtingen));
                    }
                }           
        }
    }
}