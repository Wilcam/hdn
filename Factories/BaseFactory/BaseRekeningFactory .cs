﻿#if ABN || ABNGRP || BLG || MONEYOU || NN || ASN || REGIO || SNS || TELLIUS
#define LENINGDEELBANCAIREDEKKINGSINGLE
#endif

#if !NN && !AEGON
#define DEKKING1WAARDENLIST
#endif

#if !AEGON
#define CONTRACTANTLIST
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public partial class BaseFactory
    {
        protected static void BuildMeeverhuisdeSpaarRekening(BeleggingsrekeningDataT rekening, LeningdeelEntiteitType leningdeel)
        {
            BancaireDekkingEntiteitType dekking1 = new();

            dekking1.Volgnummer = "1";
            dekking1.CodeBancaireDekking = CodeBancaireDekkingType.Item02Beleggingsrekeningalleenfondsen;
            dekking1.AanvangsDt = Utils.GetHdnDate(rekening.BestaandePolisDatum);
            dekking1.DuurInMnd = (rekening.BestaandOrigineleLooptijd.GetValueOrDefault() * 12 + rekening.BestaandOrigineleLooptijdInMnd.GetValueOrDefault()).ToString();
            /*     dekking1.ProductNaam = verzekering.VrijeProduktnaam;
                 dekking1.Maatschappij = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
                 if (dekking1.Maatschappij == MaatschappijType.ZZAnders)
                     dekking1.MaatschappijOmschr = verzekering.VrijeMaatschappijNaam;
                 if (verzekering.AfkoopwaardeDoorVoortzetting == DataBooleanFieldT.@true)
                     dekking1.PolisLopendJN = BooleanType.NNee;
                 else
                     dekking1.PolisLopendJN = BooleanType.JJa;
                 dekking1.MaatschappijPolisLopend = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
                 if (dekking1.MaatschappijPolisLopend == MaatschappijType.ZZAnders)
                     dekking1.MaatschappijPolisLopendOmschr = verzekering.VrijeMaatschappijNaam;
                 dekking1.PolisNr = verzekering.BestaandePolisNummer;
                 dekking1.PolisOmzetting = dekking1.FiscaalGeruislozeVoortzettingJN;
                 if (verzekering.BestaandePolisNietVerpand == DataBooleanFieldT.@true)
                     dekking1.VerpandJN = BooleanType.NNee;
                 else
                     dekking1.VerpandJN = BooleanType.JJa;
                 dekking1.VerzekeringNemer = GetVerzekeringNemers(verzekering);
                 dekking1.Verzekerde = GetVerzekerden(verzekering);
                 dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen(verzekering);
                 dekking1.PremieAfspraken.Add(GetPremieAfspraken(verzekering));*/

#if LENINGDEELBANCAIREDEKKINGSINGLE
            leningdeel.BancaireDekking = dekking1;
#else
            leningdeel.BancaireDekking.Add(dekking1);
#endif
        }

        protected void BuildNieuweSpaarRekening(GemengdeVerzekeringDataT verzekering, LeningdeelEntiteitType leningdeel)
        {
            BancaireDekkingEntiteitType dekking1 = new();
            dekking1.CodeBancaireDekking = CodeBancaireDekkingType.Item01Spaarrekening;
            dekking1.Aanbieder = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
            dekking1.AanvangsDt = Utils.GetHdnDate(DateTime.Now.AddMonths(1));
            dekking1.FiscaalGeruislozeVoortzettingJN = BooleanType.NNee;
            dekking1.ContractLopendJN = BooleanType.NNee;
            dekking1.ContractOmzettingJN = BooleanType.NNee;
            dekking1.DuurInMnd = (verzekering.Looptijd.GetValueOrDefault() * 12 + verzekering.BestaandOrigineleLooptijdInMnd.GetValueOrDefault()).ToString();
            dekking1.InlegAfspraken.Add(GetInlegAfspraken(verzekering, false));
#if  DEKKING1WAARDENLIST
            dekking1.Waarden.Add(GetWaarden(verzekering, false));
#else
            dekking1.Waarden = GetWaarden(verzekering, false);
#endif
            ContractantEntiteitType contractantEntiteitType = new();
            if (verzekering.Verzekeringnemers.GetValueOrDefault().IsAny(EigenaarEnum.Beiden, EigenaarEnum.Cliënt))
                contractantEntiteitType.RefPartijNAWData = _clientRef;
            else
                contractantEntiteitType.RefPartijNAWData = _partnerRef;
            contractantEntiteitType.Volgnummer = "1";
#if CONTRACTANTLIST
            dekking1.Contractant.Add(contractantEntiteitType);
#else
            dekking1.Contractant = contractantEntiteitType;
#endif

#if LENINGDEELBANCAIREDEKKINGSINGLE
            dekking1.Volgnummer = "1";
            leningdeel.BancaireDekking = dekking1;
#else
            dekking1.Volgnummer = (leningdeel.BancaireDekking.Count + 1).ToString();
            leningdeel.BancaireDekking.Add(dekking1);
#endif
        }
    }
}

