﻿#if RABO  || TEMPLATE
#define OVERBRUGGINGSRENTE
#endif

#if ABN || NN || TEMPLATE
#define HUIDIGEBEDRAGEN
#endif

#if !RABO && !ASR && !ABN && !MUNT && !VENN && !COLIBRI && !ABNGRP && !MONEYOU && !NN && !WOONNU && !DELTA && !DYNAMIC && !LLOYDS && !LOT && !MERIUS && !OBVION && !PHILIPS && !REAAL && !TULP
#define CODEOBJECT
#endif

#if !MUNT && !RABO && !AEGON && !COLIBRI && !NN && !DELTA && !LOT && !MERIUS && !PHILIPS && !REAAL && !TULP
#define AANVRAGERBESTAANDEHYPOTHEEK
#endif

#if !COLIBRI && !ABN && !ABNGRP && !AEGON && !ATTENS && !ING && !MONEYOU && !BLG && !ASN && !ASR && !DELTA && !LLOYDS && !LOT && !PHILIPS && !REAAL && !REGIO && !SNS && !SYNTRUS && !TELLIUS && !TULP
#define VERKOOPONDERVOORWAARDENAANWEZIG
#endif

#if !COLIBRI && !MUNT && !ASR && !AEGON && !VENN && !DELTA && !DYNAMIC && !LLOYDS && !LOT && !OBVION && !PHILIPS && !REAAL && !TULP
#define ONTBVWDNDT
#endif

#if !RABO && !ABN && !ABNGRP && !AEGON && !ATTENS && !NN && !BLG && !MONEYOU && !ALLIANZ && !ASN && !MERIUS && !OBVION && !PHILIPS && !REGIO && !SNS
#define EXTRALENINGDEELINFO1
#endif

#if !ASR && !HYPOTRUST && !COLIBRI && !MUNT && !VENN && !CENTRAAL && !WOONNU && !NIBC && !DELTA && !ARGENTA && !ASN && !BIJBOUWE && !BNP && !DYNAMIC && !HWOONT && !IQWOON && !LLOYDS && !LOT && !MERIUS && !OBVION && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !TULP && !VISTA && !WFONDS
#define EXTRALENINGDEELINFO2
#endif

#if !RABO && !BLG && !ABN && !ABNGRP && !AEGON && !ATTENS && !MONEYOU && !ASN && !MERIUS && !OBVION && !PHILIPS && !REGIO && !SNS
#define BEDRAGHUIDIGLENINGDEELBOX3
#endif

#if !AEGON && !COLIBRI && !ING && !MONEYOU && !RABO && !VENN && !WOONNU && !DELTA && !BIJBOUWE && !BNP && !DYNAMIC && !IQWOON && !LOT && !MERIUS && !OBVION && !ROBUUST && !VISTA
#define BRUTOHYPOTHEEKLASTEN
#endif

#if !MUNT && !VENN && !OBVION
#define VERKOOPONDERVOORWAARDETYPE
#endif

using Finix;
using System;
using System.Collections.Generic;
using System.Text;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        protected void BuildHuidigObjects()
        {
            void BuildHuidigObject(WoonsituatieEnum? situatie, HSKoopFieldT huidigObject, PersoonsGegevensFieldT persoon)
            {
                if (situatie != WoonsituatieEnum.Koopwoning)
                    return;

                HuidigObjectEntiteitType huidigObjectEntiteit = new HuidigObjectEntiteitType();

#if CODEOBJECT
                huidigObjectEntiteit.CodeObject = SpecialEnums.GetHdnCodeObject(huidigObject.SoortWoning);
#endif

                if (huidigObject.HypothecairOnbelast != DataBooleanFieldT.@true)
                {
#if AANVRAGERBESTAANDEHYPOTHEEK
                    huidigObjectEntiteit.AanvragerBestaandeHypotheek = _clientRef;
#endif
                    huidigObjectEntiteit.HuidigeFinancieringJN = BooleanType.JJa;
                }
                else
                {
                    huidigObjectEntiteit.HuidigeFinancieringJN = BooleanType.NNee;
                }

                huidigObjectEntiteit.PlaatsNaam = persoon.Plaats;
                huidigObjectEntiteit.StraatNaam = persoon.Straat;
                huidigObjectEntiteit.HuisNr = persoon.Huisnummer;
                huidigObjectEntiteit.HuisNrToevoeging = persoon.Huisnummertoevoeging;
                huidigObjectEntiteit.Postcode = persoon.Postcode;
                bool teVerkopen = false;
                if (_hypBer.SoortScenario.GetValueOrDefault().IsAny(SoortScenarioEnum.Aankoopbestaandebouw, SoortScenarioEnum.Aankoopnieuwbouw))
                    if (_finixDossier.HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.HuidigeWoonSituatieClient == WoonsituatieEnum.Koopwoning)
                        teVerkopen = true;

                huidigObjectEntiteit.TeVerkopen = Enums.GetHdnBooleanType(teVerkopen);
                if (huidigObjectEntiteit.TeVerkopen == BooleanType.JJa)
                {
                    huidigObjectEntiteit.VerkoopPrijs = Utils.GetHdnDecimal(huidigObject.VVW);
                }
                // kennelijk moet dit veld ook gezet worden als er niet wordt verkocht, in ieder geval voor NN,  anders fout in controle xml
#if VERKOOPONDERVOORWAARDENAANWEZIG
                VerkoopOnderVoorwaardenConstructieType GetHDNVerkoopOnderVoorwaarden(VerkoopOnderVoorwaardenEnum? finixEnum)
                {
                    if (finixEnum == null)
                    {
                        HDNRESULT.GlobalOutputInfo.buildErrors.Add("GetHDNVerkoopOnderVoorwaarden: ongeldige parameter (null), default waarde gebruikt");
                        return (VerkoopOnderVoorwaardenConstructieType)0;
                    }

                    string finixName = finixEnum.ToName();
                    foreach (VerkoopOnderVoorwaardenConstructieType enumValue in Enum.GetValues(typeof(VerkoopOnderVoorwaardenConstructieType)))
                    {
                        var ff = enumValue.ToName();
                        if (enumValue.ToName() == finixName)
                            return enumValue;
                    }
                    HDNRESULT.GlobalOutputInfo.buildErrors.Add("GetHDNVerkoopOnderVoorwaarden: ongeldige parameter, default waarde gebruikt");
                    return (VerkoopOnderVoorwaardenConstructieType)0;

                }
                Utils.SetHdnYesNo(huidigObject.VerkoopOnderVoorwaardenAanwezig, huidigObjectEntiteit, nameof(huidigObjectEntiteit.VerkoopOnderVoorwaardenJN));
#if VERKOOPONDERVOORWAARDETYPE
                if (huidigObject.VerkoopOnderVoorwaardenAanwezig == DataBooleanFieldT.@true)
                    huidigObjectEntiteit.VerkoopOnderVoorwaardenConstructie = GetHDNVerkoopOnderVoorwaarden(huidigObject.VerkoopOnderVoorwaardeType);
#endif
#endif
                huidigObjectEntiteit.VerkochtJN = Enums.GetHdnBooleanType(huidigObject.Verkocht);
                Utils.SetHdnYesNo(huidigObject.VerkoopakteAanwezig, huidigObjectEntiteit, nameof(huidigObjectEntiteit.VerkoopakteJN));

                if (huidigObject.Verkocht == DataBooleanFieldT.@true)
                {
                    huidigObjectEntiteit.VerkoopPrijs = Utils.GetHdnDecimal(huidigObject.VVW);
                    huidigObjectEntiteit.VerkoopDt = Utils.GetHdnDate(huidigObject.Verkoopdatum);
#if ONTBVWDNDT
                    huidigObjectEntiteit.OntbVwdnDt = Utils.GetHdnDate(huidigObject.EinddatumOntbVwd);
#endif
                }
                else
                {
                    huidigObjectEntiteit.MarktWaarde = Utils.GetHdnDecimal(huidigObject.VVW);
                }
                huidigObjectEntiteit.OverbruggingJN = Enums.GetHdnBooleanType(_hypBer.Overbrugging);

                if (huidigObjectEntiteit.OverbruggingJN == BooleanType.JJa)
                {
 //                   huidigObjectEntiteit.OverbruggingsBedrag = Utils.GetHdnDecimal(_hypBer.OverbruggingBedrag);
#if OVERBRUGGINGSRENTE
                    huidigObjectEntiteit.OverbruggingsRente = OverbruggingsRenteType.Item01Vasterente;
#endif
                }

                if (huidigObject.HypothecairOnbelast == DataBooleanFieldT.@false)
                {
                    HuidigeFinancieringEntiteitType HuidigeFinanciering = new HuidigeFinancieringEntiteitType
                    {
                        Volgnummer = "1",
                        AflossenJN = BooleanType.NNee,
                        FinancierBestaandeHyp = Enums.GetHdnMaatschappij(huidigObject.Geldverstrekker)
                    };
                    if (_hypBer.SoortScenario == SoortScenarioEnum.Oversluiten)
                        HuidigeFinanciering.AflossenJN = BooleanType.JJa;
                    if ((_hypBer.ScenariosVervolgID.HypotheekMeeverhuizen == DataBooleanFieldT.@false) &&
                        (_hypBer.SoortScenario.GetValueOrDefault().IsAny(SoortScenarioEnum.Aankoopbestaandebouw, SoortScenarioEnum.Aankoopnieuwbouw)))
                        HuidigeFinanciering.AflossenJN = BooleanType.JJa;

                    HuidigeFinanciering.EigenWonSchuld = Utils.GetHdnDecimal(huidigObject.Deel1BedragBox1.GetValueOrDefault() +
                        huidigObject.Deel2BedragBox1.GetValueOrDefault() + huidigObject.Deel3BedragBox1.GetValueOrDefault() +
                        huidigObject.Deel4BedragBox1.GetValueOrDefault());
                    HuidigeFinanciering.SaldoBestaandeHyp = HuidigeFinanciering.EigenWonSchuld + Utils.GetHdnDecimal(huidigObject.Deel1BedragBox3.GetValueOrDefault() +
                        huidigObject.Deel2BedragBox3.GetValueOrDefault() + huidigObject.Deel3BedragBox3.GetValueOrDefault() +
                        huidigObject.Deel4BedragBox3.GetValueOrDefault());
                    decimal rente = (huidigObject.Deel1BedragBox1.GetValueOrDefault() + huidigObject.Deel1BedragBox3.GetValueOrDefault()) * (huidigObject.Deel1Rente.GetValueOrDefault() / 100);
                    rente += (huidigObject.Deel2BedragBox1.GetValueOrDefault() + huidigObject.Deel2BedragBox3.GetValueOrDefault()) * (huidigObject.Deel2Rente.GetValueOrDefault() / 100);
                    rente += (huidigObject.Deel3BedragBox1.GetValueOrDefault() + huidigObject.Deel3BedragBox3.GetValueOrDefault()) * (huidigObject.Deel3Rente.GetValueOrDefault() / 100);
                    rente += (huidigObject.Deel4BedragBox1.GetValueOrDefault() + huidigObject.Deel4BedragBox3.GetValueOrDefault()) * (huidigObject.Deel4Rente.GetValueOrDefault() / 100);
#if HUIDIGEBEDRAGEN
                    HuidigeFinanciering.HuidigRenteBedrag = Utils.GetHdnDecimalZonderAfronden(rente);
                    Utils.SetHdnDecimal(0, HuidigeFinanciering, nameof(HuidigeFinanciering.HuidigInlegBedrag));

#endif
#if BRUTOHYPOTHEEKLASTEN
                    HuidigeFinanciering.BrutoHypotheekLasten = Utils.GetHdnDecimalZonderAfronden(rente);
#endif
                    HuidigeFinanciering.OorsprHoofdsom = Utils.GetHdnDecimal(huidigObject.Deel1OorsprBedrag.GetValueOrDefault() +
                        huidigObject.Deel2OorsprBedrag.GetValueOrDefault() + huidigObject.Deel3OorsprBedrag.GetValueOrDefault() +
                        huidigObject.Deel4OorsprBedrag.GetValueOrDefault());
//                    HuidigeFinanciering.Garantie = GarantieType.Item03geengarantie;
//                    if (huidigObject.NHGgarantie.GetValueOrDefault() == DataBooleanFieldT.@true)
//                        HuidigeFinanciering.Garantie = GarantieType.Item01NHGgarantie;
                    HuidigeFinanciering.RangOrde = "1";
                    if (_hypBer.SoortScenario.GetValueOrDefault().IsAny(SoortScenarioEnum.Omzettenevtverhogenbestaandleningdeel, SoortScenarioEnum.Tweedehypotheek))
                        HuidigeFinanciering.RangOrde = "2";
                    HuidigeFinanciering.NrBestaandeHyp = huidigObject.HypotheekNummer;
                    HuidigeFinanciering.OorspronkelijkeIngDt = Utils.GetHdnDate(huidigObject.Deel1IngangsDatum);
                    HuidigeFinanciering.BedragInschrijving = HuidigeFinanciering.EigenWonSchuld;
                    if (huidigObject.HypothecaireInschrijving > 0)
                        HuidigeFinanciering.BedragInschrijving = Utils.GetHdnDecimal(huidigObject.HypothecaireInschrijving.GetValueOrDefault());

                    AddHuidigeLeningdelen(HuidigeFinanciering, huidigObject);

                    huidigObjectEntiteit.Volgnummer = (huidigObjectEntiteit.HuidigeFinanciering.Count + 1).ToString();
                    huidigObjectEntiteit.HuidigeFinanciering.Add(HuidigeFinanciering);
                }
                _hdnAanvraag.HuidigObject.Add(huidigObjectEntiteit);
            }

            BuildHuidigObject(
                _finixDossier.HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.HuidigeWoonSituatieClient,
                _finixDossier.HuidigeSituatie.HuidigeWoonsituatieClient.HuidigeWoonsituatie.Koop,
                _finixDossier.HuidigeSituatie.AlgemeneGegevens.ClientGegevens);

            if (_finixDossier.HuidigeSituatie.AlgemeneGegevens.PartnerAanwezig == DataBooleanFieldT.@true)
            {
                BuildHuidigObject(
                    _finixDossier.HuidigeSituatie.OnderpandGegevens.Specificatiegegevens.HuidigeWoonSituatiePartner,
                    _finixDossier.HuidigeSituatie.HuidigeWoonsituatiePartner.HuidigeWoonsituatie.Koop,
                    _finixDossier.HuidigeSituatie.AlgemeneGegevens.PartnerGegevens);
            }
        }

        private void AddHuidigeLeningdelen(HuidigeFinancieringEntiteitType huidigeFinanciering, HSKoopFieldT huidigObject)
        {
            HuidigLeningdeelEntiteitType HuidigLeningdeel;

            for (int i = 1; i <= 4; i++)
            {

                decimal BedragHuidigLeningdeelBox1 = Utils.GetDecimalValue(huidigObject, "Deel" + i.ToString() + "BedragBox1");
                decimal BedragHuidigLeningdeelBox3 = Utils.GetDecimalValue(huidigObject, "Deel" + i.ToString() + "BedragBox3");

                decimal BedragHuidigLeningdeel = BedragHuidigLeningdeelBox1 + BedragHuidigLeningdeelBox3;

                if (BedragHuidigLeningdeel <= 0)
                    continue;

                int rvp = Utils.GetIntValue(huidigObject, "Deel" + i.ToString() + "RVP");
                if (rvp == 0)
                    continue;

                BelastingBoxType box = BelastingBoxType.Item01Box1;
                if (BedragHuidigLeningdeelBox3 > 0)
                {
                    box = (BedragHuidigLeningdeelBox1 > 0) ? BelastingBoxType.Item04Box1Box3 : BelastingBoxType.Item01Box1;
                }

                HuidigLeningdeel = new HuidigLeningdeelEntiteitType
                {



#if EXTRALENINGDEELINFO1
                HuidigLeningdeel.BelastingBoxHuidigLeningdeel = box;
#if EXTRALENINGDEELINFO2
                HuidigLeningdeel.InlegBedragHuidigLeningdeel = 0.00M;
                HuidigLeningdeel.OorspronkelijkIngDtHuidigLeningDeel = Utils.GetHdnDate(Utils.GetStringValue(huidigObject, "Deel" + i.ToString() + "IngangsDatum"));

                DateTime dateTime = Convert.ToDateTime(Utils.GetStringValue(huidigObject, "Deel" + i.ToString() + "IngangsDatum")).AddMonths(rvp);
                HuidigLeningdeel.EindDtHuidigRentevastPeriode = Utils.GetHdnDate(dateTime);
#endif
#endif
                    BedragHuidigLeningdeel = BedragHuidigLeningdeel
                };
                HuidigLeningdeel.RestantSaldoHuidigLeningdeel = HuidigLeningdeel.BedragHuidigLeningdeel;
                HuidigLeningdeel.RenteVastInMndHuidigLeningdeel = Utils.GetIntValue(huidigObject, "Deel" + i.ToString() + "OorsprRVP").ToString();
                HuidigLeningdeel.RentePctHuidigLeningdeel = Utils.GetDecimalValue(huidigObject, "Deel" + i.ToString() + "Rente", true);
                HuidigLeningdeel.AflossingsVormHuidigLeningdeel = Enums.GetHdnAflossingsVormType((Finix.SoortHypotheekEnum)Utils.GetIntValue(huidigObject, "Deel" + i.ToString() + "SoortHypotheek"));
                HuidigLeningdeel.DuurInMndHuidigLeningdeel = Utils.GetIntValue(huidigObject, "Deel" + i.ToString() + "Looptijd").ToString();
#if BEDRAGHUIDIGLENINGDEELBOX3
                Utils.SetHdnDecimal(BedragHuidigLeningdeelBox3, HuidigLeningdeel, nameof(HuidigLeningdeel.ConsumptiefBedragHuidigLeningdeel));
#endif
                HuidigLeningdeel.Volgnummer = (huidigeFinanciering.HuidigLeningdeel.Count + 1).ToString();




                if (_hypBer.SoortScenario == SoortScenarioEnum.Tweedehypotheek)
                {
                    HuidigLeningdeel.AflossenJN = BooleanType.NNee;
                }
                else
                {
                    HuidigLeningdeel.AflossenJN = BooleanType.JJa;
                    if (_hypBer.ScenariosVervolgID.BestaandeLeningenInConstructie != null)
                        if (_hypBer.ScenariosVervolgID.BestaandeLeningenInConstructie[i].Meeverhuisd == DataBooleanFieldT.@true)
                            HuidigLeningdeel.AflossenJN = BooleanType.NNee;
                }


                huidigeFinanciering.HuidigLeningdeel.Add(HuidigLeningdeel);
            }
        }
    }
}
