﻿#if AEGON || BLG || CENTRAAL || HYPOTRUST || ING || ARGENTA || ASN || BIJBOUWE || HWOONT || IQWOON  || OBVION  || ROBUUST || SNS || WFONDS || DYNAMIC || TEMPLATE 
#define CODERENTEMIJ
#endif

#if ING || DNBG || TEMPLATE 
#define COMPLEXPRODUCTJN
#endif

#if BLG || ING || NN || RABO || ASN || ROBUUST || SNS || TEMPLATE 
#define BANKGARANTIEBIJAANBIEDERJN
#endif

#if ABN || ABNGRP || MONEYOU || NN || BNP || TEMPLATE 
#define LENINGDEELPRODUCTNAAM
#endif

#if ING || RABO || TEMPLATE 
#define CODEHYPOTHEEKOPTIESMIJ
#endif

#if !AEGON && !RABO && !ING && !BLG && !LOT && !MERIUS && !OBVION && !PHILIPS && !REGIO
#define SOORTADVIES
#endif

#if !AEGON && !HYPOTRUST && !COLIBRI && !VENN && !WOONNU && !BLG && !DELTA && !ARGENTA && !ALLIANZ && !ASN && !BNP && !DYNAMIC && !HWOONT && !IQWOON && !LOT && !PHILIPS && !REAAL && !REGIO && !SNS && !VISTA && !WFONDS
#define MAATWERKOPLOSSING
#endif

#if !COLIBRI && !VENN && !CENTRAAL && !WOONNU && !BLG && !DELTA && !ASN && !BIJBOUWE &&  !DYNAMIC && !IQWOON && !LOT && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !VISTA
#define MAATWERKOPLSPEC
#endif

#if !AEGON && !ING && !NN && !WOONNU && !DYNAMIC && !PHILIPS
#define LENINGDEELFISCAALREGIME
#endif

#if !DNGB && !HWOONT && !OBVION && !TULP
#define MAATWERKOPLCODEMIJ
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        protected virtual string GetRenteVastInMnd(LeningdeelDataT leningdeelFinix)
        {
            return leningdeelFinix.DuurRVPMaanden.GetValueOrDefault().ToString();
        }

        protected virtual string GetDuurInMnd(LeningdeelDataT leningdeelFinix)
        {
            return (leningdeelFinix.Looptijd.GetValueOrDefault() * 12 + leningdeelFinix.LooptijdInMnd.GetValueOrDefault()).ToString();
        }

        

        protected void BuildLening()
        {
            string? bestaandeHypNr()
            {
                string? nr = null;

                HSKoopFieldT onderpand = SpecialUtils.GetToekomstigOnderpandFinancieel(_finixDossier.HuidigeSituatie);
                if (onderpand != null)
                    nr = onderpand.HypotheekNummer;
                return nr;
            }

            _hdnAanvraag.Lening.Volgnummer = "1";
            _hdnAanvraag.Lening.Financier = GetFinancier();
            _hdnAanvraag.Lening.RangOrde = "1";
            if (_hypBer.SoortScenario.GetValueOrDefault().IsAny(SoortScenarioEnum.Omzettenevtverhogenbestaandleningdeel, SoortScenarioEnum.Tweedehypotheek))
                _hdnAanvraag.Lening.RangOrde = "2";
            _hdnAanvraag.Lening.NrBestaandeHyp = bestaandeHypNr();

            if ((_hypBer.SoortScenario == SoortScenarioEnum.Aankoopbestaandebouw) |
                (_hypBer.SoortScenario == SoortScenarioEnum.Uitponden) |
                (_hypBer.SoortScenario == SoortScenarioEnum.Aankoopnieuwbouw))
            {
                _hdnAanvraag.Lening.AankoopWoningJN = BooleanType.JJa;
            }
            else
            {
                _hdnAanvraag.Lening.AankoopWoningJN = BooleanType.NNee;
            }

            decimal totaalHypotheekbedragMeeverhuisd = 0.00M;
            //total += _hypBer.HypotheekConstructie.LeningDelen.Where(item => item.Arrangement != "").Sum(item => item.BedragBox1.GetValueOrDefault());
            //total += _hypBer.HypotheekConstructie.LeningDelen.Where(item => item.Arrangement != "").Sum(item => item.BedragBox3.GetValueOrDefault());
            if (_hypBer.ScenariosVervolgID.HypotheekMeeverhuizen == DataBooleanFieldT.@true)
                for (int i = 0; i < 4; i++)
                {
                    BestaandeLeningInConstructieDataT bestaand = _hypBer.ScenariosVervolgID.BestaandeLeningenInConstructie[i];
                    if (bestaand.Meeverhuisd == DataBooleanFieldT.@true)
                    {
                        totaalHypotheekbedragMeeverhuisd += bestaand.Box1Bedrag.GetValueOrDefault();
                        totaalHypotheekbedragMeeverhuisd += bestaand.Box3Bedrag.GetValueOrDefault();
                    }
                }
            _hdnAanvraag.Lening.HypotheekBedrag = totaalHypotheekbedragMeeverhuisd;
            decimal vergoeding = _hypBer.Adviesvergoedingen.Sum(item => (item.AantalUur.GetValueOrDefault() * item.Uurtarief.GetValueOrDefault()) + item.VastBedrag.GetValueOrDefault() + item.VastBedragBox3.GetValueOrDefault());
            _hdnAanvraag.Lening.NettoAdvieskosten = Utils.GetHdnDecimal(vergoeding);
            _hdnAanvraag.Lening.PasseerDt = Utils.GetHdnDate(_hypBer.Ingangsdatum);
#if  ABN || ABNGRP  || MONEYOU || TMPLATE 
            _hdnAanvraag.Lening.IngangMutatieDt = Utils.GetHdnDate(_hypBer.Ingangsdatum);
            _hdnAanvraag.Lening.ArrangementsNr = _finixDossier.ExtraHDNInfo.ArrangementNummer;
#endif

            if ((_hypBer.SoortScenario == SoortScenarioEnum.Aankoopbestaandebouw) |
                (_hypBer.SoortScenario == SoortScenarioEnum.Uitponden) |
                (_hypBer.SoortScenario == SoortScenarioEnum.Aankoopnieuwbouw))
            {
                if (_onderpand.Bankgarantie == DataBooleanFieldT.@true)
                {
                    if (_onderpand.Bankgarantiebedrag.HasValue)
                    {
                        _hdnAanvraag.Lening.BankGarantieBedrag = Utils.GetHdnDecimal(_onderpand.Bankgarantiebedrag.GetValueOrDefault());
                        _hdnAanvraag.Lening.BankGarantieKosten = Utils.GetHdnDecimal(_onderpand.Bankgarantiekosten.GetValueOrDefault());
                    }
                }
            }

            MutatieType? mutatie;
            if (SpecialUtils.IsNieuweWoning(_hypBer.SoortScenario))
            {
                mutatie = SpecialEnums.GetHdnMutatie(_onderpand.HOCodeSoortMutatie);
            }
            else
            {
                HSKoopFieldT HuidigeWoonSituatie = SpecialUtils.GetToekomstigOnderpandFinancieel(_finixDossier.HuidigeSituatie);
                mutatie = SpecialEnums.GetHdnMutatie(HuidigeWoonSituatie.HOCodeSoortMutatie);
            }
            if (mutatie != null)
                _hdnAanvraag.Lening.Mutatie = mutatie.GetValueOrDefault();

#if SOORTADVIES
            _hdnAanvraag.Lening.SoortAdvies = AdviesTypeType.Item04AdviesBemiddeling;
#endif
#if COMPLEXPRODUCTJN
            _hdnAanvraag.Lening.ComplexProductJN = BooleanType.JJa;
#endif
            switch (_hypBer.SoortScenario)
            {
                case SoortScenarioEnum.Aankoopbestaandebouw:
                    BestaandeBouwDataT bestaandebouw = _hypBer.AankoopBestaandeBouw;
                    _hdnAanvraag.Lening.HypotheekBedrag = Utils.GetHdnDecimal(bestaandebouw.IngevoerdFinancieringBehoefte) + totaalHypotheekbedragMeeverhuisd;
                    _hdnAanvraag.Lening.Regeling = RegelingType.Item01eerstehypotheeknieuw;
                    _hdnAanvraag.Lening.NHG = Enums.GetHdnBooleanType(bestaandebouw.NHG);
                    if (_hdnAanvraag.Lening.NHG == BooleanType.JJa)
                        _hdnAanvraag.Lening.NHGKostenSpec = Utils.GetHdnDecimalZonderAfronden(bestaandebouw.BorgtochtprovisieNHG);
                    decimal inschrijving = Math.Max(bestaandebouw.HypothecaireInschrijving.GetValueOrDefault(),
                        bestaandebouw.IngevoerdFinancieringBehoefte.GetValueOrDefault());
                    _hdnAanvraag.Lening.HypothecaireInschrijving = Utils.GetHdnDecimal(inschrijving);
                    decimal bedrag = bestaandebouw.EigenGeld.GetValueOrDefault() + bestaandebouw.EigenWoningReserve.GetValueOrDefault() + bestaandebouw.EigenWoningReservePartner.GetValueOrDefault();
                    Utils.SetHdnDecimal(bedrag, _hdnAanvraag.Lening, nameof(_hdnAanvraag.Lening.IngebEigenMiddelen));
#if BANKGARANTIEBIJAANBIEDERJN
                    _hdnAanvraag.Lening.BankgarantieBijAanbiederJN = Enums.GetHdnBooleanType(_finixDossier.Onderpanden[ActiveOnderpand].BankgarantieWaarborg.GetValueOrDefault());
#endif
                    if (_onderpand.Bankgarantie == DataBooleanFieldT.@true)
                        _hdnAanvraag.Lening.BankGarantieBedrag = Utils.GetHdnDecimal(_onderpand.Bankgarantiebedrag.GetValueOrDefault());

                    break;
                case SoortScenarioEnum.Aankoopnieuwbouw:
                    NieuwbouwDataT nieuwbouw = _hypBer.AankoopNieuwbouw;
                    _hdnAanvraag.Lening.HypotheekBedrag = Utils.GetHdnDecimal(nieuwbouw.IngevoerdFinancieringBehoefte) + totaalHypotheekbedragMeeverhuisd;
                    _hdnAanvraag.Lening.Regeling = RegelingType.Item01eerstehypotheeknieuw;
                    _hdnAanvraag.Lening.NHG = Enums.GetHdnBooleanType(nieuwbouw.NHG);
                    if (_hdnAanvraag.Lening.NHG == BooleanType.JJa)
                        _hdnAanvraag.Lening.NHGKostenSpec = Utils.GetHdnDecimalZonderAfronden(nieuwbouw.BorgtochtprovisieNHG);
                    inschrijving = Math.Max(nieuwbouw.HypothecaireInschrijving.GetValueOrDefault(),
                        nieuwbouw.IngevoerdFinancieringBehoefte.GetValueOrDefault());
                    _hdnAanvraag.Lening.HypothecaireInschrijving = Utils.GetHdnDecimal(inschrijving);
                    bedrag = nieuwbouw.EigenGeld.GetValueOrDefault() + nieuwbouw.EigenWoningReserve.GetValueOrDefault() + nieuwbouw.EigenWoningReservePartner.GetValueOrDefault();
                    Utils.SetHdnDecimal(bedrag, _hdnAanvraag.Lening, nameof(_hdnAanvraag.Lening.IngebEigenMiddelen));
#if BANKGARANTIEBIJAANBIEDERJN
                    _hdnAanvraag.Lening.BankgarantieBijAanbiederJN = Enums.GetHdnBooleanType(_finixDossier.Onderpanden[ActiveOnderpand].BankgarantieWaarborg.GetValueOrDefault());
#endif
                    if (_onderpand.Bankgarantie == DataBooleanFieldT.@true)
                        _hdnAanvraag.Lening.BankGarantieBedrag = Utils.GetHdnDecimal(_onderpand.Bankgarantiebedrag.GetValueOrDefault());
                    break;
                case SoortScenarioEnum.Uitponden:
                    _hdnAanvraag.Lening.HypotheekBedrag = Utils.GetHdnDecimal(_hypBer.AankoopBestaandeBouw.IngevoerdFinancieringBehoefte) + totaalHypotheekbedragMeeverhuisd;
                    _hdnAanvraag.Lening.NHG = Enums.GetHdnBooleanType(_hypBer.AankoopBestaandeBouw.NHG);
                    if (_hdnAanvraag.Lening.NHG == BooleanType.JJa)
                        _hdnAanvraag.Lening.NHGKostenSpec = Utils.GetHdnDecimalZonderAfronden(_hypBer.AankoopBestaandeBouw.BorgtochtprovisieNHG);                    
                    inschrijving = Math.Max(_hypBer.AankoopBestaandeBouw.HypothecaireInschrijving.GetValueOrDefault(),
                        _hypBer.AankoopBestaandeBouw.IngevoerdFinancieringBehoefte.GetValueOrDefault());
                    _hdnAanvraag.Lening.HypothecaireInschrijving = Utils.GetHdnDecimal(inschrijving);
                    break;
                case SoortScenarioEnum.Item1stehypotheekonbelastewoning:
                    _hdnAanvraag.Lening.HypotheekBedrag = Utils.GetHdnDecimal(_hypBer.TweedeHypotheek.IngevoerdFinancieringBehoefte) + totaalHypotheekbedragMeeverhuisd;
                    _hdnAanvraag.Lening.NHG = Enums.GetHdnBooleanType(_hypBer.TweedeHypotheek.NHG);
                    if (_hdnAanvraag.Lening.NHG == BooleanType.JJa)
                        _hdnAanvraag.Lening.NHGKostenSpec = Utils.GetHdnDecimalZonderAfronden(_hypBer.TweedeHypotheek.BorgtochtprovisieNHG);
                    inschrijving = Math.Max(_hypBer.TweedeHypotheek.HypothecaireInschrijving.GetValueOrDefault(),
                        _hypBer.TweedeHypotheek.IngevoerdFinancieringBehoefte.GetValueOrDefault());
                    _hdnAanvraag.Lening.HypothecaireInschrijving = Utils.GetHdnDecimal(inschrijving);
                    break;
                case SoortScenarioEnum.Omzettenevtverhogenbestaandleningdeel:
                    _hdnAanvraag.Lening.HypotheekBedrag = Utils.GetHdnDecimal(_hypBer.Verhogen.IngevoerdFinancieringBehoefte) + totaalHypotheekbedragMeeverhuisd;
                    _hdnAanvraag.Lening.Regeling = RegelingType.Item05omzetting;
                    _hdnAanvraag.Lening.NHG = Enums.GetHdnBooleanType(_hypBer.Verhogen.NHG);
                    if (_hdnAanvraag.Lening.NHG == BooleanType.JJa)
                        _hdnAanvraag.Lening.NHGKostenSpec = Utils.GetHdnDecimalZonderAfronden(_hypBer.Verhogen.BorgtochtprovisieNHG);
                    break;
                case SoortScenarioEnum.Oversluiten:
                    OversluitenDataT oversluiten = _hypBer.Oversluiten;
                    _hdnAanvraag.Lening.HypotheekBedrag = Utils.GetHdnDecimal(oversluiten.IngevoerdFinancieringBehoefte) + totaalHypotheekbedragMeeverhuisd;

                    MaatschappijType orgFinMij = Enums.GetHdnMaatschappij(SpecialUtils.GetToekomstigOnderpandFinancieel(_finixDossier.HuidigeSituatie).Geldverstrekker);
                    if (orgFinMij == GetFinancier())
                        _hdnAanvraag.Lening.Regeling = RegelingType.Item07interneoversluiting;
                    else
                        _hdnAanvraag.Lening.Regeling = RegelingType.Item08oversluitinganderegeldgever;
                    _hdnAanvraag.Lening.NHG = Enums.GetHdnBooleanType(oversluiten.NHG);
                    if (_hdnAanvraag.Lening.NHG == BooleanType.JJa)
                        _hdnAanvraag.Lening.NHGKostenSpec = Utils.GetHdnDecimalZonderAfronden(oversluiten.BorgtochtprovisieNHG);

                    inschrijving = Math.Max(oversluiten.HypothecaireInschrijving.GetValueOrDefault(),
                        oversluiten.IngevoerdFinancieringBehoefte.GetValueOrDefault());
                    _hdnAanvraag.Lening.HypothecaireInschrijving = Utils.GetHdnDecimal(inschrijving);
                    bedrag = oversluiten.EigenGeld.GetValueOrDefault();
                    Utils.SetHdnDecimal(bedrag, _hdnAanvraag.Lening, nameof(_hdnAanvraag.Lening.IngebEigenMiddelen));
                    Utils.SetHdnDecimal(oversluiten.Boeterente, _hdnAanvraag.Lening, nameof(_hdnAanvraag.Lening.BoeteRente));

                    break;
                case SoortScenarioEnum.Tweedehypotheek:
                    _hdnAanvraag.Lening.Regeling = RegelingType.Item09tweedeofhogerinranghypotheek;
                    _hdnAanvraag.Lening.HypotheekBedrag = Utils.GetHdnDecimal(_hypBer.TweedeHypotheek.IngevoerdFinancieringBehoefte) + totaalHypotheekbedragMeeverhuisd;
                    inschrijving = Math.Max(_hypBer.TweedeHypotheek.HypothecaireInschrijving.GetValueOrDefault(),
                        _hypBer.TweedeHypotheek.IngevoerdFinancieringBehoefte.GetValueOrDefault());
                    _hdnAanvraag.Lening.HypothecaireInschrijving = Utils.GetHdnDecimal(inschrijving);
                    break;
                case SoortScenarioEnum.Verhogen:
                    _hdnAanvraag.Lening.Regeling = RegelingType.Item04onderhands;

                    break;
            }

            if (_hypBer.ScenariosVervolgID.HypotheekMeeverhuizen == DataBooleanFieldT.@true)
                _hdnAanvraag.Lening.Regeling = RegelingType.Item10meeneemhypotheek;

#if MAATWERKOPLOSSING
            _hdnAanvraag.Lening.MaatwerkOplossing = BooleanType.NNee;
#endif
#if MAATWERKOPLSPEC
            if ((_hypBer.CHFoke == DataBooleanFieldT.@false) && (_hdnAanvraag.Lening.NHG == BooleanType.NNee))
            {
#if MAATWERKOPLOSSING
                _hdnAanvraag.Lening.MaatwerkOplossing = BooleanType.JJa;
#endif
                if (_hypBer.ScenariosVervolgID.MotivatieRedenOmschrijving != null)
                {
                    MaatwerkOplSpecEntiteitType spec = new();
                    spec.Volgnummer = "1";
#if BLG || CENTRAAL || COLIBRI || DELTA || MUNT || VENN || WOONNU || ASN || BIJBOUWE || DYNAMIC || IQWOON || LLOYDS || LOT || MERIUS || PHILIPS || REAAL || REGIO || ROBUUST || SNS || TULP || VISTA || WFONDS
                    spec.RefPartijNAWData = _clientRef;
#endif
#if MAATWERKOPLCODEMIJ
                    spec.MaatwerkOplCodeMij = SpecialEnums.GetHdnMotivatieReden(_hypBer.ScenariosVervolgID.MotivatieRedenOmschrijving);                    
#else
                    // Bij HWOONT is MaatwerkOplCodeMij een string, jawel
                    spec.MaatwerkOplCodeMij = _hypBer.ScenariosVervolgID.MotivatieRedenOmschrijving;
#endif
                    spec.ExplainReden = ExplainRedenType.Item01Maatschappijspecifiek;
                    if (_hypBer.ScenariosVervolgID.RedenMotivatieToelichtingTekst != "")
                        spec.MaatwerkOplToelichting = _hypBer.ScenariosVervolgID.RedenMotivatieToelichtingTekst;

#if RABO
                    if (_hdnAanvraag.Lening.NHG == BooleanType.NNee)
                        spec.ExplainReden = ExplainRedenType.Item02Seniorenverhuisregeling;
#endif
                    _hdnAanvraag.Lening.MaatwerkOplSpec.Add(spec);
                }
            }
#endif

#if CODEHYPOTHEEKOPTIESMIJ
            if (GetHypotheekOpties() != null)
            {
#if ING
                var ff = new HypotheekOptiesEntiteitType();
                ff.CodeHypotheekOptiesMij = GetHypotheekOpties().GetValueOrDefault();
                _hdnAanvraag.Lening.HypotheekOpties.Add(ff);
#else
                _hdnAanvraag.Lening.HypotheekOpties.CodeHypotheekOptiesMij = GetHypotheekOpties().GetValueOrDefault();
#endif
            }
#endif
                    if (_hypBer.HypotheekConstructie.RisicoVerzekering.Product != null)
            {
                BuildNieuweRisicoVerzekering(_hypBer.HypotheekConstructie.RisicoVerzekering);
            }
            if (_hypBer.HypotheekConstructie.GemengdeVerzekering.Product != null)
            {
                BuildNieuweLevensVerzekering(_hypBer.HypotheekConstructie.GemengdeVerzekering);
            };
            _hdnAanvraag.Lening.CodeLeningMij = SpecialEnums.GetHdnCodeLeningMij(_hypBer.ScenariosVervolgID.CodeLeningMij);

            BuildLeningdelen();
        }

        protected void BuildLeningdelen()
        {
            var aantalLeningdelen = BuildNieuweLeningdelen();
            BuildMeeverhuisdeLeningdelen(aantalLeningdelen);
        }
        private int BuildNieuweLeningdelen()
        {
            int leningNr = 1;

            LeningdeelEntiteitType leningdeel;
            for (int i = 0; i < _hypBer.HypotheekConstructie.LeningDelen.Length - 1; i++)
            {
                var leningdeelFinix = _hypBer.HypotheekConstructie.LeningDelen[i];
                if (leningdeelFinix.Arrangement == null)
                    continue;

                decimal box1 = leningdeelFinix.BedragBox1.GetValueOrDefault();
                decimal box3 = leningdeelFinix.BedragBox3.GetValueOrDefault();

                leningdeel = new LeningdeelEntiteitType
                {
                    Volgnummer = leningNr.ToString(),
                    CodeLeningDeel = CodeLeningDeelType.Item1Nieuw,
                    AflossingsVorm = Enums.GetHdnAflossingsVormType(leningdeelFinix.Soort.GetValueOrDefault()),
                    RenteVastInMnd = GetRenteVastInMnd(leningdeelFinix),
                    BetalingsTermijn = BetalingsTermijnType.Item01permaand
                };

                if ((_finixDossier.ExtraHDNInfo.LeningInfo != null) && (_finixDossier.ExtraHDNInfo.LeningInfo.Length > i))
                {
                    leningdeel.RenteBedenkTijd = SpecialUtils.GetRenteBedenktijd(_finixDossier.ExtraHDNInfo.LeningInfo[i].RenteBedenkTijdMND.GetValueOrDefault());
                    leningdeel.RenteAfspraak = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[i].Soort.GetValueOrDefault());
                    if (leningdeel.RenteBedenkTijd != RenteBedenktijdType.Item01geen)
                        leningdeel.RenteBedenkTijdInMnd = _finixDossier.ExtraHDNInfo.LeningInfo[i].RenteBedenkTijdMND.GetValueOrDefault().ToString();
                }
                leningdeel.BetalingAchteraf = BooleanType.JJa;

#if CODERENTEMIJ
                leningdeel.CodeRenteMij = GetCodeRenteMijType(_hdnAanvraag.Lening.CodeLeningMij, leningdeelFinix, leningNr-1, null);
#endif
                leningdeel.RentePct = Utils.GetHdnDecimalZonderAfronden(leningdeelFinix.Rente.GetValueOrDefault());
                leningdeel.RenteAfspraakOmschr = leningdeelFinix.RentevastPeriode;
                
                leningdeel.BelastingBox = BelastingBoxType.Item01Box1;
                if (box3 > 0)
                {
                    leningdeel.BelastingBox = BelastingBoxType.Item03Box3;
                    if (box1 > 0)
                        leningdeel.BelastingBox = BelastingBoxType.Item04Box1Box3;
                }
                if (leningdeel.BelastingBox != BelastingBoxType.Item03Box3)
                    leningdeel.RenteAftrekEindDt = Utils.GetHdnDate(leningdeelFinix.EinddatumAftrekbaarheid);
#if RABO
                if (leningdeel.BelastingBox == BelastingBoxType.Item04Box1Box3)
                    leningdeel.BelastingBox = BelastingBoxType.Item01Box1;
#endif
                leningdeel.CodeDeelMij = GetCodeDeelMijType(leningdeelFinix, _hdnAanvraag.Lening.CodeLeningMij);
#if LENINGDEELPRODUCTNAAM
                leningdeel.ProductNaam = leningdeel.CodeDeelMij.ToName()[6..];
#endif
                leningdeel.LeningDeelBedrag = Utils.GetHdnDecimal(box1 + box3);
                Utils.SetHdnDecimal(box3, leningdeel, nameof(leningdeel.ConsumptiefBedrag));

                leningdeel.DuurInMnd = GetDuurInMnd(leningdeelFinix);
                leningdeel.LeningDeelBedrag = leningdeelFinix.BedragBox1.GetValueOrDefault() + leningdeelFinix.BedragBox3.GetValueOrDefault();
                leningdeel.VasteEindDtLeningdeelJN = BooleanType.NNee;

#if LENINGDEELFISCAALREGIME
                leningdeel.FiscaalRegime = SpecialEnums.GetHdnFiscaalRegime(leningdeelFinix.Fiscaalregime);
#endif

                // todo kijken waar ie dan heen moet, je zou zeggen als losse dekking maar in huidige AX wordt ie weggelaten.
                if (leningdeel.AflossingsVorm != AflossingsVormType.Item05Aflossingsvrij)
                    BuildMeeverhuisdeDekkingen(leningdeel, Enums.GetOngewijzigdvoortzettenNieuwEnumValue(leningNr));

                if (leningdeelFinix.RisicoVerzekering.Product != null)
                {
                    BuildNieuweRisicoVerzekering(leningdeelFinix.RisicoVerzekering, leningdeel);
                }

                for (int x = 0; x < leningdeelFinix.GemengdeVerzekeringen.Length; x++)
                    if (leningdeelFinix.GemengdeVerzekeringen[x].Product != null)
                    {
                        // als deze verzekering ClientenPartnerapart heeft dan is het ineens een spaarrekening, man man
                        if (leningdeelFinix.GemengdeVerzekeringen[x].Verzekerden == EigenaarEnum.ClientenPartnerapart)
                            BuildNieuweSpaarRekening(leningdeelFinix.GemengdeVerzekeringen[x], leningdeel);
                        else
                            BuildNieuweLevensVerzekering(leningdeelFinix.GemengdeVerzekeringen[x], leningdeel);
                    }

                // todo, gemengd en beleg nog toevoegen ala risico

                _hdnAanvraag.Lening.Leningdeel.Add(leningdeel);
                leningNr++;
            }
            return leningNr;
        }

        private void BuildMeeverhuisdeLeningdelen(int leningNr)
        {
            if (_hypBer.ScenariosVervolgID.BestaandeLeningenInConstructie == null)
                return;

            int meeverhuizingen = _hypBer.ScenariosVervolgID.BestaandeLeningenInConstructie.Count(item => item.Meeverhuisd == DataBooleanFieldT.@true);

            // de mee te verhuizen leningdelen zijn altijd de laatste in de rij
            // dus bij 0 -> niets, bij 1 de vierde, bij 2 de derde en de vierde enz.

            LeningdeelEntiteitType leningdeel;
            BestaandeLeningInConstructieDataT finixLeningdeel;

            for (int i = 1; i < _hypBer.ScenariosVervolgID.BestaandeLeningenInConstructie.Length; i++)
            {
                finixLeningdeel = _hypBer.ScenariosVervolgID.BestaandeLeningenInConstructie[i];
                if (finixLeningdeel.Meeverhuisd == DataBooleanFieldT.@false)
                    continue;

                leningdeel = new LeningdeelEntiteitType
                {
                    Volgnummer = leningNr.ToString(),
                    BetalingAchteraf = BooleanType.JJa
                };
#if CODERENTEMIJ
                
                leningdeel.CodeRenteMij = GetCodeRenteMijType(_hdnAanvraag.Lening.CodeLeningMij, null, 0, finixLeningdeel.MeeverhuisRenteIdentiek);
#endif
                if (finixLeningdeel.Meeverhuisd == DataBooleanFieldT.@true)
                    leningdeel.CodeLeningDeel = CodeLeningDeelType.Item2Meeneemzelfdegeldgeveranderobject;
                else if (finixLeningdeel.Omgezet == DataBooleanFieldT.@true)
                    leningdeel.CodeLeningDeel = CodeLeningDeelType.Item4Omzettingandereaflosvorm;
                else if (_hypBer.SoortScenario == SoortScenarioEnum.Verhogen)
                    leningdeel.CodeLeningDeel = CodeLeningDeelType.Item5Verhoginghogergeldbedrag;

                HSKoopFieldT HuidigeWoonSituatie;
                if (_hypBer.ScenariosVervolgID.HypotheekClientMeeverhuizen == DataBooleanFieldT.@true)
                {
                    HuidigeWoonSituatie = _finixDossier.HuidigeSituatie.HuidigeWoonsituatieClient.HuidigeWoonsituatie.Koop;
                }
                else
                {
                    HuidigeWoonSituatie = _finixDossier.HuidigeSituatie.HuidigeWoonsituatiePartner.HuidigeWoonsituatie.Koop;
                }

#if LENINGDEELFISCAALREGIME
                DateTime ingangsdatum = Utils.GetDateValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "IngangsDatum");
                if (ingangsdatum > new DateTime(2013, 1, 1))
                    leningdeel.FiscaalRegime = FiscaleRegimeType.Item02Vanaf112013;
                else
                    leningdeel.FiscaalRegime = FiscaleRegimeType.Item01Voor112013;
#endif

#if BLG || ING || NN || ASN || REGIO || SNS
                leningdeel.NrBestaandeHyp = HuidigeWoonSituatie.HypotheekNummer;
#endif

                leningdeel.DuurInMnd = Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "Looptijd").ToString();

                decimal box1 = Utils.GetDecimalValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "BedragBox1");
                decimal box3 = Utils.GetDecimalValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "BedragBox3");
                leningdeel.BelastingBox = BelastingBoxType.Item01Box1;
                if (box3 > 0)
                {
                    leningdeel.BelastingBox = BelastingBoxType.Item03Box3;
                    if (box1 > 0)
                        leningdeel.BelastingBox = BelastingBoxType.Item04Box1Box3;
                }

                SoortHypotheekEnum soort = (SoortHypotheekEnum)Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "SoortHypotheek");
                // todo hoe kom je aan een codeldeelmij van een andere aanbieder in de context van deze aanbieder?
                //leningdeel.CodeDeelMij = GetCodeDeelMijType(null, null);
                leningdeel.LeningDeelBedrag = Utils.GetHdnDecimal(box1 + box3);
                leningdeel.ConsumptiefBedrag = Utils.GetHdnDecimal(box3);
                leningdeel.RentePct = Utils.GetDecimalValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "Rente");
                leningdeel.VasteEindDtLeningdeelJN = BooleanType.NNee;
                leningdeel.RenteVastInMnd = Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "OorsprRVP").ToString();

                int rbt = Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "RBT");
                if (rbt == 0)
                    leningdeel.RenteBedenkTijd = RenteBedenktijdType.Item01geen;
                else
                {
                    leningdeel.RenteBedenkTijdInMnd = rbt.ToString();
                    leningdeel.RenteBedenkTijd = RenteBedenktijdType.Item03achteraf;
                }


                leningdeel.BetalingsTermijn = BetalingsTermijnType.Item01permaand;
                leningdeel.RentePct = Utils.GetHdnDecimal(finixLeningdeel.ToekomstRente.GetValueOrDefault());
                leningdeel.RenteVastInMnd = Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "OorsprRVP").ToString();
                leningdeel.AflossingsVorm = Enums.GetHdnAflossingsVormType((Finix.SoortHypotheekEnum)Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "SoortHypotheek"));
                leningdeel.DuurInMnd = Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "Looptijd").ToString();

                int rvp = Utils.GetIntValue(HuidigeWoonSituatie, "Deel" + i.ToString() + "OorsprRVP");

                if (rbt == 0)
                {
                    if (rvp >= 12)
                        leningdeel.RenteAfspraak = RenteAfspraakType.Item01rentevast;
                    else
                        leningdeel.RenteAfspraak = RenteAfspraakType.Item03continuvariabel;
                }
                else
                {
                    leningdeel.RenteAfspraak = RenteAfspraakType.Item02rentedempendsysteem;
                }

                // todo kijken waar ie dan heen moet, je zou zeggen als losse dekking maar in huidige AX wordt ie weggelaten.
                if (leningdeel.AflossingsVorm != AflossingsVormType.Item05Aflossingsvrij)
                    BuildMeeverhuisdeDekkingen(leningdeel, Enums.GetOngewijzigdvoortzettenBestaandEnumValue(i));

                _hdnAanvraag.Lening.Leningdeel.Add(leningdeel);
                leningNr++;
            }
        }

        private void BuildMeeverhuisdeDekkingen(LeningdeelEntiteitType leningdeel, KoppelingBestaandEnum koppeling)
        {
            if (_finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeLevenPolissen != null)
                foreach (GemengdeVerzekeringDataT verzekering in _finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeLevenPolissen)
                {
                    if (verzekering.BestaandePolisVerwerking == koppeling)
                    {
                        BuildMeeverhuisdeLevensVerzekering(verzekering, leningdeel);
                    }
                }

            if (_finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeSpaarPolissen != null)
                foreach (GemengdeVerzekeringDataT verzekering in _finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeSpaarPolissen)
                {
                    if (verzekering.BestaandePolisVerwerking == koppeling)
                    {
                        // todo, deze komen niet mee in de AX? 
                        //BuildMeeverhuisdeLevensVerzekering(verzekering, leningdeel);
                    }
                }

            if (_finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeRisicoPolissen != null)
                foreach (RisicoVerzekeringDataT verzekering in _finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeRisicoPolissen)
                {
                    if (verzekering.BestaandePolisVerwerking == koppeling)
                    {
                        BuildMeeverhuisdeRisicoVerzekering(verzekering, leningdeel);
                    }
                }

            if (_finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeBeleggingsRekeningen != null)
                foreach (BeleggingsrekeningDataT rekening in _finixDossier.HuidigeSituatie.BestaandeVoorzieningenGegevens.BestaandeBeleggingsRekeningen)
                {
                    if (rekening.BestaandeRekeningVerwerking == koppeling)
                    {
                        BuildMeeverhuisdeSpaarRekening(rekening, leningdeel);
                    }
                }
        }

        protected bool IsRestschuldLening(LeningdeelDataT leningdeelFinix)
        {
            int i = Constants.AantalLeningdelenInScenario - 1;
            while ((i > 0) && (_hypBer.HypotheekConstructie.LeningDelen[i].Arrangement == ""))
                i--;

            bool laatsteDeel = (_hypBer.HypotheekConstructie.LeningDelen[i] == leningdeelFinix);
            bool restschuldMeefinancieren = false;

            if (_hypBer.SoortScenario.GetValueOrDefault().IsAny(SoortScenarioEnum.Aankoopnieuwbouw, SoortScenarioEnum.Aankoopbestaandebouw, SoortScenarioEnum.Uitponden))
                if (_hypBer.ScenariosVervolgID.RestSchuldMeefinancieren > 0)
                    restschuldMeefinancieren = true;

            return laatsteDeel && restschuldMeefinancieren;
        }
    }
}
