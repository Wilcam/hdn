﻿#if !RABO && !MERIUS && !PHILIPS
#define TEKSTREGELS
#endif

using System;
using System.Collections.Generic;
using System.Text;

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        private void BuildTekstRegels()
        {

#if TEKSTREGELS
            Utils.SetHdnString(_hypBer.ScenariosVervolgID.Status.statusHDN.HDN.Tekstregel1, _hdnAanvraag.TekstRegels, nameof(_hdnAanvraag.TekstRegels.TekstRegel));
            Utils.SetHdnString(_hypBer.ScenariosVervolgID.Status.statusHDN.HDN.Tekstregel2, _hdnAanvraag.TekstRegels, nameof(_hdnAanvraag.TekstRegels.TekstRegel2));
            Utils.SetHdnString(_hypBer.ScenariosVervolgID.Status.statusHDN.HDN.Tekstregel3, _hdnAanvraag.TekstRegels, nameof(_hdnAanvraag.TekstRegels.TekstRegel3));
            Utils.SetHdnString(_hypBer.ScenariosVervolgID.Status.statusHDN.HDN.Tekstregel4, _hdnAanvraag.TekstRegels, nameof(_hdnAanvraag.TekstRegels.TekstRegel4));
            Utils.SetHdnString(_hypBer.ScenariosVervolgID.Status.statusHDN.HDN.Tekstregel5, _hdnAanvraag.TekstRegels, nameof(_hdnAanvraag.TekstRegels.TekstRegel5));
#endif
        }
    }
}
