﻿using System;
using System.Collections.Generic;
using System.Text;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        protected virtual void BuildHeader()
        {
            _hdnAanvraag.Header.AanvraagVersie = _hypBer.ScenariosVervolgID.HDNAXAanvraagVersieNr.ToString();
            _hdnAanvraag.Header.VerzendDt = Utils.GetHdnDate(DateTime.Now);
            _hdnAanvraag.Header.VerzendTijd = Utils.GetHdnTime(DateTime.Now);
#if !LX
            _hdnAanvraag.Header.BerichtSoort = BerichtSoortType.AXOfferteAanvraag;
#else
            _hdnAanvraag.Header.BerichtSoort = BerichtSoortType.LXLevenAanvraag;
#endif
            _hdnAanvraag.Header.PakketNaam = "FinixPlus";
            _hdnAanvraag.Header.PakketVersie = "1.0";

            _hdnAanvraag.Header.AanvraagVolgNr = _hypBer.ScenariosVervolgID.hypotheekberekeningID;
            _hdnAanvraag.Header.OntvangerCode = GetOntvangerCode();
            _hdnAanvraag.Header.BerichtVersie = Constants.HdnVersieNummer;

            _hdnAanvraag.Header.OntvangerNaam = GetOntvangerCode().ToName(); // _finixDossier.BrowseOnlyMij;
            _hdnAanvraag.Header.OntvangerNrHDN = _hypBer.ScenariosVervolgID.HDNAXNaarOntvangerNummerVerstuurd;

            _hdnAanvraag.Header.VerzenderNaam = _finixDossier.ExtraHDNInfo.VerzenderNaam;
            _hdnAanvraag.Header.VerzenderNrHDN = _finixDossier.ExtraHDNInfo.VerzenderNrHDN;
            _hdnAanvraag.Header.AanvraagVolgNr = _finixDossier.ExtraHDNInfo.AanvraagVolgnummer;
        }
    }
}
