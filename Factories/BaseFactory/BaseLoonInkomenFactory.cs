﻿#if ABN || AEGON || ARGENTA || MERIUS  || TEMPLATE
#define BEROEPFUNCTIE
#endif

#if !ABN && !ABNGRP && !ATTENS && !COLIBRI && !ING && !MONEYOU && !MUNT && !NN && !RABO && !DELTA && !BNP && !DYNAMIC && !MERIUS && !PHILIPS && !REAAL && !SYNTRUS && !TELLIUS && !VISTA
#define DIENSTBETREKKINGINFO
#endif 

#if !RABO && !ASR && !ABN && !ABNGRP && !AEGON && !ATTENS && !COLIBRI && !ING && !MUNT && !VENN && !MONEYOU && !NN && !NIBC && !BLG && !DELTA && !ASN && !DYNAMIC && !LLOYDS && !LOT && !MERIUS && !OBVION && !PHILIPS && !REAAL && !REGIO && !SNS && !SYNTRUS && !TELLIUS && !TULP
#define DIENSTBETREKKINGBIJFAMILIE
#endif

#if !BLG && !ASN && !OBVION && !REGIO && !SNS
#define INGANGSDTDIENSTBETREKKING
#endif

#if !AEGON && !RABO && !ING && !NN && !WOONNU && !CENTRAAL && !HYPOTRUST && !NIBC && !ALLIANZ && !BIJBOUWE && !HWOONT && !IQWOON && !LOT && !OBVION && !ROBUUST
#define GEMURENWEEK
#endif

#if !AEGON && !RABO && !ING && !NN && !WOONNU && !BNP && !DYNAMIC && !HWOONT && !MERIUS && !PHILIPS && !ROBUUST
#define GEMURENWEEK2
#endif

#if !VENN && !DELTA && !BNP && !TULP
#define DIENSTBETREKKINGLIST
#endif

#if !MUNT && !VENN && !COLIBRI && !WOONNU && !DELTA && !BNP && !DYNAMIC && !HWOONT && !LLOYDS && !LOT && !PHILIPS && !REAAL && !TULP
#define BEROEP
#endif

#if !ATTENS && !VENN && !COLIBRI && !HYPOTRUST && !CENTRAAL && !NN && !WOONNU && !DELTA && !ARGENTA && !ALLIANZ && !BIJBOUWE && !BNP && !DYNAMIC && !HWOONT && !IQWOON && !LLOYDS && !MERIUS && !PHILIPS && !REAAL && !ROBUUST && !SYNTRUS && !TELLIUS && !TULP && !VISTA && !WFONDS
#define ARBEIDSMARKTSCANJN
#endif

#if !AEGON && !COLIBRI && !NN && !WOONNU && !BLG && !ASN && !BNP && !DYNAMIC && !HWOONT && !LOT && !MERIUS && !REGIO && !ROBUUST && !SNS
#define BRUTOJAARINKVARIABEL
#endif

#if !ASR && !ABN && !MUNT && !ATTENS && !RABO && !VENN && !AEGON && !COLIBRI && !ABNGRP && !MONEYOU && !NN && !WOONNU && !BLG && !DELTA && !ASN && !DYNAMIC && !HWOONT && !LOT && !OBVION && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !SYNTRUS && !TELLIUS && !VISTA
#define HUISNRTOEVOEGINGWERKGEVER
#endif

#if !AEGON
#define SOORTDIENSTBETREKKING
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        static void BuildLoonInkomen(string idRef, PersoonsGegevensFieldT persoon, InkomenItemDataT[] loonInkomens, SpecificatieInkomenFieldT specificatieInkomen,
            BijAfVerItemDataT bijAf, AnalyseHypotheekverledenDataT hypVerleden, persoonFieldT extraPersoon, ZelfstandigInkomenGegevensFieldT onderneming,
            UPODataT[] upos, OndernemingDataT[] ondernemingen, string pensioenDatum, HypotheekgeverEntiteitType hypotheekGever)
        {
            int volgNummerDienstBetrekking = 1;
   
            if (specificatieInkomen.HeeftSVLoon == DataBooleanFieldT.@true)
            {
                hypotheekGever.Inkomsten.InkomensbepalingLoondienst.AantalDienstbetrekkingen = specificatieInkomen.SVloonAantalDienstverbanden.ToString();
                Utils.SetHdnDecimal(specificatieInkomen.SVloon, hypotheekGever.Inkomsten.InkomensbepalingLoondienst, nameof(hypotheekGever.Inkomsten.InkomensbepalingLoondienst.Toetsinkomen));
                Utils.SetHdnDecimal(specificatieInkomen.SVloonPensioenbijdrage, hypotheekGever.Inkomsten.InkomensbepalingLoondienst, nameof(hypotheekGever.Inkomsten.InkomensbepalingLoondienst.TotaalPensioenbijdrage));

#if DIENSTBETREKKINGINFO
                DienstBetrekkingEntiteitType1 dienstBetrekking1;
                for (int i = 0; i < 3; i++)
                {
                    if ((i == 0) && (specificatieInkomen.InkomenLoon == DataBooleanFieldT.@false))
                        continue;
                    if ((i == 1) && (specificatieInkomen.InkomenLoon1 == DataBooleanFieldT.@false))
                        continue;
                    if ((i == 2) && (specificatieInkomen.InkomenLoon2 == DataBooleanFieldT.@false))
                        continue;


                    InkomenItemDataT loonInkomen = loonInkomens[i];
                    dienstBetrekking1 = new DienstBetrekkingEntiteitType1();

#if DIENSTBETREKKINGBIJFAMILIE
                    Utils.SetHdnYesNo(loonInkomen.DienstbetrekkingBijFamilie, dienstBetrekking1, nameof(dienstBetrekking1.DienstbetrekkingBijFamilieJN));
#endif
#if INGANGSDTDIENSTBETREKKING
                    dienstBetrekking1.IngangsDtDienstBetrekking = Utils.GetHdnDate(loonInkomen.DatumInDienst);
#endif
#if SOORTDIENSTBETREKKING
                    dienstBetrekking1.SoortDienstBetrekking = SpecialEnums.GetHdnDienstBetrekking(loonInkomen.SoortDienstverband);
#endif
#if GEMURENWEEK
                    dienstBetrekking1.GemUrenWeek = loonInkomen.UrenPerWeek.GetValueOrDefault().ToString();
#endif
                    dienstBetrekking1.Volgnummer = volgNummerDienstBetrekking.ToString();
                    volgNummerDienstBetrekking++;
#if DIENSTBETREKKINGLIST
                    hypotheekGever.Inkomsten.InkomensbepalingLoondienst.DienstBetrekking.Add(dienstBetrekking1);
#else
                    hypotheekGever.Inkomsten.InkomensbepalingLoondienst.DienstBetrekking = dienstBetrekking1;
#endif
                }
#endif
                return;
            }

            for (int i = 0; i < 3; i++)
            {
                if ((i == 0) && (specificatieInkomen.InkomenLoon == DataBooleanFieldT.@false))
                    continue;
                if ((i == 1) && (specificatieInkomen.InkomenLoon1 == DataBooleanFieldT.@false))
                    continue;
                if ((i == 2) && (specificatieInkomen.InkomenLoon2 == DataBooleanFieldT.@false))
                    continue;

                InkomenItemDataT loonInkomen = loonInkomens[i];
                       
                if (loonInkomen.SoortDienstverband.GetValueOrDefault().IsAny(SoortDienstverbandEnum .WIAIVAuitkering80arbeidsongeschikt, SoortDienstverbandEnum.WIAWGAuitkering3580arbeidsongeschikt, SoortDienstverbandEnum.Vroegprepensioen, SoortDienstverbandEnum.ANWNabestaandeuitkering, SoortDienstverbandEnum.Nabestaandenpensioen, SoortDienstverbandEnum.Pensioen))
                {
                    var inkomen = 0.00M;
                    if (loonInkomen.SoortDienstverband == SoortDienstverbandEnum.Pensioen)
                    {
                        inkomen = Utils.GetHdnDecimal(loonInkomen.Pensioen.GetValueOrDefault());
                    }
                    else
                    {
                        inkomen = Utils.GetHdnDecimal(loonInkomen.Inkomen.GetValueOrDefault() * Utils.GetTermijnenPerJaar(loonInkomen.InkomenTermijn));
                    }

                    if (inkomen == 0)
                        continue;

                    UitkeringEntiteitType uitkering = new();
                    uitkering.SoortUitkering = SpecialEnums.GetHdnUitkering(loonInkomen.SoortDienstverband);
                    uitkering.BrutoJaarUitkering = inkomen;
                    uitkering.IngangsDtUitkering = Utils.GetHdnDate(loonInkomen.DatumInDienst);
                    uitkering.Volgnummer = (hypotheekGever.Inkomsten.Uitkering.Count + 1).ToString();
                    hypotheekGever.Inkomsten.Uitkering.Add(uitkering);
                    continue;
                };

                DienstBetrekkingEntiteitType dienstBetrekking = new();
#if GEMURENWEEK2
                dienstBetrekking.GemUrenWeek = loonInkomen.UrenPerWeek.GetValueOrDefault().ToString();
#endif
#if DIENSTBETREKKINGBIJFAMILIE 
                Utils.SetHdnYesNo(loonInkomen.DienstbetrekkingBijFamilie, dienstBetrekking, nameof(dienstBetrekking.DienstbetrekkingBijFamilieJN));
#endif
                Utils.SetHdnYesNo(loonInkomen.Proeftijd, dienstBetrekking, nameof(dienstBetrekking.ProeftijdJN));
                if (loonInkomen.Proeftijd == JaNeeEnum.Ja)
                {
                    DateTime dateTime = Convert.ToDateTime(loonInkomen.EindDatumProeftijd);
                    dienstBetrekking.ProeftijdVerstrekenJN = (dateTime < DateTime.Now) ? BooleanType.JJa : BooleanType.NNee;
                }
                dienstBetrekking.IngangsDtDienstBetrekking = Utils.GetHdnDate(loonInkomen.DatumInDienst);
                dienstBetrekking.EindDtDienstBetrekking = Utils.GetHdnDate(loonInkomen.ContractEindDatum);
                dienstBetrekking.BrutoJaarSalaris = Utils.GetHdnDecimal(loonInkomen.Inkomen.GetValueOrDefault() * Utils.GetTermijnenPerJaar(loonInkomen.InkomenTermijn));
#if BEROEP 
                dienstBetrekking.Beroep = SpecialEnums.GetHdnBeroep(loonInkomen.Beroep).GetValueOrDefault();
#endif

#if BEROEPFUNCTIE
                dienstBetrekking.BeroepFunctie = loonInkomen.Beroep;
#endif
#if ARBEIDSMARKTSCANJN                
                Utils.SetHdnYesNo(loonInkomen.Arbeidsmarktscan, dienstBetrekking, nameof(dienstBetrekking.ArbeidsmarktscanJN));
#endif

#if BRUTOJAARINKVARIABEL
                Utils.SetHdnDecimal(loonInkomen.Tantiemes + loonInkomen.Bonus, dienstBetrekking, nameof(dienstBetrekking.BrutoJaarInkVariabel));

                // add 'overig inkomen' only to the first 'dienstBetrekking'
                if (loonInkomen == loonInkomens[0])
                {
                    if (specificatieInkomen.InkomenOverig == DataBooleanFieldT.@true)
                    {
                        static decimal addBijAf(OverigInkomenFieldT inkomen)
                        {
                            if (inkomen.Soort.GetValueOrDefault().HasFlag(InkomenSoortEnum.Buitenlandsinkomen | InkomenSoortEnum.Huurinkomsten))
                            {
                                return inkomen.Bedrag.GetValueOrDefault();
                            }
                            return 0;
                        }
                        dienstBetrekking.BrutoJaarInkVariabel += addBijAf(bijAf.OverigInkomen1);
                        dienstBetrekking.BrutoJaarInkVariabel += addBijAf(bijAf.OverigInkomen2);
                        dienstBetrekking.BrutoJaarInkVariabel += addBijAf(bijAf.OverigInkomen3);
                    }

                }
#endif
                dienstBetrekking.LoonbeslagJN = BooleanType.NNee;
                if (loonInkomen.ClientLoonbeslag == DataBooleanFieldT.@true)
                    dienstBetrekking.LoonbeslagJN = BooleanType.JJa;
                dienstBetrekking.Werkgever = loonInkomen.NaamWerkgever;
                dienstBetrekking.StraatNaamWerkgever = loonInkomen.HOStraatnaamWerkgever;
                dienstBetrekking.HuisNrWerkgever = loonInkomen.HOHuisnummerWerkgever;
#if HUISNRTOEVOEGINGWERKGEVER 
                dienstBetrekking.HuisNrToevoegingWerkgever = loonInkomen.HOHuisnummerToevoegingWerkgever;
#endif
                dienstBetrekking.PostcodeWerkgever = loonInkomen.HOPostcodeWerkgever;
                dienstBetrekking.PlaatsNaamWerkgever = loonInkomen.PLaatsWerkgever;
                dienstBetrekking.SoortDienstBetrekking = SpecialEnums.GetHdnDienstBetrekking(loonInkomen.SoortDienstverband);
                dienstBetrekking.VEB = Utils.GetHdnDecimal(loonInkomen.VEBtoeslag);

                if (!(loonInkomen.SoortDienstverband.GetValueOrDefault().IsAny(SoortDienstverbandEnum.Flexwerker, SoortDienstverbandEnum.Seizoensarbeider,
                    SoortDienstverbandEnum.flexibelearbeidsrelatiemetperspectiefverklaring, SoortDienstverbandEnum.flexibelearbeidsrelatiezonderperspectiefverklaring,
                    SoortDienstverbandEnum.Vroegprepensioen, SoortDienstverbandEnum.Nabestaandenpensioen, SoortDienstverbandEnum.Wajong)))
                {
                    dienstBetrekking.VakantieToeslag = 1.00M;
                    dienstBetrekking.VakantieToeslag = Utils.GetHdnDecimal(loonInkomen.Inkomen.GetValueOrDefault() *
                        Utils.GetTermijnenPerJaar(loonInkomen.InkomenTermijn) * (loonInkomen.Vakantiegeld.GetValueOrDefault() / 100));
                }
                dienstBetrekking.Volgnummer = volgNummerDienstBetrekking.ToString();

                Utils.SetHdnDecimal(loonInkomen.Provisie, dienstBetrekking, nameof(dienstBetrekking.ProvisiePerJaar));
                Utils.SetHdnDecimal(loonInkomen.FlexJaarinkomenVorigJaar, dienstBetrekking, nameof(dienstBetrekking.InkomenFlexibel1jr));
                Utils.SetHdnDecimal(loonInkomen.FlexJaarinkomen2Jaargeleden, dienstBetrekking, nameof(dienstBetrekking.InkomenFlexibel2jr));
                Utils.SetHdnDecimal(loonInkomen.FlexJaarinkomen3Jaargeleden, dienstBetrekking, nameof(dienstBetrekking.InkomenFlexibel3jr));

                Utils.SetHdnDecimal(loonInkomen.Overwerk * Utils.GetTermijnenPerJaar(loonInkomen.InkomenTermijn), dienstBetrekking, nameof(dienstBetrekking.OverwerkPerJaar));
                Utils.SetHdnDecimal(loonInkomen.OnregelmatigheidsToeslag * Utils.GetTermijnenPerJaar(loonInkomen.InkomenTermijn), dienstBetrekking, nameof(dienstBetrekking.OnregelmatigheidsToeslag));
                Utils.SetHdnDecimal(loonInkomen.VastEindeJaar, dienstBetrekking, nameof(dienstBetrekking.VasteEindejaarsuitkering));

                if (loonInkomen.Maand13 == DataBooleanFieldT.@true)
                    dienstBetrekking.Vaste13eMnd = Utils.GetHdnDecimal(dienstBetrekking.BrutoJaarSalaris / 12);
                else if (dienstBetrekking.Vaste13eMnd != null)
                    dienstBetrekking.Vaste13eMnd = 0.00M;

                hypotheekGever.Inkomsten.DienstBetrekking.Add(dienstBetrekking);

                volgNummerDienstBetrekking++;
            }
        }
    }
}

