﻿#if AEGON || BLG || CENTRAAL || HYPOTRUST || ING || ARGENTA || ASN || BIJBOUWE || HWOONT || IQWOON || OBVION || REGIO || ROBUUST || SNS || WFONDS || DYNAMIC || TEMPLATE 
#define CODERENTEMIJ
#endif

#if ABN || ABNGRP || ING || RABO || TEMPLATE
#define CODEHYPOTHEEKOPTIESMIJ
#endif

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Finix;
using HDNRESULT;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif



namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly int ActiveHypBer = 0;
        private readonly int ActiveOnderpand = 0;
        private REFPartijNAWDataType _clientRef;
        private REFPartijNAWDataType _partnerRef;
        protected HypotheekBerekeningItemDataT _hypBer;
        protected OnderpandItemDataT _onderpand;
        protected NewDossierDataT _finixDossier;
        protected OfferteAanvraagType _hdnAanvraag;

        public OfferteAanvraagType HdnAanvraag
        {
            get { return _hdnAanvraag; }
        }
        public NewDossierDataT FinixDossier
        {
            get { return _finixDossier; }
        }

        public BaseFactory(string finixImportFile, bool isContent = true)
        {
            CultureInfo culture = CultureInfo.CurrentCulture.Clone() as CultureInfo;
            culture.NumberFormat.CurrencyDecimalSeparator = ".";
            culture.NumberFormat.NumberDecimalSeparator = ".";
            CultureInfo.CurrentCulture = culture;

            GlobalOutputInfo.assembly = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

            _finixDossier = new NewDossierDataT();
            //System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(NewDossierDataT.get, new XmlRootAttribute("response"));
            System.Xml.Serialization.XmlSerializer x = new(_finixDossier.GetType());
            if (isContent)
            {
                _finixDossier = (NewDossierDataT)x.Deserialize(new StringReader(finixImportFile));
            }
            else
            {
                using FileStream fileStreamIn = new(finixImportFile, FileMode.Open);
                _finixDossier = (NewDossierDataT)x.Deserialize(fileStreamIn);
                fileStreamIn.Close();
            }

            for (int i = 0; i < _finixDossier.Scenarios.Length-1; i++)
            {
                if (_finixDossier.Scenarios[i].ScenariosVervolgID.IsDefinitieveBerekening == DataBooleanFieldT.@true)
                    ActiveHypBer = i;
            }
            _hypBer = _finixDossier.Scenarios[ActiveHypBer];
            if (_finixDossier.Onderpanden.Length > ActiveOnderpand)
                _onderpand = _finixDossier.Onderpanden[ActiveOnderpand];
            else
                _onderpand = null;
            _hdnAanvraag = new OfferteAanvraagType();
        }

        static public bool IsCorrectCodeLeningMij(string codeMij)
        {
            return SpecialUtils.IsCorrectCodeLeningMij(codeMij);
        }

        public OutputInfo Run(string saveAXFile)
        {

            if (Build())
            {
                Check();
                Save(saveAXFile);
            }
            return new OutputInfo();
        }

        public OutputInfo Run()
        {
            if (Build())
            {
                Check();
                Save();
            }
            return new OutputInfo();
        }

        protected bool Build()
        {
            bool succes = true;
            try
            {
                GlobalOutputInfo.Init();
                log4net.GlobalContext.Properties["assembly"] = Assembly.GetExecutingAssembly().GetName().Name + "_" + Assembly.GetExecutingAssembly().GetName().Version + "_";
                log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));

                BuildHeader();
                BuildTussenPersoon();
                BuildHypotheekgevers();
                BuildHuidigObjects();
                BuildObject();
                BuildLening();
                BuildNotaris();
                BuildTekstRegels();
            }
            catch (ArgumentException e)
            {
                log.InfoFormat("{0}", e.ToString());
                GlobalOutputInfo.buildErrors.Add(e.Message);
                succes = false;
            }
            catch (Exception e)
            {
                log.InfoFormat("{0}", e.ToString());
                GlobalOutputInfo.unkownErrors.Add(e.Message);
                succes = false;
            }
            return succes;
        }

        protected void Check()
        {
            ControleXml ControleXmla = new(_hdnAanvraag);
            ControleXmla.Check();
        }

        public OutputInfo OnlyCheck(string AXfileName)
        {
            GlobalOutputInfo.Init();
            log4net.GlobalContext.Properties["assembly"] = Assembly.GetExecutingAssembly().GetName().Name + "_" + Assembly.GetExecutingAssembly().GetName().Version + "_";
            log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));

            //_finixDossier = new NewDossierDataT();
            _hdnAanvraag = new OfferteAanvraagType();
            //System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(NewDossierDataT.get, new XmlRootAttribute("response"));
            System.Xml.Serialization.XmlSerializer x = new(_hdnAanvraag.GetType());
           
                using (FileStream fileStreamIn = new(AXfileName, FileMode.Open))
                {
                _hdnAanvraag = (OfferteAanvraagType)x.Deserialize(fileStreamIn);
                    fileStreamIn.Close();
                }
     
            ControleXml ControleXmla = new(_hdnAanvraag);
            ControleXmla.Check();
            return new OutputInfo();
        }

        protected void Save(string saveAXFile = "")
        {
            System.Xml.Serialization.XmlSerializer ax = new(_hdnAanvraag.GetType());
            using (StringWriter textWriter = new())
            {
                ax.Serialize(textWriter, _hdnAanvraag);
                GlobalOutputInfo.content = textWriter.ToString();
            }

            if (saveAXFile != "")
            {
                using FileStream fileStreamOut = new(saveAXFile, FileMode.OpenOrCreate & FileMode.Truncate);
                ax.Serialize(fileStreamOut, _hdnAanvraag);
            }
        }

        protected abstract OntvangerCodeType GetOntvangerCode();
        protected abstract MaatschappijType GetFinancier();
        protected abstract CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij);

#if CODERENTEMIJ
        protected abstract CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek);
#endif

#if CODEHYPOTHEEKOPTIESMIJ
        protected abstract CodeHypotheekOptiesMijType? GetHypotheekOpties();
#endif
    }
}
