﻿#if NN || TEMPLATE
#define BEHEEROVEREENKOMSTJN
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        protected void BuildTussenPersoon()
        {
            _hdnAanvraag.TussenPersoon.Volgnummer = "1";
            _hdnAanvraag.TussenPersoon.RefContactPersoonData = BuildNawTussenPersoon();
#if !LX
            _hdnAanvraag.TussenPersoon.BedrijfsNaam = _finixDossier.OrigineleAanvrager.bedrijfsnaam;
            _hdnAanvraag.TussenPersoon.Emailadres = _finixDossier.OrigineleAanvrager.email;
            Utils.SetHdnString(_finixDossier.OrigineleAanvrager.telefoon, _hdnAanvraag.TussenPersoon, nameof(_hdnAanvraag.TussenPersoon.TelefoonNrWerk), "-");
#endif
            _hdnAanvraag.TussenPersoon.AFMRegistratieNr = _finixDossier.OrigineleAanvrager.AFMRegistratieNr;
            _hdnAanvraag.TussenPersoon.TussenpersoonNr = _finixDossier.OrigineleAanvrager.TPNummer;
#if BEHEEROVEREENKOMSTJN            
            Utils.SetHdnYesNo(_hypBer.ScenariosVervolgID.Beheerovereenkomst, _hdnAanvraag.TussenPersoon, nameof(_hdnAanvraag.TussenPersoon.BeheerovereenkomstJN));
#endif
            if (_finixDossier.ExtraHDNInfo.OrigineleAanvrager != null)
                if (_finixDossier.ExtraHDNInfo.OrigineleAanvrager == DataBooleanFieldT.@false)
                {
                    _hdnAanvraag.TussenPersoon.ServiceProvider.Volgnummer = "1";
                    _hdnAanvraag.TussenPersoon.ServiceProvider.ServiceProviderNr = _finixDossier.ExtraHDNInfo.ServiceProvider.Nummer;
#if !LX
                    _hdnAanvraag.TussenPersoon.ServiceProvider.RefContactPersoonData = BuildNawServerprovider();                   
                    _hdnAanvraag.TussenPersoon.ServiceProvider.ServiceProviderNaam = _finixDossier.ExtraHDNInfo.ServiceProvider.BedrijfsNaam;
                    _hdnAanvraag.TussenPersoon.ServiceProvider.TelefoonNrWerk = _finixDossier.ExtraHDNInfo.ServiceProvider.Telefoon.Replace("-", string.Empty); ;
                    _hdnAanvraag.TussenPersoon.ServiceProvider.Emailadres = _finixDossier.ExtraHDNInfo.ServiceProvider.Email;
                    _hdnAanvraag.TussenPersoon.ServiceProvider.ServiceProviderAFMRegistratieNr = _finixDossier.ExtraHDNInfo.ServiceProvider._AFMNummer;
#endif
                }
        }

        protected REFPartijNAWDataType BuildNawTussenPersoon()
        {
            PartijNAWDataEntiteitType naw = new PartijNAWDataEntiteitType
            {
                ID = Constants.TussenPersoonRef,
                SoortPartij = SoortPartijType.Item02rechtspersoon,
#if LX
                BedrijfsNaam = _finixDossier.OrigineleAanvrager.bedrijfsnaam,
                Emailadres = _finixDossier.OrigineleAanvrager.email,
                TelefoonNummer = _finixDossier.OrigineleAanvrager.telefoon,
#endif
                PlaatsNaam = _finixDossier.OrigineleAanvrager.plaatsnaam,
                StraatNaam = _finixDossier.OrigineleAanvrager.straatnaam,
                HuisNr = _finixDossier.OrigineleAanvrager.huisnummer,
                HuisNrToevoeging = _finixDossier.OrigineleAanvrager.huisnummertoevoeging,
                Postcode = _finixDossier.OrigineleAanvrager.postcode,
                Geslacht = (GeslachtType)(int)_finixDossier.OrigineleAanvrager.GeslachtContactpersoon.GetValueOrDefault(),
                VoorLetters = _finixDossier.OrigineleAanvrager.VoorlettersContactpersoon,
                GebAchterNaam = _finixDossier.OrigineleAanvrager.AchternaamContactpersoon,
                GebTussenVoegsels = _finixDossier.OrigineleAanvrager.TussenvoegselContactpersoon,
                Land = LandType.NLNederland,              
            };

            naw.Volgnummer = (_hdnAanvraag.PartijNAWData.Count + 1).ToString();
            _hdnAanvraag.PartijNAWData.Add(naw);
            REFPartijNAWDataType refNaw = new REFPartijNAWDataType();
            refNaw.IDREF = Constants.TussenPersoonRef;
            return refNaw;
        }

        protected REFPartijNAWDataType BuildNawServerprovider()
        {
            PartijNAWDataEntiteitType naw = new PartijNAWDataEntiteitType
            {
                ID = Constants.ServiceProviderRef,
                SoortPartij = SoortPartijType.Item02rechtspersoon,
                PlaatsNaam = _finixDossier.ExtraHDNInfo.ServiceProvider.Plaatsnaam,
                StraatNaam = _finixDossier.ExtraHDNInfo.ServiceProvider.Straatnaam,
                HuisNr = _finixDossier.ExtraHDNInfo.ServiceProvider.Huisnummer,
                HuisNrToevoeging = _finixDossier.ExtraHDNInfo.ServiceProvider.Huisnummertoevoeging,
                Postcode = _finixDossier.ExtraHDNInfo.ServiceProvider.Postcode,
                Geslacht = (GeslachtType)(int)_finixDossier.ExtraHDNInfo.ServiceProvider.Geslacht.GetValueOrDefault(),
                VoorLetters = _finixDossier.ExtraHDNInfo.ServiceProvider.VoorLetters,
                GebAchterNaam = _finixDossier.ExtraHDNInfo.ServiceProvider.GebAchterNaam,
                //GebTussenVoegsels = _finixDossier.ExtraHDNInfo.ServiceProvider.,
                Land = LandType.NLNederland,
            };
            //naw.tu
            naw.Volgnummer = (_hdnAanvraag.PartijNAWData.Count + 1).ToString();
            _hdnAanvraag.PartijNAWData.Add(naw);
            REFPartijNAWDataType refNaw = new REFPartijNAWDataType();
            refNaw.IDREF = Constants.ServiceProviderRef;
            return refNaw;
        }
    }
}
