﻿using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        void BuildUitkering(PersoonsGegevensFieldT persoon, SpecificatieInkomenFieldT specificatieInkomen,
            BijAfVerItemDataT bijAf, persoonFieldT extraPersoon, UPODataT[] upos, string pensioenDatum, HypotheekgeverEntiteitType hypotheekGever)
        {
            UitkeringEntiteitType uitkering;

            DateTime pensioenDt = Convert.ToDateTime(pensioenDatum);
            DateTime EindDt = Convert.ToDateTime(_hypBer.Ingangsdatum).AddMonths(_hypBer.MaximaalhypLooptijd.GetValueOrDefault() * 12);

            if (pensioenDt < EindDt)
            {
                uitkering = new UitkeringEntiteitType();
                uitkering.SoortUitkering = SoortUitkeringType.Item09AOW;
                uitkering.AantalAOWJaren = (50 - persoon.KortingsjarenAOW.GetValueOrDefault()).ToString();
                uitkering.VolledigeAOWOpbouwJN = BooleanType.JJa;
                if (persoon.KortingsjarenAOW.GetValueOrDefault() > 0)
                    uitkering.VolledigeAOWOpbouwJN = BooleanType.NNee;
                uitkering.BrutoJaarUitkering = Utils.GetHdnDecimal((extraPersoon.AOWToeslagPerMaand.GetValueOrDefault() + extraPersoon.AOWUitkeringPerMaand.GetValueOrDefault()));
                uitkering.IngangsDtUitkering = Utils.GetHdnDate(extraPersoon.PensioenDatum);
                uitkering.Volgnummer = (hypotheekGever.Inkomsten.Uitkering.Count + 1).ToString();
                hypotheekGever.Inkomsten.Uitkering.Add(uitkering);
            }

            if (upos != null)
            {
                DateTime dateTime;
                DateTime eindeVroegPensioenDateTime = DateTime.Now;
                DateTime beginPensioenDateTime = DateTime.Now;
                for (int i = 0; i < upos.Length; i++)
                {
                    if (upos[i].UpoVroegPensioen.GetValueOrDefault() > 0)
                    {
                        uitkering = new UitkeringEntiteitType();
                        uitkering.SoortUitkering = SoortUitkeringType.Item15Ouderdomspensioen;
                        uitkering.BrutoJaarUitkering = Utils.GetHdnDecimal(upos[i].UpoVroegPensioen.GetValueOrDefault());
                        dateTime = Convert.ToDateTime(persoon.GeboorteDatum);
                        dateTime = dateTime.AddYears(upos[i].UpoStartLeeftijdVroegPensioen.GetValueOrDefault());
                        dateTime = dateTime.AddMonths(upos[i].UpoStartLeeftijdVroegPensioenInMnd.GetValueOrDefault());
                        uitkering.IngangsDtUitkering = Utils.GetHdnDate(dateTime);
                        eindeVroegPensioenDateTime = Convert.ToDateTime(persoon.GeboorteDatum);
                        eindeVroegPensioenDateTime = eindeVroegPensioenDateTime.AddYears(upos[i].UpoEindLeeftijdVroegPensioen.GetValueOrDefault());
                        eindeVroegPensioenDateTime = eindeVroegPensioenDateTime.AddMonths(upos[i].UpoEindLeeftijdVroegPensioenInMnd.GetValueOrDefault());
                        uitkering.EindDtUitkering = Utils.GetHdnDate(eindeVroegPensioenDateTime);
                        uitkering.Volgnummer = (hypotheekGever.Inkomsten.Uitkering.Count + 1).ToString();
                        hypotheekGever.Inkomsten.Uitkering.Add(uitkering);
                    }

                    if (upos[i].UpoOP.GetValueOrDefault() > 0)
                    {
                        uitkering = new UitkeringEntiteitType();
                        uitkering.SoortUitkering = SoortUitkeringType.Item15Ouderdomspensioen;
                        uitkering.BrutoJaarUitkering = Utils.GetHdnDecimal(upos[i].UpoOP.GetValueOrDefault());
                        uitkering.Volgnummer = (hypotheekGever.Inkomsten.Uitkering.Count + 1).ToString();

                        beginPensioenDateTime = Convert.ToDateTime(persoon.GeboorteDatum);
                        beginPensioenDateTime = beginPensioenDateTime.AddYears(upos[i].UpoLeeftijd.GetValueOrDefault());
                        beginPensioenDateTime = beginPensioenDateTime.AddMonths(upos[i].UpoLeeftijdInMnd.GetValueOrDefault());
                     
                        uitkering.IngangsDtUitkering = Utils.GetHdnDate(beginPensioenDateTime);
                       
                        hypotheekGever.Inkomsten.Uitkering.Add(uitkering);
                    }
                }
            }
        }
    }
}

