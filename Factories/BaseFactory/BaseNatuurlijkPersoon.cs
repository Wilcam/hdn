﻿#if ASR || CENTRAAL || HYPOTRUST || ING || NIBC || TEMPLATE
#define OPLEIDINGSNIVEAU
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {

      
        protected REFPartijNAWDataType BuildNatuurlijkPersoon(string idRef, PersoonsGegevensFieldT persoon)
        {
            PartijNAWDataEntiteitType naw = new PartijNAWDataEntiteitType
            {
                ID = idRef,
                GebAchterNaam = persoon.Geboortenaam,
                GeboortePlaats = persoon.HOGeboortePlaats,
                VoorNamen = persoon.vnmVoluit,
                GebTussenVoegsels = persoon.Voorvoegsel,
                Geslacht = (GeslachtType)(int)persoon.Geslacht.GetValueOrDefault(),
                SoortPartij = SoortPartijType.Item01natuurlijkpersoon,
                PlaatsNaam = persoon.Plaats,
                StraatNaam = persoon.Straat,
                HuisNr = persoon.Huisnummer,
                HuisNrToevoeging = persoon.Huisnummertoevoeging,
                Postcode = persoon.Postcode,
                VoorLetters = persoon.Voornaam,
                GeboorteDt = Utils.GetHdnDate(persoon.GeboorteDatum),
                RekeningNr = persoon.IBAN,
                Land = LandType.NLNederland,
#if LX
                Emailadres = persoon.HOEMailAdres,
                TelefoonNummer = persoon.HOTelefoonNrPrive,
                BurgerlijkeStaat = Enums.GetHdnBurgerlijkeStaatType(_finixDossier.HuidigeSituatie.AlgemeneGegevens.BurgerlijkeStaat),
#endif
            };

            Enums.SetHdnLand(persoon.HOGeboorteLand, naw, nameof(naw.GeboorteLand));
            Enums.SetHdnLand(persoon.HONationaliteit, naw, nameof(naw.Nationaliteit));

            if (persoon.Geboortenaam != persoon.Achternaam)
            {
                naw.CorrespondentieNaam = persoon.Achternaam;
                naw.CorrespondentieTussenvoegsels = persoon.Voorvoegsel;
            }

#if LX
            IdentificatieEntiteitType id = new IdentificatieEntiteitType();
            id.Volgnummer = "1";
            id.LegitimatieAfgifteDt = Utils.GetHdnDate(persoon.LegitimatieDatum);
            id.LegitimatieEindDt = Utils.GetHdnDate(persoon.HOLegitimatieEindDt);
            id.LegitimatieAfgiftePlaats = persoon.LegitimatiePlaats;
            if (persoon.LegitimatieType != null)
                id.LegitimatieSoort = Enums.GetHdnLegitimatie(persoon.LegitimatieType.GetValueOrDefault());
            id.LegitimatieNr = persoon.LegitimatieNummer;
            id.VerblijfsVergunningNr = persoon.HOVerblijfsvergunningNummer;
            naw.Identificatie.Add(id);
            naw.ToekomstigAdres.StraatNaam = _onderpand.Straat;
            naw.ToekomstigAdres.HuisNr = _onderpand.Huisnummer;
            naw.ToekomstigAdres.HuisNrToevoeging = _onderpand.HuisnummerToevoeging;
            naw.ToekomstigAdres.Postcode = _onderpand.Postcode;
            naw.ToekomstigAdres.PlaatsNaam = _onderpand.Plaats;
            naw.ToekomstigAdres.Land = LandType.NLNederland;

            naw.ToekomstigAdres.IngangsDtToekomstigAdres = Utils.GetHdnDate(_finixDossier.HuidigeSituatie.AlgemeneGegevens.ClientGegevens.HODatumToekomstigeWoonSituatie);

#endif

            naw.Volgnummer = (_hdnAanvraag.PartijNAWData.Count + 1).ToString();
            _hdnAanvraag.PartijNAWData.Add(naw);
            REFPartijNAWDataType refNaw = new REFPartijNAWDataType();
            refNaw.IDREF = idRef;
            if (idRef == Constants.PartnerRef)
                _partnerRef = refNaw;
            else
                _clientRef = refNaw;
            return refNaw;
        }
    }
}
