﻿# if ABN || CENTRAAL || HYPOTRUST || NIBC || ARGENTA || ALLIANZ || BIJBOUWE  || IQWOON || ROBUUST || WFONDS || TEMPLATE 
#define KOPERHUURDER
#endif

#if AEGON || ING || NN || BIJBOUWE  || MERIUS || TEMPLATE
#define ERFPACHTPARTJN
#endif

#if !AEGON && !ING && !ASR && !ABN && !ABNGRP && !COLIBRI && !MONEYOU && !NN && !NIBC && !BLG && !ASN && !DYNAMIC && !LLOYDS && !OBVION && !PHILIPS && !REAAL && !REGIO && !SNS && !TULP
#define ONTBVWDNDT
#endif

#if !ASR && !ING && !MUNT && !VENN && !COLIBRI && !ABNGRP && !MONEYOU && !NN && !WOONNU && !BLG && !DELTA && !ASN && !DYNAMIC && !LLOYDS && !LOT && !OBVION && !PHILIPS && !REAAL && !REGIO && !SNS && !TULP
#define KOSTENKOPER
#endif

#if !DYNAMIC && !LOT && !PHILIPS && !REAAL
#define ENERGIEKLASSE
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        protected void BuildObject()
        {
#if KOPERHUURDER
            _hdnAanvraag.Object.KoperHuurder = BooleanType.NNee;
#endif
#if ONTBVWDNDT
            _hdnAanvraag.Object.OntbVwdnDt = Utils.GetHdnDate(_hypBer.Ingangsdatum);
#endif
            if (SpecialUtils.IsNieuweWoning(_hypBer.SoortScenario))
                BuildNieuwObject();
            else
                BuildBestaandObject();
        }
        private void BuildBestaandObject()
        {
            HSKoopFieldT onderpandFinancieel = SpecialUtils.GetToekomstigOnderpandFinancieel(_finixDossier.HuidigeSituatie);
            if (onderpandFinancieel == null)
                return;

            PersoonsGegevensFieldT persoonsGegevens = SpecialUtils.GetToekomstigOnderpandAdres(_finixDossier.HuidigeSituatie);
            if (persoonsGegevens == null)
                return;

            _hdnAanvraag.Object.Volgnummer = "1";
            _hdnAanvraag.Object.CodeObject = SpecialEnums.GetHdnCodeObject(onderpandFinancieel.SoortWoning);
            _hdnAanvraag.Object.Onderpand = OnderpandType.Item01bestaand;
            _hdnAanvraag.Object.PlaatsNaam = persoonsGegevens.Plaats;
            _hdnAanvraag.Object.StraatNaam = persoonsGegevens.Straat;
            _hdnAanvraag.Object.Land = LandType.NLNederland;
            _hdnAanvraag.Object.Postcode = persoonsGegevens.Postcode;
            _hdnAanvraag.Object.HuisNr = persoonsGegevens.Huisnummer;
            _hdnAanvraag.Object.HuisNrToevoeging = persoonsGegevens.Huisnummertoevoeging;
            _hdnAanvraag.Object.WOZWaarde = Utils.GetHdnDecimal(onderpandFinancieel.WOZwaarde);
#if ENERGIEKLASSE
            SpecialEnums.SetHdnEnergieLabel(onderpandFinancieel.EnergieLabel, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.EnergieKlasse));
#endif
#if !DYNAMIC
            _hdnAanvraag.Object.EigenBewoning = BooleanType.JJa;
#else
            _hdnAanvraag.Object.EigenBewoning = BooleanType.NNee;
#endif
            _hdnAanvraag.Object.HoofdVerblijf = BooleanType.JJa;
            _hdnAanvraag.Object.Erfpacht = Enums.GetHdnBooleanType(onderpandFinancieel.Erfpacht);
            _hdnAanvraag.Object.AppartementsRecht = Enums.GetHdnBooleanType(onderpandFinancieel.HOAppartementsRecht);
            _hdnAanvraag.Object.TaxatieAanwezig = Enums.GetHdnBooleanType(onderpandFinancieel.TaxatieRapportAanwezig);
            if (_hdnAanvraag.Object.TaxatieAanwezig == BooleanType.JJa)
            {
                _hdnAanvraag.Object.TaxatieDt = Utils.GetHdnDate(onderpandFinancieel.Taxatiedatum);
            }

            Utils.SetHdnYesNo(onderpandFinancieel.VerkoopOnderVoorwaardenAanwezig, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerkoopOnderVoorwaardenJN));
            //           if (onderpandFinancieel.VerkoopOnderVoorwaardenAanwezig == DataBooleanFieldT.@true)
            //                _hdnAanvraag.Object.VerkoopOnderVoorwaardenConstructie = Enums.GetHDNVerkoopOnderVoorwaarden(onderpandFinancieel.VerkoopOnderVoorwaardeType);

            Utils.SetHdnDecimal(onderpandFinancieel.HOVerbouwDepotBedrag, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerbouwingsDepot));
            Utils.SetHdnDecimal(onderpandFinancieel.HOAchterstalligOnderhoudbedr, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.AchterstalligOnderhoud));

            _hdnAanvraag.Object.BouwJaar = onderpandFinancieel.Bouwjaar.GetValueOrDefault().ToString();

            if (onderpandFinancieel.Erfpacht == DataBooleanFieldT.@true)
            {
                _hdnAanvraag.Object.Erfpacht = BooleanType.JJa;
                Utils.SetHdnYesNo(onderpandFinancieel.ErfpachtIsWordtAfgekocht, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.AfkoopErfpachtJN));
                _hdnAanvraag.Object.ErfpachtBeginDt = Utils.GetHdnDate(onderpandFinancieel.HOErfpachtBegindatum);
#if ERFPACHTPARTJN
                Utils.SetHdnYesNo(onderpandFinancieel.ErfpachtParticulier, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.ErfpachtPartJN));
#endif
                Utils.SetHdnYesNo(onderpandFinancieel.ErfpachtIsEeuwigdurend, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.ErfpachtEeuwigDurend));
                _hdnAanvraag.Object.ErfpachtCanonInVerledenAfgekochtJN = Enums.GetHdnBooleanType(onderpandFinancieel.ErfpachtInVerledenAfgekocht);
                _hdnAanvraag.Object.ErfpachtCanonEeuwigDurendAfgekocht = Enums.GetHdnBooleanType(onderpandFinancieel.ErfpachtEeuwigAfgekocht);

                if (_hdnAanvraag.Object.ErfpachtCanonEeuwigDurendAfgekocht == BooleanType.NNee)
                {
                    _hdnAanvraag.Object.ErfpachtCanonAfgekochtTotDt = Utils.GetHdnDate(onderpandFinancieel.ErfpachtAfgekochtTot);
                    _hdnAanvraag.Object.ErfpachtCanon = Utils.GetHdnDecimal(onderpandFinancieel.Erfpachtcanon);
                }

                switch (_hypBer.SoortScenario)
                {
                    case SoortScenarioEnum.Tweedehypotheek:
                    case SoortScenarioEnum.Item1stehypotheekonbelastewoning:
                        if (_hdnAanvraag.Object.ErfpachtCanonEeuwigDurendAfgekocht == BooleanType.NNee)
                            _hdnAanvraag.Object.ErfpachtAfkoopsom = Utils.GetHdnDecimal(_hypBer.TweedeHypotheek.BedragAfkoopErfpacht);
                        break;
                    case SoortScenarioEnum.Verhogen:
                    case SoortScenarioEnum.Omzettenevtverhogenbestaandleningdeel:
                        if (_hdnAanvraag.Object.ErfpachtCanonEeuwigDurendAfgekocht == BooleanType.NNee)
                            _hdnAanvraag.Object.ErfpachtAfkoopsom = Utils.GetHdnDecimal(_hypBer.Verhogen.BedragAfkoopErfpacht);
                        break;
                    case SoortScenarioEnum.Oversluiten:
                        if (_hdnAanvraag.Object.ErfpachtCanonEeuwigDurendAfgekocht == BooleanType.NNee)
                            _hdnAanvraag.Object.ErfpachtAfkoopsom = Utils.GetHdnDecimal(_hypBer.Oversluiten.BedragAfkoopErfpacht);
                        break;
                    default:
                        break;
                }
                _hdnAanvraag.Object.ErfpachtMeeFinancieren = (_hdnAanvraag.Object.ErfpachtAfkoopsom > 0) ? BooleanType.JJa : BooleanType.NNee;

            }
            switch (_hypBer.SoortScenario)
            {
                case SoortScenarioEnum.Tweedehypotheek:
                case SoortScenarioEnum.Item1stehypotheekonbelastewoning:
                    Utils.SetHdnDecimal(_hypBer.TweedeHypotheek.Overdrachtsbelasting, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.OverdrachtsBelastingSpec));
                    Utils.SetHdnDecimal(_hypBer.TweedeHypotheek.Verbouwingsbedrag, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerbouwingsKosten));
                    if ((_hdnAanvraag.Object.VerbouwingsKosten != null) && (_hdnAanvraag.Object.VerbouwingsKosten > 0))
                        Utils.SetHdnDecimal(_hypBer.TweedeHypotheek.VOVWaardeNaVerbouwing, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.MarktWaardeNaVerb));
                    else
                        Utils.SetHdnDecimal(_hypBer.TweedeHypotheek.VOVWaardeNaVerbouwing, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.MarktWaarde));
                    Utils.SetHdnDecimal(_hypBer.TweedeHypotheek.Taxatiekosten, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.KostenTaxatie));
                    Utils.SetHdnDecimal(_hypBer.TweedeHypotheek.BedragAfkoopExPartner, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.AfkoopExPartner));
                    Utils.SetHdnDecimal(_hypBer.TweedeHypotheek.EnergieBesparendeVoorzieningen, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.BedragKwaliteitsverbeteringEBV));
                    break;
                case SoortScenarioEnum.Verhogen:
                case SoortScenarioEnum.Omzettenevtverhogenbestaandleningdeel:
                    Utils.SetHdnDecimal(_hypBer.Verhogen.Overdrachtsbelasting, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.OverdrachtsBelastingSpec));
                    Utils.SetHdnDecimal(_hypBer.Verhogen.NotariskostenLevering, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.KostenLeveringsAkteSpec));
                    Utils.SetHdnDecimal(_hypBer.Verhogen.Verbouwingsbedrag, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerbouwingsKosten));
                    if ((_hdnAanvraag.Object.VerbouwingsKosten != null) && (_hdnAanvraag.Object.VerbouwingsKosten > 0))
                        Utils.SetHdnDecimal(_hypBer.Verhogen.VOVWaardeNaVerbouwing, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.MarktWaardeNaVerb));
                    else
                        Utils.SetHdnDecimal(_hypBer.Verhogen.VOVWaardeNaVerbouwing, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.MarktWaarde));
                    Utils.SetHdnDecimal(_hypBer.Verhogen.Taxatiekosten, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.KostenTaxatie));
                    Utils.SetHdnDecimal(_hypBer.Verhogen.EnergieBesparendeVoorzieningen, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.BedragKwaliteitsverbeteringEBV));
                    break;
                case SoortScenarioEnum.Oversluiten:
                    Utils.SetHdnDecimal(_hypBer.Oversluiten.Overdrachtsbelasting, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.OverdrachtsBelastingSpec));
                    Utils.SetHdnDecimal(_hypBer.Oversluiten.Verbouwingsbedrag, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerbouwingsKosten));
                    if ((_hdnAanvraag.Object.VerbouwingsKosten != null) && (_hdnAanvraag.Object.VerbouwingsKosten > 0))
                        Utils.SetHdnDecimal(_hypBer.Oversluiten.VOVWaardeNaVerbouwing, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.MarktWaardeNaVerb));
                    else
                        Utils.SetHdnDecimal(_hypBer.Oversluiten.VOVWaardeNaVerbouwing, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.MarktWaarde));
                    Utils.SetHdnDecimal(_hypBer.Oversluiten.Taxatiekosten, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.KostenTaxatie));
                    Utils.SetHdnDecimal(_hypBer.Oversluiten.EnergieBesparendeVoorzieningen, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.BedragKwaliteitsverbeteringEBV));

                    break;
                default:
                    break;
            }

            if (_hdnAanvraag.Object.VerbouwingsKosten > 0)
                _hdnAanvraag.Object.Onderpand = OnderpandType.Item02bestaandverbouw;
        }

        private void BuildNieuwObject()
        {
            BooleanType getKK(KoopvoorwaardeEnum? voorwaarde)
            {
                switch (voorwaarde)
                {
                    case KoopvoorwaardeEnum.KK:
                        return BooleanType.JJa;
                    case KoopvoorwaardeEnum.VON:
                    default:
                        return BooleanType.NNee;
                }
            }

            _hdnAanvraag.Object.Volgnummer = "1";
            _hdnAanvraag.Object.CodeObject = SpecialEnums.GetHdnCodeObject(_onderpand.SoortWoning);
            _hdnAanvraag.Object.PlaatsNaam = _onderpand.Plaats;
            _hdnAanvraag.Object.StraatNaam = _onderpand.Straat;
            _hdnAanvraag.Object.Land = LandType.NLNederland;
            _hdnAanvraag.Object.Postcode = _onderpand.Postcode;
            _hdnAanvraag.Object.HuisNr = _onderpand.Huisnummer;
            _hdnAanvraag.Object.HuisNrToevoeging = _onderpand.HuisnummerToevoeging;
            _hdnAanvraag.Object.WOZWaarde = Utils.GetHdnDecimal(_onderpand.WOZwaarde);
#if ENERGIEKLASSE
            SpecialEnums.SetHdnEnergieLabel(_onderpand.EnergieLabel, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.EnergieKlasse));
#endif
            if (_onderpand.Bewoning == BewoningEnum.Hoofdverblijf || _onderpand.Bewoning == BewoningEnum.Item2ewoningeigengebruik)
                _hdnAanvraag.Object.HoofdVerblijf = BooleanType.JJa;
            else
                _hdnAanvraag.Object.HoofdVerblijf = BooleanType.NNee;
#if !DYNAMIC
            _hdnAanvraag.Object.EigenBewoning = BooleanType.JJa;
#else
            _hdnAanvraag.Object.EigenBewoning = BooleanType.NNee;
#endif
            _hdnAanvraag.Object.Erfpacht = Enums.GetHdnBooleanType(_onderpand.Erfpacht);

            if (_onderpand.Erfpacht == DataBooleanFieldT.@true)
            {
                _hdnAanvraag.Object.Erfpacht = BooleanType.JJa;
                Utils.SetHdnYesNo(_onderpand.ErfpachtIsWordtAfgekocht, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.AfkoopErfpachtJN));
                _hdnAanvraag.Object.ErfpachtBeginDt = Utils.GetHdnDate(_onderpand.HOErfpachtBegindatum);
#if ERFPACHTPARTJN
                Utils.SetHdnYesNo(_onderpand.ErfpachtParticulier, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.ErfpachtPartJN));
#endif
                Utils.SetHdnYesNo(_onderpand.ErfpachtIsEeuwigdurend, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.ErfpachtEeuwigDurend));
                _hdnAanvraag.Object.ErfpachtCanonInVerledenAfgekochtJN = Enums.GetHdnBooleanType(_onderpand.ErfpachtInVerledenAfgekocht);
                _hdnAanvraag.Object.ErfpachtCanonEeuwigDurendAfgekocht = Enums.GetHdnBooleanType(_onderpand.ErfpachtEeuwigAfgekocht);

                if (_hdnAanvraag.Object.ErfpachtCanonEeuwigDurendAfgekocht == BooleanType.NNee)
                {
                    _hdnAanvraag.Object.ErfpachtCanonAfgekochtTotDt = Utils.GetHdnDate(_onderpand.ErfpachtAfgekochtTot);
                    _hdnAanvraag.Object.ErfpachtCanon = Utils.GetHdnDecimal(_onderpand.Erfpachtcanon);
                    _hdnAanvraag.Object.ErfpachtAfkoopsom = Utils.GetHdnDecimal(_hypBer.AankoopNieuwbouw.BedragAfkoopErfpacht);
                    _hdnAanvraag.Object.ErfpachtMeeFinancieren = (_hdnAanvraag.Object.ErfpachtAfkoopsom > 0) ? BooleanType.JJa : BooleanType.NNee;
                }
            }

            if (_onderpand.HOVerhuurType != null)
                _hdnAanvraag.Object.CodeVerhuur = SpecialEnums.GetHdnVerhuurcode(_onderpand.HOVerhuurType);
            _hdnAanvraag.Object.AppartementsRecht = Enums.GetHdnBooleanType(_onderpand.HOAppartementsRecht);
            _hdnAanvraag.Object.TaxatieAanwezig = Enums.GetHdnBooleanType(_onderpand.TaxatieRapportAanwezig);
            if (_hdnAanvraag.Object.TaxatieAanwezig == BooleanType.JJa)
            {
                _hdnAanvraag.Object.TaxatieDt = Utils.GetHdnDate(_onderpand.Taxatiedatum);
            }

#if ONTBVWDNDT
            _hdnAanvraag.Object.OntbVwdnDt = Utils.GetHdnDate(_hypBer.Ingangsdatum);
#endif
            Utils.SetHdnYesNo(_onderpand.VerkoopOnderVoorwaardenAanwezig, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerkoopOnderVoorwaardenJN));

            Utils.SetHdnDecimal(_onderpand.HOVerbouwDepotBedrag, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerbouwingsDepot));

            if (_hypBer.SoortScenario == SoortScenarioEnum.Aankoopnieuwbouw)
            {
                _hdnAanvraag.Object.Onderpand = OnderpandType.Item03nieuw;
                _hdnAanvraag.Object.BouwPlanNr = _onderpand.BouwPlanNr;
                _hdnAanvraag.Object.SituatieNr = _onderpand.SituatieNr;
                _hdnAanvraag.Object.Koopsom = Utils.GetHdnDecimal(_hypBer.AankoopNieuwbouw.Aanneemsom);
                if (_onderpand.SoortNieuwbouw == SoortNieuwbouwEnum.Zelfbouw)
                    if (_onderpand.GetaxeerdeMarktwaarde.GetValueOrDefault() > 0)
                        _hdnAanvraag.Object.MarktWaarde = Utils.GetHdnDecimal(_onderpand.GetaxeerdeMarktwaarde);
                _hdnAanvraag.Object.Meerwerk = Utils.GetHdnDecimal(_hypBer.AankoopNieuwbouw.Meerwerk + _hypBer.AankoopNieuwbouw.VolumeMeerwerk);
#if KOSTENKOPER
                _hdnAanvraag.Object.KostenKoper = getKK(_hypBer.AankoopNieuwbouw.Koopvoorwaarde);
#endif
                _hdnAanvraag.Object.GrondReedsInBezit = Enums.GetHdnBooleanType(_onderpand.GrondReedsInBezit);
                _hdnAanvraag.Object.KoopsomGrond = Utils.GetHdnDecimal(_onderpand.KoopsomGrond);
                _hdnAanvraag.Object.RenteVerlies = Utils.GetHdnDecimal(_hypBer.AankoopNieuwbouw.Renteverlies);
                _hdnAanvraag.Object.SoortNieuwbouw = SpecialEnums.GetHdnSoortNieuwbouw(_onderpand.SoortNieuwbouw);
                _hdnAanvraag.Object.OpleveringsDt = Utils.GetHdnDate(_onderpand.Opleverdatum);
                Utils.SetHdnDecimal(_hypBer.AankoopNieuwbouw.EnergieBesparendeVoorzieningen, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.BedragKwaliteitsverbeteringEBV));
                if (Utils.GetHdnDate(_onderpand.Opleverdatum) != null)
                    _hdnAanvraag.Object.BouwJaar = Utils.GetHdnDate(_onderpand.Opleverdatum).Jaar;
                Utils.SetHdnDecimal(_hypBer.AankoopNieuwbouw.NotariskostenLevering, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.KostenLeveringsAkteSpec));
            }
            else
            {

#if KOPERHUURDER
                if (_hypBer.SoortScenario == SoortScenarioEnum.Uitponden)
                    _hdnAanvraag.Object.KoperHuurder = BooleanType.JJa;
#endif
                _hdnAanvraag.Object.Onderpand = OnderpandType.Item01bestaand;

                _hdnAanvraag.Object.Koopsom = Utils.GetHdnDecimal(_hypBer.AankoopBestaandeBouw.Koopsom - _hypBer.AankoopBestaandeBouw.KoopsomRoerend.GetValueOrDefault());
                _hdnAanvraag.Object.MarktWaarde = Utils.GetHdnDecimal(_onderpand.VVW);
                _hdnAanvraag.Object.OverdrachtsBelastingSpec = Utils.GetHdnDecimal(_hypBer.AankoopBestaandeBouw.Overdrachtsbelasting);
                Utils.SetHdnDecimal(_hypBer.AankoopBestaandeBouw.NotariskostenLevering, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.KostenLeveringsAkteSpec));
                _hdnAanvraag.Object.RoerendeZaken = Utils.GetHdnDecimal(_hypBer.AankoopBestaandeBouw.KoopsomRoerend);
                Utils.SetHdnDecimal(_onderpand.HOAchterstalligOnderhoudbedr, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.AchterstalligOnderhoud));
#if KOSTENKOPER
                _hdnAanvraag.Object.KostenKoper = getKK(_hypBer.AankoopBestaandeBouw.Koopvoorwaarde);
#endif
                _hdnAanvraag.Object.BouwJaar = _onderpand.Bouwjaar.GetValueOrDefault().ToString();
                _hdnAanvraag.Object.OpleveringsDt = Utils.GetHdnDate(_onderpand.Opleverdatum);

                Utils.SetHdnDecimal(_hypBer.AankoopBestaandeBouw.EnergieBesparendeVoorzieningen, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.BedragKwaliteitsverbeteringEBV));
                Utils.SetHdnDecimal(_hypBer.AankoopBestaandeBouw.Taxatiekosten, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.KostenTaxatie));
                Utils.SetHdnDecimal(_hypBer.AankoopBestaandeBouw.Verbouwingskosten, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.VerbouwingsKosten));
                if (_hypBer.AankoopBestaandeBouw.Verbouwingskosten > 0)
                {
                    _hdnAanvraag.Object.Onderpand = OnderpandType.Item02bestaandverbouw;
                    Utils.SetHdnDecimal(_hypBer.AankoopBestaandeBouw.VOVWaardeNaVerbouwing, _hdnAanvraag.Object, nameof(_hdnAanvraag.Object.MarktWaardeNaVerb));
                }
                //_hdnAanvraag.Object.AfkoopErfgenamen = 1.00M;
            }
        }
    }
}
