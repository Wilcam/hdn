﻿#if CENTRAAL || HYPOTRUST || ING || NIBC || WFONDS || TEMPLATE
#define PCTCONSUMPTIEF
#endif

#if !ASR && !COLIBRI && !MUNT && !VENN && !DELTA && !LLOYDS && !LOT && !OBVION && !PHILIPS && !REAAL && !TULP
#define INKOMENUITVERMOGEN
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
      

        void BuildInkomenUitVermogen(SpecificatieInkomenFieldT specificatieInkomen, BijAfVerItemDataT bijAf, HypotheekgeverEntiteitType hypotheekGever)
        {
            if (specificatieInkomen.InkomenOverig == DataBooleanFieldT.@false)
                return;

            void addInkomenUitVermogen(OverigInkomenFieldT inkomen)
            {
#if INKOMENUITVERMOGEN
                if (inkomen.Soort.GetValueOrDefault().HasFlag(InkomenSoortEnum.Inkomenuitvermogen))
                {
                    VermogenEntiteitType vermogenEntiteitType = new VermogenEntiteitType();
                    vermogenEntiteitType.NettoRendement = Utils.GetHdnDecimal(inkomen.Bedrag);
                    vermogenEntiteitType.WaardeVermogen = 0;
                    vermogenEntiteitType.VerpandVermogenJN = BooleanType.NNee;                  
                    vermogenEntiteitType.Volgnummer = (hypotheekGever.Inkomsten.Vermogen.Count + 1).ToString();
                    hypotheekGever.Inkomsten.Vermogen.Add(vermogenEntiteitType);
                }
#endif
            }
            addInkomenUitVermogen(bijAf.OverigInkomen1);
            addInkomenUitVermogen(bijAf.OverigInkomen2);
            addInkomenUitVermogen(bijAf.OverigInkomen3);
        }
    }
}