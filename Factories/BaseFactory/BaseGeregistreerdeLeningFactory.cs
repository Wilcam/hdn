﻿#if CENTRAAL || HYPOTRUS || ING || NIBC || ARGENTA || ALLIANZ || BIJBOUWE || BNP || IQWOON || MERIUS || WFONDS || TEMPLATE 
#define PCTCONSUMPTIEF
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {
        static GeregistreerdeLeningEntiteitType BuildBkrGeregistreerdeLening(VerplichtingBKRDataT finixVerplichting, ref int volgnummerVerplichtingen)
        {          
            GeregistreerdeLeningEntiteitType verplichting = new();
            verplichting.Volgnummer = volgnummerVerplichtingen.ToString();
            volgnummerVerplichtingen++;
            verplichting.Maatschappij = Enums.GetHdnMaatschappij(finixVerplichting.HDNNaam) ;
            verplichting.MaatschappijOmschr = finixVerplichting.HDNNaam;
#if PCTCONSUMPTIEF
            verplichting.PctConsumptief = 100.00M;
#endif
            verplichting.SoortGeregistreerdeLening = SpecialEnums.GetHdnGeregistreerdeLeningType(finixVerplichting.Soort);           
            verplichting.LeningPerJaar = Utils.GetHdnDecimal(finixVerplichting.Maandtermijn * 12);
            verplichting.ActueelSaldo = Utils.GetHdnDecimal(finixVerplichting.UitstaandSaldo);
            verplichting.StartDt = Utils.GetHdnDate(finixVerplichting.Ingangsdatum);
            verplichting.EindDt = Utils.GetHdnDate(finixVerplichting.Einddatum);
            if (verplichting.SoortGeregistreerdeLening == GeregistreerdeLeningType.RKDoorlopendkrediet)
                verplichting.LimietDoorlopendKrediet = Utils.GetHdnDecimal(finixVerplichting.Limiet);
            verplichting.AflossingVoorPasserenJN = (finixVerplichting.Aflossen == VerplichtingAflossenEnum.Vóórpasserenaflossen) ? BooleanType.JJa : BooleanType.NNee;            
                
            return verplichting;
        }

        static void BuildGeregistreerdeLening( VerplichtingBKRDataT[] bkrVerplichtingen, EigenaarEnum eigenaar, HypotheekgeverEntiteitType hypotheekGever)

        {
            int volgnummerVerplichtingen = 1;
                   
            if (bkrVerplichtingen != null)
                for (int i = 0; i < bkrVerplichtingen.Length; i++)
                {
                    if (bkrVerplichtingen[i].Contractant.GetValueOrDefault().IsAny(eigenaar))
                        hypotheekGever.GeregistreerdeLening.Add(BuildBkrGeregistreerdeLening(bkrVerplichtingen[i], ref volgnummerVerplichtingen));
                    else if (bkrVerplichtingen[i].Contractant.GetValueOrDefault().IsAny(EigenaarEnum.Beiden))
                    {
                        // indien beiden dan maar 1 maal meenemen
                        if (eigenaar == EigenaarEnum.Cliënt)
                            hypotheekGever.GeregistreerdeLening.Add(BuildBkrGeregistreerdeLening(bkrVerplichtingen[i], ref volgnummerVerplichtingen));
                    }
                }
        }
    }
}