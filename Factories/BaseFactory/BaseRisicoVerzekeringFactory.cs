﻿#if ING || TEMPLATE
#define LENINGDEELUSETYPE1DEKKINGONDERLIGGEND
#endif

#if ABN || ABNGRP || ARGENTA || ATTENS || HYPOTRUST || MONEYOU || NN || RABO || SYNTRUS || TELLIUS || TEMPLATE
#define ROKERJN
#endif

#if ABN || ABNGRP || ASR || DELTA || MONEYOU || MUNT || NIBC || RABO || VENN || LLOYDS || MERIUS || OBVION || TULP || TEMPLATE
#define BURGERLIJKESTAAT
#endif

#if ABN || ABNGRP || ARGENTA || CENTRAAL || DELTA || HYPOTRUST ||  MONEYOU ||  NIBC ||  ALLIANZ || BIJBOUWE || BNP || IQWOON || WFONDS || TEMPLATE
#define EINDWAARDEDEKKING
#endif

#if !ABN && !ABNGRP && !ATTENS && !MONEYOU && !NN && !SYNTRUS && !TELLIUS
#define ENTITEITTYPE1
#endif

#if !RABO && !AEGON && !ASR && !ING && !WOONNU && !BLG && !COLIBRI && !MUNT && !VENN && !DELTA && !ASN && !LLOYDS && !LOT && !MERIUS && !OBVION && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !TULP && !VISTA
#define PERCENTAGEMUTATIE
#endif

#if !RABO && !ASR && !AEGON && !COLIBRI && !MUNT && !VENN && !WOONNU && !BLG && !DELTA && !ASN && !HWOONT && !DYNAMIC && !LLOYDS && !LOT && !OBVION && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !TULP && !VISTA
#define VERPANDJN
#endif

#if !ABN && !ATTENS && !ABNGRP && !MONEYOU && !NN && !ING  && !SYNTRUS && !TELLIUS
#define LENINGDEELUSETYPE1DEKKING
#endif

#if !ABN && !ABNGRP && !MONEYOU  && !BLG && !ASN && !REGIO && !SNS
#define LENINGDEELDEKKINGLIST
#endif

#if !ABN && !ABNGRP && !MONEYOU && !NN && !BLG && !ATTENS 
#define AANVRAAGDEKKINGLIST
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public partial class BaseFactory
    {
        List<VerzekeringNemerEntiteitType> GetVerzekeringNemers(RisicoVerzekeringDataT verzekering)
        {
            List<VerzekeringNemerEntiteitType> verzekeringNemers = new List<VerzekeringNemerEntiteitType>();
            int verzekeringNemerVolgnr = 1;
            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekeringNemerEntiteitType verzekeringNemer = new VerzekeringNemerEntiteitType();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.ClientRef;
                verzekeringNemers.Add(verzekeringNemer);
                verzekeringNemerVolgnr++;
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekeringNemerEntiteitType verzekeringNemer = new VerzekeringNemerEntiteitType();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.PartnerRef;
                verzekeringNemers.Add(verzekeringNemer);
                verzekeringNemerVolgnr++;
            }

            return verzekeringNemers;
        }

        List<VerzekerdeEntiteitType> GetVerzekerden(RisicoVerzekeringDataT verzekering)
        {
            List<VerzekerdeEntiteitType> verzekerden = new List<VerzekerdeEntiteitType>();
            int verzekerdeVolgnr = 1;

            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekerdeEntiteitType verzekerde = new VerzekerdeEntiteitType();
                verzekerde.Volgnummer = verzekerdeVolgnr.ToString();
                verzekerde.RefPartijNAWData.IDREF = Constants.ClientRef;
#if BURGERLIJKESTAAT
                verzekerde.BurgerlijkeStaat = Enums.GetHdnBurgerlijkeStaatType(_finixDossier.HuidigeSituatie.AlgemeneGegevens.BurgerlijkeStaat);
#endif
                verzekerden.Add(verzekerde);
                verzekerdeVolgnr++;
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekerdeEntiteitType verzekerde = new VerzekerdeEntiteitType();
                verzekerde.Volgnummer = verzekerdeVolgnr.ToString();
                verzekerde.RefPartijNAWData.IDREF = Constants.PartnerRef;
#if BURGERLIJKESTAAT
                verzekerde.BurgerlijkeStaat = Enums.GetHdnBurgerlijkeStaatType(_finixDossier.HuidigeSituatie.AlgemeneGegevens.BurgerlijkeStaat);
#endif
                verzekerden.Add(verzekerde);
                verzekerdeVolgnr++;
            }
            return verzekerden;
        }

        List<VerzekerdeBedragenEntiteitType> GetVerzekerdeBedragen(RisicoVerzekeringDataT verzekering)
        {
            List<VerzekerdeBedragenEntiteitType> verzekerdeBedragen = new List<VerzekerdeBedragenEntiteitType>();
            int verzekerdeBedragenVolgnr = 1;
            VerzekerdeBedragenEntiteitType bedrag = new VerzekerdeBedragenEntiteitType(); ;

            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                bedrag = new VerzekerdeBedragenEntiteitType();
                bedrag.Volgnummer = verzekerdeBedragenVolgnr.ToString();
                bedrag.VerzekerdKap = Utils.GetHdnDecimal(verzekering.BedragClient);
                bedrag.CodeUitkering = SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingClient);
                bedrag.SoortVerzekerdKap = SoortVerzekerdKapType.Item01Verzekerdbedragbijoverlijden;
                bedrag.BelastingBox = BelastingBoxType.Item03Box3;
#if EINDWAARDEDEKKING
                Utils.SetHdnDecimal(verzekering.BedragDaaltNaarClient, bedrag, nameof(bedrag.EindWaardeDekking));
#endif
                if (verzekering.Verzekerden == EigenaarEnum.Beiden)
                {
                    if (bedrag.CodeUitkering == SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingPartner))
                        bedrag.VerzekerdKap2 = Utils.GetHdnDecimal(verzekering.BedragPartner);
                }
                verzekerdeBedragen.Add(bedrag);
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                if (verzekering.Verzekerden == EigenaarEnum.Beiden)
                {
                    if (bedrag.CodeUitkering == SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingPartner))
                    {
                        return verzekerdeBedragen;
                    }
                }

                bedrag = new VerzekerdeBedragenEntiteitType();
                bedrag.Volgnummer = verzekerdeBedragenVolgnr.ToString();
                bedrag.VerzekerdKap = Utils.GetHdnDecimal(verzekering.BedragPartner);
                bedrag.CodeUitkering = SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingPartner);
                bedrag.SoortVerzekerdKap = SoortVerzekerdKapType.Item01Verzekerdbedragbijoverlijden;
                bedrag.BelastingBox = BelastingBoxType.Item03Box3;
#if BEDRAGDAALTNAAR
                Utils.SetHdnDecimal(verzekering.BedragDaaltNaarPartner, bedrag, nameof(bedrag.EindWaardeDekking));
#endif
                verzekerdeBedragen.Add(bedrag);
            }
            return verzekerdeBedragen;
        }

        PremieAfsprakenEntiteitType GetPremieAfspraken(RisicoVerzekeringDataT verzekering)
        {
            PremieAfsprakenEntiteitType afspraken = new PremieAfsprakenEntiteitType();
            afspraken.Volgnummer = "1";
            afspraken.AanvangNaIngangsDtInMnd = "0";
            afspraken.BetalingsTermijn = BetalingsTermijnType.Item01permaand;
            afspraken.SoortPremie = PremieType.Item01Risicopremienormaal;
            afspraken.DuurInMnd = (verzekering.LooptijdPremie.GetValueOrDefault() * 12 + verzekering.LooptijdPremieInMnd.GetValueOrDefault()).ToString();
            afspraken.PremieBedrag = Utils.GetHdnDecimalZonderAfronden(verzekering.Premie);
            afspraken.RefDebiteurNAWData = _clientRef;
            if (verzekering.Verzekerden == EigenaarEnum.Partner)
                afspraken.RefDebiteurNAWData = _partnerRef;

            return afspraken;
        }

#if ENTITEITTYPE1
        List<VerzekeringNemerEntiteitType1> GetVerzekeringNemers1(RisicoVerzekeringDataT verzekering)
        {
            List<VerzekeringNemerEntiteitType1> verzekeringNemers = new List<VerzekeringNemerEntiteitType1>();
            int verzekeringNemerVolgnr = 1;
            VerzekerdeBedragenEntiteitType bedrag;
            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekeringNemerEntiteitType1 verzekeringNemer = new VerzekeringNemerEntiteitType1();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.ClientRef;
                verzekeringNemers.Add(verzekeringNemer);
                verzekeringNemerVolgnr++;
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekeringNemerEntiteitType1 verzekeringNemer = new VerzekeringNemerEntiteitType1();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.PartnerRef;
                verzekeringNemers.Add(verzekeringNemer);
                verzekeringNemerVolgnr++;
            }
            return verzekeringNemers;
        }

        List<VerzekerdeEntiteitType1> GetVerzekerden1(RisicoVerzekeringDataT verzekering)
        {
            List<VerzekerdeEntiteitType1> verzekerden = new List<VerzekerdeEntiteitType1>();
            int verzekerdeVolgnr = 1;

            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekerdeEntiteitType1 verzekerde = new VerzekerdeEntiteitType1();
                verzekerde.Volgnummer = verzekerdeVolgnr.ToString();
                verzekerde.RefPartijNAWData.IDREF = Constants.ClientRef;
                if (_finixDossier.HuidigeSituatie.InkomenGegevens.LoonInkomensClient.Length > 0)
                    verzekerde.Beroep = SpecialEnums.GetHdnBeroep(_finixDossier.HuidigeSituatie.InkomenGegevens.LoonInkomensClient[0].Beroep).GetValueOrDefault();
#if ROKERJN
                verzekerde.RokerJN = (_finixDossier.HuidigeSituatie.AlgemeneGegevens.ClientGegevens.Roker == DataBooleanFieldT.@true ? BooleanType.JJa : BooleanType.NNee);
#endif
#if BURGERLIJKESTAAT
                verzekerde.BurgerlijkeStaat = Enums.GetHdnBurgerlijkeStaatType(_finixDossier.HuidigeSituatie.AlgemeneGegevens.BurgerlijkeStaat);
#endif
                verzekerden.Add(verzekerde);
                verzekerdeVolgnr++;
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekerdeEntiteitType1 verzekerde = new VerzekerdeEntiteitType1();
                verzekerde.Volgnummer = verzekerdeVolgnr.ToString();
                verzekerde.RefPartijNAWData.IDREF = Constants.PartnerRef;
                if (_finixDossier.HuidigeSituatie.InkomenGegevens.LoonInkomensPartner.Length > 0)
                    verzekerde.Beroep = SpecialEnums.GetHdnBeroep(_finixDossier.HuidigeSituatie.InkomenGegevens.LoonInkomensPartner[0].Beroep).GetValueOrDefault();
#if ROKERJN
                verzekerde.RokerJN = (_finixDossier.HuidigeSituatie.AlgemeneGegevens.PartnerGegevens.Roker == DataBooleanFieldT.@true ? BooleanType.JJa : BooleanType.NNee);
#endif

#if BURGERLIJKESTAAT
                verzekerde.BurgerlijkeStaat = Enums.GetHdnBurgerlijkeStaatType(_finixDossier.HuidigeSituatie.AlgemeneGegevens.BurgerlijkeStaat);
#endif
                verzekerden.Add(verzekerde);
                verzekerdeVolgnr++;
            }

            return verzekerden;
        }

        List<VerzekerdeBedragenEntiteitType1> GetVerzekerdeBedragen1(RisicoVerzekeringDataT verzekering)
        {
            List<VerzekerdeBedragenEntiteitType1> verzekerdeBedragen = new List<VerzekerdeBedragenEntiteitType1>();
            int verzekerdeBedragenVolgnr = 1;
            VerzekerdeBedragenEntiteitType1 bedrag = new VerzekerdeBedragenEntiteitType1();

            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                bedrag = new VerzekerdeBedragenEntiteitType1();
                bedrag.Volgnummer = verzekerdeBedragenVolgnr.ToString();
                bedrag.VerzekerdKap = Utils.GetHdnDecimal(verzekering.BedragClient);
                bedrag.CodeUitkering = SpecialUtils.getHdnCodeUitkering(verzekering.DekkingClient);
                bedrag.SoortVerzekerdKap = SoortVerzekerdKapType.Item01Verzekerdbedragbijoverlijden;
#if PERCENTAGEMUTATIE
                if (bedrag.CodeUitkering == CodeUitkeringType.Item02annuitairdalend)
                    bedrag.PercentageMutatie = Utils.GetHdnDecimalZonderAfronden(verzekering.AnnPercClient);
#endif
                bedrag.BelastingBox = BelastingBoxType.Item03Box3;
                Utils.SetHdnDecimal(verzekering.BedragDaaltNaarClient, bedrag, nameof(bedrag.EindWaardeDekking));
                bedrag.DuurInMnd = (verzekering.Looptijd.GetValueOrDefault() * 12 + verzekering.LooptijdInMnd.GetValueOrDefault()).ToString();
                if (verzekering.Verzekerden == EigenaarEnum.Beiden)
                {
                    if (bedrag.CodeUitkering == SpecialUtils.getHdnCodeUitkering(verzekering.DekkingPartner))
                        bedrag.VerzekerdKap2 = Utils.GetHdnDecimal(verzekering.BedragPartner);
                }
                verzekerdeBedragen.Add(bedrag);
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                if (verzekering.Verzekerden == EigenaarEnum.Beiden)
                {
                    if (bedrag.CodeUitkering == SpecialUtils.getHdnCodeUitkering(verzekering.DekkingPartner))
                    {
                        return verzekerdeBedragen;
                    }
                }
                bedrag = new VerzekerdeBedragenEntiteitType1();
                bedrag.Volgnummer = verzekerdeBedragenVolgnr.ToString();
                bedrag.VerzekerdKap = Utils.GetHdnDecimal(verzekering.BedragPartner);
                bedrag.CodeUitkering = SpecialUtils.getHdnCodeUitkering(verzekering.DekkingPartner);
                bedrag.SoortVerzekerdKap = SoortVerzekerdKapType.Item01Verzekerdbedragbijoverlijden;
#if CODEUITKERING
                if (bedrag.CodeUitkering == CodeUitkeringType.Item02annuitairdalend)
                    bedrag.PercentageMutatie = Utils.GetHdnDecimalZonderAfronden(verzekering.AnnPercPartner);
#endif
                bedrag.BelastingBox = BelastingBoxType.Item03Box3;
                Utils.SetHdnDecimal(verzekering.BedragDaaltNaarPartner, bedrag, nameof(bedrag.EindWaardeDekking));
                bedrag.DuurInMnd = (verzekering.Looptijd.GetValueOrDefault() * 12 + verzekering.LooptijdInMnd.GetValueOrDefault()).ToString();
                verzekerdeBedragen.Add(bedrag);
            }
            return verzekerdeBedragen;
        }

        PremieAfsprakenEntiteitType1 GetPremieAfspraken1(RisicoVerzekeringDataT verzekering)
        {
            PremieAfsprakenEntiteitType1 afspraken = new PremieAfsprakenEntiteitType1();
            afspraken.Volgnummer = "1";
            afspraken.AanvangNaIngangsDtInMnd = "0";
            afspraken.BetalingsTermijn = BetalingsTermijnType.Item01permaand;
            afspraken.SoortPremie = PremieType.Item01Risicopremienormaal;
            afspraken.DuurInMnd = (verzekering.LooptijdPremie.GetValueOrDefault() * 12 + verzekering.LooptijdPremieInMnd.GetValueOrDefault()).ToString();
            afspraken.PremieBedrag = Utils.GetHdnDecimalZonderAfronden(verzekering.Premie);
            afspraken.RefDebiteurNAWData = _clientRef;
            if (verzekering.Verzekerden == EigenaarEnum.Partner)
                afspraken.RefDebiteurNAWData = _partnerRef;
            return afspraken;
        }
#endif
        protected void BuildMeeverhuisdeRisicoVerzekering(RisicoVerzekeringDataT verzekering, LeningdeelEntiteitType leningdeel)
        {
#if LENINGDEELUSETYPE1DEKKING
            FinancieleDekkingEntiteitType1 dekking1 = new FinancieleDekkingEntiteitType1();
#else
            FinancieleDekkingEntiteitType dekking1 = new FinancieleDekkingEntiteitType();
#endif
            dekking1.CodeFinancieleDekking = CodeFinancieleDekkingType.Item04zelfstandigerisicoverzekering;
            dekking1.AanvangsDt = Utils.GetHdnDate(verzekering.BestaandBeginDatum);
            dekking1.DuurInMnd = (verzekering.BestaandOrigineleLooptijd.GetValueOrDefault() * 12 + verzekering.BestaandOrigineleLooptijdInMnd.GetValueOrDefault()).ToString();
            dekking1.ProductNaam = verzekering.VrijeProduktnaam;
            dekking1.Maatschappij = Enums.GetHdnMaatschappij(verzekering.BestaandMaatschappij);
            if (dekking1.Maatschappij == MaatschappijType.ZZAnders)
                dekking1.MaatschappijOmschr = verzekering.BestaandMaatschappij;
            dekking1.PolisLopendJN = BooleanType.JJa;
            dekking1.MaatschappijPolisLopend = Enums.GetHdnMaatschappij(verzekering.BestaandMaatschappij);
            if (dekking1.MaatschappijPolisLopend == MaatschappijType.ZZAnders)
                dekking1.MaatschappijPolisLopendOmschr = verzekering.BestaandMaatschappij;
            dekking1.PolisNr = verzekering.BestaandPolisNr;
            dekking1.PolisOmzetting = BooleanType.NNee;
            if (verzekering.BestaandePolisNietVerpand == DataBooleanFieldT.@true)
                dekking1.VerpandJN = BooleanType.NNee;
            else
                dekking1.VerpandJN = BooleanType.JJa;

#if LENINGDEELUSETYPE1DEKKINGONDERLIGGEND
            dekking1.VerzekeringNemer = GetVerzekeringNemers1(verzekering);
            dekking1.Verzekerde = GetVerzekerden1(verzekering);
            dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen1(verzekering);
            dekking1.PremieAfspraken.Add(GetPremieAfspraken1(verzekering));
            dekking1.BerekeningsUitgangspunt = BerekeningsUitgangspuntType.Item02Vankapitaalnaarpremie;
#else
            dekking1.VerzekeringNemer = GetVerzekeringNemers(verzekering);
            dekking1.Verzekerde = GetVerzekerden(verzekering);
            dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen(verzekering);
            dekking1.PremieAfspraken.Add(GetPremieAfspraken(verzekering));
            dekking1.BerekeningsUitgangspunt = BerekeningsUitgangspuntType.Item02Vankapitaalnaarpremie;
#endif

#if !LENINGDEELDEKKINGLIST
            leningdeel.FinancieleDekking = dekking1;
#else
#if ING
            dekking1.Volgnummer = (_hdnAanvraag.FinancieleDekking.Count + 1).ToString();
            _hdnAanvraag.FinancieleDekking.Add(dekking1);
#else
            dekking1.Volgnummer = (leningdeel.FinancieleDekking.Count + 1).ToString();
            leningdeel.FinancieleDekking.Add(dekking1);
#endif
#endif
        }

        protected void BuildNieuweRisicoVerzekering(RisicoVerzekeringDataT verzekering, LeningdeelEntiteitType leningdeel)
        {
#if LENINGDEELUSETYPE1DEKKING

            FinancieleDekkingEntiteitType1 dekking1 = new FinancieleDekkingEntiteitType1();
#else
            FinancieleDekkingEntiteitType dekking1 = new FinancieleDekkingEntiteitType();
#endif
            dekking1.CodeFinancieleDekking = CodeFinancieleDekkingType.Item04zelfstandigerisicoverzekering;
            dekking1.AanvangsDt = Utils.GetHdnDate(verzekering.ingangsdatum);
            dekking1.DuurInMnd = (verzekering.Looptijd.GetValueOrDefault() * 12 + verzekering.LooptijdInMnd.GetValueOrDefault()).ToString();
            dekking1.ProductNaam = verzekering.VrijeProduktnaam?.Substring(0, Math.Min(verzekering.VrijeProduktnaam.Length, 35));
            dekking1.Maatschappij = Enums.GetHdnMaatschappij(verzekering.BestaandMaatschappij);
            if (dekking1.Maatschappij == MaatschappijType.ZZAnders)
                dekking1.MaatschappijOmschr = verzekering.BestaandMaatschappij;
            dekking1.PolisLopendJN = BooleanType.NNee;
            dekking1.PolisNr = verzekering.BestaandPolisNr;

#if LENINGDEELUSETYPE1DEKKINGONDERLIGGEND
            dekking1.VerzekeringNemer = GetVerzekeringNemers1(verzekering);
            dekking1.Verzekerde = GetVerzekerden1(verzekering);
            dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen1(verzekering);
            dekking1.PremieAfspraken.Add(GetPremieAfspraken1(verzekering));
            dekking1.BerekeningsUitgangspunt = BerekeningsUitgangspuntType.Item02Vankapitaalnaarpremie;
#else
            dekking1.VerzekeringNemer = GetVerzekeringNemers(verzekering);
            dekking1.Verzekerde = GetVerzekerden(verzekering);
            dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen(verzekering);
            dekking1.PremieAfspraken.Add(GetPremieAfspraken(verzekering));
            dekking1.BerekeningsUitgangspunt = BerekeningsUitgangspuntType.Item02Vankapitaalnaarpremie;
#endif

#if VERPANDJN
            if (verzekering.BestaandePolisNietVerpand == DataBooleanFieldT.@true)
                dekking1.VerpandJN = BooleanType.NNee;
            else
                dekking1.VerpandJN = BooleanType.JJa;
#endif

#if LENINGDEELDEKKINGLIST
#if ING
            dekking1.Volgnummer = (_hdnAanvraag.FinancieleDekking.Count + 1).ToString();
            _hdnAanvraag.FinancieleDekking.Add(dekking1);
#else
            dekking1.Volgnummer = (leningdeel.FinancieleDekking.Count + 1).ToString();
            leningdeel.FinancieleDekking.Add(dekking1);
#endif
#else
            leningdeel.FinancieleDekking = dekking1;
#endif
        }
        protected void BuildNieuweRisicoVerzekering(RisicoVerzekeringDataT verzekering)
        {

#if !ABN && !ABNGRP && !ATTENS && !MONEYOU && !NN && !SYNTRUS && !TELLIUS && !VISTA
            FinancieleDekkingEntiteitType dekking1 = new FinancieleDekkingEntiteitType();
            dekking1.CodeFinancieleDekking = CodeFinancieleDekkingType.Item04zelfstandigerisicoverzekering;
            dekking1.AanvangsDt = Utils.GetHdnDate(verzekering.ingangsdatum);
            dekking1.DuurInMnd = (verzekering.Looptijd.GetValueOrDefault() * 12 + verzekering.LooptijdInMnd.GetValueOrDefault()).ToString();
            dekking1.ProductNaam = verzekering.VrijeProduktnaam?.Substring(0, Math.Min(verzekering.VrijeProduktnaam.Length, 35));
            dekking1.Maatschappij = Enums.GetHdnMaatschappij(verzekering.BestaandMaatschappij);
            if (dekking1.Maatschappij == MaatschappijType.ZZAnders)
                dekking1.MaatschappijOmschr = verzekering.BestaandMaatschappij;
            dekking1.PolisLopendJN = BooleanType.NNee;
            dekking1.PolisNr = verzekering.BestaandPolisNr;
            dekking1.VerzekeringNemer = GetVerzekeringNemers1(verzekering);
            dekking1.Verzekerde = GetVerzekerden1(verzekering);
            dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen1(verzekering);
            dekking1.PremieAfspraken.Add(GetPremieAfspraken1(verzekering));
            dekking1.BerekeningsUitgangspunt = BerekeningsUitgangspuntType.Item02Vankapitaalnaarpremie;
#if VERPANDJN
            if (verzekering.BestaandePolisNietVerpand == DataBooleanFieldT.@true)
                dekking1.VerpandJN = BooleanType.NNee;
            else
                dekking1.VerpandJN = BooleanType.JJa;
#endif
            dekking1.Volgnummer = (_hdnAanvraag.FinancieleDekking.Count + 1).ToString();
            _hdnAanvraag.FinancieleDekking.Add(dekking1);
#endif

        }
    }
}


