﻿#if ABN || ABNGRP || BLG || MONEYOU || NN || ASN || REGIO || SNS || TELLIUS || TEMPLATE
#define BANCAIRESINGLE
#endif

#if !NN && !AEGON 
#define WAARDENLIST
#endif

#if ABN || ABNGRP || ASR || BLG || DELTA || MONEYOU || NIBC || VENN || ASN || LLOYDS || OBVION || REGIO || SNS || WFONDS
#define DUURINMND
#endif

#if AEGON || ASR || DELTA || MUNT || NIBC || VENN || LLOYDS || OBVION || REAAL || TULP
#define GARANTIEVERZEKERINGJN
#endif

#if AEGON || ASR || DELTA || MUNT || NIBC || VENN || LLOYDS || REAAL || TULP
#define OORSPRONKELIJKEAANVANGSDTLOPENDEDEKKING
#endif

#if !ARGENTA && !ATTENS && !COLIBRI && !HYPOTRUST && !NN && !RABO && !WOONNU && !ALLIANZ && !BNP && !DYNAMIC && !HWOONT && !LOT && !MERIUS && !PHILIPS && !REAAL && !ROBUUST && !SYNTRUS && !TELLIUS && !VISTA
#define DEKKINGNR
#endif

#if !BLG && !AEGON && !ASR && !ATTENS && !COLIBRI && !ING && !MUNT && !NN && !RABO && !VENN && !WOONNU && !ASN && !DYNAMIC && !HWOONT && !LLOYDS && !LOT && !MERIUS && !OBVION && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !SYNTRUS && !TELLIUS && !TULP && !VISTA
#define EINDWAARDEDEKKING
#endif

#if !NN && !RABO && !WOONNU && !BLG && !AEGON && !ATTENS && !COLIBRI  && !ASN && !DYNAMIC && !HWOONT && !LOT && !MERIUS && !PHILIPS && !REAAL && !REGIO && !ROBUUST && !SNS && !SYNTRUS && !TELLIUS && !VISTA
#define CONTRACTAFKOOPWAARDE
#endif

#if !ABN && !ATTENS && !ABNGRP && !MONEYOU && !NN && !SYNTRUS && !TELLIUS 
#define LENINGDEELUSETYPE1DEKKING
#endif

#if   !DOEDEZENOGMEE 
#define LENINGDEELUSETYPEBANCAIREDEKKING
#endif

#if !ABN && !ATTENS && !ABNGRP && !MONEYOU && !NN && !AEGON && !ASR && !NIBC
#define AANVRAAGUSETYPE1DEKKING
#endif

#if !ABN && !ABNGRP && !MONEYOU && !NN && !BLG && !ATTENS && !ASN
#define AANVRAAGDEKKINGLIST
#endif

#if !ABN && !ABNGRP && !MONEYOU && !BLG && !ASN && !REGIO && !SNS
#define LENINGDEELDEKKINGLIST
#endif

#if !ABN && !ABNGRP && !MONEYOU
#define FISCAALGERUISLOZEVOORTZETTINGSINGLE
#endif

#if !AEGON
#define CONTRACTANTLIST
#endif


using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#endif

namespace HDN.Factories
{
    public partial class BaseFactory
    {
        static List<VerzekeringNemerEntiteitType> GetVerzekeringNemers(GemengdeVerzekeringDataT verzekering)
        {
            List<VerzekeringNemerEntiteitType> verzekeringNemers = new();
            int verzekeringNemerVolgnr = 1;
            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekeringNemerEntiteitType verzekeringNemer = new();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.ClientRef;
                verzekeringNemers.Add(verzekeringNemer);
                verzekeringNemerVolgnr++;
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekeringNemerEntiteitType verzekeringNemer = new();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.PartnerRef;
                verzekeringNemers.Add(verzekeringNemer);
                
            }

            return verzekeringNemers;
        }

        static List<ContractantEntiteitType> GetContractanten(GemengdeVerzekeringDataT verzekering)
        {
            List<ContractantEntiteitType> verzekeringNemers = new();
            int verzekeringNemerVolgnr = 1;
            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                ContractantEntiteitType verzekeringNemer = new();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.ClientRef;
                verzekeringNemers.Add(verzekeringNemer);
                verzekeringNemerVolgnr++;
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                ContractantEntiteitType verzekeringNemer = new();
                verzekeringNemer.Volgnummer = verzekeringNemerVolgnr.ToString();
                verzekeringNemer.RefPartijNAWData.IDREF = Constants.PartnerRef;
                verzekeringNemers.Add(verzekeringNemer);
            }

            return verzekeringNemers;
        }

        static List<VerzekerdeEntiteitType> GetVerzekerden(GemengdeVerzekeringDataT verzekering)
        {
            List<VerzekerdeEntiteitType> verzekerden = new();
            int verzekerdeVolgnr = 1;

            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekerdeEntiteitType verzekerde = new();
                verzekerde.Volgnummer = verzekerdeVolgnr.ToString();
                verzekerde.RefPartijNAWData.IDREF = Constants.ClientRef;
                verzekerden.Add(verzekerde);
                verzekerdeVolgnr++;
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                VerzekerdeEntiteitType verzekerde = new();
                verzekerde.Volgnummer = verzekerdeVolgnr.ToString();
                verzekerde.RefPartijNAWData.IDREF = Constants.PartnerRef;
                verzekerden.Add(verzekerde);
            }
            return verzekerden;
        }

        static List<VerzekerdeBedragenEntiteitType> GetVerzekerdeBedragen(GemengdeVerzekeringDataT verzekering, bool meeVerhuisd)
        {
            List<VerzekerdeBedragenEntiteitType> verzekerdeBedragen = new();
            VerzekerdeBedragenEntiteitType bedrag = new();

            if ((verzekering.Verzekerden == EigenaarEnum.Cliënt) || (verzekering.Verzekerden == EigenaarEnum.Beiden))
            {
                bedrag = new VerzekerdeBedragenEntiteitType
                {
                    Volgnummer = (verzekerdeBedragen.Count + 1).ToString(),
                    VerzekerdKap = Utils.GetHdnDecimal(verzekering.BedragClient),
                    CodeUitkering = SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingsmethodeClient),
                    SoortVerzekerdKap = SoortVerzekerdKapType.Item01Verzekerdbedragbijoverlijden,
                    BelastingBox = BelastingBoxType.Item03Box3
                };
                if (verzekering.FiscaleBehandeling == FiscaleBehandelingKVEnum.Box1)
                    bedrag.BelastingBox = BelastingBoxType.Item01Box1;
#if EINDWAARDEDEKKING
                Utils.SetHdnDecimal(verzekering.BedragClientDaaltNaar, bedrag, nameof(bedrag.EindWaardeDekking));
                bedrag.DuurInMnd = (verzekering.Looptijd.GetValueOrDefault() * 12 + verzekering.LooptijdInMnd.GetValueOrDefault()).ToString();
#endif
                if (verzekering.Verzekerden == EigenaarEnum.Beiden)
                {
                    if (bedrag.CodeUitkering == SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingsmethodePartner))
                        bedrag.VerzekerdKap2 = Utils.GetHdnDecimal(verzekering.BedragPartner);
                }
                verzekerdeBedragen.Add(bedrag);
            }

            if ((verzekering.Verzekerden == EigenaarEnum.Partner))
            {
                bedrag = new VerzekerdeBedragenEntiteitType
                {
                    Volgnummer = (verzekerdeBedragen.Count + 1).ToString(),
                    VerzekerdKap = Utils.GetHdnDecimal(verzekering.BedragPartner),
                    CodeUitkering = SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingsmethodePartner),
                    SoortVerzekerdKap = SoortVerzekerdKapType.Item01Verzekerdbedragbijoverlijden,
                    BelastingBox = BelastingBoxType.Item03Box3
                };
                if (verzekering.FiscaleBehandeling == FiscaleBehandelingKVEnum.Box1)
                    bedrag.BelastingBox = BelastingBoxType.Item01Box1;
#if BEDRAGDAALTNAAR
                Utils.SetHdnDecimal(verzekering.BedragPartnerDaaltNaar, bedrag, nameof(bedrag.EindWaardeDekking));
                bedrag.DuurInMnd = (verzekering.Looptijd.GetValueOrDefault() * 12 + verzekering.LooptijdInMnd.GetValueOrDefault()).ToString();
#endif          
                verzekerdeBedragen.Add(bedrag);

            }

            bedrag = new VerzekerdeBedragenEntiteitType
            {
                Volgnummer = (verzekerdeBedragen.Count + 1).ToString(),
                VerzekerdKap = Utils.GetHdnDecimal(verzekering.KapitaalBijLeven),
                CodeUitkering = SpecialUtils.GetHdnCodeUitkering(verzekering.DekkingsmethodeClient),
                SoortVerzekerdKap = SoortVerzekerdKapType.Item02Bedragbijlevenexpiratieprognosekapitaal,
                BelastingBox = BelastingBoxType.Item03Box3
            };
            if (verzekering.FiscaleBehandeling == FiscaleBehandelingKVEnum.Box1)
                bedrag.BelastingBox = BelastingBoxType.Item01Box1;

            verzekerdeBedragen.Add(bedrag);

            return verzekerdeBedragen;
        }

        PremieAfsprakenEntiteitType GetPremieAfspraken(GemengdeVerzekeringDataT verzekering, bool meeVerhuisd)
        {
            PremieAfsprakenEntiteitType afspraken = new();
            afspraken.Volgnummer = "1";
            afspraken.AanvangNaIngangsDtInMnd = "0";
            afspraken.BetalingsTermijn = BetalingsTermijnType.Item01permaand;
            afspraken.SoortPremie = PremieType.Item03Premietotaalnormaal;
            afspraken.DuurInMnd = (verzekering.LooptijdPremie.GetValueOrDefault() * 12 + verzekering.LooptijdPremieInMnd.GetValueOrDefault()).ToString();
            if (meeVerhuisd)
            {
                afspraken.PremieBedrag = Utils.GetHdnDecimalZonderAfronden(verzekering.BestaandePremie);
            }
            else
            {
                afspraken.PremieBedrag = Utils.GetHdnDecimalZonderAfronden(verzekering.Premie);
            }
            afspraken.RefDebiteurNAWData = _clientRef;
            if (verzekering.Verzekerden == EigenaarEnum.Partner)
                afspraken.RefDebiteurNAWData = _partnerRef;

            return afspraken;
        }

        InlegAfsprakenEntiteitType GetInlegAfspraken(GemengdeVerzekeringDataT verzekering, bool meeVerhuisd)
        {
            InlegAfsprakenEntiteitType afspraken = new();
            afspraken.Volgnummer = "1";
            afspraken.AanvangNaIngangsDtInMnd = "0";
            afspraken.BetalingsTermijn = BetalingsTermijnType.Item01permaand;
            afspraken.SoortInleg = InlegType.Item01Periodiekeinleg;
            afspraken.DuurInMnd = (verzekering.LooptijdPremie.GetValueOrDefault() * 12 + verzekering.LooptijdPremieInMnd.GetValueOrDefault()).ToString();
            if (meeVerhuisd)
            {
                afspraken.InlegBedrag = Utils.GetHdnDecimalZonderAfronden(verzekering.BestaandePremie);
            }
            else
            {
                afspraken.InlegBedrag = Utils.GetHdnDecimalZonderAfronden(verzekering.Premie);
            }

            afspraken.RefDebiteurNAWData = _clientRef;
            if (verzekering.Verzekerden == EigenaarEnum.Partner)
                afspraken.RefDebiteurNAWData = _partnerRef;

            return afspraken;
        }

        static WaardenEntiteitType GetWaarden(GemengdeVerzekeringDataT verzekering, bool meeVerhuisd)
        {
            WaardenEntiteitType waarden = new();
            waarden.Volgnummer = "1";
            waarden.SoortWaarde = SoortWaardeType.Item01Waardeopeinddatum;
            waarden.BedragWaarde = Utils.GetHdnDecimal(verzekering.KapitaalBijLeven);
            waarden.BelastingBox = (verzekering.FiscaleBehandeling == FiscaleBehandelingKVEnum.Box1) ? BelastingBoxType.Item01Box1 : BelastingBoxType.Item03Box3;
            if (meeVerhuisd)
            {
                waarden.RekenRentePct = Utils.GetHdnDecimalZonderAfronden(verzekering.BestaandBrutoRendement);
                waarden.IngangsDtWaarde = Utils.GetHdnDate(verzekering.BestaandePolisDatum);

            }
            else
            {
                waarden.RekenRentePct = Utils.GetHdnDecimalZonderAfronden(verzekering.Rendement);
                waarden.IngangsDtWaarde = Utils.GetHdnDate(DateTime.Now.AddMonths(1));
            }


            return waarden;
        }

        static List<FiscaalGeruislozeVoortzettingEntiteitType1> GetFiscaalGeruisloosVoortzetting1(GemengdeVerzekeringDataT verzekering)
        {
            List<FiscaalGeruislozeVoortzettingEntiteitType1> lst = new();
            FiscaalGeruislozeVoortzettingEntiteitType1 obj = new();
            if (verzekering.BestaandePolisFiscBeh == FiscaleBehandelingKVEnum.Box1)
                obj.FiscaleVormDekking = FiscaleVormType.Item03KEW;
            else if (verzekering.BestaandePolisFiscBeh == FiscaleBehandelingKVEnum.Box3vrijgesteld)
                obj.FiscaleVormDekking = FiscaleVormType.Item04Kapitaalverzekeringzondereigenwoningclausule;
            obj.MaatschappijDekkingLopend = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
            if (obj.MaatschappijDekkingLopend == MaatschappijType.ZZAnders)
                obj.MaatschappijDekkingLopendOmschr = verzekering.VrijeMaatschappijNaam;
#if DUURINMND
            obj.DuurInMnd = ((verzekering.BestaandOrigineleLooptijd.GetValueOrDefault() * 12) + verzekering.BestaandOrigineleLooptijdInMnd.GetValueOrDefault()).ToString();
#endif
#if DEKKINGNR
            obj.DekkingNr = verzekering.BestaandePolisNummer;
#endif
            lst.Add(obj);

            return lst;
        }

        static List<FiscaalGeruislozeVoortzettingEntiteitType> GetFiscaalGeruisloosVoortzetting(GemengdeVerzekeringDataT verzekering)
        {
            List<FiscaalGeruislozeVoortzettingEntiteitType> lst = new();
            FiscaalGeruislozeVoortzettingEntiteitType obj = new();
            if (verzekering.BestaandePolisFiscBeh == FiscaleBehandelingKVEnum.Box1)
                obj.FiscaleVormDekking = FiscaleVormType.Item03KEW;
            else if (verzekering.BestaandePolisFiscBeh == FiscaleBehandelingKVEnum.Box3vrijgesteld)
                obj.FiscaleVormDekking = FiscaleVormType.Item04Kapitaalverzekeringzondereigenwoningclausule;
#if OORSPRONKELIJKEAANVANGSDTLOPENDEDEKKING
            obj.OorspronkelijkeAanvangsDtLopendeDekking = Utils.GetHdnDate(verzekering.BestaandePolisDatum);
#endif
            int OorspronkelijkeLooptijd = (verzekering.BestaandOrigineleLooptijd.GetValueOrDefault() * 12) + verzekering.BestaandOrigineleLooptijdInMnd.GetValueOrDefault();
            DateTime startDate = Convert.ToDateTime(verzekering.BestaandePolisDatum);
            obj.OorspronkelijkeEindDtLopendeDekking = Utils.GetHdnDate(startDate.AddMonths(OorspronkelijkeLooptijd));
            obj.PremieBedrag = Utils.GetHdnDecimalZonderAfronden(verzekering.BestaandePremie);
#if GARANTIEVERZEKERINGJN
            Utils.SetHdnYesNo(verzekering.GarantieVerzekeringJN, obj, nameof(obj.GarantieVerzekeringJN)); 
#endif
            obj.DekkingNr = verzekering.BestaandePolisNummer;
            lst.Add(obj);

            return lst;
        }
        protected void BuildMeeverhuisdeLevensVerzekering(GemengdeVerzekeringDataT verzekering, LeningdeelEntiteitType leningdeel)
        {
            if (verzekering.IsBankspaar == DataBooleanFieldT.@true)
                BuildMeeverhuisdeBancaireDekking(verzekering, leningdeel);
            else
                BuildMeeverhuisdeFinancieleDekking(verzekering, leningdeel);
        }
        protected void BuildMeeverhuisdeFinancieleDekking(GemengdeVerzekeringDataT verzekering, LeningdeelEntiteitType leningdeel)
        {
#if LENINGDEELUSETYPE1DEKKING
            FinancieleDekkingEntiteitType1 dekking1 = new FinancieleDekkingEntiteitType1();
#else
            FinancieleDekkingEntiteitType dekking1 = new();
#endif
            if (verzekering.IsBankspaar == DataBooleanFieldT.@true)
                dekking1.CodeFinancieleDekking = CodeFinancieleDekkingType.Item07Spaarverzekering;
            else
                dekking1.CodeFinancieleDekking = CodeFinancieleDekkingType.Item01levensverzekering;
            if (verzekering.AfkoopwaardeDoorVoortzetting == DataBooleanFieldT.@true)
            {
                dekking1.FiscaalGeruislozeVoortzettingJN = BooleanType.JJa;
                dekking1.PolisOmzetting = BooleanType.JJa;
            }
            else
            {
                dekking1.FiscaalGeruislozeVoortzettingJN = BooleanType.NNee;
                dekking1.PolisOmzetting = BooleanType.NNee;
            }
            dekking1.AanvangsDt = Utils.GetHdnDate(verzekering.BestaandePolisDatum);
            dekking1.DuurInMnd = (verzekering.BestaandOrigineleLooptijd.GetValueOrDefault() * 12 + verzekering.BestaandOrigineleLooptijdInMnd.GetValueOrDefault()).ToString();
            dekking1.ProductNaam = verzekering.VrijeProduktnaam;
            dekking1.Maatschappij = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
            if (dekking1.Maatschappij == MaatschappijType.ZZAnders)
                dekking1.MaatschappijOmschr = verzekering.VrijeMaatschappijNaam;
            if (verzekering.AfkoopwaardeDoorVoortzetting == DataBooleanFieldT.@true)
                dekking1.PolisLopendJN = BooleanType.NNee;
            else
                dekking1.PolisLopendJN = BooleanType.JJa;
            dekking1.MaatschappijPolisLopend = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
            if (dekking1.MaatschappijPolisLopend == MaatschappijType.ZZAnders)
                dekking1.MaatschappijPolisLopendOmschr = verzekering.VrijeMaatschappijNaam;
            dekking1.PolisNr = verzekering.BestaandePolisNummer;

            if (verzekering.BestaandePolisNietVerpand == DataBooleanFieldT.@true)
                dekking1.VerpandJN = BooleanType.NNee;
            else
                dekking1.VerpandJN = BooleanType.JJa;
            dekking1.VerzekeringNemer = GetVerzekeringNemers(verzekering);
            dekking1.Verzekerde = GetVerzekerden(verzekering);
            dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen(verzekering, true);
            dekking1.PremieAfspraken.Add(GetPremieAfspraken(verzekering, true));

#if LENINGDEELDEKKINGLIST
            dekking1.Volgnummer = (leningdeel.FinancieleDekking.Count + 1).ToString();
            leningdeel.FinancieleDekking.Add(dekking1);
#else
            leningdeel.FinancieleDekking = dekking1;
#endif
        }

        protected void BuildMeeverhuisdeBancaireDekking(GemengdeVerzekeringDataT verzekering, LeningdeelEntiteitType leningdeel)
        {

#if LENINGDEELUSETYPEBANCAIREDEKKING
            BancaireDekkingEntiteitType dekking1 = new();
#else
            BancaireDekkingEntiteitType1 dekking1 = new BancaireDekkingEntiteitType1();
#endif
            if (verzekering.IsBankspaar == DataBooleanFieldT.@true)
                dekking1.CodeBancaireDekking = CodeBancaireDekkingType.Item01Spaarrekening;
            else
                dekking1.CodeBancaireDekking = CodeBancaireDekkingType.Item02Beleggingsrekeningalleenfondsen;

            if (verzekering.AfkoopwaardeDoorVoortzetting == DataBooleanFieldT.@true)
            {
                dekking1.FiscaalGeruislozeVoortzettingJN = BooleanType.JJa;
                dekking1.ContractOmzettingJN = BooleanType.NNee;
            }
            else
            {
                dekking1.FiscaalGeruislozeVoortzettingJN = BooleanType.NNee;
                dekking1.ContractOmzettingJN = BooleanType.JJa;
            }
            dekking1.ContractLopendJN = BooleanType.JJa;

            dekking1.ContractNr = verzekering.BestaandePolisNummer;
#if CONTRACTAFKOOPWAARDE
            if (verzekering.AfkoopwaardeDoorVoortzetting == DataBooleanFieldT.@true)
                dekking1.ContractAfkoopWaarde = Utils.GetHdnDecimal(verzekering.BestaandePolisAfkoopwaarde);
            else
                dekking1.ContractAfkoopWaarde = Utils.GetHdnDecimal(verzekering.BestaandePolisWaarde);
#endif
            dekking1.AanvangsDt = Utils.GetHdnDate(verzekering.BestaandePolisDatum);
            dekking1.DuurInMnd = (verzekering.BestaandOrigineleLooptijd.GetValueOrDefault() * 12 + verzekering.BestaandOrigineleLooptijdInMnd.GetValueOrDefault()).ToString();
            // todo  HDN naam meegeven via het datamodel?
            dekking1.Aanbieder = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
            List<ContractantEntiteitType> contractanten = GetContractanten(verzekering);
#if  CONTRACTANTLIST
            dekking1.Contractant = GetContractanten(verzekering);
#else
            if ((contractanten != null) && (contractanten.Count > 0))
                dekking1.Contractant = contractanten[0];
#endif
            dekking1.InlegAfspraken.Add(GetInlegAfspraken(verzekering, true));
#if FISCAALGERUISLOZEVOORTZETTINGSINGLE
            dekking1.FiscaalGeruislozeVoortzetting = GetFiscaalGeruisloosVoortzetting1(verzekering);
#else
            //dekking1.FiscaalGeruislozeVoortzetting = GetFiscaalGeruisloosVoortzetting(verzekering);
            List<FiscaalGeruislozeVoortzettingEntiteitType1> lst = GetFiscaalGeruisloosVoortzetting1(verzekering);
            if ((lst != null) && (lst.Count > 0))
                dekking1.FiscaalGeruislozeVoortzetting = lst[0];

#endif

#if WAARDENLIST
            dekking1.Waarden.Add(GetWaarden(verzekering, true));
#else
            dekking1.Waarden = GetWaarden(verzekering, true);
#endif

#if BANCAIRESINGLE
            dekking1.Volgnummer = "1";
            leningdeel.BancaireDekking = dekking1;
#else
            dekking1.Volgnummer = (leningdeel.BancaireDekking.Count + 1).ToString();
            leningdeel.BancaireDekking.Add(dekking1);
#endif
        }

        protected void BuildNieuweLevensVerzekering(GemengdeVerzekeringDataT verzekering, LeningdeelEntiteitType leningdeel = null)
        {
#if LENINGDEELUSETYPE1DEKKING
            FinancieleDekkingEntiteitType1 dekking1 = new FinancieleDekkingEntiteitType1();
#else
            FinancieleDekkingEntiteitType dekking1 = new();
#endif
            if (verzekering.IsBankspaar == DataBooleanFieldT.@true)
                dekking1.CodeFinancieleDekking = CodeFinancieleDekkingType.Item07Spaarverzekering;
            else
                dekking1.CodeFinancieleDekking = CodeFinancieleDekkingType.Item01levensverzekering;
            if (verzekering.AfkoopwaardeDoorVoortzetting == DataBooleanFieldT.@true)
            {
                dekking1.FiscaalGeruislozeVoortzettingJN = BooleanType.JJa;
                dekking1.PolisOmzetting = BooleanType.JJa;
            }
            else
            {
                dekking1.FiscaalGeruislozeVoortzettingJN = BooleanType.NNee;
                dekking1.PolisOmzetting = BooleanType.NNee;
            }
            dekking1.AanvangsDt = Utils.GetHdnDate(verzekering.BestaandePolisDatum);
            dekking1.DuurInMnd = (verzekering.BestaandOrigineleLooptijd.GetValueOrDefault() * 12 + verzekering.BestaandOrigineleLooptijdInMnd.GetValueOrDefault()).ToString();
            dekking1.ProductNaam = verzekering.VrijeProduktnaam.Substring(0, 35);
            dekking1.Maatschappij = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
            if (dekking1.Maatschappij == MaatschappijType.ZZAnders)
                dekking1.MaatschappijOmschr = verzekering.VrijeMaatschappijNaam;
            if (verzekering.AfkoopwaardeDoorVoortzetting == DataBooleanFieldT.@true)
                dekking1.PolisLopendJN = BooleanType.NNee;
            else
                dekking1.PolisLopendJN = BooleanType.JJa;
            dekking1.MaatschappijPolisLopend = Enums.GetHdnMaatschappij(verzekering.VrijeMaatschappijNaam);
            if (dekking1.MaatschappijPolisLopend == MaatschappijType.ZZAnders)
                dekking1.MaatschappijPolisLopendOmschr = verzekering.VrijeMaatschappijNaam;
            dekking1.PolisNr = verzekering.BestaandePolisNummer;

            if (verzekering.BestaandePolisNietVerpand == DataBooleanFieldT.@true)
                dekking1.VerpandJN = BooleanType.NNee;
            else
                dekking1.VerpandJN = BooleanType.JJa;

            dekking1.VerzekeringNemer = GetVerzekeringNemers(verzekering);
            dekking1.Verzekerde = GetVerzekerden(verzekering);
            dekking1.VerzekerdeBedragen = GetVerzekerdeBedragen(verzekering, false);
            dekking1.PremieAfspraken.Add(GetPremieAfspraken(verzekering, false));

#if LENINGDEELDEKKINGLIST            
            dekking1.Volgnummer = (leningdeel.FinancieleDekking.Count + 1).ToString();
            leningdeel.FinancieleDekking.Add(dekking1);
#else
            leningdeel.FinancieleDekking = dekking1;
#endif
        }
    }
}
