﻿#if ASR || CENTRAAL || HYPOTRUST || ING || NIBC || TEMPLATE
#define OPLEIDINGSNIVEAU
#endif

using System;
using System.Collections.Generic;
using System.Text;
using Finix;

#if ASR
using HDN.ASR;
#elif ABN
using HDN.ABN;
#elif ABNGRP
using HDN.ABNGRP;
#elif ING
using HDN.ING;
#elif MUNT
using HDN.MUNT;
#elif ATTENS
using HDN.ATTENS;
#elif RABO
using HDN.RABO;
#elif COLIBRI
using HDN.COLIBRI;
#elif VENN
using HDN.VENN;
#elif AEGON
using HDN.AEGON;
#elif HYPOTRUST
using HDN.HYPOTRUST;
#elif MONEYOU
using HDN.MONEYOU;
#elif CENTRAAL
using HDN.CENTRAAL;
#elif NN
using HDN.NN;
#elif WOONNU
using HDN.WOONNU;
#elif NIBC
using HDN.NIBC;
#elif BLG
using HDN.BLG;
#elif DELTA
using HDN.DELTA;
#elif ARGENTA
using HDN.ARGENTA;
#elif ALLIANZ
using HDN.ALLIANZ;
#elif ASN
using HDN.ASN;
#elif BIJBOUWE
using HDN.BIJBOUWE;
#elif BNP
using HDN.BNP;
#elif DNGB
using HDN.DNGB;
#elif DYNAMIC
using HDN.DYNAMIC;
#elif HWOONT
using HDN.HWOONT;
#elif IQWOON
using HDN.IQWOON;
#elif LLOYDS
using HDN.LLOYDS;
#elif LOT
using HDN.LOT;
#elif MERIUS
using HDN.MERIUS;
#elif OBVION
using HDN.OBVION;
#elif PHILIPS
using HDN.PHILIPS;
#elif REAAL
using HDN.REAAL;
#elif REGIO
using HDN.REGIO;
#elif ROBUUST
using HDN.ROBUUST;
#elif SNS
using HDN.SNS;
#elif SYNTRUS
using HDN.SYNTRUS;
#elif TELLIUS
using HDN.TELLIUS;
#elif TULP
using HDN.TULP;
#elif VISTA
using HDN.VISTA;
#elif WFONDS
using HDN.WFONDS;
#elif CARDIF
using HDN.CARDIF;
#endif

namespace HDN.Factories
{
    public abstract partial class BaseFactory
    {

        protected void BuildHypotheekgevers()
        {
            void BuildHypotheekgever(string idRef, PersoonsGegevensFieldT persoon, InkomenItemDataT[] loonInkomens, SpecificatieInkomenFieldT specificatieInkomen,
                BijAfVerItemDataT bijAf, AnalyseHypotheekverledenDataT hypVerleden, persoonFieldT extraPersoon, ZelfstandigInkomenGegevensFieldT onderneming,
                UPODataT[] upos, OndernemingDataT[] ondernemingen, VerplichtingBKRDataT[] bkrVerplichtingen, EigenaarEnum eigenaar, string pensioenDatum)
            {
                HypotheekgeverEntiteitType hypotheekGever = new();
                if (persoon.HOTelefoonNrWerk != null)
                    hypotheekGever.TelefoonNrMobiel = persoon.HOTelefoonNrWerk.Replace("-", string.Empty);
                if (persoon.HOTelefoonNrPrive != null)
                    hypotheekGever.TelefoonNrPrive = persoon.HOTelefoonNrPrive.Replace("-", string.Empty);
                hypotheekGever.RefPartijNAWData = BuildNatuurlijkPersoon(idRef, persoon);
                Utils.SetHdnYesNo(persoon.HOOoitHypotheekGehad, hypotheekGever, nameof(hypotheekGever.EersteHypotheekOoit));
                hypotheekGever.Emailadres = persoon.HOEMailAdres;
                hypotheekGever.HuwelijkOntbonden = Enums.GetHdnBooleanType(persoon.HOHuwelijkOntbondenscheiding);
                hypotheekGever.HuwelijkOntbondenDt = Utils.GetHdnDate(persoon.HOHuwelijkOntbondenDatum);
                hypotheekGever.BurgerlijkeStaat = Enums.GetHdnBurgerlijkeStaatType(_finixDossier.HuidigeSituatie.AlgemeneGegevens.BurgerlijkeStaat);
                hypotheekGever.EigenWonReserve = Utils.GetHdnDecimal(hypVerleden.Eigenwoningreserve.GetValueOrDefault());
                if (persoon.HOFiscaalInwonerNL != null)
                    hypotheekGever.FiscaalInwonerVanNederlandJN = Enums.GetHdnBooleanType(persoon.HOFiscaalInwonerNL);
                hypotheekGever.UitsluitendOfMedeFiscaalInwonerBuitenNedJN = Enums.GetHdnBooleanType(persoon.HOUitsluitendOfMedeFiscaalInwonerBuitenNed);
                hypotheekGever.HoofdAanspr = Enums.GetHdnBooleanType(persoon.HOHoofdAansprakelijk);
                Utils.SetHdnYesNo(_hypBer.AfwijkendKlantprofiel, hypotheekGever, nameof(hypotheekGever.AfwKlantProfiel));

#if OPLEIDINGSNIVEAU
                if ((persoon.HOOpleiding != null) || !Utils.IsNullable(hypotheekGever.OpleidingsNiveau))
                    hypotheekGever.OpleidingsNiveau = SpecialEnums.getHdnOpleidingsNiveau(persoon.HOOpleiding);
#endif
                BuildUitkering(persoon, specificatieInkomen, bijAf, extraPersoon, upos, pensioenDatum, hypotheekGever);
                BuildLoonInkomen(idRef, persoon, loonInkomens, specificatieInkomen,
                   bijAf, hypVerleden, extraPersoon, onderneming,
                   upos, ondernemingen, pensioenDatum, hypotheekGever);
                BuildZelfstandigInkomen(idRef, persoon, loonInkomens, specificatieInkomen,
                   bijAf, hypVerleden, extraPersoon, onderneming,
                   upos, ondernemingen, pensioenDatum, hypotheekGever);
                BuildVerplichtingen(specificatieInkomen, bijAf, eigenaar, hypotheekGever, persoon);
                BuildGeregistreerdeLening(bkrVerplichtingen, eigenaar, hypotheekGever);
                BuildBijtellingen(specificatieInkomen, bijAf, hypotheekGever);
                BuildInkomenUitVermogen(specificatieInkomen, bijAf, hypotheekGever);


                hypotheekGever.Identificatie.Volgnummer = "1";
                hypotheekGever.Identificatie.LegitimatieAfgifteDt = Utils.GetHdnDate(persoon.LegitimatieDatum);
                hypotheekGever.Identificatie.LegitimatieEindDt = Utils.GetHdnDate(persoon.HOLegitimatieEindDt);
                hypotheekGever.Identificatie.LegitimatieAfgiftePlaats = persoon.LegitimatiePlaats;
                if (persoon.LegitimatieType != null)
                    hypotheekGever.Identificatie.LegitimatieSoort = Enums.GetHdnLegitimatie(persoon.LegitimatieType.GetValueOrDefault());
                hypotheekGever.Identificatie.LegitimatieNr = persoon.LegitimatieNummer;
                hypotheekGever.Identificatie.VerblijfsVergunningNr = persoon.HOVerblijfsvergunningNummer;

                if (persoon.HOLandFiscaleWoonstaat != null)
                {
                    FiscaleWoonstaatEntiteitType woonstaat = new();
                    woonstaat.Volgnummer = "1";
                    Enums.SetHdnLand(persoon.HOLandFiscaleWoonstaat, woonstaat, nameof(woonstaat.LandFiscaleWoonstaat));
                    woonstaat.TIN = persoon.HOTIN;
                    hypotheekGever.FiscaleWoonstaat.Add(woonstaat);
                }
                hypotheekGever.Volgnummer = (_hdnAanvraag.Hypotheekgever.Count + 1).ToString();
                _hdnAanvraag.Hypotheekgever.Add(hypotheekGever);
            }

            var hs = _finixDossier.HuidigeSituatie;

            BuildHypotheekgever(Constants.ClientRef, hs.AlgemeneGegevens.ClientGegevens, hs.InkomenGegevens.LoonInkomensClient,
                hs.InkomenGegevens.SpecInkomenClient, hs.InkomenGegevens.BijAfVersClient, hs.HypVerledenClient, _finixDossier.ExtraHDNInfo.Client,
                hs.InkomenGegevens.ZelfstandigInkomenGegevens.ClientGegevens, hs.InkomenGegevens.UPOCL, hs.InkomenGegevens.OndernemingenClient,
                hs.InkomenGegevens.Verplichtingen, EigenaarEnum.Cliënt, _finixDossier.ExtraHDNInfo.Client.PensioenDatum);
            if (_finixDossier.HuidigeSituatie.AlgemeneGegevens.PartnerAanwezig == DataBooleanFieldT.@true)
            {
                // zet adres info goed
                if (hs.AlgemeneGegevens.PartnerHeeftAdresClient == DataBooleanFieldT.@true)
                {
                    hs.AlgemeneGegevens.PartnerGegevens.Straat = hs.AlgemeneGegevens.ClientGegevens.Straat;
                    hs.AlgemeneGegevens.PartnerGegevens.Huisnummer = hs.AlgemeneGegevens.ClientGegevens.Huisnummer;
                    hs.AlgemeneGegevens.PartnerGegevens.Huisnummertoevoeging = hs.AlgemeneGegevens.ClientGegevens.Huisnummertoevoeging;
                    hs.AlgemeneGegevens.PartnerGegevens.Postcode = hs.AlgemeneGegevens.ClientGegevens.Postcode;
                    hs.AlgemeneGegevens.PartnerGegevens.Plaats = hs.AlgemeneGegevens.ClientGegevens.Plaats;

                }
                BuildHypotheekgever(Constants.PartnerRef, hs.AlgemeneGegevens.PartnerGegevens, hs.InkomenGegevens.LoonInkomensPartner,
                    hs.InkomenGegevens.SpecInkomenPartner, hs.InkomenGegevens.BijAfVersPartner, hs.HypVerledenPartner, _finixDossier.ExtraHDNInfo.Partner,
                    hs.InkomenGegevens.ZelfstandigInkomenGegevens.PartnerGegevens, hs.InkomenGegevens.UPOPA, hs.InkomenGegevens.OndernemingenPartner,
                    hs.InkomenGegevens.Verplichtingen, EigenaarEnum.Partner, _finixDossier.ExtraHDNInfo.Partner.PensioenDatum);
            }
        }
        
    }
}
