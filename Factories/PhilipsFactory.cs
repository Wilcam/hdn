﻿using Finix;
using HDN.PHILIPS;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if PHILIPS

namespace HDN.Factories
{
    public class PhilipsFactory : BaseFactory
    {

        public PhilipsFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.PPPhilipsPensioenfonds;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.PPPhilipsPensioenfonds;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.PP001Aflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.PP002Annuïteitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.PP003Lineairehypotheek;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.PP004Levenhypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:              
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }
    };
}

#endif