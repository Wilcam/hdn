﻿using Finix;
using HDN.SNS;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if SNS

namespace HDN.Factories
{
    public class SnsFactory : BaseFactory
    {

        public SnsFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.SNSNSBank;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.SNSNSBank;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            if (isRestschuldLening(leningdeelFinix))
                return CodeDeelMijType.SN115SNSBudgetRestschuldfinanciering;

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.SN106SNSBudgetAflossingsvrijeHypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.SN108SNSBudgetAnnuitaireHypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.SN107SNSBudgetLineaireHypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.SN003SNSVariabeleRente;
            else
                return CodeRenteMijType.SN001SNSRentevast;
        }
    }
}

#endif