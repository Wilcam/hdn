﻿using Finix;
using HDN.REGIO;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if REGIO

namespace HDN.Factories
{
    public class RegioFactory : BaseFactory
    {

        public RegioFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.RGRegioBank;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.RGRegioBank;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.RG103BudgetAflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.RG105BudgetAnnuitairehypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.RG104BudgetLiniairehypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.RG003Variabelerente;
            else
                return CodeRenteMijType.RG001Rentevast;
        }
    }
}

#endif