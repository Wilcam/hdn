﻿using Finix;
using HDN.AEGON;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if AEGON

namespace HDN.Factories
{
    public class AegonFactory : BaseFactory
    {

        public AegonFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.AEAegonVerzekeringen;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.AEAEGONVerzekeringen;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
     
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.AE001AegonAflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.AE002AegonAnnuïteitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.AE009AegonLineaireHypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.AE008AegonBankspaarHypotheek;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.AE006AegonSpaarhypotheek;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.AE003AegonLevenhypotheek;
                case SoortHypotheekEnum.Effectenhypotheek:                  
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:               
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
            }
        }


        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            List<int> theList = SpecialUtils.GetHypotheekOpties(_hypBer.ProductKenmerken);
            if (theList.Exists(x => x == (int)HypotheekoptiesFinix.Opties.pkAEGOfferteoptiesKeuze))
                return CodeRenteMijType.AE002Vasterenteofferterente;

            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.AE003Variabelerente;
            else
                return CodeRenteMijType.AE001Vasterentedagrente;
        }
    };
}

#endif
