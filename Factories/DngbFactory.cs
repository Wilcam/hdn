using Finix;
using HDN.DNGB;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if DNGB

namespace HDN.Factories
{
    public class DngbFactory : BaseFactory
    {

        public DngbFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.DNDNGB;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.DNDNGB;
        }

        

    };
}

#endif