using Finix;
using HDN.ASN;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if ASN

namespace HDN.Factories
{
    public class AsnFactory : BaseFactory
    {

        public AsnFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.AKASNBank;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.AKASNBank;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.AK003ASNAflossingsvrijeHypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.AK001ASNAnnu´teitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.AK002ASNLineaireHypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT? leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            return CodeRenteMijType.AK001ASNRentevast;
        }

    };
}

#endif