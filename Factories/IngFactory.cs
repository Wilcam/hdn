﻿using Finix;
using HDN.ING;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if ING

namespace HDN.Factories
{
    public class IngFactory : BaseFactory
    {

        public IngFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.INING;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.ININGBank;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.IN001AflossingsvrijeHypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.IN017RestschuldfinancieringAnnuitair;
                    return CodeDeelMijType.IN002Annuiteitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.IN018RestschuldfinancieringLineair;
                    return CodeDeelMijType.IN003LineaireHypotheek;
                case SoortHypotheekEnum.Krediethypotheek:
                    return CodeDeelMijType.IN008Krediethypotheek;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.IN010Spaarhypotheek;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.IN006Levenhypotheek;
                case SoortHypotheekEnum.Effectenhypotheek:
                    return CodeDeelMijType.IN006Levenhypotheek;
                case SoortHypotheekEnum.Hybridehypotheek:
                    return CodeDeelMijType.IN006Levenhypotheek;
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                    return CodeDeelMijType.IN006Levenhypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.IN016Bankspaarhypotheek;
                case SoortHypotheekEnum.BEWhypotheek:
                    return CodeDeelMijType.IN007Beleggershypotheek;
                case SoortHypotheekEnum.Ongedefinieerd:
                    return CodeDeelMijType.IN001AflossingsvrijeHypotheek;
                default:
                    break;
            }

            return CodeDeelMijType.IN001AflossingsvrijeHypotheek;
        }

      

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT? leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            if (leningdeelFinix == null)
            {
                if ((identiek != null) && (identiek == DataBooleanFieldT.@true))
                    return CodeRenteMijType.IN002RestantRentevastePeriode;
                return CodeRenteMijType.IN001Middelrente;

            }
            return CodeRenteMijType.IN003ActueleRente;

        }

        protected override CodeHypotheekOptiesMijType? GetHypotheekOpties()
        {
            List<int> theList = SpecialUtils.GetHypotheekOpties(_hypBer.ProductKenmerken);
            if (theList.Exists(x => x == (int)HypotheekoptiesFinix.Opties.pkINSalarisrekRente))
                return CodeHypotheekOptiesMijType.IN012ActieveBetaalKlant;
            return null;
        }
    };
}

#endif