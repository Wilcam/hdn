﻿using Finix;
using HDN.BIJBOUWE;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;


#if BIJBOUWE

namespace HDN.Factories
{
    public class BijbouweFactory : BaseFactory
    {

        public BijbouweFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.BBBijBouwe;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.BBBijBouwe;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
  
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.BB001bijBouweAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.BB002bijBouweAnnuitair;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.BB003bijBouweLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            if (leningdeelFinix == null)
            {
                return CodeRenteMijType.BB001bijBouweVasteRente;
            }

            //  RVPsoortT = ( rvpsOngedefinieerd,rvpsVast,rvpsVariabel, rvpsBandbreedte, rvpsCorrectierente, rvpsPlafond, rvpsOverbrug, rvpsRentePlafond);
            switch (_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value)
            {
                case 1:
                    return CodeRenteMijType.BB001bijBouweVasteRente;
                case 2:
                    return CodeRenteMijType.BB002bijBouweVariabeleRente;
                case 5:
                case 3:
                case 4:
                default:
                    GlobalOutputInfo.buildErrors.Add("RenteAfspraakType: ongeldige parameter, default waarde gebruikt");
                    return (CodeRenteMijType)0;
            }
        }
    };
}

#endif