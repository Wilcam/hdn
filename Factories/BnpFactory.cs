using Finix;
using HDN.BNP;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if BNP

namespace HDN.Factories
{
    public class BnpFactory : BaseFactory
    {

        public BnpFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.BNBNPParibasPersonalFinance;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.BNBNPParibasPersonalFinance;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.BN003AflossingsvrijeHypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                   
                        return CodeDeelMijType.BN001AnnuiteitenHypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    
                        return CodeDeelMijType.BN004LineaireHypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.BN013BankspaarHypotheek;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.BN012SpaarverzekeringHypotheek;
                case SoortHypotheekEnum.Hybridehypotheek:
                    return CodeDeelMijType.BN007HybrideHypotheek;
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:               
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                     GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
           }
        }

    };
}

#endif