﻿using Finix;
using HDN.DELTA;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;


#if DELTA

namespace HDN.Factories
{
    public class DeltaFactory : BaseFactory
    {

        public DeltaFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.DLDeltaLloyd;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.DLDeltaLloyd;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {                
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.DL003DeltaLloydFixeAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.DL036RestschuldAnnuïtairterugbetalen;
                    return CodeDeelMijType.DL001DeltaLloydAnnuiteit;
                case SoortHypotheekEnum.Lineairehypotheek:

                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.DL037RestschuldLineairterugbetalen;
                    return CodeDeelMijType.DL006DeltaLloydLineair;              
                case SoortHypotheekEnum.Bankspaarhypotheek:              
                case SoortHypotheekEnum.Spaarhypotheek:                  
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
           }
        }
    };
}

#endif