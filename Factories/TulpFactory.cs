﻿using Finix;
using HDN.TULP;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if TULP

namespace HDN.Factories
{
    public class TulpFactory : BaseFactory
    {

        public TulpFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.THTulpHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.THTulpHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.TH002Aflosvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.TH001Annuïteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.TH003Lineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }       
    }
}

#endif