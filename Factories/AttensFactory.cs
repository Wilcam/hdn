﻿using Finix;
using HDN.ATTENS;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if ATTENS

namespace HDN.Factories
{
    public class AttensFactory : BaseFactory
    {

        public AttensFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.AQAttensHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.AQAttensHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.AQ001AttensHypothekenAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.AQ002AttensHypothekenAnnuïteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.AQ003AttensHypothekenLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                   GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
            }
        }
    }
};


#endif