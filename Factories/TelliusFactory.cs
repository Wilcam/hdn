﻿using Finix;
using HDN.TELLIUS;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if TELLIUS

namespace HDN.Factories
{
    public class TelliusFactory : BaseFactory
    {

        public TelliusFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.TLTelliusHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.TLTelliusHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.TL001TelliusHypothekenAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.TL002TelliusHypothekenAnnuïteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.TL003TelliusHypothekenLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }       
    }
}

#endif