﻿using Finix;
using HDN.BLG;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;


#if BLG

namespace HDN.Factories
{
    public class BlgFactory : BaseFactory
    {

        public BlgFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.BLBLGHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.BLBLGHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            if (isRestschuldLening(leningdeelFinix))
                return CodeDeelMijType.BL008BLGRestschuldfinanciering;

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.BL002BLGAflossingsvrijeHypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.BL001BLGAnnuïteitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.BL007BLGLineaireHypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            if (leningdeelFinix == null)
            {
                if ((identiek != null) && (identiek == DataBooleanFieldT.@false))
                    return CodeRenteMijType.BL007BLGMiddelrente;

                return CodeRenteMijType.BL001BLGRentevast;
            }

            //  RVPsoortT = ( rvpsOngedefinieerd,rvpsVast,rvpsVariabel, rvpsBandbreedte, rvpsCorrectierente, rvpsPlafond, rvpsOverbrug, rvpsRentePlafond);
            switch (_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value)
            {
                case 1:
                    return CodeRenteMijType.BL001BLGRentevast;
                case 2:
                    if (_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].PeriodeMND == 1)
                        return CodeRenteMijType.BL003BLGVariabelerente;
                    else
                        return CodeRenteMijType.BL007BLGMiddelrente;
                case 5:
                    return CodeRenteMijType.BL005BLGPlafondrente;
                case 3:
                case 4:
                default:
                    GlobalOutputInfo.buildErrors.Add("RenteAfspraakType: ongeldige parameter, default waarde gebruikt");
                    return (CodeRenteMijType)0;
            }
        }
    };
}

#endif