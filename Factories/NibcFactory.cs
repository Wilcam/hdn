﻿using Finix;
using HDN.NIBC;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;


#if NIBC

namespace HDN.Factories
{
    public class NibcFactory : BaseFactory
    {

        public NibcFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.NINIBCBankNV;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.NINIBCDirectHypothekenBV;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix,  CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.NI002NIBCDirectAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.NI005NIBCDirectRestschuldAnnuïteit;
                    return CodeDeelMijType.NI001NIBCDirectAnnuiteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.NI004NIBCDirectRestschuldLineair;
                    return CodeDeelMijType.NI003NIBCDirectLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:              
                case SoortHypotheekEnum.Spaarhypotheek:                  
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                   GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
            }
        }
    };
}

#endif