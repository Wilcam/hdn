using Finix;
using HDN.HWOONT;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if HWOONT

namespace HDN.Factories
{
    public class HwoontFactory : BaseFactory
    {

        public HwoontFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.HVHollandWoont;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.HVHollandWoont;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.HV001HollandWoontAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.HV002HollandWoontAnnuitair;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.HV003HollandWoontLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            return CodeRenteMijType.HV001HollandWoontVasteRente;
        }
    };
}

#endif