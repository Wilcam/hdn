using Finix;
using HDN.DYNAMIC;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if DYNAMIC

namespace HDN.Factories
{
    public class DynamicFactory : BaseFactory
    {

        public DynamicFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.DYDynamic;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.DYDynamic;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.DY001DynamicAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.DY002DynamicAnnuitair;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.DY003DynamicLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.DY002DynamicVariabeleRente;
            else
                return CodeRenteMijType.DY001DynamicVasteRente;
        }
    };
}

#endif