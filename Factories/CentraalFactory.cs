﻿using Finix;
using HDN.CENTRAAL;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if CENTRAAL

namespace HDN.Factories
{
    public class CentraalFactory : BaseFactory
    {

        public CentraalFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.CBCentraalBeheer;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.CBCentraalBeheer;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                        return CodeDeelMijType.CB001CentraalBeheerThuisHypotheekAflossingsvrij;
                    else if (leningMij == CodeLeningMijType.CB003CentraalBeheerLeefHypotheek)
                        return CodeDeelMijType.CB017CentraalBeheerLeefHypotheekAflossingsvrij;
                    return CodeDeelMijType.CB009CentraalBeheerBasisLijnAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                    {
                        if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                            return CodeDeelMijType.CB007CentraalBeheerThuisHypotheekRestschuldAnnuïtair;
                        return CodeDeelMijType.CB015CentraalBeheerBasisLijnRestschuldAnnuïtair;
                    }
                    if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                        return CodeDeelMijType.CB002CentraalBeheerThuisHypotheekAnnuïteit;
                    else if (leningMij == CodeLeningMijType.CB003CentraalBeheerLeefHypotheek)
                        return CodeDeelMijType.CB018CentraalBeheerLeefHypotheekAnnuïtair;
                    return CodeDeelMijType.CB010CentraalBeheerBasisLijnAnnuïteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                    {
                        if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                            return CodeDeelMijType.CB008CentraalBeheerThuisHypotheekRestschuldLineair;
                        return CodeDeelMijType.CB016CentraalBeheerBasisLijnRestschuldLineair;
                    }
                    if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                        return CodeDeelMijType.CB003CentraalBeheerThuisHypotheekLineair;
                    else if (leningMij == CodeLeningMijType.CB003CentraalBeheerLeefHypotheek)
                        return CodeDeelMijType.CB019CentraalBeheerLeefHypotheekLineair;
                    return CodeDeelMijType.CB011CentraalBeheerBasisLijnLineair;
                case SoortHypotheekEnum.Spaarhypotheek:
                    if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                        return CodeDeelMijType.CB005CentraalBeheerThuisHypotheekSpaar;
                    return CodeDeelMijType.CB013CentraalBeheerBasisLijnSpaar;
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                    if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                        return CodeDeelMijType.CB014CentraalBeheerBasisLijnLeven;
                    return CodeDeelMijType.CB014CentraalBeheerBasisLijnLeven;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                        return CodeDeelMijType.CB012CentraalBeheerBasisLijnBankspaar;
                    return CodeDeelMijType.CB012CentraalBeheerBasisLijnBankspaar;
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            if (leningMij == CodeLeningMijType.CB001CentraalBeheerThuisHypotheek)
                return CodeRenteMijType.CB001CentraalBeheerThuisHypotheekVasteRente;
            return CodeRenteMijType.CB003CentraalBeheerBasisLijnVasteRente;
        }
    };
}

#endif
