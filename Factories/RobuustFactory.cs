﻿using Finix;
using HDN.ROBUUST;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if ROBUUST

namespace HDN.Factories
{
    public class RobuustFactory : BaseFactory
    {

        public RobuustFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.RFRobuustHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.RFRobuustHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.RF001RobuustAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.RF002RobuustAnnuïteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.RF003RobuustLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.RF002RobuustVariabeleRente;
            else
                return CodeRenteMijType.RF001RobuustVasteRente;
        }
    }
}

#endif