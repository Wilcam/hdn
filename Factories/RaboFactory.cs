﻿using Finix;
using HDN.RABO;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if RABO

namespace HDN.Factories
{
    public class RaboFactory : BaseFactory
    {

        public RaboFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.RBRabobank;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.RBRabohypotheekbankNV;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.RB003RabobankAflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (!isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.RB001RabobankAnnuïteitenhypotheek;
                    else
                        return CodeDeelMijType.RB007RabobankAnnuïteitmetrestschuld;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (!isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.RB002RabobankLineaireHypotheek;
                    else
                        return CodeDeelMijType.RB008RabobankLineairmetrestschuld;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.RB004RaboOpbouwHypotheekSparen;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.RB004RaboOpbouwHypotheekSparen;
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                   GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                   return (CodeDeelMijType)0;                 
            }
        }


        protected override CodeHypotheekOptiesMijType? GetHypotheekOpties()
        {
            List<int> theList = SpecialUtils.GetHypotheekOpties(_hypBer.ProductKenmerken);
            if (theList.Exists(x => x == (int)HypotheekoptiesFinix.Opties.pkRabobankBetaalpakket))
                return CodeHypotheekOptiesMijType.RB001Betaalpakketkorting;
            return null;
        }




    };
}

#endif