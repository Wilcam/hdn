﻿using Finix;
using HDN.ARGENTA;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if ARGENTA

namespace HDN.Factories
{
    public class ArgentaFactory : BaseFactory
    {

        public ArgentaFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.ARArgenta;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.ARArgenta;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {

                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.AR003ArgentaAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.AR001ArgentaAnnuiteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.AR012ArgentaLineair;              
                case SoortHypotheekEnum.Bankspaarhypotheek:              
                case SoortHypotheekEnum.Spaarhypotheek:                  
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
           }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT? leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {        
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.AR001VariabeleRente;
            else
                return CodeRenteMijType.AR002VasteRente;
        }

    };
}

#endif