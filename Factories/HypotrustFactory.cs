﻿using Finix;
using HDN.HYPOTRUST;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if HYPOTRUST

namespace HDN.Factories
{
    public class HypotrustFactory : BaseFactory
    {

        public HypotrustFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.HTHypotrust;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.HTHypotrust;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            var soortHypotheek = leningdeelFinix.Soort;

            switch (leningMij)
            {
                case CodeLeningMijType.HT017HypotrustGoedeStartHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT024HypotrustGoedeStartAnnuïteit;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT025HypotrustGoedeStartLineair;
                    break;
                case CodeLeningMijType.HT023HypotrustVrijLevenHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT043HypotrustVrijLevenAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT044HypotrustVrijLevenAnnuïtair;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT045HypotrustVrijLevenLineair;
                    break;
                case CodeLeningMijType.HT016HypotrustOKHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT019HypotrustOKAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT021HypotrustOKAnnuïteit;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT022HypotrustOKLineair;
                    else if (soortHypotheek == SoortHypotheekEnum.Levenhypotheek)
                        return CodeDeelMijType.HT020HypotrustOKLeven;
                    else if (soortHypotheek == SoortHypotheekEnum.Spaarhypotheek)
                        return CodeDeelMijType.HT023HypotrustOKSpaar;
                    break;
                case CodeLeningMijType.HT020HypotrustComfortStandaard:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT032HypotrustComfortStandaardAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT033HypotrustComfortStandaardAnnuïteiten;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT034HypotrustComfortStandaardLineair;
                    else if (soortHypotheek == SoortHypotheekEnum.Levenhypotheek)
                        return CodeDeelMijType.HT035HypotrustComfortStandaardLeven;
                    break;
                case CodeLeningMijType.HT021HypotrustComfortProfijt:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT036HypotrustComfortProfijtAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT037HypotrustComfortProfijtAnnuïteiten;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT038HypotrustComfortProfijtLineair;
                    else if (soortHypotheek == SoortHypotheekEnum.Levenhypotheek)
                        return CodeDeelMijType.HT039HypotrustComfortProfijtLeven;
                    break;
                case CodeLeningMijType.HT018HypotrustWoonBewustHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT026HypotrustWoonBewustAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT027HypotrustWoonBewustAnnuïteit;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT028HypotrustWoonBewustLineair;
                    break;
                case CodeLeningMijType.HT007ZekerWetenHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Spaarhypotheek)
                        return CodeDeelMijType.HT008ZekerWetenHypotheekSpaarhypotheek;
                    break;
                case CodeLeningMijType.HT015HypotrustSpaarOKHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT014HypotrustSpaarOKAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT016HypotrustSpaarOKAnnuïteit;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT017HypotrustSpaarOKLineair;
                    else if (soortHypotheek == SoortHypotheekEnum.Levenhypotheek)
                        return CodeDeelMijType.HT015HypotrustSpaarOKLeven;
                    else if (soortHypotheek == SoortHypotheekEnum.Spaarhypotheek)
                        return CodeDeelMijType.HT018HypotrustSpaarOKSpaar;
                    break;
                case CodeLeningMijType.HT014HypotrustTrend:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT009HypotrustTrendAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT010HypotrustTrendAnnuiteit;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT011HypotrustTrendLineair;
                    else if (soortHypotheek == SoortHypotheekEnum.Levenhypotheek)
                        return CodeDeelMijType.HT012HypotrustTrendLeven;
                    else if (soortHypotheek == SoortHypotheekEnum.Spaarhypotheek)
                        return CodeDeelMijType.HT013HypotrustTrendSpaar;
                    break;
                case CodeLeningMijType.HT010ZekerWetenStartershypotheekProfijt:
                    break;
                case CodeLeningMijType.HT013HypotrustBudget:
                    break;
                case CodeLeningMijType.HT009ZekerWetenStartershypotheek:
                    break;
                case CodeLeningMijType.HT012ZekerWetenBudgetStartershypotheek:
                    break;
                case CodeLeningMijType.HT019HypotrustELANHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT029HypotrustELANAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT030HypotrustELANAnnuïteit;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT031HypotrustELANLineair;
                    break;
                case CodeLeningMijType.HT022HypotrustELANPlusHypotheek:
                    if (soortHypotheek == SoortHypotheekEnum.Aflossingsvrijehypotheek)
                        return CodeDeelMijType.HT040HypotrustELANPlusAflossingsvrij;
                    else if (soortHypotheek == SoortHypotheekEnum.Annuiteitenhypotheek)
                        return CodeDeelMijType.HT041HypotrustELANPlusAnnuïteit;
                    else if (soortHypotheek == SoortHypotheekEnum.Lineairehypotheek)
                        return CodeDeelMijType.HT042HypotrustELANPlusLineair;
                    break;
                case CodeLeningMijType.HT008ZekerWetenHypotheekProfijt:
                    break;
                case CodeLeningMijType.HT011ZekerWetenBudgethypotheek:
                    break;
                default:
                    break;
            }
            GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
            return (CodeDeelMijType)0;

        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
            {

                switch (leningMij)
                {
                    case CodeLeningMijType.HT016HypotrustOKHypotheek:
                        return CodeRenteMijType.HT013HypotrustOKVariabeleRente;
                    case CodeLeningMijType.HT020HypotrustComfortStandaard:
                        return CodeRenteMijType.HT021HypotrustComfortStandaardVariabel;
                    case CodeLeningMijType.HT021HypotrustComfortProfijt:
                        return CodeRenteMijType.HT017HypotrustProfijtVariabeleRente;
                    case CodeLeningMijType.HT015HypotrustSpaarOKHypotheek:
                        return CodeRenteMijType.HT011HypotrustSpaarOKVariabeleRente;
                    case CodeLeningMijType.HT019HypotrustELANHypotheek:
                        return CodeRenteMijType.HT019HypotrustELANVariabeleRente;
                    case CodeLeningMijType.HT022HypotrustELANPlusHypotheek:
                        return CodeRenteMijType.HT024HypotrustELANPlusVariabeleRente;
                    default:
                        return CodeRenteMijType.HT003VRVariabeleRente;
                }
            }

            switch (leningMij)
            {
                case CodeLeningMijType.HT017HypotrustGoedeStartHypotheek:
                    return CodeRenteMijType.HT014HypotrustGoedeStartVasteRente;
                case CodeLeningMijType.HT023HypotrustVrijLevenHypotheek:
                    return CodeRenteMijType.HT025HypotrustVrijLevenVasteRente;
                case CodeLeningMijType.HT016HypotrustOKHypotheek:
                    return CodeRenteMijType.HT012HypotrustOKVasteRente;
                case CodeLeningMijType.HT020HypotrustComfortStandaard:
                    return CodeRenteMijType.HT020HypotrustComfortStandaardVast;
                case CodeLeningMijType.HT021HypotrustComfortProfijt:
                    return CodeRenteMijType.HT022HypotrustComfortProfijtVast;
                case CodeLeningMijType.HT018HypotrustWoonBewustHypotheek:
                    return CodeRenteMijType.HT015HypotrustWoonBewustVasteRente;
                case CodeLeningMijType.HT007ZekerWetenHypotheek:
                    return CodeRenteMijType.HT007SPStandaardProduct;
                case CodeLeningMijType.HT015HypotrustSpaarOKHypotheek:
                    return CodeRenteMijType.HT010HypotrustSpaarOKVasteRente;
                case CodeLeningMijType.HT014HypotrustTrend:
                    return CodeRenteMijType.HT009HypotrustTrend;
                case CodeLeningMijType.HT010ZekerWetenStartershypotheekProfijt:
                    return CodeRenteMijType.HT016HypotrustProfijtVasteRente;
                case CodeLeningMijType.HT013HypotrustBudget:
                    return CodeRenteMijType.HT006BPBudgetproduct;
                case CodeLeningMijType.HT009ZekerWetenStartershypotheek:
                    return CodeRenteMijType.HT007SPStandaardProduct;
                case CodeLeningMijType.HT012ZekerWetenBudgetStartershypotheek:
                    return CodeRenteMijType.HT007SPStandaardProduct;
                case CodeLeningMijType.HT019HypotrustELANHypotheek:
                    return CodeRenteMijType.HT018HypotrustELANVasteRente;
                case CodeLeningMijType.HT022HypotrustELANPlusHypotheek:
                    return CodeRenteMijType.HT023HypotrustELANPlusVasteRente;
                case CodeLeningMijType.HT008ZekerWetenHypotheekProfijt:
                    return CodeRenteMijType.HT016HypotrustProfijtVasteRente;
                case CodeLeningMijType.HT011ZekerWetenBudgethypotheek:
                    return CodeRenteMijType.HT007SPStandaardProduct;
                default:
                    return CodeRenteMijType.HT007SPStandaardProduct;
            }
        }
    };
}

#endif
