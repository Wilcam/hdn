using Finix;
using HDN.ABN;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if ABN

namespace HDN.Factories
{
    public class AbnFactory : BaseFactory
    {

        public AbnFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.AAABNAMROBank;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.AAABNAMROBankNV;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.AA003ABNAMROAflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (IsRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.AA016ABNAMRORestschuldhypotheekannuitair;
                    if (leningMij == CodeLeningMijType.AA001ABNAMROWoninghypotheek)
                        return CodeDeelMijType.AA024DuurzaamWonenAnnu�teitenHypotheek;
                    else
                        return CodeDeelMijType.AA001ABNAMROAnnu�teitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (IsRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.AA017ABNAMRORestschuldhypotheeklineair;
                    if (leningMij == CodeLeningMijType.AA001ABNAMROWoninghypotheek)
                        return CodeDeelMijType.AA025DuurzaamWonenLineaireHypotheek;
                    else
                        return CodeDeelMijType.AA005ABNAMROLineairehypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.AA009ABNAMROBankSpaarhypotheek;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.AA011ABNAMROSpaarhypotheek;
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeHypotheekOptiesMijType? GetHypotheekOpties()
        {
            List<int> theList = SpecialUtils.GetHypotheekOpties(_hypBer.ProductKenmerken);
            if (theList.Exists(x => x == (int)HypotheekoptiesFinix.Opties.pkABNAMROBetaalrek))
                return CodeHypotheekOptiesMijType.AA001Huisbankierkorting;
            return null;
        }

    };
}

#endif