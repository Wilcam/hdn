﻿using Finix;
using HDN.OBVION;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if OBVION

namespace HDN.Factories
{
    public class ObvionFactory : BaseFactory
    {

        public ObvionFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.RBRabobank;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.RBRabohypotheekbankNV;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {

            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.AB001ABPAflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (!isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.OV002ObvionAnnuïteitenhypotheek;
                    else
                        return CodeDeelMijType.OV014ObvionRestschuldAnnuïtair;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (!isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.OV015ObvionRestschuldLineair;
                    else
                        return CodeDeelMijType.AB008ABPRestschuldLineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.OV002Variabelerente;
            else
                return CodeRenteMijType.OV002Variabelerente;
        }
    }
}

#endif