using Finix;
using HDN.IQWOON;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if IQWOON

namespace HDN.Factories
{
    public class IqwoonFactory : BaseFactory
    {

        public IqwoonFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.IQIQWOON;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.IQIQWOON;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.IQ001IQWOONAflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.IQ002IQWOONAnnu�teit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.IQ003IQWOONLineair;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.IQ004IQWOONLeven;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            if (ff == RenteAfspraakType.Item03continuvariabel)
                return CodeRenteMijType.IQ002IQWOONVariabeleRente;
            else
                return CodeRenteMijType.IQ001IQWOONVasteRente;
        }

    };
}

#endif