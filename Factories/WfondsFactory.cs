﻿using Finix;
using HDN.WFONDS;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if WFONDS

namespace HDN.Factories
{
    public class WoonfondsFactory : BaseFactory
    {

        public WoonfondsFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.WFWoonfondsHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.WFWoonfondsHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.WF001Aflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.WF016RestschuldAnnuïtair;
                    return CodeDeelMijType.WF002Annuiteitenhypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.WF017RestschuldLineair;
                    return CodeDeelMijType.WF006Lineairehypotheek;
                case SoortHypotheekEnum.Krediethypotheek:
                    return CodeDeelMijType.WF031KredietHypotheek;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.WF008Spaarhypotheek;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.WF005Levenhypotheek;
                case SoortHypotheekEnum.Effectenhypotheek:
                    return CodeDeelMijType.WF005Levenhypotheek;
                case SoortHypotheekEnum.Hybridehypotheek:
                    return CodeDeelMijType.WF005Levenhypotheek;
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                    return CodeDeelMijType.WF005Levenhypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.WF015Bankspaarhypotheek;
                case SoortHypotheekEnum.BEWhypotheek:
                    return CodeDeelMijType.WF012Beleggingshypotheek;
            default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;
            }
        }

        protected override CodeRenteMijType GetCodeRenteMijType(CodeLeningMijType? leningMij, LeningdeelDataT? leningdeelFinix, int leningdeelNr, DataBooleanFieldT? identiek)
        {
            RenteAfspraakType ff = SpecialUtils.GetRenteAfspraak(_finixDossier.ExtraHDNInfo.LeningInfo[leningdeelNr].Soort.Value);
            
            if (_hypBer.ArrangementId == "WFWV")
            {
                if (ff == RenteAfspraakType.Item03continuvariabel)
                    return CodeRenteMijType.WF007WoningverhuurVariabeleRente;
                else
                    return CodeRenteMijType.WF006WoningverhuurVasteRente;
            }
                else if (_hypBer.ArrangementId == "WFWG")
            {
                if (ff == RenteAfspraakType.Item03continuvariabel)
                    return CodeRenteMijType.WF005WoongenotVariabeleRente;
                else
                    return CodeRenteMijType.WF004WoongenotVasteRente;
            }
            return CodeRenteMijType.WF009KrediethypotheekMaandVariabeleRente;

        }
    }
}

#endif