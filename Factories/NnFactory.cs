﻿using Finix;
using HDN.NN;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if NN

namespace HDN.Factories
{
    public class NnFactory : BaseFactory
    {

        public NnFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.NNNationaleNederlandenHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.NNNationaleNederlandenHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.NN006Aflossingsvrijehypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.NN007Annuïtairehypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.NN008Lineairehypotheek;
                case SoortHypotheekEnum.Krediethypotheek:
                    break;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.NN002Spaarhypotheek;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.NN004Levenhypotheek;
                case SoortHypotheekEnum.Effectenhypotheek:
                    return CodeDeelMijType.NN004Levenhypotheek;
                case SoortHypotheekEnum.Hybridehypotheek:
                    return CodeDeelMijType.NN004Levenhypotheek;
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                    return CodeDeelMijType.NN004Levenhypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.NN012BankSpaarPlushypotheek;
                case SoortHypotheekEnum.BEWhypotheek:
                    break;
                case SoortHypotheekEnum.Ongedefinieerd:
                    break;
                default:
                    break;
            }

            return CodeDeelMijType.NN006Aflossingsvrijehypotheek;
        }

      

       
    };
}

#endif