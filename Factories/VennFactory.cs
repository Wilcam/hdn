﻿using Finix;
using HDN.VENN;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if VENN

namespace HDN.Factories
{
    public class VennFactory : BaseFactory
    {

        public VennFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.VEVennHypotheken;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.VEVennHypotheken;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            switch (leningdeelFinix.Soort)
            {
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.VE003VennAflossingsvrijeHypotheek;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    return CodeDeelMijType.VE001VennAnnuïteitenHypotheek;
                case SoortHypotheekEnum.Lineairehypotheek:
                    return CodeDeelMijType.VE002VennLineaireHypotheek;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                case SoortHypotheekEnum.Spaarhypotheek:
                case SoortHypotheekEnum.Krediethypotheek:
                case SoortHypotheekEnum.Levenhypotheek:
                case SoortHypotheekEnum.Effectenhypotheek:
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
            }
        }

       
    };
}

#endif
