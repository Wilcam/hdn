using Finix;
using HDN.ABNGRP;
using HDNRESULT;
using System;
using System.Collections.Generic;
using System.Text;

#if ABNGRP

namespace HDN.Factories
{
    public class AbnGroepFactory : BaseFactory
    {

        public AbnGroepFactory(string finixDossier, bool isContent = true) : base(finixDossier, isContent)
        {
        }

        protected override OntvangerCodeType GetOntvangerCode()
        {
            return OntvangerCodeType.AGABNAMROHypothekenGroep;
        }

        protected override MaatschappijType GetFinancier()
        {
            return MaatschappijType.FHFlorius;
        }

        protected override CodeDeelMijType GetCodeDeelMijType(LeningdeelDataT leningdeelFinix, CodeLeningMijType? leningMij)
        {
            
            switch (leningdeelFinix.Soort)
            {
               
                case SoortHypotheekEnum.Aflossingsvrijehypotheek:
                    return CodeDeelMijType.FH004Aflossingsvrij;
                case SoortHypotheekEnum.Annuiteitenhypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.FH012FloriusRestschuldhypotheek;
                    return CodeDeelMijType.FH003Annuiteit;
                case SoortHypotheekEnum.Lineairehypotheek:
                    if (isRestschuldLening(leningdeelFinix))
                        return CodeDeelMijType.FH013FloriusRestschuldhypotheeklineair;
                    return CodeDeelMijType.FH011Lineair;
                case SoortHypotheekEnum.Bankspaarhypotheek:
                    return CodeDeelMijType.FH010Bankspaar;
                case SoortHypotheekEnum.Spaarhypotheek:
                    return CodeDeelMijType.FH005Spaarverzekering;
                case SoortHypotheekEnum.Krediethypotheek:
                    return CodeDeelMijType.FH008Krediet;
                case SoortHypotheekEnum.Levenhypotheek:
                    return CodeDeelMijType.FH002Leven;
                case SoortHypotheekEnum.Effectenhypotheek:                 
                case SoortHypotheekEnum.Hybridehypotheek:
                case SoortHypotheekEnum.UnitLinkedhypotheek:
                case SoortHypotheekEnum.BEWhypotheek:
                case SoortHypotheekEnum.Ongedefinieerd:
                default:
                    GlobalOutputInfo.buildErrors.Add("GetCodeDeelMijType: ongeldige parameter, default waarde gebruikt");
                    return (CodeDeelMijType)0;                 
            }
        }

        protected override CodeHypotheekOptiesMijType? GetHypotheekOpties()
        {            
            return null;
        }

    };
}

#endif